/*
*********************************************************************************************************
*                                                uC/OS-III
*                                          The Real-Time Kernel
*                             Atmel Xmega128A1 Application Configuration File
*
*                                 (c) Copyright 2008; Micrium; Weston, FL
*                                           All Rights Reserved
*
* Filename      : app_cfg.c
* Version       : V1.00
* Programmer(s) : FK
*                 FT
*********************************************************************************************************
*/

#ifndef _APP_CFG_H_
#define _APP_CFG_H_

#if HW_VERSION=='A'
	#define APP_CFG_N_TASKS 7
#elif HW_VERSION=='B'
	#define APP_CFG_N_TASKS 8
#endif

/*
*********************************************************************************************************
*                                             TASK PRIORITIES
*********************************************************************************************************
*/

#define  APP_TASK_START_PRIO                2
#define  APP_TASK_TEST_PRIO                 3
#define  XBEE_RX_PACKET_HANDLER_PRIO        4
#define  XBEE_TX_PACKET_HANDLER_PRIO        5
#define  USARTE1RXTASK_PRIO                 6
#define  IRTASK_PRIO						7
#define  CHARGETASK_PRIO					8
#define  DISPLAY_PRIO						OS_CFG_PRIO_MAX-4

/*
*********************************************************************************************************
*                                               STACK SIZES
*********************************************************************************************************
*/

#define  APP_TASK_START_STK_SIZE            150
#define  APP_TASK_TEST_STK_SIZE             150
#define  XBEE_RX_PACKET_HANDLER_STK_SIZE    300
#define  XBEE_TX_PACKET_HANDLER_STK_SIZE    150
#define  USARTE1RXTASK_STK_SIZE             150
#define  DISPLAY_STK_SIZE					200
#define  IRTASK_STK_SIZE					150
#define  CHARGETASK_STK_SIZE				300

/*
*********************************************************************************************************
*                                            TASK STACK SIZES LIMIT
*********************************************************************************************************
*/

#define  APP_TASK_START_STK_SIZE_PCT_FULL				100u
#define  APP_TASK_START_STK_SIZE_LIMIT					(APP_TASK_START_STK_SIZE * (100u - APP_TASK_START_STK_SIZE_PCT_FULL)) / 100u

#define  APP_TASK_TEST_STK_SIZE_PCT_FULL				100u
#define  APP_TASK_TEST_STK_SIZE_LIMIT					(APP_TASK_TEST_STK_SIZE * (100u - APP_TASK_TEST_STK_SIZE_PCT_FULL)) / 100u


#define  XBEE_RX_PACKET_HANDLER_STK_SIZE_PCT_FULL       100u
#define  XBEE_RX_PACKET_HANDLER_STK_SIZE_LIMIT          (XBEE_RX_PACKET_HANDLER_STK_SIZE * (100u - XBEE_RX_PACKET_HANDLER_STK_SIZE_PCT_FULL)) / 100u

#define  XBEE_TX_PACKET_HANDLER_STK_SIZE_PCT_FULL       100u
#define  XBEE_TX_PACKET_HANDLER_STK_SIZE_LIMIT          (XBEE_TX_PACKET_HANDLER_STK_SIZE * (100u - XBEE_TX_PACKET_HANDLER_STK_SIZE_PCT_FULL)) / 100u

#define  USARTE1RXTASK_STK_SIZE_PCT_FULL				100u
#define  USARTE1RXTASK_STK_SIZE_LIMIT					(USARTE1RXTASK_STK_SIZE * (100u - USARTE1RXTASK_STK_SIZE_PCT_FULL)) / 100u

#define  DISPLAY_STK_SIZE_PCT_FULL						100u
#define  DISPLAY_STK_SIZE_LIMIT							(DISPLAY_STK_SIZE * (100u - DISPLAY_STK_SIZE_PCT_FULL)) / 100u

#define  IRTASK_STK_SIZE_PCT_FULL						100u
#define  IRTASK_STK_SIZE_LIMIT							(IRTASK_STK_SIZE * (100u - IRTASK_STK_SIZE_PCT_FULL)) / 100u

#define  CHARGETASK_STK_SIZE_PCT_FULL					100u
#define  CHARGETASK_STK_SIZE_LIMIT						(CHARGETASK_STK_SIZE * (100u - CHARGETASK_STK_SIZE_PCT_FULL)) / 100u

#endif