/*

tlc5947.c

Copyright 2010 Matthew T. Pandina. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY MATTHEW T. PANDINA "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL MATTHEW T. PANDINA OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*

Note: This library has been modified for use with the TLC5947 by Nick D'Ademo.

*/

#include "tlc5947.h"

#if (3 * 24 * TLC5947_N > 255)
    #define channel3_t uint16_t
#else
    #define channel3_t uint8_t
#endif

uint8_t gsData[gsDataSize];

void TLC5947_SetGS(channel_t channel, uint16_t value)
{
    channel = numChannels - 1 - channel;
    channel3_t i = (channel3_t)channel * 3 / 2;
    switch (channel % 2)
    {
        case 0:
            gsData[i] = (value >> 4);
            i++;
            gsData[i] = (gsData[i] & 0x0F) | (uint8_t)(value << 4);
            break;
        default:
            gsData[i] = (gsData[i] & 0xF0) | (value >> 8);
            i++;
            gsData[i] = (uint8_t)value;
            break;
    }
}

void TLC5947_SetAllGS(uint16_t value, uint16_t mask)
{
    uint8_t tmp1 = (value >> 4);
    uint8_t tmp2 = (value << 4);
    
    gsData_t i=0;
    uint8_t shift=0;

    // Each loop iteration writes 24-bits (2 channels)
    // Total number of loop iterations = 24 (2x24=48 channels)
    // Note: The lower indexes of gsData contain the higher channel numbers.
    // In other words, Channel 48 is shifted into the cascaded TLC5947s first.
    do
    {
        if((i==9)||(i==18)||(i==27)||(i==36)||(i==45)||(i==54)||(i==63))
            shift++;

        // Bits: 11 10 09 08 07 06 05 04  
        if((0x01<<shift)&mask)
            gsData[i] = tmp1;
        i++;

        // Bits: 03 02 01 00 11 10 09 08
        if((0x01<<shift)&mask)
            gsData[i] = tmp2;
                
        if((i==4)||(i==13)||(i==22)||(i==31)||(i==40)||(i==49)||(i==58)||(i==67))
            shift++;
            
        if((0x01<<shift)&mask)
            gsData[i] |= (tmp1>>4);
        i++;

        // Bits: 07 06 05 04 03 02 01 00
        if((0x01<<shift)&mask)
            gsData[i] = (uint8_t)value;
        i++;
    } while (i < gsDataSize);
}
