/*

tlc5947.h

Copyright 2010 Matthew T. Pandina. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY MATTHEW T. PANDINA "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL MATTHEW T. PANDINA OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

/*

Note: This library has been modified for use with the TLC5947 by Nick D'Ademo.

*/

#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>

// Number of cascaded TLC5947 chips
#define TLC5947_N 2

// XLAT: PC4
#define SET_RGB_XLAT_LOW {PORTC.OUTCLR=0x10;}
#define SET_RGB_XLAT_HIGH {PORTC.OUTSET=0x10;}
// BLANK: PH3
#define SET_RGB_BLANK_LOW {PORTH.OUTCLR=0x08;}
#define SET_RGB_BLANK_HIGH {PORTH.OUTSET=0x08;}


#if (36 * TLC5947_N > 255)
    #define gsData_t uint16_t
#else
    #define gsData_t uint8_t
#endif

#if (24 * TLC5947_N > 255)
    #define channel_t uint16_t
#else
    #define channel_t uint8_t
#endif

#define gsDataSize ((gsData_t)36 * TLC5947_N)
#define numChannels ((channel_t)24 * TLC5947_N)

extern uint8_t gsData[gsDataSize];

void TLC5947_SetGS(channel_t channel, uint16_t value);
void TLC5947_SetAllGS(uint16_t value, uint16_t mask);
