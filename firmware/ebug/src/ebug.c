/*
*********************************************************************************************************
*                       eBug - An Open Robotics Platform for Teaching and Research
*********************************************************************************************************
*
* Project Version:      7th April 2013
* uC/OS-III Version:    3.03.00
*
* This project has been tested with the following:
*
* Target Board(s):  eBug [HW_VERSION defined in Atmel Studio 6 Project Properties]
* Programmer:       Atmel AVRISP mkII (via PDI)
* Debugger:         Atmel JTAGICE mkII (via PDI)
* IDE:              Atmel Studio 6 (Version 6.1.2440 - beta)
* Compiler:         See "AtmelStudio6.txt"
* Assembler:        See "AtmelStudio6.txt"
*
* Author(s):        Nick D'Ademo [nickdademo(at)gmail.com]
*
* Portions of code contained in this project were produced by the CodeWizardAVR V2.05.6 Standard
* Automatic Program Generator
* � Copyright 1998-2012 Pavel Haiduc, HP InfoTech s.r.l.
* http://www.hpinfotech.com
*
*********************************************************************************************************
*           Please send all comments, suggestions, bug reports to: nickdademo(at)gmail.com
*********************************************************************************************************
*/

#include <includes.h>

/*
*********************************************************************************************************
*                                               PROGRAM MEMORY
*********************************************************************************************************
*/

// Constant (static) variables are placed in the Program Memory (FLASH) to conserve SRAM usage.

// RESET SOURCE
const char resetPowerOn[] PROGMEM   = "Power ON Reset";
const char resetExternal[] PROGMEM  = "External Reset";
const char resetBO[] PROGMEM        = "Brown-Out Reset";
const char resetWD[] PROGMEM        = "Watchdog Reset";
const char resetPDI[] PROGMEM       = "PDI Reset";
const char resetSW[] PROGMEM        = "Software Reset";
const char resetSrc[] PROGMEM       = "RESET SOURCE:";
const char* const ResetSourceStringsPtrs[] PROGMEM = {resetPowerOn,resetExternal,resetBO,resetWD,resetPDI,resetSW,resetSrc};
// RESET COUNTDOWN
const char resetCD1[] PROGMEM = "  DEVICE WILL   ";
const char resetCD2[] PROGMEM = "  RESET IN 2s  ";
const char resetCD3[] PROGMEM = "  RESET IN 1s  ";
const char* const ResetCountdownStringsPtrs[] PROGMEM = {resetCD1,resetCD2,resetCD3};
// STARTUP
const char operatingModeStr[] PROGMEM = "Operating Mode:";
const char runTests[] PROGMEM = "Run tests?";
const char* const StartupStringsPtrs[] PROGMEM = {operatingModeStr,runTests};
// CHARGING
const char pwrSrcDCPSU[] PROGMEM                = "PWRSRC=DC PSU";
const char chargingPCHG[] PROGMEM               = "ChgStat=PCHG    ";
const char chargingMCHG[] PROGMEM               = "ChgStat=MCHG    ";
const char chargingLTCHG[] PROGMEM              = "ChgStat=LTCHG   ";
const char chargingST1CHG[] PROGMEM             = "ChgStat=ST1CHG  ";
const char chargingST2CHG[] PROGMEM             = "ChgStat=ST2CHG  ";
const char chargingHTCHG[] PROGMEM              = "ChgStat=HTCHG   ";
const char chargingCB[] PROGMEM                 = "ChgStat=CB      ";
const char chargeComplete[] PROGMEM             = "Charge Complete.";
const char errNoCharger[] PROGMEM               = "ERR: No Charger ";
const char removeCharger[] PROGMEM              = " Remove Charger ";
const char storageCharge[] PROGMEM              = " Storage Charge ";
const char* const ChargingStringsPtrs[] PROGMEM = {pwrSrcDCPSU,chargeComplete,errNoCharger,removeCharger,chargingPCHG,chargingMCHG,chargingLTCHG,chargingST1CHG,chargingST2CHG,chargingHTCHG,chargingCB,storageCharge};
// LIPO
const char pwrSrcLiPo[] PROGMEM = "PWRSRC=LiPo";
const char errNoGG[] PROGMEM    = "ERR:No Gas Gauge";
const char errPF[] PROGMEM      = "ERR: GG PFailure";
const char clearPF[] PROGMEM    = "ReleaseToClearPF";
const char* const LiPoStringsPtrs[] PROGMEM = {pwrSrcLiPo,errNoGG,errPF,clearPF};
// XBEE
const char modemStatus1[] PROGMEM       = "XBEE:HWARE RESET";
const char modemStatus2[] PROGMEM       = "XBEE:WD TIMR RST";
const char modemStatus3[] PROGMEM       = "XBEE:JOINED NETW";
const char modemStatus4[] PROGMEM       = "XBEE:DISASSOC   ";
const char modemStatus5[] PROGMEM       = "XBEE:COORD STRTD";
const char modemStatus6[] PROGMEM       = "XBEE:NW KEY UPDT";
const char modemStatus7[] PROGMEM       = "XBEE:V EXCEEDED ";
const char modemStatus8[] PROGMEM       = "XBEE:MDM CFG CHG";
const char modemStatus9[] PROGMEM       = "XBEE:STACK ERROR";
const char XBunhandledPacket[] PROGMEM  = "XBEE:UnhandldPkt";
const char XBchecksumFail[] PROGMEM     = "XBEE:Chksum Fail";
const char* const XBeeStringsPtrs[] PROGMEM = {modemStatus1,modemStatus2,modemStatus3,modemStatus4,modemStatus5,modemStatus6,modemStatus7,modemStatus8,modemStatus9,XBunhandledPacket,XBchecksumFail};
// API
const char APIchecksumFail[] PROGMEM        = "API: Chksum Fail";
const char APIunhandledCommand[] PROGMEM    = "API:UnhandledCmd";
const char APIStepperMotor0[] PROGMEM       = "API: 0x01->0x00 ";
const char APIStepperMotor1[] PROGMEM       = "API: 0x01->0x01 ";
const char APIStepperMotor2[] PROGMEM       = "API: 0x01->0x02 ";
const char APIStepperMotor3[] PROGMEM       = "API: 0x01->0x03 ";
const char APIStepperMotor4[] PROGMEM       = "API: 0x01->0x04 ";
const char APIStepperMotor5[] PROGMEM       = "API: 0x01->0x05 ";
const char APIRGB0[] PROGMEM                = "API: 0x02->0x00 ";
const char APIRGB1[] PROGMEM                = "API: 0x02->0x01 ";
const char APIRGB2[] PROGMEM                = "API: 0x02->0x02 ";
const char APIRGB3[] PROGMEM                = "API: 0x02->0x03 ";
const char APIRGB4[] PROGMEM                = "API: 0x02->0x04 ";
const char APIRGB5[] PROGMEM                = "API: 0x02->0x05 ";
const char APIRGB6[] PROGMEM                = "API: 0x02->0x06 ";
const char APIRGB7[] PROGMEM                = "API: 0x02->0x07 ";
const char APIRGB8[] PROGMEM                = "API: 0x02->0x08 ";
const char APIRGB9[] PROGMEM                = "API: 0x02->0x09 ";
const char APIRGB10[] PROGMEM               = "API: 0x02->0x0A ";
const char APIRGB11[] PROGMEM               = "API: 0x02->0x0B ";
const char APITemperatureSensor0[] PROGMEM  = "API: 0x03->0x00 ";
const char APISystem0[] PROGMEM             = "API: 0x00->0x00 ";
const char* const APIStringsPtrs[] PROGMEM = {APIchecksumFail,APIunhandledCommand,
                                        APIStepperMotor0,APIStepperMotor1,APIStepperMotor2,APIStepperMotor3,APIStepperMotor4,APIStepperMotor5,
                                        APIRGB0,APIRGB1,APIRGB2,APIRGB3,APIRGB4,APIRGB5,APIRGB6,APIRGB7,APIRGB8,APIRGB9,APIRGB10,APIRGB11,
                                        APITemperatureSensor0,
                                        APISystem0};
// MCU
const char mcuUnsupported[] PROGMEM     = "uC NOT SUPPORTED";
const char mcuATXMEGA128A1[] PROGMEM    = "uC=ATXMEGA128A1 ";
const char mcuATXMEGA128A1U[] PROGMEM   = "uC=ATXMEGA128A1U";
const char* const MCUStringsPtrs[] PROGMEM = {mcuUnsupported,mcuATXMEGA128A1,mcuATXMEGA128A1U};
// TEST MODE
const char test1[] PROGMEM              = "[01/08]: RGB    ";
const char test2[] PROGMEM              = "[02/08]: Temp   ";
const char test3[] PROGMEM              = "[03/08]: Light L";
const char test4[] PROGMEM              = "[04/08]: Light M";
const char test5[] PROGMEM              = "[05/08]: Light H";
const char test6[] PROGMEM              = "[06/08]: Speaker";
const char test7[] PROGMEM              = "[07/08]: Motors ";
const char test8[] PROGMEM              = "[08/08]: XBee ND";
const char test8EndOfResults[] PROGMEM  = " End of results ";
const char demoMode[] PROGMEM           = "   DEMO MODE    ";
const char* const TestStringsPtrs[] PROGMEM = {test1,test2,test3,test4,test5,test6,test7,test8,test8EndOfResults,demoMode};

/*
*********************************************************************************************************
*                                              GLOBAL VARIABLES
*********************************************************************************************************
*/

// uC/OS-III Tasks
static OS_TCB AppTaskTCB[APP_CFG_N_TASKS]; // OS_TCB array to hold all the tasks
static CPU_STK AppTaskStart_Stk[APP_TASK_START_STK_SIZE];
static CPU_STK AppTaskTest_Stk[APP_TASK_TEST_STK_SIZE];
static CPU_STK XBeeRXPacketHandler_Stk[XBEE_RX_PACKET_HANDLER_STK_SIZE];
static CPU_STK XBeeTXPacketHandler_Stk[XBEE_TX_PACKET_HANDLER_STK_SIZE];
static CPU_STK USARTE1RXTask_Stk[USARTE1RXTASK_STK_SIZE];
static CPU_STK Display_Stk[DISPLAY_STK_SIZE];
#if HW_VERSION=='B' // RevB: Features IR functionality
    static CPU_STK IRTask_Stk[IRTASK_STK_SIZE];
#endif
static CPU_STK ChargeTask_Stk[CHARGETASK_STK_SIZE];

// TCB array index definitions
#define APP_TASK_START_TCB_ARRAY_INDEX 0
#define APP_TASK_TEST_TCB_ARRAY_INDEX 1
#define XBEE_RX_PACKET_HANDLER_TCB_ARRAY_INDEX 2
#define XBEE_TX_PACKET_HANDLER_TCB_ARRAY_INDEX 3
#define USARTE1RXTASK_TCB_ARRAY_INDEX 4
#define DISPLAY_TCB_ARRAY_INDEX 5
#define CHARGETASK_TCB_ARRAY_INDEX 6
#if HW_VERSION=='B' // RevB: Features IR functionality
    #define IRTASK_TCB_ARRAY_INDEX 7
#endif

// uC/OS-III Mutexes
OS_MUTEX XBeeTXDataProtect;
OS_MUTEX XBeeRXDataProtect;
OS_MUTEX LCDDataProtect;

// Variable to hold uC/OS-III error codes
OS_ERR err;

// uC/OS-III running time
volatile uint8_t timeSeconds;
volatile uint8_t timeMinutes;
volatile uint8_t timeHours;

// Flags
volatile OperatingState operatingState;
volatile bool testButtonState;
volatile bool testRepeat;
volatile uint8_t displayIndex;
volatile uint8_t backlightIndex;
volatile uint8_t nLCDStatItems;
volatile bool doDemo;

// Operating Mode (set via 4-bit rotary switch)
uint8_t operatingMode;

// XBee USART
uint8_t rx_buffer_XBee[RX_BUFFER_SIZE_XBEE];
volatile uint16_t rx_counter_XBee;
uint16_t rx_wr_index_XBee, rx_rd_index_XBee;

// USARTE1
uint8_t rx_buffer_usarte1[RX_BUFFER_SIZE_USARTE1];
volatile uint16_t rx_counter_usarte1;
uint16_t rx_wr_index_usarte1, rx_rd_index_usarte1;

// LCD
char LCDData[17];
char *LCDDataP;
bool isLcdP;

// ADC
unsigned char adcb_offset; // Variable used to store the ADC offset for 12-bit unsigned conversion mode

// DACB
volatile int DACdata12[100];
volatile int gWaveNumSamp;

// TWI
TWI_Master_t twiMaster;
uint8_t sendBuffer[TWI_SEND_BUFFER_SIZE];

// DMA
volatile unsigned long int dmaCH1TransactionCountLimit;

// STEPPER MOTORS
volatile unsigned long int stepperMotor1NSteps;
volatile unsigned long int stepperMotor2NSteps;
volatile unsigned long int stepperMotor1StepCount;
volatile unsigned long int stepperMotor2StepCount;
volatile bool stepper1rgbOn;
volatile bool stepper2rgbOn;

// XBEE
uint8_t XBeeNodeTableIndex=0;
struct XBeeNodeTable{
    uint8_t commandStatus;
    uint16_t MY;
    uint16_t SH_high;
    uint16_t SH_low;
    uint16_t SL_high;
    uint16_t SL_low;
    uint8_t NI[20];
    uint16_t parentNetworkAddress;
    uint8_t deviceType;
    uint8_t status;
    uint16_t profileID;
    uint16_t manufacturerID;
} xBeeNodeTable[MAX_NODES];
uint8_t XBeeTXData[MAX_XBEE_TXPACKET_LENGTH];

// USART RX COUNTERS
volatile unsigned long int XBeeRXCount;
volatile unsigned long int usarte1RXCount;

// IR RECEIVERS
volatile uint16_t pulseWidthIR1;
volatile uint16_t pulseWidthIR2;
volatile uint16_t pulseWidthIR3;
volatile uint16_t pulseWidthIR4;
volatile uint16_t pulseWidthIR5;
volatile uint16_t pulseWidthIR6;
volatile uint16_t pulseWidthIR7;
volatile uint16_t pulseWidthIR8;

// RESET SOURCE
uint8_t resetSource;

// EEPROM global for RAND seed
uint16_t EEMEM randInit; 

/*
*********************************************************************************************************
*                                                   MAIN
*********************************************************************************************************
*/
int main(void)
{
    // Local variables
    char lcdString1[17];
    char lcdString2[17];
    char deviceIDString[7];
    double powerSourceVoltage=0;
    uint16_t lipoPackVoltage=0;
    uint16_t cell1Voltage=0;
    uint16_t cell2Voltage=0;
    uint16_t cell3Voltage=0;
    uint16_t GGdeviceType=0;
    uint16_t chargerDeviceID=0;
    uint8_t rsoc=0;
    uint16_t cycleCount=0;
    uint16_t GGFWVersion=0;
    uint16_t GGHWVersion=0;
    uint8_t byte0=0;
    uint8_t byte1=0;
    CPU_SR_ALLOC();
    
    // Initialize global volatile variables
    operatingState=STARTUP;
    testButtonState=false;  // TEST button initially in toggled OFF state
    testRepeat=false;       // Test Mode runs in non-repeat mode by default
    displayIndex=0;
    backlightIndex=0;
    timeSeconds=0;
    timeMinutes=0;
    timeHours=0;
    gWaveNumSamp=50;
    stepper1rgbOn=false;
    stepper2rgbOn=false;
    XBeeRXCount=0;
    usarte1RXCount=0;
    nLCDStatItems=N_LCD_STAT_ITEMS_NORMAL;
    doDemo=false;

    /////////////////////////////
    // STARTUP DISPLAY ROUTINE //
    /////////////////////////////
    // Check the reset source
    resetSource=RST.STATUS;
    char* string = "";
    if (resetSource & RST_PORF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[0]);
    else if (resetSource & RST_EXTRF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[1]);
    else if (resetSource & RST_BORF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[2]);
    else if (resetSource & RST_WDRF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[3]);
    else if (resetSource & RST_PDIRF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[4]);
    else if (resetSource & RST_SRF_bm)
        string = (char*) pgm_read_word(&ResetSourceStringsPtrs[5]);
    // Clear the reset flag
    RST.STATUS=resetSource;

    // Initialize AVR
    AVRInit();
    // Clear STATUS LEDs
    PORTH.OUTCLR=0x12;

    // Display string on LCD
    lcd_puts_p((char*)pgm_read_word(&ResetSourceStringsPtrs[6]));
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts_p(string);
    // Display strings for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr(); 

    // Create string containing eBug hardware and firmware version
    sprintf(lcdString1,"eBug=HW:%c FW=%d", HW_VERSION, FW_VERSION);
    // Create string containing uC/OS-III version
    sprintf(lcdString2,"uC/OS-III:v%d",OS_VERSION);
    // Display string on LCD
    lcd_puts(lcdString1);
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts(lcdString2);
    // Display strings for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr(); 
    
    // Create string containing chip DEVID
    sprintf(deviceIDString,"%2x%2x%2x",MCU.DEVID0,MCU.DEVID1,MCU.DEVID2);
    if(strcmp(deviceIDString,"1e974c")==0)
    {
        // Display string on LCD
        #if HW_VERSION=='A' // RevA: Uses ATXMEGA128A1
            lcd_puts_p((char*)pgm_read_word(&MCUStringsPtrs[1]));
        #elif HW_VERSION=='B' // RevB: Uses ATXMEGA128A1U
            lcd_puts_p((char*)pgm_read_word(&MCUStringsPtrs[2]));
        #endif
    }
    // MCU not supported
    else
    {
        // Set RED STATUS LED
        PORTH.OUTCLR=0x02;
        PORTH.OUTSET=0x10;
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts_p((char*)pgm_read_word(&MCUStringsPtrs[0]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Clear LCD
        lcd_clrscr();
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Reset XMEGA
        CPU_CRITICAL_ENTER();
            CPU_CCP=CCP_IOREG_gc; 
            RST.CTRL=RST_SWRST_bm;
        CPU_CRITICAL_EXIT();
    }   
    // Create string containing chip REVID
    sprintf(lcdString2,"RevisionID=0x%x",MCU.REVID);
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts(lcdString2);
    // Display strings for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr();
    
    // Create string containing clock speed
    sprintf(lcdString1,"Clock=%ldHz",F_CPU);
    // Create string containing ticks per second
    sprintf(lcdString2,"Ticks/s=%d",OS_CFG_TICK_RATE_HZ);
    // Display string on LCD
    lcd_puts(lcdString1);
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts(lcdString2);
    // Display strings for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr();

    // POWER SOURCE=DC PSU [PC6=HIGH]
    if(PORTC.IN & (1<<6))
    {
        // Display string on LCD
        lcd_puts_p((char*)pgm_read_word(&ChargingStringsPtrs[0]));
        // Read power source voltage from ADC
        PORTQ.OUTSET=0x08;
        _delay_ms(10);
        powerSourceVoltage=16*((double)adcb_read(2,1)/4096);
        PORTQ.OUTCLR=0x08;
        // Create string containing power source voltage
        sprintf(lcdString2,"Voltage=%0.2fV",powerSourceVoltage);
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);
        
        // Attempt to get Charger Device ID [0xFF]
        sendBuffer[0]=0xFF;
        if(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        chargerDeviceID=(byte1<<8)|byte0;
        // Check if charger device is valid
        // NOTE: Add any other valid device IDs to this conditional statement
        if(chargerDeviceID==BQ24725_DEVICE_ID)
        {
            // Create string containing Charger Device ID
            sprintf(lcdString2,"Chgr Dev ID=%04x",chargerDeviceID);
            // Clear bottom line
            LCD_ClearLineAndSetXPos(1,0);
            // Display string on LCD
            lcd_puts(lcdString2);
            // Display strings for 1 second
            _delay_ms(1000);
            
            // Start ChargeTask
            operatingState=CHARGE;
            nLCDStatItems=N_LCD_STAT_ITEMS_CHARGE;
            // Disable interrupts
            CPU_IntDis();
            // Initialize "uC/OS-III, The Real-Time Kernel"
            OSInit(&err);
            // Create the start task
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[APP_TASK_START_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"App Task Start",
                    (OS_TASK_PTR )AppTaskStart,
                    (void       *)0,
                    (OS_PRIO     )APP_TASK_START_PRIO,
                    (CPU_STK    *)&AppTaskStart_Stk[0],
                    (CPU_STK_SIZE)APP_TASK_START_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)APP_TASK_START_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // Start multitasking (i.e. give control to uC/OS-III)
            OSStart(&err);     
        }
        // DEVICE NOT VALID: Reset eBug
        else
        {
            // Set RED STATUS LED
            PORTH.OUTCLR=0x02;
            PORTH.OUTSET=0x10;
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*)pgm_read_word(&ChargingStringsPtrs[2]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Clear LCD
            lcd_clrscr();
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Reset XMEGA
            CPU_CRITICAL_ENTER();
                CPU_CCP=CCP_IOREG_gc; 
                RST.CTRL=RST_SWRST_bm;
            CPU_CRITICAL_EXIT();
        }
    }
    // POWER SOURCE=LiPo [PC6=LOW]
    else
    {
        // Display string on LCD
        lcd_puts_p((char*)pgm_read_word(&LiPoStringsPtrs[0]));
        // Get device type (IC part number) via SMBus
        sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
        sendBuffer[1]=0x01; // lower byte of command
        sendBuffer[2]=0x00; // upper byte of command
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        GGdeviceType=(byte1<<8)|byte0;
        // Check if gas gauge device is valid
        // NOTE: Add any other valid device IDs to this conditional statement
        if(GGdeviceType==BQ20Z65_DEVICE_ID)
        {
            // Create string containing Device Type
            sprintf(lcdString2,"Device Type=%04x",GGdeviceType);
            // Clear bottom line
            LCD_ClearLineAndSetXPos(1,0);
            // Display string on LCD
            lcd_puts(lcdString2);
            // Display strings for 1 second
            _delay_ms(1000);
        }
        // DEVICE NOT VALID: Reset eBug
        else
        {
            // Set RED STATUS LED
            PORTH.OUTCLR=0x02;
            PORTH.OUTSET=0x10;
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*)pgm_read_word(&LiPoStringsPtrs[1]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Clear LCD
            lcd_clrscr();
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Reset XMEGA
            CPU_CRITICAL_ENTER();
                CPU_CCP=CCP_IOREG_gc; 
                RST.CTRL=RST_SWRST_bm;
            CPU_CRITICAL_EXIT();
        }

        // Check Gas Gauge PFStatus [0x53] and PFStatus2 [0x6b]
        checkForAndHandleGGPermanentFailure();
        
        // Get Firmware Version via SMBus
        sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
        sendBuffer[1]=0x02; // lower byte of command
        sendBuffer[2]=0x00; // upper byte of command
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        GGFWVersion=(byte1<<8)|byte0;
        // Create string containing Device Type
        sprintf(lcdString2,"GG FW Vers=%04x",GGFWVersion);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);
        
        // Get Hardware Version via SMBus
        sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
        sendBuffer[1]=0x03; // lower byte of command
        sendBuffer[2]=0x00; // upper byte of command
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        GGHWVersion=(byte1<<8)|byte0;
        // Create string containing Device Type
        sprintf(lcdString2,"GG HW Vers=%04x",GGHWVersion);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);
                
        // Get Pack Voltage [0x09] via SMBus
        sendBuffer[0]=0x09;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        lipoPackVoltage=(byte1<<8)|byte0;
        // Create string containing Pack Voltage
        sprintf(lcdString2,"Voltage=%dmV",lipoPackVoltage);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);

        // Get Cell 1 Voltage [0x3f] via SMBus
        sendBuffer[0]=0x3f;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        cell1Voltage=(byte1<<8)|byte0;
        // Create string containing Cell 1 Voltage
        sprintf(lcdString2,"C1Voltage=%dmV",cell1Voltage);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);

        // Get Cell 2 Voltage [0x3e] via SMBus
        sendBuffer[0]=0x3e;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        cell2Voltage=(byte1<<8)|byte0;
        // Create string containing Cell 2 Voltage
        sprintf(lcdString2,"C2Voltage=%dmV",cell2Voltage);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);

        // Get Cell 3 Voltage [0x3d] via SMBus
        sendBuffer[0]=0x3d;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0]; 
        byte1 = twiMaster.readData[1];
        cell3Voltage=(byte1<<8)|byte0;
        // Create string containing Cell 3 Voltage
        sprintf(lcdString2,"C3Voltage=%dmV",cell3Voltage);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);

        // Get RSOC [0x0d] via SMBus
        sendBuffer[0]=0x0d;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,1)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError(); 
        byte0 = twiMaster.readData[0]; 
        rsoc=byte0;
        // Create string containing RSOC
        sprintf(lcdString2,"RSOC=%d%%",rsoc);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);

        // Get RSOC [0x17] via SMBus
        sendBuffer[0]=0x17;
        if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
        byte0 = twiMaster.readData[0];
        byte1 = twiMaster.readData[1]; 
        cycleCount=(byte1<<8)|byte0;
        // Create string containing Cycle Count
        sprintf(lcdString2,"Cycle Count=%d",cycleCount);
        // Clear bottom line
        LCD_ClearLineAndSetXPos(1,0);
        // Display string on LCD
        lcd_puts(lcdString2);
        // Display strings for 1 second
        _delay_ms(1000);
    }
    // Clear LCD
    lcd_clrscr();

    /* Read rotary switch to get operating mode */
    operatingMode=getRotarySwitchState();
    // Setup Expansion Header
    setupExpansionHeader(operatingMode);
    // Display string on LCD
    lcd_puts_p((char*)pgm_read_word(&StartupStringsPtrs[0]));
    // Create string
    sprintf(lcdString2,"%d",operatingMode);
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts(lcdString2);
    // Delay for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr();

    // Enable interrupts on TEST switch
    PORTK.INT0MASK=0x80;
    // Display string on LCD
    lcd_puts_p((char*)pgm_read_word(&StartupStringsPtrs[1]));
    for(uint8_t i=0;i<STARTUP_TIMEOUT_SECS;i++)
    {
        // Create string containing time until boot
        if(i==(STARTUP_TIMEOUT_SECS-1))
            sprintf(lcdString1,"Booting in %dsec ",STARTUP_TIMEOUT_SECS-i);
        else
            sprintf(lcdString1,"Booting in %dsecs",STARTUP_TIMEOUT_SECS-i);
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts(lcdString1);
        // Delay for 1 second
        _delay_ms(1000);
    }
    // Clear LCD
    lcd_clrscr(); 
    // Disable interrupts on TEST switch
    PORTK.INT0MASK=0x00;

    /* DEBUG CODE */
    /*
    // Peripheral Clock output: PORTE Bit7 (Expansion Header Pin 14)
    PORTCFG.CLKEVOUT=(PORTCFG.CLKEVOUT & (~PORTCFG_CLKOUT_gm)) | PORTCFG_CLKOUT_PE7_gc; 
    PORTE.DIR|=0x80;
    */

    // Set state
    if(testButtonState)
        operatingState=TEST;
    else
        operatingState=NORMAL;
          
    // Disable interrupts
    CPU_IntDis();                 
    // Initialize "uC/OS-III, The Real-Time Kernel"
    OSInit(&err);
    // Create the start task
    OSTaskCreate((OS_TCB     *)&AppTaskTCB[APP_TASK_START_TCB_ARRAY_INDEX],                
                 (CPU_CHAR   *)"App Task Start",
                 (OS_TASK_PTR )AppTaskStart,
                 (void       *)0,
                 (OS_PRIO     )APP_TASK_START_PRIO,
                 (CPU_STK    *)&AppTaskStart_Stk[0],
                 (CPU_STK_SIZE)APP_TASK_START_STK_SIZE_LIMIT,
                 (CPU_STK_SIZE)APP_TASK_START_STK_SIZE,
                 (OS_MSG_QTY  )0,
                 (OS_TICK     )0,
                 (void       *)0,
                 (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                 (OS_ERR    *)&err);     
    // Start multitasking (i.e. give control to uC/OS-III)
    OSStart(&err);
    // Main never returns
    return 0;
}

/*
*********************************************************************************************************
*                                   START TASK (PRIO=APP_TASK_START_PRIO)
*********************************************************************************************************
*/
void AppTaskStart(void *p_arg)
{
    // Create mutexes
    OSMutexCreate(&XBeeRXDataProtect,"XBeeRXDataProtect",&err);
    OSMutexCreate(&XBeeTXDataProtect,"XBeeTXDataProtect",&err);
    OSMutexCreate(&LCDDataProtect,"LCDDataProtect",&err);
    // Start uC/OS-III Tick Timer
    uCTickTimerStart();
    // Determine CPU capacity
    #if (OS_CFG_STAT_TASK_EN > 0)
        OSStatTaskCPUUsageInit(&err);
    #endif

    // Set Application Hooks
    App_OS_SetAllHooks();
    // Create the application tasks
    AppTaskStart_CreateTasks(operatingState);
    
    /* Final initializations */
    // Enable interrupts on TEST switch
    PORTK.INT0MASK=0x80;
    // Real-Time Counter
    rtcxm_init();
    // Globally enable interrupts
    asm("sei");
                                             
    // Delete task
    OSTaskDel(&AppTaskTCB[APP_TASK_START_TCB_ARRAY_INDEX],&err);
} // AppTaskStart()

/*
*********************************************************************************************************
*                                                   TASKS
*********************************************************************************************************
*/

// TestTask
static void TestTask(void *p_arg)
{
    // Local variables
    uint8_t testIndex=0;
    char lcdString[17];
    uint8_t nXBeeNodes=0;
    uint16_t random_R=0;
    uint16_t random_G=0;
    uint16_t random_B=0;
    CPU_SR_ALLOC();
    
    // Disable interrupts on TEST switch (prevents LCD display item from changing on button release)
    PORTK.INT0MASK=0x00;
    // Check if TEST button is held down (enter Demo mode if so)
    if(!(PORTK.IN & 0x80))
    {
        testIndex=N_TEST_STEPS;
        doDemo=true;
        // Show string on LCD
        lcd_puts_ucos((char*)pgm_read_word(&TestStringsPtrs[9]), 0, 1, false, true);
        // Sleep for 2 ticks (so string is displayed)
        OSTimeDly(2,OS_OPT_TIME_PERIODIC,&err);
        // Display until button is released
        while(!(PORTK.IN & 0x80));
    }
    // Enable interrupts on TEST switch
    PORTK.INT0MASK=0x80;
    
    // Task body (always written as an infinite loop) 
    while(1)
    {
        switch(testIndex)
        {
            /* RGB LEDs (2+1 seconds) */
            case 0:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[0]),0,1,false,true);
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // All RED
                TLC5947_SetAllRed(4095);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // All GREEN
                TLC5947_SetAllGreen(4095);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // All BLUE
                TLC5947_SetAllBlue(4095);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // All ON (WHITE)
                TLC5947_SetAll(4095);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // RED CHASER
                for(uint8_t i=1;i<17;i++)
                {
                   TLC5947_SetRed(i,4095);
                   OSTimeDlyHMSM(0,0,0,75,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // GREEN CHASER
                for(uint8_t i=1;i<17;i++)
                {
                   TLC5947_SetGreen(i,4095);
                   OSTimeDlyHMSM(0,0,0,75,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // BLUE CHASER
                for(uint8_t i=1;i<17;i++)
                {
                   TLC5947_SetBlue(i,4095);
                   OSTimeDlyHMSM(0,0,0,75,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // All OFF
                TLC5947_SetAllOff();
                break;
            }
            /* TEMPERATURE SENSOR (20 readings, 5+1 seconds) */
            case 1:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[1]),0,1,false,true);
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                for(uint8_t i=0;i<20;i++)
                {
                    // Create string containing temperature
                    sprintf(lcdString,"Temp=%0.2f degC",getTemperature());
                    // Display string on LCD
                    lcd_puts_ucos(lcdString,0,1,true,false);
                    OSTimeDlyHMSM(0,0,0,250,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                break;
            }
            /* LIGHT SENSOR (LOW GAIN) (20 readings, 5+1 seconds) */
            case 2:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[2]),0,1,false,true);
                // Set gain
                setLightSensorLowGain();
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                for(uint8_t i=0;i<20;i++)
                {
                    // Create string containing reading
                    sprintf(lcdString,"%4u",getLightSensorReading());
                    // Display string on LCD
                    lcd_puts_ucos(lcdString,9,1,true,false);
                    OSTimeDlyHMSM(0,0,0,250,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                break;
            }
            /* LIGHT SENSOR (MEDIUM GAIN) (20 readings, 5+1 seconds) */
            case 3:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[3]),0,1,false,true);
                // Set gain
                setLightSensorMediumGain();
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                for(uint8_t i=0;i<20;i++)
                {
                    // Create string containing reading
                    sprintf(lcdString,"%4u",getLightSensorReading());
                    // Display string on LCD
                    lcd_puts_ucos(lcdString,9,1,true,false);
                    OSTimeDlyHMSM(0,0,0,250,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                break;
            }
            /* LIGHT SENSOR (HIGH GAIN) (20 readings, 5+1 seconds) */
            case 4:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[4]),0,1,false,true);
                // Set gain
                setLightSensorHighGain();
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                for(uint8_t i=0;i<20;i++)
                {
                    // Create string containing reading
                    sprintf(lcdString,"%4u",getLightSensorReading());
                    // Display string on LCD
                    lcd_puts_ucos(lcdString,9,1,true,false);
                    OSTimeDlyHMSM(0,0,0,250,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                break;
            }
            /* SPEAKER (DAC) (10+1 seconds) */
            case 5:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[5]),0,1,false,true);
                // Turn ON amplifier
                shutdownAudioAmplifier(false);
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn ON speaker
                GenerateArbWave12_Speaker(DACdata12,gWaveNumSamp*2,1000,1000);
                OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn ON speaker
                GenerateArbWave12_Speaker(DACdata12,gWaveNumSamp*2,2000,1000);
                OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn ON speaker
                GenerateArbWave12_Speaker(DACdata12,gWaveNumSamp*2,3000,1000);
                OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn ON speaker
                GenerateArbWave12_Speaker(DACdata12,gWaveNumSamp*2,4000,1000);
                OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn ON speaker
                GenerateArbWave12_Speaker(DACdata12,gWaveNumSamp*2,5000,1000);
                OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Turn OFF amplifier
                shutdownAudioAmplifier(true);
                break;
            }
            /* STEPPER MOTORS (10+1 seconds) */
            case 6:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[6]),0,1,false,true);
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                /* FORWARDS */
                // Continuous FULL stepping @ 250 Hz
                lcd_puts_ucos("Full",9,1,true,false);
                stepperMotor1_step(250,0,true,0x00,false,false);
                stepperMotor2_step(250,0,true,0x00,false,false);
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous HALF stepping @ 500 Hz
                lcd_puts_ucos("Half",9,1,true,false);
                stepperMotor1_step(500,0,true,0x01,false,false);
                stepperMotor2_step(500,0,true,0x01,false,false);  
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous QUARTER stepping @ 1000 Hz
                lcd_puts_ucos("Quart",9,1,true,false);
                stepperMotor1_step(1000,0,true,0x02,false,false);
                stepperMotor2_step(1000,0,true,0x02,false,false);   
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous 8th stepping @ 2000 Hz
                lcd_puts_ucos("8th",9,1,true,false);
                stepperMotor1_step(2000,0,true,0x03,false,false);
                stepperMotor2_step(2000,0,true,0x03,false,false); 
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous 16th stepping @ 4000 Hz
                lcd_puts_ucos("16th",9,1,true,false);
                stepperMotor1_step(4000,0,true,0x08,false,false);
                stepperMotor2_step(4000,0,true,0x08,false,false);  
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                /* BACKWARDS */
                // Continuous FULL stepping @ 250 Hz
                lcd_puts_ucos("Full",9,1,true,false);
                stepperMotor1_step(250,0,false,0x00,false,false);
                stepperMotor2_step(250,0,false,0x00,false,false);  
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous HALF stepping @ 500 Hz
                lcd_puts_ucos("Half",9,1,true,false);
                stepperMotor1_step(500,0,false,0x01,false,false);
                stepperMotor2_step(500,0,false,0x01,false,false);  
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous QUARTER stepping @ 1000 Hz
                lcd_puts_ucos("Quart",9,1,true,false);
                stepperMotor1_step(1000,0,false,0x02,false,false);
                stepperMotor2_step(1000,0,false,0x02,false,false);   
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous 8th stepping @ 2000 Hz
                lcd_puts_ucos("8th",9,1,true,false);
                stepperMotor1_step(2000,0,false,0x03,false,false);
                stepperMotor2_step(2000,0,false,0x03,false,false); 
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Continuous 16th stepping @ 4000 Hz
                lcd_puts_ucos("16th",9,1,true,false);
                stepperMotor1_step(4000,0,false,0x08,false,false);
                stepperMotor2_step(4000,0,false,0x08,false,false); 
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0); 
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Stop motors
                stepperMotor1_stopDisable(true);
                stepperMotor2_stopDisable(true);
                break;
            }
            /* XBEE (10+1+1 + x seconds) */
            case 7:
            {
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[7]),0,1,false,true);
                // Send Node Discovery AT Command
                SendXBeeATCommand_ND();
                // Allow 10 seconds to discover all nodes
                OSTimeDlyHMSM(0,0,10,0,OS_OPT_TIME_HMSM_STRICT,&err);
                /////////////////////
                // Display results //
                /////////////////////
                // Get mutex
                OSMutexPend(&XBeeRXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
                // Save number of nodes
                nXBeeNodes = XBeeNodeTableIndex;
                // Create string containing number of nodes discovered
                sprintf(lcdString,"%d Node(s) Found",XBeeNodeTableIndex);
                // Release mutex
                OSMutexPost(&XBeeRXDataProtect,OS_OPT_POST_NONE,&err);
                // Display string on LCD
                lcd_puts_ucos(lcdString,0,1,true,false);
                // Sleep for one second
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                // Display node info (else show "End of Results" text)
                if(nXBeeNodes>0)
                {   
                    for(uint8_t j=0;j<nXBeeNodes;j++)
                    { 
                        // Get mutex
                        OSMutexPend(&XBeeRXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
                        // Only display information if ND response was OK: Create string with 16-bit address of node and device type
                        if((xBeeNodeTable[XBeeNodeTableIndex-1].commandStatus)==0x00)
                            sprintf(lcdString,"[%d/%d] %02x MY=%04x",j+1, XBeeNodeTableIndex, xBeeNodeTable[j].deviceType, xBeeNodeTable[j].MY);
                        // Create string with error code
                        else    
                            sprintf(lcdString,"ERROR: CODE %02x",xBeeNodeTable[XBeeNodeTableIndex-1].commandStatus);
                        // Release mutex
                        OSMutexPost(&XBeeRXDataProtect,OS_OPT_POST_NONE,&err);
                        // Display string on LCD
                        lcd_puts_ucos(lcdString,0,1,true,false);
                        // Sleep for 2 seconds
                        OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                        // Get mutex
                        OSMutexPend(&XBeeRXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
                        // Only display information if ND response was OK: Create string with 64-bit address of node
                        if((xBeeNodeTable[XBeeNodeTableIndex-1].commandStatus)==0x00)
                            sprintf(lcdString,"%04x%04x%04x%04x",xBeeNodeTable[j].SH_high,xBeeNodeTable[j].SH_low,xBeeNodeTable[j].SL_high,xBeeNodeTable[j].SL_low);
                        // Create string with error code
                        else    
                            sprintf(lcdString,"ERROR: CODE %02x",xBeeNodeTable[XBeeNodeTableIndex-1].commandStatus);
                        // Release mutex
                        OSMutexPost(&XBeeRXDataProtect,OS_OPT_POST_NONE,&err);
                        // Display string on LCD
                        lcd_puts_ucos(lcdString,0,1,true,false);
                        // Sleep for 2 seconds
                        OSTimeDlyHMSM(0,0,2,0,OS_OPT_TIME_HMSM_STRICT,&err);
                    }
                }
                // Display string on LCD
                lcd_puts_ucos((char*) pgm_read_word(&TestStringsPtrs[8]),0,1,false,true);
                // Sleep for one second
                OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
                break;
            }
            //////////////////
            // DEMO ROUTINE //
            //////////////////
            default:
            {
                // RED CHASER
                for(uint8_t i=1;i<17;i++)
                {
                    TLC5947_SetRed(i,4095);
                    OSTimeDlyHMSM(0,0,0,25,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // GREEN CHASER
                for(uint8_t i=1;i<17;i++)
                {
                    TLC5947_SetGreen(i,4095);
                    OSTimeDlyHMSM(0,0,0,25,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // BLUE CHASER
                for(uint8_t i=1;i<17;i++)
                {
                    TLC5947_SetBlue(i,4095);
                    OSTimeDlyHMSM(0,0,0,25,OS_OPT_TIME_HMSM_STRICT,&err);
                }
                // All OFF
                TLC5947_SetAllOff();

                // Continuous 16th stepping @ 4000 Hz
                // FORWARDS
                stepperMotor1_step(4000,0,true,0x08,true,false);
                stepperMotor2_step(4000,0,true,0x08,true,false);
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // BACKWARDS
                stepperMotor1_step(4000,0,false,0x08,true,false);
                stepperMotor2_step(4000,0,false,0x08,true,false);
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // Stop motors
                stepperMotor1_stopDisable(true);
                stepperMotor2_stopDisable(true);
                
                // Random RGB colour [0,4095]
                random_R = rand() / (RAND_MAX + 1.0) * (4095 - 0 + 1) + 0;
                random_G = rand() / (RAND_MAX + 1.0) * (4095 - 0 + 1) + 0;
                random_B = rand() / (RAND_MAX + 1.0) * (4095 - 0 + 1) + 0;
                TLC5947_SetAllRGB(random_R, random_G, random_B);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // All OFF
                TLC5947_SetAllOff();

                // Turn approximately 90 degrees clockwise
                stepperMotor1_step(4000,0,true,0x08,true,false);
                stepperMotor2_step(4000,0,false,0x08,true,false);
                stepperMotor1_startStepping(0);
                stepperMotor2_startStepping(0);
                OSTimeDlyHMSM(0,0,0,500,OS_OPT_TIME_HMSM_STRICT,&err);
                // Stop motors
                stepperMotor1_stopDisable(true);
                stepperMotor2_stopDisable(true);
                break;
            }               
        }
        CPU_CRITICAL_ENTER();
        if(!testRepeat)
        {
            CPU_CRITICAL_EXIT();
            // Increment testIndex
            if(!doDemo)
                testIndex++;
            // Reset device if all tests are complete
            if(testIndex>(N_TEST_STEPS-1) && !doDemo)
            {
                // Clear MCU LED
                PORTQ.OUTCLR=0x04;
                // Clear LCD
                lcd_clrscr();
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
                // Go to (0,1) on LCD
                lcd_gotoxy(0,1);
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
                // Display strings for 1 second
                _delay_ms(1000);
                // Go to (0,1) on LCD
                lcd_gotoxy(0,1);
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
                // Display strings for 1 second
                _delay_ms(1000);
                // Reset XMEGA
                CPU_CRITICAL_ENTER();
                    CPU_CCP=CCP_IOREG_gc; 
                    RST.CTRL=RST_SWRST_bm;
                CPU_CRITICAL_EXIT();
            }
        }
        else
            CPU_CRITICAL_EXIT();
    }
}

// XBeeRXPacketHandler
static void XBeeRXPacketHandler(void *p_arg)
{   
    // Read junk data (2 bytes) from USART (as data may have been RXed at power-on)
    uint8_t data;
    #if HW_VERSION=='A' // RevA: XBee uses USARTD1
        data=USARTD1.DATA;
        data=USARTD1.DATA;
        /* Enable USARTD1 RX Interrupt */
        // Receive complete interrupt: High Level
        // Transmit complete interrupt: Disabled
        // Data register empty interrupt: Disabled
        USARTD1.CTRLA=(USARTD1.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
            USART_RXCINTLVL_HI_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;
    #elif HW_VERSION=='B' // RevB: XBee uses USARTE0
        data=USARTE0.DATA;
        data=USARTE0.DATA;
        /* Enable USARTE0 RX Interrupt */
        // Receive complete interrupt: High Level
        // Transmit complete interrupt: Disabled
        // Data register empty interrupt: Disabled
        USARTE0.CTRLA=(USARTE0.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
        USART_RXCINTLVL_HI_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;
    #endif
    // Reset buffer (just in case)
    XBee_resetRXbuffer(true);
    // Wake XBee
    PORTH.OUTCLR=0x20;
    // Task body (always written as an infinite loop) 
    while(1)
    {
        // Wait on semaphore (unblocks when entire packet has been RXed)
        OSTaskSemPend(0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Read packet (and take appropriate action)
        ReadXBeePacket();
    }
} // XBeeRXPacketHandler()

// XBeeTXPacketHandler
static void XBeeTXPacketHandler(void *p_arg)
{
    // Local variables
    uint8_t XBeeTXDataIndex;
    uint16_t packetLength;
    // Task body (always written as an infinite loop) 
    while(1)
    {   
        // Wait on semaphore
        OSTaskSemPend(0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Get mutex
        OSMutexPend(&XBeeTXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Initialize XBeeTXDataIndex to 0
        XBeeTXDataIndex=0;
        // Save packet length in local 16-bit variable
        packetLength=(XBeeTXData[1]<<8)|(XBeeTXData[2]);
        // Send packet
        while(XBeeTXDataIndex<(packetLength+4))
        {
            putchar_XBee(XBeeTXData[XBeeTXDataIndex++]);
            // Sleep for one clock tick
            // Note: Without this, the USART Data Register Empty Flag fails to set for some reason (causing the system to hang)
            OSTimeDly(1,OS_OPT_TIME_PERIODIC,&err);
        }
        // Release mutex
        OSMutexPost(&XBeeTXDataProtect,OS_OPT_POST_NONE,&err);
    }
} // XBeeTXPacketHandler()

// USARTE1RXTask
static void USARTE1RXTask(void *p_arg)
{
    // Read junk data (2 bytes) from USARTE1 (as data may have been RXed at power-on)
    uint8_t data;
    data=USARTE1.DATA;
    data=USARTE1.DATA;
    /* Enable USARTE1 RX Interrupt */
    // Receive complete interrupt: High Level
    // Transmit complete interrupt: Disabled
    // Data register empty interrupt: Disabled
    USARTE1.CTRLA=(USARTE1.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
        USART_RXCINTLVL_HI_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;
    // Reset buffer (just in case)
    usarte1_resetRXbuffer(true);
    // Task body (always written as an infinite loop) 
    while(1)
    {
        // Wait on semaphore (unblocks when entire packet has been RXed)
        OSTaskSemPend(0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Read packet (and take appropriate action)
        ReadEBugPacket();
    }
} // USARTE1RXTask()

// IRTask
#if HW_VERSION=='B' // RevB: Features IR functionality
static void IRTask(void *p_arg)
{
    // Task body (always written as an infinite loop) 
    while(1)
    {   
        // Wait on semaphore
        OSTaskSemPend(0,OS_OPT_PEND_BLOCKING,NULL,&err);
    }
} // IRTask()
#endif

////////////////////////////////////
// HOW TO ADD NEW uC/OS-III TASKS //
////////////////////////////////////
// 1. Increase APP_CFG_N_TASKS definition in app_cfg.h by ONE.
// 2. Create new task stack at the top of this file: i.e. static CPU_STK AppTaskName_Stk[APP_TASK_NAME_STK_SIZE];
// 3. Add additional defines to app_cfg.h: i.e. priorities, stack sizes and stack limits
// 4. Add appropriate call to OSTaskCreate() in AppTaskStart_CreateTasks() to create task.
// 5. Place the new task above this comment block.
// 6. When in doubt, use existing code as a guide.

// Display
static void Display(void *p_arg)
{
    // Task body (always written as an infinite loop) 
    while(1)
    {   
        // Wait on semaphore
        OSTaskSemPend(0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Get mutex
        OSMutexPend(&LCDDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Display string on LCD
        if(!isLcdP)
            lcd_puts(LCDData);
        else
            lcd_puts_p(LCDDataP);
        // Release mutex
        OSMutexPost(&LCDDataProtect,OS_OPT_POST_NONE,&err);
    }
} // Display()

// ChargeTask
static void ChargeTask(void *p_arg)
{
    // Local variables
    char lcdString[17];
    ChargingState chargingState = NOT_CHARGING;
    uint16_t chargingStatus;
    uint16_t batteryStatus;
    uint16_t manufacturerStatus;
    uint8_t byte0;
    uint8_t byte1;
    bool doStorageCharge=false;
    uint8_t rsoc;
    uint16_t chargeOption;
    CPU_SR_ALLOC();
    
    // Disable interrupts on TEST switch (prevents LCD display item from changing on button release)
    PORTK.INT0MASK=0x00;
    // Check if TEST button is held down (enter Storage Charge mode if so)
    if(!(PORTK.IN & 0x80))
    {
        doStorageCharge=true;
        // Show string on LCD
        lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[11]), 0, 1, false, true);
        // Sleep for 2 ticks (so string is displayed)
        OSTimeDly(2,OS_OPT_TIME_PERIODIC,&err);
        // Display until button is released
        while(!(PORTK.IN & 0x80));
    }
    // Ensure charger is enabled
    else
    {
        // Charge Option [0x12]
        sendBuffer[0]=0x12;
        while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
        byte0 = twiMaster.readData[0];
        byte1 = twiMaster.readData[1];
        chargeOption=(byte1<<8)|byte0;
                            
        // Charge Inhibit (Bit 0)   = 0 (Enabled Charge)
        // Learn (Bit 6)            = 0 (Disable)
        chargeOption &= ~(1 << 0);
        chargeOption &= ~(1 << 6);
        sendBuffer[0]=0x12;
        sendBuffer[1]=chargeOption & 0xFF;
        sendBuffer[2]=(chargeOption >> 8) & 0xFF;
        while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
    }
    // Enable interrupts on TEST switch
    PORTK.INT0MASK=0x80;                         
        
    // Task body (always written as an infinite loop)
    while(1)
    {
        // Check if charger is connected
        if(PORTC.IN & (1<<6))
        {
            // Check Gas Gauge PFStatus [0x53] and PFStatus2 [0x6b]
            checkForAndHandleGGPermanentFailure();
            
            // BatteryStatus [0x16]
            sendBuffer[0]=0x16;
            while(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
            byte0 = twiMaster.readData[0];
            byte1 = twiMaster.readData[1];
            batteryStatus=(byte1<<8)|byte0;
            
            // Manufacturer Status [0x0006]
            sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
            sendBuffer[1]=0x06; // lower byte of command
            sendBuffer[2]=0x00; // upper byte of command
            while(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
            while(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
            byte0 = twiMaster.readData[0];
            byte1 = twiMaster.readData[1];
            manufacturerStatus=(byte1<<8)|byte0;                
            // Check Manufacturer Status->State
            switch((manufacturerStatus>>8) & 0x000F)
            {
                // Wake Up
                case 0:
                    chargingState = NOT_CHARGING;
                    break;
                // Normal Discharge
                case 1:
                    chargingState = NOT_CHARGING;
                    break;
                // Pre-Charge
                case 3:
                    chargingState = CHARGING;
                    break;
                // Charge
                case 5:
                    chargingState = CHARGING;
                    break;
                // Charge Termination
                case 7:
                    chargingState = CHARGE_TERMINATED;
                    break;
                // Fault Charge Terminate
                case 8:
                    chargingState = CHARGE_TERMINATED;
                    break;
                // Permanent Failure
                case 9:
                    chargingState = NOT_CHARGING;
                    break;
                // Overcurrent
                case 10:
                    chargingState = NOT_CHARGING;
                    break;
                // Overtemperature
                case 11:
                    chargingState = NOT_CHARGING;
                    break;
                // Battery Failure
                case 12:
                    chargingState = NOT_CHARGING;
                    break;
                // Sleep
                case 13:
                    chargingState = NOT_CHARGING;
                    break;
                // Discharge Prohibited
                case 14:
                    chargingState = NOT_CHARGING;
                    break;
                // Battery Removed
                case 15:
                    chargingState = NOT_CHARGING;
                    break;
            }
            // Also check FC flag in BatteryStatus
            if((batteryStatus&0x0020)==0x0020)
                chargingState = CHARGE_TERMINATED;
            
            // Sleep for 1 second
            OSTimeDlyHMSM(0,0,1,0,OS_OPT_TIME_HMSM_STRICT,&err);
            
            // Storage charge mode
            if(doStorageCharge)
            {
                // Charge Option [0x12]
                sendBuffer[0]=0x12;
                while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                byte0 = twiMaster.readData[0];
                byte1 = twiMaster.readData[1];
                chargeOption=(byte1<<8)|byte0;
                
                // RSOC [0x0D]
                sendBuffer[0]=0x0D;
                while(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,1)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                byte0=twiMaster.readData[0];
                rsoc=byte0;
                
                // Perform discharge
                // Also check for valid RSOC bounds
                if((rsoc > STORAGE_CHARGE_RSOC) && (rsoc > 0) && (rsoc <= 100))
                {
                    // Set state
                    chargingState = NOT_CHARGING;                       
                    // Charge Inhibit (Bit 0)   = 1 (Inhibit Charge)
                    // Learn (Bit 6)            = 1 (Enable)
                    chargeOption |= 1 << 0;
                    chargeOption |= 1 << 6;
                    sendBuffer[0]=0x12;
                    sendBuffer[1]=chargeOption & 0xFF;
                    sendBuffer[2]=(chargeOption >> 8) & 0xFF;
                    while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                    
                    // Accelerate discharge by turning on all RGB LEDs          
                    TLC5947_SetAll(4095);
                    // Enable stepper motors (ENABLE set to LOW)
                    if((rsoc - STORAGE_CHARGE_RSOC) > STORAGE_CHARGE_RSOC_SM_EN_THRESH)
                    {
                        PORTJ.OUTCLR=0x10;
                        PORTK.OUTCLR=0x08;
                    }
                    else
                    {
                        PORTJ.OUTSET=0x10;
                        PORTK.OUTSET=0x08;
                    }
                }
                // Terminate storage charge
                else if(rsoc == STORAGE_CHARGE_RSOC)
                {
                    // Set state
                    chargingState = CHARGE_TERMINATED;
                    // Disable RGB LEDs
                    TLC5947_SetAllOff();
                    // Disable stepper motors (ENABLE set to HIGH)
                    PORTJ.OUTSET=0x10;
                    PORTK.OUTSET=0x08;
                                        
                    // Charge Inhibit (Bit 0)   = 1 (Inhibit Charge)
                    // Learn (Bit 6)            = 0 (Disable)
                    chargeOption |= 1 << 0;
                    chargeOption &= ~(1 << 6);
                    sendBuffer[0]=0x12;
                    sendBuffer[1]=chargeOption & 0xFF;
                    sendBuffer[2]=(chargeOption >> 8) & 0xFF;
                    while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                }                   
                // Perform charge
                else
                {   
                    // Disable RGB LEDs
                    TLC5947_SetAllOff();
                    // Disable stepper motors (ENABLE set to HIGH)
                    PORTJ.OUTSET=0x10;
                    PORTK.OUTSET=0x08;
                            
                    // Charge Inhibit (Bit 0)   = 0 (Enabled Charge)
                    // Learn (Bit 6)            = 0 (Disable)
                    chargeOption &= ~(1 << 0);
                    chargeOption &= ~(1 << 6);
                    sendBuffer[0]=0x12;
                    sendBuffer[1]=chargeOption & 0xFF;
                    sendBuffer[2]=(chargeOption >> 8) & 0xFF;
                    while(!TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,sendBuffer,3,0)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                }
            }
            
            // STATE == CHARGE_TERMINATED
            if(chargingState==CHARGE_TERMINATED)
            {   
                // Save charge time
                uint8_t timeSeconds_save = timeSeconds;
                uint8_t timeMinutes_save = timeMinutes;
                uint8_t timeHours_save = timeHours;
                // Clear MCU LED
                PORTQ.OUTCLR=0x04;
                // Turn ON GREEN STATUS LED
                PORTH.OUTSET=0x02;
                PORTH.OUTCLR=0x10;
                /* Display message on LCD */
                // Clear top line
                LCD_ClearLineAndSetXPos(0,0);
                // Display string on LCD
                lcd_puts_p((char*)pgm_read_word(&ChargingStringsPtrs[1]));
                // Clear bottom line
                LCD_ClearLineAndSetXPos(1,0);
                // Display string on LCD
                int i=0;
                while(PORTC.IN & (1<<6))
                {
                    // Clear bottom line
                    LCD_ClearLineAndSetXPos(1,0);
                    // Display string on LCD
                    if(i==0)
                        lcd_puts_p((char*)pgm_read_word(&ChargingStringsPtrs[3]));
                    else if(i==1)
                    {
                        sprintf(lcdString,"ChgDur=%02d:%02d:%02d",timeHours_save,timeMinutes_save,timeSeconds_save);
                        lcd_puts(lcdString);    
                    }   
                    // Wrap increment
                    if(i==1)
                        i=0;
                    else
                        i++;                    
                    // Display string for 1 second
                    _delay_ms(1000);
                }
                // Clear LCD
                lcd_clrscr();
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
                // Go to (0,1) on LCD
                lcd_gotoxy(0,1);
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
                // Display strings for 1 second
                _delay_ms(1000);
                // Go to (0,1) on LCD
                lcd_gotoxy(0,1);
                // Display string on LCD
                lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
                // Display strings for 1 second
                _delay_ms(1000);
                // Reset XMEGA
                CPU_CRITICAL_ENTER();
                    CPU_CCP=CCP_IOREG_gc;
                    RST.CTRL=RST_SWRST_bm;
                CPU_CRITICAL_EXIT();
            }
            // STATE == CHARGING
            else if(chargingState==CHARGING)
            {
                // ChargingStatus [0x55]
                sendBuffer[0]=0x55;
                while(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT));
                byte0 = twiMaster.readData[0];
                byte1 = twiMaster.readData[1];
                chargingStatus=(byte1<<8)|byte0;
                // Display string on LCD
                if(chargingStatus&0x2000)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[4]), 0, 1, false, true);
                else if(chargingStatus&0x1000)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[5]), 0, 1, false, true);
                else if(chargingStatus&0x0800)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[6]), 0, 1, false, true);
                else if(chargingStatus&0x0400)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[7]), 0, 1, false, true);
                else if(chargingStatus&0x0200)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[8]), 0, 1, false, true);
                else if(chargingStatus&0x0100)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[9]), 0, 1, false, true);
                else if(chargingStatus&0x0040)
                    lcd_puts_ucos((char*)pgm_read_word(&ChargingStringsPtrs[10]), 0, 1, false, true);
                        
                // Toggle GREEN STATUS LED
                PORTH.OUTTGL=0x02;
                PORTH.OUTCLR=0x10;
            }
            else
            {
                // Display Manufacturer Status on LCD
                sprintf(lcdString,"ManufStat=0x%04x",manufacturerStatus);
                lcd_puts_ucos(lcdString,0,1,true,false);
                // Toggle RED STATUS LED
                PORTH.OUTCLR=0x02;
                PORTH.OUTTGL=0x10;
            }
        }
        // No charger: reset device
        else
        {
            // Clear MCU LED
            PORTQ.OUTCLR=0x04;
            // Set RED STATUS LED
            PORTH.OUTCLR=0x02;
            PORTH.OUTSET=0x10;
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*)pgm_read_word(&ChargingStringsPtrs[2]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Clear LCD
            lcd_clrscr();
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Display string on LCD
            lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
            // Display strings for 1 second
            _delay_ms(1000);
            // Reset XMEGA
            CPU_CRITICAL_ENTER();
                CPU_CCP=CCP_IOREG_gc;
                RST.CTRL=RST_SWRST_bm;
            CPU_CRITICAL_EXIT();
        }
    }
} // ChargeTask()

/*
*********************************************************************************************************
*                                               INITIALIZATION
*********************************************************************************************************
*/
void AppTaskStart_CreateTasks(OperatingState operatingState)
{
    switch(operatingState)
    {
        case TEST:
            // TestTask
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[APP_TASK_TEST_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"TestTask",
                    (OS_TASK_PTR )TestTask,
                    (void       *)0,
                    (OS_PRIO     )APP_TASK_TEST_PRIO,
                    (CPU_STK    *)&AppTaskTest_Stk[0],
                    (CPU_STK_SIZE)APP_TASK_TEST_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)APP_TASK_TEST_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
        case NORMAL:
            // XBeeRXPacketHandler
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[XBEE_RX_PACKET_HANDLER_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"XBeeRXPacketHandler",
                    (OS_TASK_PTR )XBeeRXPacketHandler,
                    (void       *)0,
                    (OS_PRIO     )XBEE_RX_PACKET_HANDLER_PRIO,
                    (CPU_STK    *)&XBeeRXPacketHandler_Stk[0],
                    (CPU_STK_SIZE)XBEE_RX_PACKET_HANDLER_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)XBEE_RX_PACKET_HANDLER_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // XBeeTXPacketHandler
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[XBEE_TX_PACKET_HANDLER_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"XBeeTXPacketHandler",
                    (OS_TASK_PTR )XBeeTXPacketHandler,
                    (void       *)0,
                    (OS_PRIO     )XBEE_TX_PACKET_HANDLER_PRIO,
                    (CPU_STK    *)&XBeeTXPacketHandler_Stk[0],
                    (CPU_STK_SIZE)XBEE_TX_PACKET_HANDLER_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)XBEE_TX_PACKET_HANDLER_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // USARTE1RXTask
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[USARTE1RXTASK_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"USARTE1RXTask",
                    (OS_TASK_PTR )USARTE1RXTask,
                    (void       *)0,
                    (OS_PRIO     )USARTE1RXTASK_PRIO,
                    (CPU_STK    *)&USARTE1RXTask_Stk[0],
                    (CPU_STK_SIZE)USARTE1RXTASK_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)USARTE1RXTASK_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // Display
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[DISPLAY_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"Display",
                    (OS_TASK_PTR )Display,
                    (void       *)0,
                    (OS_PRIO     )DISPLAY_PRIO,
                    (CPU_STK    *)&Display_Stk[0],
                    (CPU_STK_SIZE)DISPLAY_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)DISPLAY_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // IRTask
            #if HW_VERSION=='B' // RevB: Features IR functionality
                OSTaskCreate((OS_TCB     *)&AppTaskTCB[IRTASK_TCB_ARRAY_INDEX],
                (CPU_CHAR   *)"IRTask",
                (OS_TASK_PTR )IRTask,
                (void       *)0,
                (OS_PRIO     )IRTASK_PRIO,
                (CPU_STK    *)&IRTask_Stk[0],
                (CPU_STK_SIZE)IRTASK_STK_SIZE_LIMIT,
                (CPU_STK_SIZE)IRTASK_STK_SIZE,
                (OS_MSG_QTY  )0,
                (OS_TICK     )0,
                (void       *)0,
                (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                (OS_ERR    *)&err);
            #endif  
            break;
        case CHARGE:
            // ChargeTask
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[CHARGETASK_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"ChargeTask",
                    (OS_TASK_PTR )ChargeTask,
                    (void       *)0,
                    (OS_PRIO     )CHARGETASK_PRIO,
                    (CPU_STK    *)&ChargeTask_Stk[0],
                    (CPU_STK_SIZE)CHARGETASK_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)CHARGETASK_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            // Display
            OSTaskCreate((OS_TCB     *)&AppTaskTCB[DISPLAY_TCB_ARRAY_INDEX],
                    (CPU_CHAR   *)"Display",
                    (OS_TASK_PTR )Display,
                    (void       *)0,
                    (OS_PRIO     )DISPLAY_PRIO,
                    (CPU_STK    *)&Display_Stk[0],
                    (CPU_STK_SIZE)DISPLAY_STK_SIZE_LIMIT,
                    (CPU_STK_SIZE)DISPLAY_STK_SIZE,
                    (OS_MSG_QTY  )0,
                    (OS_TICK     )0,
                    (void       *)0,
                    (OS_OPT     )(OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
                    (OS_ERR    *)&err);
            break;
        default:
            break;
    }    
} // AppTaskStart_CreateTasks()

// AVR initialization
void AVRInit(void)
{
    // Local variables
    unsigned char n;
    // Interrupt system initialization
    // Make sure the interrupts are disabled
    asm("cli");
    // Low level interrupt: On
    // Round-robin scheduling for low level interrupt: On
    // Medium level interrupt: On
    // High level interrupt: On
    // The interrupt vectors will be placed at the start of the Application FLASH section
    n = (PMIC.CTRL & (~(PMIC_RREN_bm | PMIC_IVSEL_bm | PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm))) |
        PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
    CCP = CCP_IOREG_gc;
    PMIC.CTRL = n;
    // Set the default priority for round-robin scheduling
    PMIC.INTPRI = 0x00;

    /* Setup XMEGA peripherals */
    // CLOCKS
    SystemClocksInit();
    // PORTS
    PortsInit();
    // VIRTUAL PORTS
    vports_init();
    // USART
    usartc1_init();
    #if HW_VERSION=='A' // RevA: XBee uses USARTD1
        usartd1_init();
    #elif HW_VERSION=='B' // RevA: XBee uses USARTE0
        usarte0_init();
    #endif
    // TIMER/COUNTER
    tcc0_init(); // Stepper Motor 1 (STEP)
    tce0_init(); // Stepper Motor 2 (STEP)
    #if HW_VERSION=='B' // RevB: Features IR functionality
        setIREmitters(0xFFFF,false); // Disable all IR emitters
        tcf1_init(); // 38 kHz signal for IR emitters
    #endif
    // DMA
    DMA_Enable();
    TLC5947_SPI_DMA_init(RGB_DMA);
    // TWI
    TWI_MasterInit(&twiMaster,&TWID,TWI_MASTER_INTLVL_LO_gc,TWI_BAUDSETTING);
    /* Enable internal pull-ups on PD0, PD1 [Uncomment if you don't have external pull-ups] */
    // Configure several PINxCTRL registers at the same time
    //PORTCFG.MPCMASK = 0x03;
    //PORTD.PIN0CTRL = (PORTD.PIN0CTRL & ~PORT_OPC_gm) | PORT_OPC_PULLUP_gc;

    // LCD
    // If TEST push-button is held down at a non-POR reset event, disable LCD
    if(!(resetSource & RST_PORF_bm)&&!(PORTK.IN & 0x80))
    {
        lcd_clrscr();
        lcd_init(LCD_DISP_OFF);
        PORTA.DIRCLR=0x80; // Set PA7 to input (high-impedance) to disable LCD backlight
    }
    else
    {
        lcd_init(LCD_DISP_ON);
        PORTA.OUTSET=0x80; // Turn ON LCD backlight
    }
    // ADC
    adcb_init(1);
    // DAC
    dacb_init();
    LoadSineWave(gWaveNumSamp);
    // Turn ON stepper motor drivers (SLEEP set to HIGH)
    PORTJ.OUTSET=0x08;
    PORTK.OUTSET=0x10;
    // Stepper motors initially are disabled (ENABLE set to HIGH)
    PORTJ.OUTSET=0x10;
    PORTK.OUTSET=0x08;
    // Event System
    event_system_init();
    // IR Receivers
    #if HW_VERSION=='B' // RevB: Features IR functionality
        setupIRReceivers();
    #endif
    // Seed RAND
    srand(eeprom_read_word(&randInit));
    eeprom_write_word(&randInit,rand());
    // Globally enable interrupts
    asm("sei");
} // AVRInit()

void PortsInit(void)
{
    //////////////////////////
    // PORTA initialization //
    //////////////////////////
    /*
    0   LCD display (Register Select)
    1   LCD display (Read/Write)
    2   LCD display (Enable)
    3   LCD display (Data Bit 4)
    4   LCD display (Data Bit 5)
    5   LCD display (Data Bit 6)
    6   LCD display (Data Bit 7)
    7   LCD backlight control (to gate of Q17)
    */
    PORTA.DIR=0x80; // Only PA7 set as an OUTPUT
    // Note: lcd_init() will set the remaining port pins as OUTPUTS

    //////////////////////////
    // PORTB initialization //
    //////////////////////////
    /*
    0   ADR440 2.048V external analog reference voltage
    1   Connected to ground
    2   TPA0253 audio amplifier (LIN)
    3   Expansion Header Pin 1
    4   LM35 output (temperature sensor)
    5   Light Sensor Gain Control 1
    6   Light Sensor analog output
    7   Battery+ voltage
    */
    PORTB.DIR=0x24; // PB2,PB5 set as OUTPUTS
    // Note: The direction of PC3 is set when the Expansion Header is initialized

    //////////////////////////
    // PORTC initialization //
    //////////////////////////
    #if HW_VERSION=='A'
    /*
    0   Stepper Motor 1 (STEP)
    1   IR Emitter Pair (1,2)
    2   IR Emitter Pair (3,4)
    3   IR Emitter Pair (5,6)
    4   TCL5947 RGB LED Drivers (XLAT)
    5   TCL5947 RGB LED Drivers (SCLK)
    6   Power source (HIGH=DC PSU, LOW=LiPo)
    7   TCL5947 RGB LED Drivers (SIN)
    */
    // Prevent USARTC1 from generating a false start bit during initialization
    // Also, set SPI Slave Select (PC4/XLAT) HIGH (active-low)
    PORTC.OUT=0x90; 
    PORTC.DIR=0xBF; // Set all pins except PC6 as OUTPUTS
    #elif HW_VERSION=='B'
    /*
    0   Stepper Motor 1 (STEP)
    1   TPIC6A596 SRCK
    2   TPIC6A596 SER IN
    3   TPIC6A596 RCK
    4   TCL5947 RGB LED Drivers (XLAT)
    5   TCL5947 RGB LED Drivers (SCLK)
    6   Power source (HIGH=DC PSU, LOW=LiPo)
    7   TCL5947 RGB LED Drivers (SIN)
    */
    // Prevent USARTC1 from generating a false start bit during initialization
    // Also, set SPI Slave Select (PC4/XLAT) HIGH (active-low)
    PORTC.OUT=0x90;
    PORTC.DIR=0xBF; // Set all pins except PC6 as OUTPUTS
    #endif
    
    // Bit6 Output/Pull configuration: Totempole/Pull-down (on input)
    // Bit6 Input/Sense configuration: Sense rising edge
    // Bit6 inverted: Off
    // Bit6 slew rate limitation: Off
    PORTC.PIN6CTRL=PORT_OPC_PULLDOWN_gc | PORT_ISC_RISING_gc;
    PORTC.INTCTRL=(PORTC.INTCTRL & (~(PORT_INT1LVL_gm | PORT_INT0LVL_gm))) |
        PORT_INT1LVL_OFF_gc | PORT_INT0LVL_HI_gc;
    PORTC.INT0MASK=0x40;

    //////////////////////////
    // PORTD initialization //
    //////////////////////////
    #if HW_VERSION=='A'
    /*
    0   SMBus DATA
    1   SMBus CLOCK
    2   IR Receiver/Proximity Sensor Digital Output (IR1)
    3   IR Receiver/Proximity Sensor Digital Output (IR2)
    4   IR Receiver/Proximity Sensor Digital Output (IR3)
    5   IR Receiver/Proximity Sensor Digital Output (IR4)
    6   XMEGA USARTD1 RX (XBee Pin 2) (DOUT)
    7   XMEGA USARTD1 TX (XBee Pin 3) (DIN)
    */
    // Prevent USARTD1 from generating a false start bit during initialization
    PORTD.OUT=0x80; 
    PORTD.DIR=0x80; // PD7 set as an OUTPUT
    #elif HW_VERSION=='B'
    /*
    0   SMBus DATA
    1   SMBus CLOCK
    2   IR Receiver/Proximity Sensor Digital Output (IR1)
    3   IR Receiver/Proximity Sensor Digital Output (IR2)
    4   IR Receiver/Proximity Sensor Digital Output (IR3)
    5   IR Receiver/Proximity Sensor Digital Output (IR4)
    6   Unused
    7   Unused
    */
    #endif
    // Note: No need to specify direction of TWID. Pull-ups for TWID (if required) are setup in TWID_Master_init()
    
    // Bit2 Output/Pull configuration: Totempole/No
    // Bit2 Input/Sense configuration: Sense both edges
    // Bit2 inverted: On
    // Bit2 slew rate limitation: Off
    PORTD.PIN2CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit3 Output/Pull configuration: Totempole/No
    // Bit3 Input/Sense configuration: Sense both edges
    // Bit3 inverted: On
    // Bit3 slew rate limitation: Off
    PORTD.PIN3CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit4 Output/Pull configuration: Totempole/No
    // Bit4 Input/Sense configuration: Sense both edges
    // Bit4 inverted: On
    // Bit4 slew rate limitation: Off
    PORTD.PIN4CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit5 Output/Pull configuration: Totempole/No
    // Bit5 Input/Sense configuration: Sense both edges
    // Bit5 inverted: On
    // Bit5 slew rate limitation: Off
    PORTD.PIN5CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;

    //////////////////////////
    // PORTE initialization //
    //////////////////////////
    #if HW_VERSION=='A'
    /*
    0   Stepper Motor 2 (STEP)
    1   IR Emitter Pair (7,8)
    2   IR Emitter Pair (9,10)
    3   IR Emitter Pair (11,12)
    4   Expansion Header Pin 6
    5   Expansion Header Pin 12
    6   Expansion Header Pin 13
    7   Expansion Header Pin 14
    */
    PORTE.DIR=0x0F; // PE0-PE3 set as OUTPUTS
    #elif HW_VERSION=='B'
    /*
    0   Stepper Motor 2 (STEP)
    1   Push-Button Switch
    2   XMEGA USARTE0 RX (XBee Pin 2) (DOUT)
    3   XMEGA USARTE0 TX (XBee Pin 3) (DIN)
    4   Expansion Header Pin 6
    5   Expansion Header Pin 12
    6   Expansion Header Pin 13
    7   Expansion Header Pin 14
    */
    // Prevent USARTE0 from generating a false start bit during initialization
    PORTE.OUT=0x08;
    PORTE.DIR=0x09; // PE0, PE3 set as an OUTPUTS
    #endif
    // Note: The direction of PE4-PE7 is set when the Expansion Header is initialized

    //////////////////////////
    // PORTF initialization //
    //////////////////////////
    #if HW_VERSION=='A'
    /*
    0   IR Receiver/Proximity Sensor Digital Output (IR5)
    1   IR Receiver/Proximity Sensor Digital Output (IR6)
    2   IR Receiver/Proximity Sensor Digital Output (IR7)
    3   IR Receiver/Proximity Sensor Digital Output (IR8)
    4   IR Emitter Pair (13,14)
    5   IR Emitter Pair (15,16)
    6   Expansion Header Pin 4
    7   Expansion Header Pin 5
    */
    PORTF.DIR=0x30;
    #elif HW_VERSION=='B'
    /*
    0   IR Receiver/Proximity Sensor Digital Output (IR5)
    1   IR Receiver/Proximity Sensor Digital Output (IR6)
    2   IR Receiver/Proximity Sensor Digital Output (IR7)
    3   IR Receiver/Proximity Sensor Digital Output (IR8)
    4   IR Emitter Gate Drive (Modulation) Signal
    5   TPIC6A596 nSRCLR
    6   Expansion Header Pin 4
    7   Expansion Header Pin 5
    */
    PORTF.OUT=0x20; // Set nSRCLR HIGH 
    PORTF.DIR=0x30;
    #endif
    // Note: The direction of PF6 and PF7 is set when the Expansion Header is initialized

    // Bit0 Output/Pull configuration: Totempole/No
    // Bit0 Input/Sense configuration: Sense both edges
    // Bit0 inverted: On
    // Bit0 slew rate limitation: Off
    PORTF.PIN0CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit1 Output/Pull configuration: Totempole/No
    // Bit1 Input/Sense configuration: Sense both edges
    // Bit1 inverted: On
    // Bit1 slew rate limitation: Off
    PORTF.PIN1CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit2 Output/Pull configuration: Totempole/No
    // Bit2 Input/Sense configuration: Sense both edges
    // Bit2 inverted: On
    // Bit2 slew rate limitation: Off
    PORTF.PIN2CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit3 Output/Pull configuration: Totempole/No
    // Bit3 Input/Sense configuration: Sense both edges
    // Bit3 inverted: On
    // Bit3 slew rate limitation: Off
    PORTF.PIN3CTRL=PORT_INVEN_bm | PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;

    //////////////////////////
    // PORTH initialization //
    //////////////////////////
    /*
    0   TPA0253 audio amplifier shutdown (active low)
    1   Bi-Colour LED (GREEN)
    2   Expansion Header Pin 15
    3   TCL5947 RGB LED Drivers (BLANK) (active high)
    4   Bi-Colour LED (RED)
    5   XBee Pin 9 (Sleep_RQ) (active high)
    6   Rotary DIP Switch S5 (1)
    7   Rotary DIP Switch S5 (2)
    */
    PORTH.DIR=0x3B; // PH0,PH1,PH3,PH4,PH5 set as OUTPUTS
    // Note: The direction of PH2 is set when the Expansion Header is initialized
    
    /* BLANK (PH3) should be at a high level when powered on to keep the outputs off until
    valid grayscale data are written to the latch. This avoids the LED being randomly
    illuminated immediately after power-up.
    */
    SET_RGB_BLANK_HIGH; // HIGH = All constant current outputs off.
    PORTH.OUTSET=0x20; // XBee is in SLEEP MODE at startup

    // Bit6 Output/Pull configuration: Totempole/Pull-up (on input)
    // Bit6 Input/Sense configuration: Sense both edges
    // Bit6 inverted: On
    // Bit6 slew rate limitation: Off
    PORTH.PIN6CTRL=PORT_INVEN_bm | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit7 Output/Pull configuration: Totempole/Pull-up (on input)
    // Bit7 Input/Sense configuration: Sense both edges
    // Bit7 inverted: On
    // Bit7 slew rate limitation: Off
    PORTH.PIN7CTRL=PORT_INVEN_bm | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;

    //////////////////////////
    // PORTJ initialization //
    //////////////////////////
    /*
    0   Rotary DIP Switch S5 (4)
    1   Rotary DIP Switch S5 (8)
    2   Stepper Motor 1 (DIRECTION)
    3   Stepper Motor 1 (SLEEP)
    4   Stepper Motor 1 (ENABLE)
    5   Stepper Motor 1 (MS1)
    6   Stepper Motor 1 (MS2)
    7   Stepper Motor 1 (MS3)
    */
    PORTJ.DIR=0xFC; // Set all pins except PJ0 and PJ1 as OUTPUTS

    // Bit0 Output/Pull configuration: Totempole/Pull-up (on input)
    // Bit0 Input/Sense configuration: Sense both edges
    // Bit0 inverted: On
    // Bit0 slew rate limitation: Off
    PORTJ.PIN0CTRL=PORT_INVEN_bm | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;
    // Bit1 Output/Pull configuration: Totempole/Pull-up (on input)
    // Bit1 Input/Sense configuration: Sense both edges
    // Bit1 inverted: On
    // Bit1 slew rate limitation: Off
    PORTJ.PIN1CTRL=PORT_INVEN_bm | PORT_OPC_PULLUP_gc | PORT_ISC_BOTHEDGES_gc;

    //////////////////////////
    // PORTK initialization //
    //////////////////////////
    /*
    0   Stepper Motor 2 (MS3)
    1   Stepper Motor 2 (MS2)
    2   Stepper Motor 2 (MS1)
    3   Stepper Motor 2 (ENABLE)
    4   Stepper Motor 2 (SLEEP)
    5   Stepper Motor 2 (DIRECTION)
    6   Light Sensor Gain Control 2
    7   TEST (S1) push-button switch (active low)
    */
    PORTK.DIR=0x7F; // Set all pins except PK7 as OUTPUTS

    PORTK.PIN7CTRL=PORT_OPC_TOTEM_gc | PORT_ISC_BOTHEDGES_gc;
    // Interrupt 0 level: Low
    PORTK.INTCTRL=(PORTK.INTCTRL & (~(PORT_INT1LVL_gm | PORT_INT0LVL_gm))) |
        PORT_INT1LVL_OFF_gc | PORT_INT0LVL_LO_gc;

    //////////////////////////
    // PORTQ initialization //
    //////////////////////////
    /*
    0   External crystal oscillator (32.768 KHz) [TOSC1]
    1   External crystal oscillator  (32.768 KHz) [TOSC2]
    2   MCU Status LED
    3   ADC input enable (HIGH=ENABLE, LOW=DISABLE)
    */
    PORTQ.DIR=0x0C; // PQ2 and PQ3 set as OUTPUTS

    //////////////////////////
    // PORTR initialization //
    //////////////////////////
    /*
    0   External crystal oscillator (14.7456 MHz) [XTAL2]
    1   External crystal oscillator (14.7456 MHz) [XTAL1]
    */
    PORTR.DIR=0x00;
} // PortsInit()

// System clocks initialization
void SystemClocksInit(void)
{
    unsigned char n, s;
    // Save interrupts enabled/disabled state
    s = SREG;
    // Disable interrupts
    asm("cli");
    
    /* EXTERNAL 14.7456 MHz OSCILLATOR */
    // Note: PLL is used to get CLK=2*14.7456=29.4912 MHz
    #if F_CPU==29491200UL
        // External 14745.600 kHz oscillator initialization
        OSC.XOSCCTRL=OSC_FRQRANGE_12TO16_gc | OSC_XOSCSEL_XTAL_16KCLK_gc;
        // Enable the external oscillator
        OSC.CTRL|=OSC_XOSCEN_bm;

        // Wait for the external oscillator to stabilize
        while ((OSC.STATUS & OSC_XOSCRDY_bm)==0);

        // PLL initialization
        // PLL clock cource: External Osc. or Clock
        // PLL multiplication factor: 2
        // PLL frequency: 29.491200 MHz
        // Set the PLL clock source and multiplication factor
        n=(OSC.PLLCTRL & (~(OSC_PLLSRC_gm | OSC_PLLFAC_gm))) |
            OSC_PLLSRC_XOSC_gc | 2;
        CCP=CCP_IOREG_gc;
        OSC.PLLCTRL=n;
        // Enable the PLL
        OSC.CTRL|=OSC_PLLEN_bm;

        // System Clock prescaler A division factor: 1
        // System Clock prescalers B & C division factors: B:1, C:1
        // ClkPer4: 29491.200 kHz
        // ClkPer2: 29491.200 kHz
        // ClkPer:  29491.200 kHz
        // ClkCPU:  29491.200 kHz
        n=(CLK.PSCTRL & (~(CLK_PSADIV_gm | CLK_PSBCDIV1_bm | CLK_PSBCDIV0_bm))) |
            CLK_PSADIV_1_gc | CLK_PSBCDIV_1_1_gc;
        CCP=CCP_IOREG_gc;
        CLK.PSCTRL=n;

        // Wait for the PLL to stabilize
        while ((OSC.STATUS & OSC_PLLRDY_bm)==0);

        // Select the system clock source: Phase Locked Loop
        n=(CLK.CTRL & (~CLK_SCLKSEL_gm)) | CLK_SCLKSEL_PLL_gc;
        CCP=CCP_IOREG_gc;
        CLK.CTRL=n;

        // Disable the unused oscillators: 2 MHz, 32 MHz, internal 32 kHz
        OSC.CTRL&= ~(OSC_RC2MEN_bm | OSC_RC32MEN_bm | OSC_RC32KEN_bm);
    /* INTERNAL 32 MHz OSCILLATOR */
    #elif F_CPU == 32000000UL
        /* 
        * XMEGA-A1 XPLAINED DFLL errata workaround.  Uses the on-board 32.768KHz 
        * crystal and TOSC oscillator as the calibration reference for the Digital 
        * Frequency Locked Loops of the RC2M and RC32M oscillators. 
        * 
        * For more information on the workaround see the XMEGA-A1 data sheet 
        * errata (Atmel document 8067M-AVR-09/10, page 94, item 20).  It requires 
        * both the RC2M and RC32M oscillators and DFLLs to be enabled for either 
        * of them to function properly. 
        * 
        * Greg Muth (AVRFreaks: GTKNarwhal) 14-AUG-2012 
        */ 

        // Enable RC2M, RC32M and XOSC/TOSC oscillators and wait for ready 
        OSC.XOSCCTRL = OSC_XOSCSEL_32KHz_gc; 
        OSC.CTRL = OSC_RC2MEN_bm | OSC_RC32MEN_bm | OSC_XOSCEN_bm; 
        while (~OSC.STATUS & (OSC_RC2MRDY_bm | OSC_RC32MRDY_bm | OSC_XOSCRDY_bm));
    
        // Select TOSC as calibration reference for both DFLLs and enable each 
        OSC.DFLLCTRL = OSC_RC32MCREF_bm | OSC_RC2MCREF_bm; 
        DFLLRC32M.CTRL = DFLL_ENABLE_bm; 
        DFLLRC2M.CTRL = DFLL_ENABLE_bm; 
        
        // Select the system clock source: 32 MHz Internal RC Osc. 
        CCP = CCP_IOREG_gc; 
        CLK.CTRL = CLK_SCLKSEL_RC32M_gc;
        
        // System Clock prescaler A division factor: 1
        // System Clock prescalers B & C division factors: B:1, C:1
        // ClkPer4: 32000.000 kHz
        // ClkPer2: 32000.000 kHz
        // ClkPer:  32000.000 kHz
        // ClkCPU:  32000.000 kHz
        n = (CLK.PSCTRL & (~(CLK_PSADIV_gm | CLK_PSBCDIV1_bm | CLK_PSBCDIV0_bm))) |
            CLK_PSADIV_1_gc | CLK_PSBCDIV_1_1_gc;
        CCP = CCP_IOREG_gc;
        CLK.PSCTRL = n;
    #endif
    
    // Peripheral Clock output: Disabled
    PORTCFG.CLKEVOUT = (PORTCFG.CLKEVOUT & (~PORTCFG_CLKOUT_gm)) | PORTCFG_CLKOUT_OFF_gc;
    // Restore interrupts enabled/disabled state
    SREG = s;
} // SystemClocksInit()

// Timer/Counter TCC0 initialization
void tcc0_init(void)
{
    unsigned char s;
    unsigned char n;

    // Note: the correct PORTC direction for the Compare Channels outputs
    // is configured in the PortsInit function

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc0_disable(&TCC0);
    // Clock source: Peripheral Clock/1
    TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
    // Mode: Frequency Waveform Gen., Overflow Int./Event on TOP
    // Compare/Capture on channel A: Off
    // Compare/Capture on channel B: Off
    // Compare/Capture on channel C: Off
    // Compare/Capture on channel D: Off
    TCC0.CTRLB=(TCC0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC_WGMODE_FRQ_gc;

    // Capture event source: None
    // Capture event action: None
    TCC0.CTRLD=(TCC0.CTRLD & (~(TC0_EVACT_gm | TC0_EVSEL_gm))) |
        TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCC0.INTCTRLA=(TCC0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    // Compare/Capture channel C interrupt: Disabled
    // Compare/Capture channel D interrupt: Disabled
    TCC0.INTCTRLB=(TCC0.INTCTRLB & (~(TC0_CCDINTLVL_gm | TC0_CCCINTLVL_gm | TC0_CCBINTLVL_gm | TC0_CCAINTLVL_gm))) |
        TC_CCDINTLVL_OFF_gc | TC_CCCINTLVL_OFF_gc | TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESC.CTRLA&= ~HIRES_HREN0_bm;

    // Advanced Waveform Extension initialization
    // Disable locking the AWEX configuration registers just to be sure
    n=MCU.AWEXLOCK & (~MCU_AWEXCLOCK_bm);
    CCP=CCP_IOREG_gc;
    MCU.AWEXLOCK=n;

    // Pattern generation: Off
    // Common waveform channel mode: Off
    // Dead time insertion for compare channel A: Off
    // Dead time insertion for compare channel B: Off
    // Dead time insertion for compare channel C: Off
    // Dead time insertion for compare channel D: Off
    AWEXC.CTRL=(AWEXC.CTRL & (~(AWEX_PGM_bm | AWEX_CWCM_bm | AWEX_DTICCDEN_bm | AWEX_DTICCCEN_bm | AWEX_DTICCBEN_bm | AWEX_DTICCAEN_bm)));
    // Low side dead time duration [ClkPer cycles]
    AWEXC.DTLS=0;
    // High side dead time duration [ClkPer cycles]
    AWEXC.DTHS=0;
    // PORTC output register override
    AWEXC.OUTOVEN=0b00000000;

    // Fault protection initialization
    // Fault detection on OCD Break detection: On
    // Fault detection restart mode: Latched Mode
    // Fault detection action: None (Fault protection disabled)
    AWEXC.FDCTRL=(AWEXC.FDCTRL & (~(AWEX_FDDBD_bm | AWEX_FDMODE_bm | AWEX_FDACT_gm))) |
        AWEX_FDACT_NONE_gc;
    // Fault detect events: 
    // Event channel 0: Off
    // Event channel 1: Off
    // Event channel 2: Off
    // Event channel 3: Off
    // Event channel 4: Off
    // Event channel 5: Off
    // Event channel 6: Off
    // Event channel 7: Off
    AWEXC.FDEMASK=0b00000000;
    // Make sure the fault detect flag is cleared
    AWEXC.STATUS|=AWEXC.STATUS & AWEX_FDF_bm;

    // Clear the interrupt flags
    TCC0.INTFLAGS=TCC0.INTFLAGS;
    // Set counter register
    TCC0.CNT=0x0000;
    // Set period register
    // Not used in Frequency Waveform Generation mode
    TCC0.PER=0x0000;
    // Set channel A Compare/Capture register
    // Controls the period in Frequency Waveform Generation mode
    TCC0.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCC0.CCB=0x0000;
    // Set channel C Compare/Capture register
    TCC0.CCC=0x0000;
    // Set channel D Compare/Capture register
    TCC0.CCD=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;
} // tcc0_init()

// Timer/Counter TCE0 initialization
void tce0_init(void)
{
    unsigned char s;
    unsigned char n;

    // Note: the correct PORTE direction for the Compare Channels outputs
    // is configured in the PortsInit function

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc0_disable(&TCE0);
    // Clock source: Peripheral Clock/1
    TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
    // Mode: Frequency Waveform Gen., Overflow Int./Event on TOP
    // Compare/Capture on channel A: Off
    // Compare/Capture on channel B: Off
    // Compare/Capture on channel C: Off
    // Compare/Capture on channel D: Off
    TCE0.CTRLB=(TCE0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC_WGMODE_FRQ_gc;

    // Capture event source: None
    // Capture event action: None
    TCE0.CTRLD=(TCE0.CTRLD & (~(TC0_EVACT_gm | TC0_EVSEL_gm))) |
        TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCE0.INTCTRLA=(TCE0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    // Compare/Capture channel C interrupt: Disabled
    // Compare/Capture channel D interrupt: Disabled
    TCE0.INTCTRLB=(TCE0.INTCTRLB & (~(TC0_CCDINTLVL_gm | TC0_CCCINTLVL_gm | TC0_CCBINTLVL_gm | TC0_CCAINTLVL_gm))) |
        TC_CCDINTLVL_OFF_gc | TC_CCCINTLVL_OFF_gc | TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESE.CTRLA&= ~HIRES_HREN0_bm;

    // Advanced Waveform Extension initialization
    // Disable locking the AWEX configuration registers just to be sure
    n=MCU.AWEXLOCK & (~MCU_AWEXELOCK_bm);
    CCP=CCP_IOREG_gc;
    MCU.AWEXLOCK=n;

    // Pattern generation: Off
    // Common waveform channel mode: Off
    // Dead time insertion for compare channel A: Off
    // Dead time insertion for compare channel B: Off
    // Dead time insertion for compare channel C: Off
    // Dead time insertion for compare channel D: Off
    AWEXE.CTRL=(AWEXE.CTRL & (~(AWEX_PGM_bm | AWEX_CWCM_bm | AWEX_DTICCDEN_bm | AWEX_DTICCCEN_bm | AWEX_DTICCBEN_bm | AWEX_DTICCAEN_bm)));
    // Low side dead time duration [ClkPer cycles]
    AWEXE.DTLS=0;
    // High side dead time duration [ClkPer cycles]
    AWEXE.DTHS=0;
    // PORTE output register override
    AWEXE.OUTOVEN=0b00000000;

    // Fault protection initialization
    // Fault detection on OCD Break detection: On
    // Fault detection restart mode: Latched Mode
    // Fault detection action: None (Fault protection disabled)
    AWEXE.FDCTRL=(AWEXE.FDCTRL & (~(AWEX_FDDBD_bm | AWEX_FDMODE_bm | AWEX_FDACT_gm))) |
        AWEX_FDACT_NONE_gc;
    // Fault detect events: 
    // Event channel 0: Off
    // Event channel 1: Off
    // Event channel 2: Off
    // Event channel 3: Off
    // Event channel 4: Off
    // Event channel 5: Off
    // Event channel 6: Off
    // Event channel 7: Off
    AWEXE.FDEMASK=0b00000000;
    // Make sure the fault detect flag is cleared
    AWEXE.STATUS|=AWEXE.STATUS & AWEX_FDF_bm;

    // Clear the interrupt flags
    TCE0.INTFLAGS=TCE0.INTFLAGS;
    // Set counter register
    TCE0.CNT=0x0000;
    // Set period register
    // Not used in Frequency Waveform Generation mode
    TCE0.PER=0x0000;
    // Set channel A Compare/Capture register
    // Controls the period in Frequency Waveform Generation mode
    TCE0.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCE0.CCB=0x0000;
    // Set channel C Compare/Capture register
    TCE0.CCC=0x0000;
    // Set channel D Compare/Capture register
    TCE0.CCD=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;
} // tce0_init()

// USARTC1 initialization
void usartc1_init(void)
{
    // Note: the correct PORTC direction for the RxD, TxD and XCK signals
    // is configured in the PortsInit function

    // Transmitter is enabled in Master SPI mode: 0
    // Set XCK (SCK)=0, TxD (MOSI)=0
    PORTC.OUTCLR=0xA0;

    // Communication mode: Master SPI
    // SPI Mode: 0
    // Data Order: MSB First
    USARTC1.CTRLC=USART_CMODE_MSPI_gc | 0x00 | 0x00;
    PORTC.PIN5CTRL&=~PORT_INVEN_bm;

    // Receive complete interrupt: Disabled
    // Transmit complete interrupt: Disabled
    // Data register empty interrupt: Disabled
    USARTC1.CTRLA=(USARTC1.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
        USART_RXCINTLVL_OFF_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;

    // CLK=32MHz:           2*8MHz SPI clock
    // CLK=14.7456*2 MHz:   2*7.3728MHz SPI clock
    // Note: According to TLC5947 datasheet: 15-MHz Data Transfer Rate (Cascaded Devices, SCLK Duty = 50%)
    // Thus, we are using a slightly higher than recommended clock speed.
    USARTC1.BAUDCTRLA=1; 
    USARTC1.BAUDCTRLB=0;

    // Receiver: Off
    // Transmitter: On
    // Double transmission speed mode: On
    // Multi-processor communication mode: Off
    USARTC1.CTRLB=(USARTC1.CTRLB & (~(USART_RXEN_bm | USART_TXEN_bm | USART_MPCM_bm | USART_TXB8_bm))) |
        USART_TXEN_bm | USART_CLK2X_bm;
} // usartc1_init()

// USART_D1 initialization
void usartd1_init(void)
{
    // Initialize global variables
    rx_wr_index_XBee=0;
    rx_rd_index_XBee=0;
    rx_counter_XBee=0;

    // Note: the correct PORTD direction for the RxD, TxD and XCK signals
    // is configured in the PortsInit function

    // Transmitter is enabled
    // Set TxD=1
    PORTD.OUTSET=0x80;

    // Communication mode: Asynchronous USART
    // Data bits: 8
    // Stop bits: 1
    // Parity: Disabled
    USARTD1.CTRLC=USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc;

    // Receive complete interrupt: Disabled
    // Transmit complete interrupt: Disabled
    // Data register empty interrupt: Disabled
    USARTD1.CTRLA=(USARTD1.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
        USART_RXCINTLVL_OFF_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;

    #if F_CPU==32000000UL
    // Required Baud rate: 115200
    // Real Baud Rate: 115211.5 (x1 Mode), Error: 0.0 %
    USARTD1.BAUDCTRLA=0x2E;
    USARTD1.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x08;
    #elif F_CPU==29491200
    // Required Baud rate: 115200
    // Real Baud Rate: 115200.0 (x1 Mode), Error: 0.0 %
    USARTD1.BAUDCTRLA=0x80;
    USARTD1.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x07;
    #endif

    // Receiver: On
    // Transmitter: On
    // Double transmission speed mode: Off
    // Multi-processor communication mode: Off
    USARTD1.CTRLB=(USARTD1.CTRLB & (~(USART_RXEN_bm | USART_TXEN_bm | USART_CLK2X_bm | USART_MPCM_bm | USART_TXB8_bm))) |
        USART_RXEN_bm | USART_TXEN_bm;
} // usartd1_init()

// USART_E0 initialization
void usarte0_init(void)
{
    // Initialize global variables
    rx_wr_index_XBee=0;
    rx_rd_index_XBee=0;
    rx_counter_XBee=0;

    // Note: the correct PORTE direction for the RxD, TxD and XCK signals
    // is configured in the PortsInit function

    // Transmitter is enabled
    // Set TxD=1
    PORTE.OUTSET=0x08;

    // Communication mode: Asynchronous USART
    // Data bits: 8
    // Stop bits: 1
    // Parity: Disabled
    USARTE0.CTRLC=USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc;

    // Receive complete interrupt: Disabled
    // Transmit complete interrupt: Disabled
    // Data register empty interrupt: Disabled
    USARTE0.CTRLA=(USARTE0.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
    USART_RXCINTLVL_OFF_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;

    #if F_CPU==32000000UL
    // Required Baud rate: 115200
    // Real Baud Rate: 115211.5 (x1 Mode), Error: 0.0 %
    USARTE0.BAUDCTRLA=0x2E;
    USARTE0.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x08;
    #elif F_CPU==29491200
    // Required Baud rate: 115200
    // Real Baud Rate: 115200.0 (x1 Mode), Error: 0.0 %
    USARTE0.BAUDCTRLA=0x80;
    USARTE0.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x07;
    #endif

    // Receiver: On
    // Transmitter: On
    // Double transmission speed mode: Off
    // Multi-processor communication mode: Off
    USARTE0.CTRLB=(USARTE0.CTRLB & (~(USART_RXEN_bm | USART_TXEN_bm | USART_CLK2X_bm | USART_MPCM_bm | USART_TXB8_bm))) |
    USART_RXEN_bm | USART_TXEN_bm;
} // usarte0_init()

void TLC5947_SPI_DMA_init(volatile DMA_CH_t * dmaChannel)
{   
    // Setup transaction
    // Note: 2*24*12=576 bits (72 bytes) will be shifted each transaction
    DMA_SetupBlock( dmaChannel,
                    gsData,
                    DMA_CH_SRCRELOAD_TRANSACTION_gc,
                    DMA_CH_SRCDIR_INC_gc,
                    (void *) &USARTC1.DATA,
                    DMA_CH_DESTRELOAD_NONE_gc,
                    DMA_CH_DESTDIR_FIXED_gc,
                    sizeof(gsData),
                    DMA_CH_BURSTLEN_1BYTE_gc,
                    0,
                    false );
    // Trigger source: USART C1 Data Register Empty
    DMA_SetTriggerSource(dmaChannel,DMA_CH_TRIGSRC_USARTC1_DRE_gc);
    // Single-shot mode
    DMA_EnableSingleShot(dmaChannel);
    // Setup interrupts
    DMA_SetIntLevel(dmaChannel,DMA_CH_TRNINTLVL_HI_gc,DMA_CH_ERRINTLVL_HI_gc);
} // TLC5947_SPI_DMA_init()

// ADCB initialization
void adcb_init(int mode)
{
    unsigned int offs;

    // Initialize global variable
    adcb_offset=0;

    // ADCB is enabled
    // Resolution: 12 Bits
    // Load the calibration value for 12 Bit resolution
    // from the signature row
    ADCB.CALL=ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,ADCACAL0));
    ADCB.CALH=ReadCalibrationByte(offsetof(NVM_PROD_SIGNATURES_t,ADCACAL1));

    // Free Running mode: Off
    // Conversion mode: Unsigned
    if(mode==0)
        ADCB.CTRLB=(ADCB.CTRLB & (~(ADC_CONMODE_bm | ADC_FREERUN_bm | ADC_RESOLUTION_gm))) |
            ADC_RESOLUTION_12BIT_gc;
    // Conversion mode: Unsigned
    else if(mode==1)    
        ADCB.CTRLB=(ADCB.CTRLB & (~(ADC_CONMODE_bm | ADC_FREERUN_bm | ADC_RESOLUTION_gm))) |
            ADC_RESOLUTION_12BIT_gc;

    #if ADCB_CLK_DIV==512
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV512_gc;
    #elif ADCB_CLK_DIV==256
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV256_gc;
    #elif ADCB_CLK_DIV==128
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV128_gc;
    #elif ADCB_CLK_DIV==64
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV64_gc;
    #elif ADCB_CLK_DIV==32
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV32_gc;
    #elif ADCB_CLK_DIV==16
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV16_gc;
    #elif ADCB_CLK_DIV==8
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV8_gc;
    #elif ADCB_CLK_DIV==4
    ADCB.PRESCALER=(ADCB.PRESCALER & (~ADC_PRESCALER_gm)) | ADC_PRESCALER_DIV4_gc;      
    #endif

    // Reference: AREF pin on PORTB
    // Temperature reference: On
    ADCB.REFCTRL=(ADCB.REFCTRL & ((~(ADC_REFSEL_gm | ADC_TEMPREF_bm)) | ADC_BANDGAP_bm)) |
        ADC_REFSEL_AREFB_gc | ADC_TEMPREF_bm;

    // Read and save the ADC offset using channel 0
    // ADC1 pin connected to GND
    ADCB.CH0.CTRL=(ADCB.CH0.CTRL & (~(ADC_CH_START_bm | ADC_CH_GAINFAC_gm | ADC_CH_INPUTMODE_gm))) |
        ADC_CH_INPUTMODE_SINGLEENDED_gc;
    ADCB.CH0.MUXCTRL=(ADCB.CH0.MUXCTRL & (~(ADC_CH_MUXPOS_gm | ADC_CH_MUXNEG_gm))) |
        ADC_CH_MUXPOS_PIN1_gc;
    // Enable the ADC in order to read the offset
    ADCB.CTRLA|=ADC_ENABLE_bm;
    // Insert a delay to allow the ADC common mode voltage to stabilize
    _delay_us(2);
    // Perform several offset measurements and store the mean value
    offs=0;
    for (uint8_t i=0; i<16; i++)
    {
        // Start the AD conversion on channel 0
        ADCB.CH0.CTRL|=ADC_CH_START_bm;
        // Wait for the AD conversion to complete
        while ((ADCB.CH0.INTFLAGS & ADC_CH_CHIF_bm)==0);
        // Clear the interrupt flag
        ADCB.CH0.INTFLAGS=ADC_CH_CHIF_bm;
        // Read the offset
        offs+=(unsigned char) ADCB.CH0.RESL;
    }
    // Disable the ADC
    ADCB.CTRLA&= ~ADC_ENABLE_bm;
    // Store the mean value of the offset
    adcb_offset=(unsigned char) (offs/16);

    // Initialize the ADC Compare register
    ADCB.CMPL=0x00;
    ADCB.CMPH=0x00;

    // ADC channel 0 gain: 1
    // ADC channel 0 input mode: Single-ended positive input signal
    ADCB.CH0.CTRL=(ADCB.CH0.CTRL & (~(ADC_CH_START_bm | ADC_CH_GAINFAC_gm | ADC_CH_INPUTMODE_gm))) |
        ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;

    // ADC channel 0 positive input: ADC4 pin
    // ADC channel 0 negative input: GND
    ADCB.CH0.MUXCTRL=(ADCB.CH0.MUXCTRL & (~(ADC_CH_MUXPOS_gm | ADC_CH_MUXNEG_gm))) |
        ADC_CH_MUXPOS_PIN4_gc;

    // ADC channel 1 gain: 1
    // ADC channel 1 input mode: Single-ended positive input signal
    ADCB.CH1.CTRL=(ADCB.CH1.CTRL & (~(ADC_CH_START_bm | ADC_CH_GAINFAC_gm | ADC_CH_INPUTMODE_gm))) |
        ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;

    // ADC channel 1 positive input: ADC6 pin
    // ADC channel 1 negative input: GND
    ADCB.CH1.MUXCTRL=(ADCB.CH1.MUXCTRL & (~(ADC_CH_MUXPOS_gm | ADC_CH_MUXNEG_gm))) |
        ADC_CH_MUXPOS_PIN6_gc;

    // ADC channel 2 gain: 1
    // ADC channel 2 input mode: Single-ended positive input signal
    ADCB.CH2.CTRL=(ADCB.CH2.CTRL & (~(ADC_CH_START_bm | ADC_CH_GAINFAC_gm | ADC_CH_INPUTMODE_gm))) |
        ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_SINGLEENDED_gc;

    // ADC channel 2 positive input: ADC7 pin
    // ADC channel 2 negative input: GND
    ADCB.CH2.MUXCTRL=(ADCB.CH2.MUXCTRL & (~(ADC_CH_MUXPOS_gm | ADC_CH_MUXNEG_gm))) |
        ADC_CH_MUXPOS_PIN7_gc;

    // ADC channel 3 gain: 1
    // ADC channel 3 input mode: Internal positive input signal
    ADCB.CH3.CTRL=(ADCB.CH3.CTRL & (~(ADC_CH_START_bm | ADC_CH_GAINFAC_gm | ADC_CH_INPUTMODE_gm))) |
        ADC_CH_GAIN_1X_gc | ADC_CH_INPUTMODE_INTERNAL_gc;

    // ADC channel 3 positive input: Temp. Reference
    // ADC channel 3 negative input: GND
    ADCB.CH3.MUXCTRL=(ADCB.CH3.MUXCTRL & (~(ADC_CH_MUXPOS_gm | ADC_CH_MUXNEG_gm))) |
        ADC_CH_MUXINT_TEMP_gc;

    // AD conversion is started by software
    if(mode==0)
        ADCB.EVCTRL=ADC_EVACT_NONE_gc;
    // ADC is free-running, sweeped channel(s): 0, 1, 2, 3
    else if(mode==1)
        ADCB.EVCTRL=ADC_SWEEP_0123_gc | ADC_EVACT_NONE_gc;

    // Channel 0 interrupt: Disabled
    ADCB.CH0.INTCTRL=(ADCB.CH0.INTCTRL & (~(ADC_CH_INTMODE_gm | ADC_CH_INTLVL_gm))) |
        ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_OFF_gc;
    // Channel 1 interrupt: Disabled
    ADCB.CH1.INTCTRL=(ADCB.CH1.INTCTRL & (~(ADC_CH_INTMODE_gm | ADC_CH_INTLVL_gm))) |
        ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_OFF_gc;
    // Channel 2 interrupt: Disabled
    ADCB.CH2.INTCTRL=(ADCB.CH2.INTCTRL & (~(ADC_CH_INTMODE_gm | ADC_CH_INTLVL_gm))) |
        ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_OFF_gc;
    // Channel 3 interrupt: Disabled
    ADCB.CH3.INTCTRL=(ADCB.CH3.INTCTRL & (~(ADC_CH_INTMODE_gm | ADC_CH_INTLVL_gm))) |
        ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_OFF_gc;

    // Free Running mode: On
    if(mode==1)
        ADCB.CTRLB|=ADC_FREERUN_bm;

    // Enable the ADC
    ADCB.CTRLA|=ADC_ENABLE_bm;

    // Insert a delay to allow the ADC common mode voltage to stabilize
    _delay_us(2);
} // adcb_init()

// DACB initialization
void dacb_init(void)
{
    // Operating mode: Single Channel (Ch0)
    // Channel 0 triggered by the event system: On
    DACB.CTRLB=(DACB.CTRLB & (~(DAC_CHSEL_gm | DAC_CH0TRIG_bm | DAC_CH1TRIG_bm))) |
        DAC_CHSEL_SINGLE_gc | DAC_CH0TRIG_bm;

    // Reference: AVcc
    // Left adjust value: Off
    DACB.CTRLC=(DACB.CTRLC & (~(DAC_REFSEL_gm | DAC_LEFTADJ_bm))) |
        DAC_REFSEL_AVCC_gc;

    // DACB is triggered by the Event System Channel 0
    DACB.EVCTRL=(DACB.EVCTRL & (~DAC_EVSEL_gm)) | DAC_EVSEL_0_gc;

    // Conversion interval: 1.000 us (32 ClkPer cycles)
    DACB.TIMCTRL=(DACB.TIMCTRL & (~(DAC_CONINTVAL_gm | DAC_REFRESH_gm))) |
        DAC_CONINTVAL_32CLK_gc | DAC_REFRESH_OFF_gc;

    // DACB is enabled
    // Low power mode: Off
    // Channel 0 output: On
    // Channel 1 output: Off
    // Internal output connected to the ADCB and Analog Comparator MUX-es: Off
    DACB.CTRLA=(DACB.CTRLA & (~(DAC_IDOEN_bm | DAC_CH0EN_bm | DAC_CH1EN_bm | DAC_LPMODE_bm))) |
        DAC_CH0EN_bm | DAC_ENABLE_bm;
} // dacb_init()

// USARTE1 initialization
void usarte1_init(void)
{
    // Initialize global variables
    rx_wr_index_usarte1=0;
    rx_rd_index_usarte1=0;
    rx_counter_usarte1=0;

    // Note: the correct PORTE direction for the RxD, TxD and XCK signals
    // is configured in the PortsInit function

    // Transmitter is enabled
    // Set TxD=1
    PORTE.OUTSET=0x80;

    // Communication mode: Asynchronous USART
    // Data bits: 8
    // Stop bits: 1
    // Parity: Disabled
    USARTE1.CTRLC=USART_CMODE_ASYNCHRONOUS_gc | USART_PMODE_DISABLED_gc | USART_CHSIZE_8BIT_gc;

    // Receive complete interrupt: Disabled
    // Transmit complete interrupt: Disabled
    // Data register empty interrupt: Disabled
    USARTE1.CTRLA=(USARTE1.CTRLA & (~(USART_RXCINTLVL_gm | USART_TXCINTLVL_gm | USART_DREINTLVL_gm))) |
        USART_RXCINTLVL_OFF_gc | USART_TXCINTLVL_OFF_gc | USART_DREINTLVL_OFF_gc;

    #if F_CPU==32000000UL
    // Required Baud rate: 115200
    // Real Baud Rate: 115211.5 (x1 Mode), Error: 0.0 %
    USARTE1.BAUDCTRLA=0x2E;
    USARTE1.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x08;
    #elif F_CPU==29491200
    // Required Baud rate: 115200
    // Real Baud Rate: 115200.0 (x1 Mode), Error: 0.0 %
    USARTE1.BAUDCTRLA=0x80;
    USARTE1.BAUDCTRLB=((0x09 << USART_BSCALE_bp) & USART_BSCALE_gm) | 0x07;
    #endif

    // Receiver: On
    // Transmitter: On
    // Double transmission speed mode: Off
    // Multi-processor communication mode: Off
    USARTE1.CTRLB=(USARTE1.CTRLB & (~(USART_RXEN_bm | USART_TXEN_bm | USART_CLK2X_bm | USART_MPCM_bm | USART_TXB8_bm))) |
        USART_RXEN_bm | USART_TXEN_bm;
} // usarte1_init()

// Virtual Ports initialization
void vports_init(void)
{
    // PORTA mapped to VPORT0: LCD
    // PORTB mapped to VPORT1:
    PORTCFG.VPCTRLA=PORTCFG_VP1MAP_PORTB_gc | PORTCFG_VP0MAP_PORTA_gc;
    // PORTC mapped to VPORT2:
    // PORTF mapped to VPORT3: SW I2C in Operating Mode=0
    PORTCFG.VPCTRLB=PORTCFG_VP3MAP_PORTF_gc | PORTCFG_VP2MAP_PORTC_gc;
} // vports_init()

// Timer/Counter TCF1 initialization
void tcf1_init(void)
{
    unsigned char s;

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc1_disable(&TCF1);
    // Clock source: ClkPer/1
    TCF1.CTRLA=(TCF1.CTRLA & (~TC1_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
    // Mode: Frequency Waveform Gen., Overflow Int./Event on TOP
    // Compare/Capture on channel A: On
    // Compare/Capture on channel B: Off
    TCF1.CTRLB=(TCF1.CTRLB & (~(TC1_CCBEN_bm | TC1_WGMODE_gm))) |
        TC1_CCAEN_bm | TC_WGMODE_FRQ_gc;

    // Capture event source: None
    // Capture event action: None
    TCF1.CTRLD=(TCF1.CTRLD & (~(TC1_EVACT_gm | TC1_EVSEL_gm))) |
        TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCF1.INTCTRLA=(TCF1.INTCTRLA & (~(TC1_ERRINTLVL_gm | TC1_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    TCF1.INTCTRLB=(TCF1.INTCTRLB & (~(TC1_CCBINTLVL_gm | TC1_CCAINTLVL_gm))) |
        TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESC.CTRLA&= ~HIRES_HREN1_bm;

    // Clear the interrupt flags
    TCF1.INTFLAGS=TCF1.INTFLAGS;
    // Set counter register
    TCF1.CNT=0x0000;
    // Set period register
    // Not used in Frequency Waveform Generation mode
    TCF1.PER=0x0000;
    // Set channel A Compare/Capture register
    // Controls the period in Frequency Waveform Generation mode
    #if F_CPU==32000000UL
        TCF1.CCA=0x01A4;
    #elif F_CPU==29491200
        TCF1.CCA=0x0183;
    #endif
    // Set channel B Compare/Capture register
    TCF1.CCB=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;
} // tcf1_init()

// RTC initialization
void rtcxm_init(void)
{
    unsigned char s;

    // RTC clock source: 1024 Hz from 32.768 kHz Crystal Osc. on TOSC
    // External 32.768 kHz crystal oscillator initialization
    // External 32.768 kHz crystal oscillator low power mode: On
    OSC.XOSCCTRL=OSC_X32KLPM_bm | OSC_XOSCSEL_32KHz_gc;
    // Enable the external 32.768 kHz crystal oscillator
    OSC.CTRL|=OSC_XOSCEN_bm;
    // Wait for the external 32.768 kHz crystal oscillator to stabilize
    while ((OSC.STATUS & OSC_XOSCRDY_bm)==0);

    // Select the clock source and enable the RTC clock
    CLK.RTCCTRL=(CLK.RTCCTRL & (~CLK_RTCSRC_gm)) | CLK_RTCSRC_TOSC_gc | CLK_RTCEN_bm;
    // Make sure that the RTC is stopped before initializing it
    RTC.CTRL=(RTC.CTRL & (~RTC_PRESCALER_gm)) | RTC_PRESCALER_OFF_gc;

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Wait until the RTC is not busy
    while (RTC.STATUS & RTC_SYNCBUSY_bm);
    // Set the RTC period register
    RTC.PER=0x0400;
    // Set the RTC count register
    RTC.CNT=0x0000;
    // Set the RTC compare register
    RTC.COMP=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;

    // Set the clock prescaler: RTC Clock/1
    // and start the RTC
    RTC.CTRL=(RTC.CTRL & (~RTC_PRESCALER_gm)) | RTC_PRESCALER_DIV1_gc;

    // RTC overflow interrupt: Low Level
    // RTC compare interrupt: Disabled
    RTC.INTCTRL=(RTC.INTCTRL & (~(RTC_OVFINTLVL_gm | RTC_COMPINTLVL_gm))) |
        RTC_OVFINTLVL_LO_gc | RTC_COMPINTLVL_OFF_gc;
} // rtcxm_init()

// Timer/Counter TCD0 initialization
void tcd0_init(void)
{
    unsigned char s;

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc0_disable(&TCD0);
    // Clock source: ClkPer/256
    TCD0.CTRLA=(TCD0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
    // Mode: Normal Operation, Overflow Int./Event on TOP
    // Compare/Capture on channel A: Off
    // Compare/Capture on channel B: Off
    // Compare/Capture on channel C: On
    // Compare/Capture on channel D: On
    TCD0.CTRLB=(TCD0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC0_CCCEN_bm | TC0_CCDEN_bm |
        TC_WGMODE_NORMAL_gc;

    // Capture event source: Event Channel 0
    // Capture event action: Pulse Width Capture
    TCD0.CTRLD=(TCD0.CTRLD & (~(TC0_EVACT_gm | TC0_EVSEL_gm))) |
        TC_EVACT_PW_gc | TC_EVSEL_CH0_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCD0.INTCTRLA=(TCD0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    // Compare/Capture channel C interrupt: Disabled
    // Compare/Capture channel D interrupt: Disabled
    TCD0.INTCTRLB=(TCD0.INTCTRLB & (~(TC0_CCDINTLVL_gm | TC0_CCCINTLVL_gm | TC0_CCBINTLVL_gm | TC0_CCAINTLVL_gm))) |
        TC_CCDINTLVL_OFF_gc | TC_CCCINTLVL_OFF_gc | TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESD.CTRLA&= ~HIRES_HREN0_bm;

    // Clear the interrupt flags
    TCD0.INTFLAGS=TCD0.INTFLAGS;
    // Set counter register
    TCD0.CNT=0x0000;
    // Set period register
    TCD0.PER=0xFFFF;
    // Set channel A Compare/Capture register
    TCD0.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCD0.CCB=0x0000;
    // Set channel C Compare/Capture register
    TCD0.CCC=0x0000;
    // Set channel D Compare/Capture register
    TCD0.CCD=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;
} // tcd0_init()

// Timer/Counter TCD0 initialization
void tcd1_init(void)
{
    unsigned char s;

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc1_disable(&TCD1);
    // Clock source: ClkPer/256
    TCD1.CTRLA=(TCD1.CTRLA & (~TC1_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
    // Mode: Normal Operation, Overflow Int./Event on TOP
    // Compare/Capture on channel A: On
    // Compare/Capture on channel B: On
    TCD1.CTRLB=(TCD1.CTRLB & (~(TC1_CCAEN_bm | TC1_CCBEN_bm | TC1_WGMODE_gm))) |
        TC1_CCAEN_bm | TC1_CCBEN_bm |
        TC_WGMODE_NORMAL_gc;

    // Capture event source: Event Channel 3
    // Capture event action: Pulse Width Capture
    TCD1.CTRLD=(TCD1.CTRLD & (~(TC1_EVACT_gm | TC1_EVSEL_gm))) |
        TC_EVACT_PW_gc | TC_EVSEL_CH3_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCD1.INTCTRLA=(TCD1.INTCTRLA & (~(TC1_ERRINTLVL_gm | TC1_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    TCD1.INTCTRLB=(TCD1.INTCTRLB & (~(TC1_CCBINTLVL_gm | TC1_CCAINTLVL_gm))) |
        TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESD.CTRLA&= ~HIRES_HREN1_bm;

    // Clear the interrupt flags
    TCD1.INTFLAGS=TCD1.INTFLAGS;
    // Set counter register
    TCD1.CNT=0x0000;
    // Set period register
    TCD1.PER=0xFFFF;
    // Set channel A Compare/Capture register
    TCD1.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCD1.CCB=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;
} // tcd1_init()

// Timer/Counter TCF0 initialization
void tcf0_init(void)
{
    unsigned char s;

    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");

    // Disable and reset the timer/counter just to be sure
    tc0_disable(&TCF0);
    // Clock source: ClkPer/256
    TCF0.CTRLA=(TCF0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
    // Mode: Normal Operation, Overflow Int./Event on TOP
    // Compare/Capture on channel A: On
    // Compare/Capture on channel B: On
    // Compare/Capture on channel C: On
    // Compare/Capture on channel D: On
    TCF0.CTRLB=(TCF0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm |
        TC_WGMODE_NORMAL_gc;

    // Capture event source: Event Channel 4
    // Capture event action: Pulse Width Capture
    TCF0.CTRLD=(TCF0.CTRLD & (~(TC0_EVACT_gm | TC0_EVSEL_gm))) |
        TC_EVACT_PW_gc | TC_EVSEL_CH4_gc;

    // Overflow interrupt: Disabled
    // Error interrupt: Disabled
    TCF0.INTCTRLA=(TCF0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;

    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    // Compare/Capture channel C interrupt: Disabled
    // Compare/Capture channel D interrupt: Disabled
    TCF0.INTCTRLB=(TCF0.INTCTRLB & (~(TC0_CCDINTLVL_gm | TC0_CCCINTLVL_gm | TC0_CCBINTLVL_gm | TC0_CCAINTLVL_gm))) |
        TC_CCDINTLVL_OFF_gc | TC_CCCINTLVL_OFF_gc | TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;

    // High resolution extension: Off
    HIRESF.CTRLA&= ~HIRES_HREN0_bm;

    // Clear the interrupt flags
    TCF0.INTFLAGS=TCF0.INTFLAGS;
    // Set counter register
    TCF0.CNT=0x0000;
    // Set period register
    TCF0.PER=0xFFFF;
    // Set channel A Compare/Capture register
    TCF0.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCF0.CCB=0x0000;
    // Set channel C Compare/Capture register
    TCF0.CCC=0x0000;
    // Set channel D Compare/Capture register
    TCF0.CCD=0x0000;

    // Restore interrupts enabled/disabled state
    SREG=s;  
} // tcf0_init()

// Event System initialization
void event_system_init(void)
{
    /* DACB */
    // Event System Channel 0 Digital Filter Coefficient: 1 Sample
    EVSYS.CH0CTRL=(EVSYS.CH0CTRL & (~(EVSYS_QDIRM_gm | EVSYS_QDIEN_bm | EVSYS_QDEN_bm | EVSYS_DIGFILT_gm))) |
        EVSYS_DIGFILT_1SAMPLE_gc;

    /* UNUSED */
    // Event System Channel 1 Digital Filter Coefficient: 1 Sample
    EVSYS.CH1CTRL=EVSYS_DIGFILT_1SAMPLE_gc;

    /* IR RECEIVERS */
    // Event System Channel 2 Digital Filter Coefficient: 1 Sample
    EVSYS.CH2CTRL=(EVSYS.CH2CTRL & (~(EVSYS_QDIRM_gm | EVSYS_QDIEN_bm | EVSYS_QDEN_bm | EVSYS_DIGFILT_gm))) |
        EVSYS_DIGFILT_1SAMPLE_gc;
    // Event System Channel 3 Digital Filter Coefficient: 1 Sample
    EVSYS.CH3CTRL=EVSYS_DIGFILT_1SAMPLE_gc;
    // Event System Channel 4 Digital Filter Coefficient: 1 Sample
    EVSYS.CH4CTRL=(EVSYS.CH4CTRL & (~(EVSYS_QDIRM_gm | EVSYS_QDIEN_bm | EVSYS_QDEN_bm | EVSYS_DIGFILT_gm))) |
        EVSYS_DIGFILT_1SAMPLE_gc;
    // Event System Channel 5 Digital Filter Coefficient: 1 Sample
    EVSYS.CH5CTRL=EVSYS_DIGFILT_1SAMPLE_gc;
    // Event System Channel 6 Digital Filter Coefficient: 1 Sample
    EVSYS.CH6CTRL=EVSYS_DIGFILT_1SAMPLE_gc;
    // Event System Channel 7 Digital Filter Coefficient: 1 Sample
    EVSYS.CH7CTRL=EVSYS_DIGFILT_1SAMPLE_gc;

    // Event System Channel 0 output: Disabled
    PORTCFG.CLKEVOUT&= ~PORTCFG_EVOUT_gm;
} // event_system_init()

void setupIRReceivers()
{
    // For 32 MHz clock:
        // Longest pulse that can be measured is approximately 524ms
        // Resolution = 8us
    // For 29.4912 MHz clock:
        // Longest pulse that can be measured is approximately 570ms
        // Resolution = 8.70us
    // IR1,IR2
    tcd0_init();
    // IR3,IR4
    tcd1_init();
    // IR5,IR6,IR7,IR8
    tcf0_init();
} // setupIRReceivers()

/*
*********************************************************************************************************
*                                                PERIPHERALS
*********************************************************************************************************
*/

uint8_t ReadCalibrationByte(uint8_t index) 
{ 
    uint8_t result; 
    /* Load the NVM Command register to read the calibration row. */ 
    NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc; 
    result = pgm_read_byte(index); 
    /* Clean up NVM Command register. */ 
    NVM_CMD = NVM_CMD_NO_OPERATION_gc; 
    return result; 
} // ReadCalibrationByte()

// Disable a Timer/Counter type 0
void tc0_disable(TC0_t *ptc)
{
    // Timer/Counter off
    ptc->CTRLA = (ptc->CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_OFF_gc;
    // Issue a reset command
    ptc->CTRLFSET = TC_CMD_RESET_gc;
} // tc0_disable()

// Disable a Timer/Counter type 1
void tc1_disable(TC1_t *ptc)
{
    // Timer/Counter off
    ptc->CTRLA = (ptc->CTRLA & (~TC1_CLKSEL_gm)) | TC_CLKSEL_OFF_gc;
    // Issue a reset command
    ptc->CTRLFSET = TC_CMD_RESET_gc;
} // tc1_disable()

// Write a character to the USART connected to the XBee
void putchar_XBee(char c)
{
    #if HW_VERSION=='A' // RevA: XBee uses USARTD1
        while ((USARTD1.STATUS & USART_DREIF_bm) == 0);
        USARTD1.DATA=c;
    #elif HW_VERSION=='B' // RevB: XBee uses USARTE0
        while ((USARTE0.STATUS & USART_DREIF_bm) == 0);
        USARTE0.DATA=c;
    #endif
} // putchar_XBee()

// Write a character to the USARTE1 Transmitter
void putchar_usarte1(char c)
{
    while ((USARTE1.STATUS & USART_DREIF_bm) == 0);
    USARTE1.DATA=c;
} // putchar_usarte1()

// Receive a character from XBee buffer
uint8_t getchar_XBee(void)
{
    // Local variable
    uint8_t data;
    CPU_SR_ALLOC();

    // If there is no unread data in buffer, wait (block)
    while (rx_counter_XBee==0);
    data=rx_buffer_XBee[rx_rd_index_XBee++];
    // Handle wrap-around
    if (rx_rd_index_XBee == RX_BUFFER_SIZE_XBEE)
        rx_rd_index_XBee=0;
    // Decrement RX counter to indicate byte has been READ
    // This variable is also modified by the RX ISR, so interrupts must be disabled and then re-enabled
    CPU_CRITICAL_ENTER();
        --rx_counter_XBee;
    CPU_CRITICAL_EXIT();
    // Return byte READ
    return data;
} // getchar_XBee()

// Receive a character from USARTE1
uint8_t getchar_usarte1(void)
{
    // Local variable
    uint8_t data;
    CPU_SR_ALLOC();

    // If there is no unread data in buffer, wait (block)
    while (rx_counter_usarte1==0);
    data=rx_buffer_usarte1[rx_rd_index_usarte1++];
    // Handle wrap-around
    if (rx_rd_index_usarte1 == RX_BUFFER_SIZE_USARTE1)
        rx_rd_index_usarte1=0;
    // Decrement RX counter to indicate byte has been READ
    // This variable is also modified by the RX ISR, so interrupts must be disabled and then re-enabled
    CPU_CRITICAL_ENTER();
        --rx_counter_usarte1;
    CPU_CRITICAL_EXIT();
    // Return byte READ
    return data;
} // getchar_usarte1()

void TLC5947_SetAllRed(uint16_t value)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    for(uint8_t i=0;i<numChannels;i++)
    {
        if((i==0)|((i % 3)==0))
            TLC5947_SetGS(i,value);
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllRed()

void TLC5947_SetAllBlue(uint16_t value)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    for(uint8_t i=0;i<numChannels;i++)
    {
        if((i==2)|(((i-2) % 3)==0))
            TLC5947_SetGS(i,value);
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllBlue()

void TLC5947_SetAllGreen(uint16_t value)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    for(uint8_t i=0;i<numChannels;i++)
    {
        if((i==1)|(((i-1) % 3)==0))
            TLC5947_SetGS(i,value);
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllGreen()

void TLC5947_SetAll(uint16_t value)
{
    // Initialize grayscale data variable
    TLC5947_SetAllGS(value,0xFFFF);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAll()

void TLC5947_SetAllHold(uint16_t value, uint16_t mask)
{
    // Initialize grayscale data variable
    TLC5947_SetAllGS(value,mask);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllHold()

void TLC5947_SetAllOff(void)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllOff()

void TLC5947_SetRed(uint8_t led, uint16_t value)
{
    // NOTE: led corresponds to LED number (i.e. LED16-> led=16)
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    // Set LED
    TLC5947_SetGS((led-1)*3,value);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetRed()

void TLC5947_SetGreen(uint8_t led, uint16_t value)
{
    // NOTE: led corresponds to LED number (i.e. LED16-> led=16)
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    // Set LED
    TLC5947_SetGS(((led-1)*3)+1,value);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetGreen()

void TLC5947_SetBlue(uint8_t led, uint16_t value)
{
    // NOTE: led corresponds to LED number (i.e. LED16-> led=16)
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    // Set LED
    TLC5947_SetGS(((led-1)*3)+2,value);
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetBlue()

void TLC5947_SetMultiple(uint16_t mask1, uint16_t mask2, uint16_t mask3, uint16_t value)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    // MASK: MSB-> {mask1,mask2,mask3} <-LSB [48-bits total]
    // Set LEDs
    for(uint8_t i=0;i<numChannels;i++)
    {
        // Channels 0-15 [LED1->LED6(RED)]
        if(i<16)
        {
            if(((mask3>>i)&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
        // Channels 16-31 [LED6[GREEN,BLUE]->LED11(GREEN,BLUE)]
        else if((i>=16)&&(i<32))
        {
            if(((mask2>>(i-16))&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
        // Channels 32-47 [LED12(BLUE)->LED16]
        else if((i>=32)&&(i<48))
        {
            if(((mask1>>(i-32))&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetMultiple()

void TLC5947_SetMultipleHold(uint16_t mask1, uint16_t mask2, uint16_t mask3, uint16_t value, uint16_t mask)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,mask);
    // MASK: MSB-> {mask1,mask2,mask3} <-LSB [48-bits total]
    // Set LEDs
    for(uint8_t i=0;i<numChannels;i++)
    {
        // Channels 0-15 [LED1->LED6(RED)]
        if(i<16)
        {
            if(((mask3>>i)&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
        // Channels 16-31 [LED6(GREEN,BLUE)->LED11(GREEN,BLUE)]
        else if((i>=16)&&(i<32))
        {
            if(((mask2>>(i-16))&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
        // Channels 32-47 [LED12(BLUE)->LED16]
        else if((i>=32)&&(i<48))
        {
            if(((mask1>>(i-32))&0x1)==0x1)
                TLC5947_SetGS(i,value);
        }
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetMultipleHold()

void TLC5947_SetAllRGB(uint16_t r_value, uint16_t g_value, uint16_t b_value)
{
    // Initialize grayscale data variable to zero
    TLC5947_SetAllGS(0,0xFFFF);
    for(uint8_t i=0;i<numChannels;i++)
    {
        // RED
        if((i==0)|((i % 3)==0))
            TLC5947_SetGS(i,r_value);
        // GREEN
        else if((i==1)|(((i-1) % 3)==0))
            TLC5947_SetGS(i,g_value);
        // BLUE
        else if((i==2)|(((i-2) % 3)==0))
            TLC5947_SetGS(i,b_value);
    }
    // Set XLAT low
    SET_RGB_XLAT_LOW;
    // Trigger DMA transaction
    while((DMA_CH_IsOngoing(RGB_DMA)!=0)||(DMA_CH_IsPending(RGB_DMA)!=0));
    DMA_EnableChannel(RGB_DMA);
} // TLC5947_SetAllRGB()

void LCD_ClearLineAndSetXPos(int line, int x)
{
    // Go to (x,line) on LCD
    lcd_gotoxy(x,line);
    // Clear entire line
    lcd_puts("                ");
    // Go to (x,line) on LCD
    lcd_gotoxy(x,line);
} // LCD_ClearLineAndSetXPos()

void lcd_puts_ucos(char *string, int x, int y, bool clearLine, bool p)
{
    // Get mutex
    OSMutexPend(&LCDDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
    // Set flag
    isLcdP=p;
    // String in SRAM
    if(!isLcdP)
    {
        // Save length
        uint8_t length=strlen(string);
        // Save passed string to global variable
        for(uint8_t i=0;i<(clearLine?16:length);i++)
        {
            // Clear remainder of line
            if(i>=length)
                LCDData[i]=' ';
            // Set string
            else
                LCDData[i]=string[i];
        }
    }
    // String in PROGRAM MEMORY (FLASH)
    else
        LCDDataP=string;
    // Go to (x,y) on LCD
    lcd_gotoxy(x,y);
    // Release mutex
    OSMutexPost(&LCDDataProtect,OS_OPT_POST_NONE,&err);
    // Post mutex
    OSTaskSemPost(&AppTaskTCB[DISPLAY_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
} // lcd_puts_ucos()

void LCD_ClearLineFromXPos_ucos(int line, int x)
{
    // Get mutex
    OSMutexPend(&LCDDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
    // Clear global string variable
    for(uint8_t i=0;i<(16-x);i++)
        LCDData[i]=' ';
    // Go to (x,line) on LCD
    lcd_gotoxy(x,line);
    // Release mutex
    OSMutexPost(&LCDDataProtect,OS_OPT_POST_NONE,&err);
    // Post mutex
    OSTaskSemPost(&AppTaskTCB[DISPLAY_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
} // LCD_ClearLineFromXPos_ucos

uint8_t getRotarySwitchState(void)
{
    return ((PORTH.IN>>6)&0x01) + 2*((PORTH.IN>>7)&0x01) + 4*((PORTJ.IN>>0)&0x01) + 8*((PORTJ.IN>>1)&0x01);
} // getRotarySwitchState()

// ADCB channel data read function using polled mode
unsigned int adcb_read(unsigned char channel, int mode)
{
    ADC_CH_t *pch=&ADCB.CH0+channel;
    unsigned int data;

    // Start the AD conversion
    if(mode==0)
        pch->CTRL|=ADC_CH_START_bm;

    // Wait for the AD conversion to complete
    while ((pch->INTFLAGS & ADC_CH_CHIF_bm)==0);

    // Clear the interrupt flag
    pch->INTFLAGS=ADC_CH_CHIF_bm;
    // Read the AD conversion result
    ((unsigned char *) &data)[0]=pch->RESL;
    ((unsigned char *) &data)[1]=pch->RESH;
    // Compensate the ADC offset (and saturate to zero)
    if(adcb_offset>data)
        data=0;
    else
        data-=adcb_offset;
    return data;
} // adcb_read()

unsigned int getLightSensorReading(void)
{
    // Read ADC
    return adcb_read(1,1);
} // getLightSensorReading()

void setLightSensorLowGain(void)
{
    // Set gain control
    PORTB.OUTSET=0x20; // [GC1=PB5: HIGH]
    PORTK.OUTSET=0x40; // [GC2=PK6: HIGH]
} // setLightSensorLowGain()

void setLightSensorMediumGain(void)
{
    // Set gain control
    PORTB.OUTCLR=0x20; // [GC1=PB5: LOW]
    PORTK.OUTSET=0x40; // [GC2=PK6: HIGH]
} // setLightSensorMediumGain()

void setLightSensorHighGain(void)
{
    // Set gain control
    PORTB.OUTSET=0x20; // [GC1=PB5: HIGH]
    PORTK.OUTCLR=0x40; // [GC2=PK6: LOW]
} // setLightSensorHighGain()

void shutdownLightSensor(void)
{
    // Set gain control
    PORTB.OUTCLR=0x20; // [GC1=PB5: LOW]
    PORTK.OUTCLR=0x40; // [GC2=PK6: LOW]
} // shutdownLightSensor()

double getTemperature(void)
{
    return (double)adcb_read(0,1)/20;
} // getTemperature()

void LoadSineWave(int len)
{
    // Fill array
    for(int i=0;i<len;i++)
        DACdata12[i]=((sin((2.0/(float)len)*(float)i*M_PI)*0.5 + 0.5)*4095);
} // LoadSineWave()

void GenerateArbWave12_Speaker(volatile int *data, int len, unsigned long int freq, unsigned long int durationMS)
{
    // Reset DMA before outputting a new waveform
    DMA_ResetChannel(SPEAKER_DMA);
    // Set limit
    dmaCH1TransactionCountLimit=(freq*durationMS)/1000;
    // Note: This function triggers DACB to output the samples in DACdata12[] @ freq
    // Setup event (timer/counter)
    EVSYS.CH0MUX=EVSYS_CHMUX_TCE1_OVF_gc;   // CH0 = TCE1 overflow
    TCE1.CTRLA = 0x03;                      // Prescaler: clk/4
    TCE1.PER   = F_CPU/(len/2)/freq/4;      // 31=1MHz,63=500K,127=250K
    // Setup DMA transaction
    DMA_SetupBlock( SPEAKER_DMA,
                    (const void *) data,
                    DMA_CH_SRCRELOAD_TRANSACTION_gc,
                    DMA_CH_SRCDIR_INC_gc,
                    (void *) &DACB.CH0DATA,
                    DMA_CH_DESTRELOAD_BURST_gc,
                    DMA_CH_DESTDIR_INC_gc,
                    len,
                    DMA_CH_BURSTLEN_2BYTE_gc,
                    0,
                    true );
    // Trigger source: DACB CH0
    DMA_SetTriggerSource(SPEAKER_DMA,DMA_CH_TRIGSRC_DACB_CH0_gc);
    // Enable single-shot
    DMA_EnableSingleShot(SPEAKER_DMA);
    // Setup interrupts
    DMA_SetIntLevel(SPEAKER_DMA,DMA_CH_TRNINTLVL_LO_gc,DMA_CH_ERRINTLVL_LO_gc);
    // Enable DMA
    DMA_EnableChannel(SPEAKER_DMA);
} // GenerateArbWave12_Speaker()

void shutdownAudioAmplifier(bool in)
{
    // Shutdown amplifier
    if(in)
        PORTH.OUTCLR=0x01;
    // Turn ON amplifier
    else
        PORTH.OUTSET=0x01;
} // setAudioAmplifierShutdown()

void setupExpansionHeader(uint8_t mode)
{
    switch(mode)
    {
        /* ADD ADDITIONAL CONFIGURATIONS HERE */
        // Operating Mode=1 (BB-xM/Kinect)
        case 1:
        {
            /* Setup USARTE1 */
            // Prevent USARTE1 from generating a false start bit during initialization
            PORTE.OUT=0x80; 
            // XMEGA RX: PE6 (Expansion Header Pin 13)
            // XMEGA TX: PE7 (Expansion Header Pin 14)
            PORTE.DIRSET=PIN7_bm; // PE7 set as an OUTPUT
            usarte1_init();
            /* Setup software I2C */
            // Note(s):
            // 1. Pin directions are set automatically within the i2cmaster driver.
            // 2. The i2c_delay_T2 function in i2cmaster.S is setup for 400kHz FAST mode.
            // 3. PF6=SDA, PF7=SCL [set in i2cmaster.S] (Use 4.7k pull-up resistors on both lines).
            i2c_init();
            /* Setup pins to output isSteppingInProgress signals */
            // isSteppingInProgress1: PB3 (Expansion Header Pin 1)
            // isSteppingInProgress2: PE4 (Expansion Header Pin 6)
            PORTB.DIRSET=0x08;
            PORTE.DIRSET=0x10;
            /* RST_BBxM = PH2 (set as output, set high) */
            PORTH.DIRSET=0x04;
            PORTH.OUTSET=0x04;
            /* Kinect Power Enable = PE5 (set as output, set high) */
            PORTE.DIRSET=0x20;
            PORTE.OUTSET=0x20;  
            break;
        }
        // Operating Mode=0
        default:
        {
            /* Setup USARTE1 */
            // Prevent USARTE1 from generating a false start bit during initialization
            PORTE.OUT=0x80; 
            // XMEGA RX: PE6 (Expansion Header Pin 13)
            // XMEGA TX: PE7 (Expansion Header Pin 14)
            PORTE.DIRSET=PIN7_bm; // PE7 set as an OUTPUT
            usarte1_init();
            /* Setup software I2C */
            // Note(s):
            // 1. Pin directions are set automatically within the i2cmaster driver.
            // 2. The i2c_delay_T2 function in i2cmaster.S is setup for 400kHz FAST mode.
            // 3. PF6=SDA, PF7=SCL [set in i2cmaster.S] (Use 4.7k pull-up resistors on both lines).
            i2c_init();
            /* Setup pins to output isSteppingInProgress signals */
            // isSteppingInProgress1: PB3 (Expansion Header Pin 1)
            // isSteppingInProgress2: PE4 (Expansion Header Pin 6)
            PORTB.DIRSET=0x08;
            PORTE.DIRSET=0x10;
            break;
        }
    }
} // setupExpansionHeader()

void stepperMotor1_step(uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON, bool start)
{
    // Enable stepper motor
    PORTJ.OUTCLR=0x10;
    // Turn off LEDs if they were enabled on the previous stepping routine
    if(stepper1rgbOn)
        TLC5947_SetAllHold(0,0x7F00);
    stepper1rgbOn=rgbON;
    // Set direction
    // FORWARD
    if(directionForward)
    {
        PORTJ.OUTCLR=0x04;
        if(rgbON)
        {
            TLC5947_SetMultipleHold(0x0000, 0x0049, 0x2490, 4095, 0x7F00);
        }
    }
    // BACKWARD
    else
    {
        PORTJ.OUTSET=0x04;
        if(rgbON)
        {
            TLC5947_SetMultipleHold(0x0000, 0x0092, 0x4920, 4095, 0x7F00);
        }
    }
    // Set step mode: stepMode={MS3,MS2,MS1} -> {PJ7,PJ6,PJ5}
    // Valid modes: 0,1,2,3,8 (Full,Half,Quarter,Eighth,Sixteenth)
    switch(stepMode)
    {
        case 0:
            PORTJ.OUTCLR=0xE0;
            break;
        case 1:
            PORTJ.OUTSET=0x20;
            PORTJ.OUTCLR=0xC0;
            break;    
        case 2:
            PORTJ.OUTSET=0x40;
            PORTJ.OUTCLR=0xA0;
            break;
        case 3:
            PORTJ.OUTSET=0x60;
            PORTJ.OUTCLR=0x80;
            break;    
        case 8:
            PORTJ.OUTSET=0xE0;
            break;
        default:
            PORTJ.OUTCLR=0xE0;
            break;    
    }

    // Set frequency of STEP signal
    /* Frequency = F_CPU / (2*CLOCK_DIVISION*CCA)        */
    /* where CCA is a 16-bit value with range [0->65535] */
        if(freqHz==0)
            freqHz=1;
    #if F_CPU==32000000UL
        // freqHz=[245,65535]
        if(freqHz>244)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
            TCC0.CCA=F_CPU/(2*1*freqHz);
        }
        // freqHz=[1,3]
        else if(freqHz<4)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
            TCC0.CCA=F_CPU/(2*256*freqHz);
        }
        // freqHz=[4,30]
        else if(freqHz<31)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV64_gc;
            TCC0.CCA=F_CPU/(2*64*freqHz);
        }
        // freqHz=[31,61]
        else if(freqHz<62)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV8_gc;
            TCC0.CCA=F_CPU/(2*8*freqHz);
        }
        // freqHz=[62,122]
        else if(freqHz<123)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV4_gc;
            TCC0.CCA=F_CPU/(2*4*freqHz);
        }
        // freqHz=[123,244]
        else if(freqHz<245)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV2_gc;
            TCC0.CCA=F_CPU/(2*2*freqHz);
        }
    #elif F_CPU==29491200
        // freqHz=[225,65535]
        if(freqHz>224)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
            TCC0.CCA=F_CPU/(2*1*freqHz);
        }
        // freqHz=[1,3]
        else if(freqHz<4)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
            TCC0.CCA=F_CPU/(2*256*freqHz);
        }
        // freqHz=[4,28]
        else if(freqHz<29)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV64_gc;
            TCC0.CCA=F_CPU/(2*64*freqHz);
        }
        // freqHz=[29,56]
        else if(freqHz<57)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV8_gc;
            TCC0.CCA=F_CPU/(2*8*freqHz);
        }
        // freqHz=[57,112]
        else if(freqHz<113)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV4_gc;
            TCC0.CCA=F_CPU/(2*4*freqHz);
        }
        // freqHz=[113,224]
        else if(freqHz<225)
        {
            TCC0.CTRLA=(TCC0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV2_gc;
            TCC0.CCA=F_CPU/(2*2*freqHz);
        }
    #endif

    if(start)
        stepperMotor1_startStepping(nSteps);
} // stepperMotor1_step()

void stepperMotor2_step(uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON, bool start)
{
    // Enable stepper motor
    PORTK.OUTCLR=0x08;
    // Turn off LEDs if they were enabled on the previous stepping routine
    if(stepper2rgbOn)
        TLC5947_SetAllHold(0,0x007F);
    stepper2rgbOn=rgbON;
    // Set direction
    // FORWARD
    if(directionForward)
    {
        PORTK.OUTCLR=0x20;
        if(rgbON)
        {
            TLC5947_SetMultipleHold(0x4924, 0x9000, 0x0000, 4095, 0x007F);
        }
    }
    // BACKWARD
    else
    {
        PORTK.OUTSET=0x20;
        if(rgbON)
        {
            TLC5947_SetMultipleHold(0x9249, 0x2000, 0x0000, 4095, 0x007F);
        }
    }
    // Set step mode: stepMode={MS3,MS2,MS1} -> {PK0,PK1,PK2}
    // Valid modes: 0,1,2,3,8 (Full,Half,Quarter,Eighth,Sixteenth)
    switch(stepMode)
    {
        case 0:
            PORTK.OUTCLR=0x07;
            break;  
        case 1:
            PORTK.OUTSET=0x04;
            PORTK.OUTCLR=0x03;
            break;    
        case 2:
            PORTK.OUTSET=0x02;
            PORTK.OUTCLR=0x05;
            break;
        case 3:
            PORTK.OUTSET=0x06;
            PORTK.OUTCLR=0x01;
            break;    
        case 8:
            PORTK.OUTSET=0x07;
            break;
        default:
            PORTK.OUTCLR=0x07;
            break;    
    }

    // Set frequency of STEP signal
    /* Frequency = F_CPU / (2*CLOCK_DIVISION*CCA)        */
    /* where CCA is a 16-bit value with range [0->65535] */
    if(freqHz==0)
        freqHz=1;
    #if F_CPU==32000000UL
        // freqHz=[245,65535]
        if(freqHz>244)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
            TCE0.CCA=F_CPU/(2*1*freqHz);
        }
        // freqHz=[1,3]
        else if(freqHz<4)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
            TCE0.CCA=F_CPU/(2*256*freqHz);
        }
        // freqHz=[4,30]
        else if(freqHz<31)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV64_gc;
            TCE0.CCA=F_CPU/(2*64*freqHz);
        }
        // freqHz=[31,61]
        else if(freqHz<62)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV8_gc;
            TCE0.CCA=F_CPU/(2*8*freqHz);
        }
        // freqHz=[62,122]
        else if(freqHz<123)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV4_gc;
            TCE0.CCA=F_CPU/(2*4*freqHz);
        }
        // freqHz=[123,244]
        else if(freqHz<245)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV2_gc;
            TCE0.CCA=F_CPU/(2*2*freqHz);
        }
    #elif F_CPU==29491200
        // freqHz=[225,65535]
        if(freqHz>224)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV1_gc;
            TCE0.CCA=F_CPU/(2*1*freqHz);
        }
        // freqHz=[1,3]
        else if(freqHz<4)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
            TCE0.CCA=F_CPU/(2*256*freqHz);
        }
        // freqHz=[4,28]
        else if(freqHz<29)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV64_gc;
            TCE0.CCA=F_CPU/(2*64*freqHz);
        }
        // freqHz=[29,56]
        else if(freqHz<57)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV8_gc;
            TCE0.CCA=F_CPU/(2*8*freqHz);
        }
        // freqHz=[57,112]
        else if(freqHz<113)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV4_gc;
            TCE0.CCA=F_CPU/(2*4*freqHz);
        }
        // freqHz=[113,224]
        else if(freqHz<225)
        {
            TCE0.CTRLA=(TCE0.CTRLA & (~TC0_CLKSEL_gm)) | TC_CLKSEL_DIV2_gc;
            TCE0.CCA=F_CPU/(2*2*freqHz);
        }
    #endif

    if(start)
        stepperMotor2_startStepping(nSteps);
} // stepperMotor2_step()

void stepperMotor1_stopDisable(bool disable)
{
    // Disable output of STEP signal
    TCC0.CTRLB=(TCC0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
                TC_WGMODE_FRQ_gc;
    // Change LEDs to RED if they were ON
    if(stepper1rgbOn)
        TLC5947_SetMultipleHold(0x0000, 0x0024, 0x9248, 4095, 0x7F00);
    // Disable stepper motor
    if(disable)
        PORTJ.OUTSET=0x10;
    else
        PORTJ.OUTCLR=0x10;
    // Set signal low
    if(operatingMode==0)
        PORTB.OUTCLR=0x08;
    // Disable overflow interrupt
    TCC0.INTCTRLA=(TCC0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;
} // stepperMotor1_stopDisable()

void stepperMotor2_stopDisable(bool disable)
{
    // Disable output of STEP signal
    TCE0.CTRLB=(TCE0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
                TC_WGMODE_FRQ_gc;
    // Change LEDs to RED if they were ON
    if(stepper2rgbOn)
        TLC5947_SetMultipleHold(0x2492, 0x4800, 0x0000, 4095, 0x007F);
    // Disable stepper motor
    if(disable)
        PORTK.OUTSET=0x08;
    else
        PORTK.OUTCLR=0x08;
    // Set signal low
    if(operatingMode==0)
        PORTE.OUTCLR=0x10;
    // Disable overflow interrupt
    TCE0.INTCTRLA=(TCE0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;
} // stepperMotor2_stopDisable()

void stepperMotor1_startStepping(uint16_t nSteps)
{
    CPU_SR_ALLOC();

    // Enable output of STEP signal
    TCC0.CTRLB=(TCC0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC0_CCAEN_bm | TC_WGMODE_FRQ_gc;
    // Set signal high
    if(operatingMode==0)
        PORTB.OUTSET=0x08;
    // Set global variables
    CPU_CRITICAL_ENTER();
        // Note: nSteps=0 corresponds to continuous stepping
        stepperMotor1NSteps=nSteps*2;
        // Reset count
        stepperMotor1StepCount=0;
        /* INTERRUPT SETUP */
        // Overflow interrupt: Enabled
        // Error interrupt: Disabled
        TCC0.INTCTRLA=(TCC0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
            TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_LO_gc;
    CPU_CRITICAL_EXIT();
} // stepperMotor1_startStepping()

void stepperMotor2_startStepping(uint16_t nSteps)
{
    CPU_SR_ALLOC();

    // Enable output of STEP signal
    TCE0.CTRLB=(TCE0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
        TC0_CCAEN_bm | TC_WGMODE_FRQ_gc;
    // Set signal high
    if(operatingMode==0)
        PORTE.OUTSET=0x10;
    // Set global variables
    CPU_CRITICAL_ENTER();
        // Note: nSteps=0 corresponds to continuous stepping
        stepperMotor2NSteps=nSteps*2;
        // Reset count
        stepperMotor2StepCount=0;
        /* INTERRUPT SETUP */
        // Overflow interrupt: Enabled
        // Error interrupt: Disabled
        TCE0.INTCTRLA=(TCE0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
            TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_LO_gc;
    CPU_CRITICAL_EXIT();
} // stepperMotor2_startStepping()

bool waitForTWIReady_Timeout(uint32_t count)
{
    // Note: This function implements a crude timeout by incrementing a count variable
    // Reset count
    uint32_t i=0;
    while (twiMaster.status != TWIM_STATUS_READY)
    {
        // Increment count
        i++;
        // Handle timeout
        if(i>count)
            return false;
    }
    return true;
} // waitForTWIReady_Timeout()

void handleTWIError(void)
{
    CPU_SR_ALLOC();

    // Set RED STATUS LED
    PORTH.OUTCLR=0x02;
    PORTH.OUTSET=0x10;
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts("SMBus R/W Error!");
    // Display strings for 1 second
    _delay_ms(1000);
    // Clear LCD
    lcd_clrscr();
    // Display string on LCD
    lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
    // Display strings for 1 second
    _delay_ms(1000);
    // Go to (0,1) on LCD
    lcd_gotoxy(0,1);
    // Display string on LCD
    lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
    // Display strings for 1 second
    _delay_ms(1000);
    // Reset XMEGA
    CPU_CRITICAL_ENTER();
        CPU_CCP=CCP_IOREG_gc; 
        RST.CTRL=RST_SWRST_bm;
    CPU_CRITICAL_EXIT();
} // handleTWIError()

void ReadXBeePacket(void)
{
    // Local variables
    uint8_t data;
    static uint8_t XBeeRXFrame[MAX_XBEE_RXPACKET_LENGTH-4]; // First 4 bytes are NOT saved in XBeeRXFrame buffer
    uint8_t index=0;
    uint8_t checksumTotal=0;
    uint8_t lengthMSB=0;
    uint8_t lengthLSB=0;
    uint16_t packetLength=0;
    uint8_t apiID=0;
    uint16_t SH_high=0;
    uint16_t SH_low=0;
    uint16_t SL_high=0;
    uint16_t SL_low=0;
    uint16_t MY=0;
    bool savePacket=true;
    int responseEBugPacketLength=0;
    static uint8_t responseEBugPacket[MAX_EBUG_PACKET_LENGTH];
    // Point pointer to start of eBug Packet
    uint8_t *ptr=&XBeeRXFrame[11];

    // Save packet from buffer
    for(uint8_t i=0;(i<RX_BUFFER_SIZE_XBEE)&&(savePacket);i++)
    {
        // Get data
        data=getchar_XBee();

        // New packet start before previous packeted completed: discard previous packet and start over
        if((index>0)&&(data==XBEE_START_DELIMITER))
        {
            index=0;
            checksumTotal=0;
        }

        // Checksum includes all bytes starting with API ID
        if(index>=API_ID_INDEX)
            checksumTotal+=data;

        // Read packet
        switch(index)
        {
            // Start Delimiter
            case 0:
                if(data==XBEE_START_DELIMITER)      
                    index++;
                break;
            // Length (MSB)
            case 1:
                lengthMSB=data;
                index++;
                break;
            // Length (LSB)
            case 2:
                lengthLSB=data;
                index++;
                // Save packet length
                packetLength=(lengthMSB<<8)|(lengthLSB);
                break;
            // API ID
            case 3:
                apiID=data;
                index++;
                break;
            default:
                // END OF PACKET: Verify checksum
                // Note: Packet Length = Number of bytes between the length and the checksum
                if(index==(packetLength+3))
                {
                    // Only take action if checksum passes
                    if((checksumTotal&0xFF)==0xFF)
                    {
                        // ZigBee Receive Packet
                        if(apiID==0x90)
                        {
                            // Only handle eBug Packet in NORMAL MODE
                            if(operatingState==NORMAL)
                            {
                                // Handle eBug packet
                                if((responseEBugPacketLength=HandleEBugPacket(ptr,responseEBugPacket))>0)
                                {
                                    // Save address information
                                    SH_high=(XBeeRXFrame[0]<<8)|(XBeeRXFrame[1]);
                                    SH_low=(XBeeRXFrame[2]<<8)|(XBeeRXFrame[3]);
                                    SL_high=(XBeeRXFrame[4]<<8)|(XBeeRXFrame[5]);
                                    SL_low=(XBeeRXFrame[6]<<8)|(XBeeRXFrame[7]);
                                    MY=(XBeeRXFrame[8]<<8)|(XBeeRXFrame[9]);
                                    // Send response packet
                                    SendXBeeTXRequestPacket(SH_high,SH_low,SL_high,SL_low,MY,responseEBugPacket,responseEBugPacketLength);
                                }
                            }
                        }
                        // AT Command Response
                        else if(apiID==0x88)
                        {
                            HandleXBeeATCommandResponse(XBeeRXFrame);
                        }
                        // Modem Status
                        else if(apiID==0x8A)
                        {
                            // Only display messages if in NORMAL MODE
                            if(operatingState==NORMAL)
                            {
                                switch(XBeeRXFrame[0])
                                {
                                    case 0:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[0]),0,1,false,true);
                                        break;
                                    case 1:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[1]),0,1,false,true);
                                        break;
                                    case 2:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[2]),0,1,false,true);
                                        break;
                                    case 3:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[3]),0,1,false,true);
                                        break;
                                    case 6:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[4]),0,1,false,true);
                                        break;
                                    case 7:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[5]),0,1,false,true);
                                        break;
                                    case 0x0D:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[6]),0,1,false,true);
                                        break;
                                    case 0x11:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[7]),0,1,false,true);
                                        break;
                                    default:
                                        lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[8]),0,1,false,true);
                                        break;
                                }
                            }
                        }
                        // Unhandled packet
                        else
                        {
                            // Only display messages if in NORMAL MODE
                            if(operatingState==NORMAL)
                                lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[9]),0,1,false,true);
                        }
                    }
                    // Checksum fail
                    else
                    {
                            // Only display messages if in NORMAL MODE
                            if(operatingState==NORMAL)
                            lcd_puts_ucos((char*)pgm_read_word(&XBeeStringsPtrs[10]),0,1,false,true);
                    }
                    // Break out of for-loop so that function can return
                    savePacket=false;
                }
                // SAVE PACKET: Starting after API ID (i.e. Frame Type)
                else
                {
                    XBeeRXFrame[index-4]=data;
                    index++;
                }
                break;
        }
    }
} // ReadXBeePacket()

void ReadEBugPacket(void)
{
    // Local variables
    uint8_t data;
    static uint8_t EBugPacketRXFrame[MAX_EBUG_PACKET_LENGTH];
    uint8_t index=0;
    uint8_t packetLength=0;
    int responseEBugPacketLength=0;
    static uint8_t responseEBugPacket[MAX_EBUG_PACKET_LENGTH];
    bool savePacket=true;

    // Save packet from buffer
    for(uint8_t i=0;(i<MAX_EBUG_PACKET_LENGTH)&&(savePacket);i++)
    {
        // Get data
        data=getchar_usarte1();

        // New packet start before previous packeted completed: discard previous packet and start over
        if((index>0)&&(data==XBEE_START_DELIMITER))
        {
            index=0;
        }

        // Read and save packet
        switch(index)
        {
            // Start Delimiter
            case 0:
                if(data==XBEE_START_DELIMITER)
                    EBugPacketRXFrame[index++]=data;
                break;
            // Length
            case 1:
                packetLength=data;
                EBugPacketRXFrame[index++]=data;
                break;
            default:
                // SAVE PACKET: Entire packet is saved
                EBugPacketRXFrame[index++]=data;

                // END OF PACKET: Take action
                // Note: Packet Length = Number of bytes between the length and the checksum
                // 3 is added to packetLength as index increments for Start Delimiter (1 byte), Length (1 byte) and Checksum (1 byte)
                if(index==(packetLength+3))
                {
                    // Only handle eBug Packet in NORMAL MODE
                    if(operatingState==NORMAL)
                    {
                        // Handle eBug packet
                        if((responseEBugPacketLength=HandleEBugPacket(EBugPacketRXFrame,responseEBugPacket))>0)
                        {
                            // Send response packet
                            for(uint8_t j=0;j<responseEBugPacketLength;j++)
                                putchar_usarte1(responseEBugPacket[j]);
                        }
                    }
                    // Break out of for-loop so that function can return
                    savePacket=false;
                }
                break;
        }
        
    }
} // ReadEBugPacket()

int HandleEBugPacket(uint8_t *Packet, uint8_t *ResponsePacket)
{
    // Return value:
    // -1: API Command not recognized/executed OR checksum failed.
    //  0: No response required to be sent back to sender.
    // >0: Length of eBug Packet to be sent back to sender.
    
    ///////////////////
    // Packet Format //
    ///////////////////
    // Packet[0]:       Frame Start [XBee=0x00, Expansion Header=0x7E]
    // Packet[1]:       Packet Length (number of bytes between Packet Length and Checksum)
    // Packet[2]:       Options {Checksum (1-bit), Class (4-bits), Reserved (3-bits)}
    // Packet[3]:       Type
    // Packet[4]:       Sub-Type
    // Packet[5+n]:     Payload (n bytes)
    // Packet[5+n+1]:   Checksum [optional: set to 0x00 if unused]

    // Local variables
    uint8_t checksumTotal=0;

    // OPTIONS
    //// Checksum:
    if((Packet[2]&0x80)==0x80)
    {
        for(uint8_t i=0;i<(Packet[1]+1);i++)
            checksumTotal+=Packet[i+2];
        if((checksumTotal&0xFF)!=0xFF)
        {
            lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[0]),0,1,false,true);
            return -1;
        }
    }

    // TYPE
    switch(Packet[3])
    {
        // SYSTEM
        case 0x00:
            // SendData
            if(Packet[4]==0x00)
            {
                // Destination: XBee
                if(Packet[5]==0x00)
                {
                    for(int i=0; i<(Packet[1]-4); i++)
                        putchar_XBee(Packet[6+i]);  
                }
                // Destination: Expansion Header
                else if(Packet[5]==0x01)
                {
                    for(int i=0; i<(Packet[1]-4); i++)
                        putchar_usarte1(Packet[6+i]);
                }                               
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[21]),0,1,false,true);
                return 0;
            }
            break;
        // STEPPERMOTOR
        case 0x01:
        {
            // LeftStep
            if(Packet[4]==0x00)
            {
                bool directionForward = (Packet[9]>>4) & 0x01;
                uint8_t stepMode = Packet[9] & 0x0F;
                bool rgbON = (Packet[9]>>5) & 0x01;

                // Parameters: uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON
                stepperMotor1_step((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8],directionForward,stepMode,rgbON,true);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[2]),0,1,false,true);
                return 0;
            }
            // RightStep
            else if(Packet[4]==0x01)
            {
                bool directionForward = (Packet[9]>>4) & 0x01;
                uint8_t stepMode = Packet[9] & 0x0F;
                bool rgbON = (Packet[9]>>5) & 0x01;

                // Parameters: uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON
                stepperMotor2_step((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8],directionForward,stepMode,rgbON,true);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[3]),0,1,false,true);
                return 0;
            }
            // LeftStop
            else if(Packet[4]==0x02)
            {
                // Parameters: bool disable
                stepperMotor1_stopDisable(Packet[5]);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[4]),0,1,false,true);
                return 0;
            }
            // RightStop
            else if(Packet[4]==0x03)
            {
                // Parameters: bool disable
                stepperMotor2_stopDisable(Packet[5]);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[5]),0,1,false,true);
                return 0;
            }
            // LeftRightStop
            else if(Packet[4]==0x04)
            {
                // Parameters: bool disable
                stepperMotor1_stopDisable(Packet[5]);
                stepperMotor2_stopDisable(Packet[6]);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[6]),0,1,false,true);
                return 0;
            }
            // LeftRightStep
            else if(Packet[4]==0x05)
            {   
                bool directionForward1 = (Packet[9]>>4) & 0x01;
                uint8_t stepMode1 = Packet[9] & 0x0F;
                bool rgbON1 = (Packet[9]>>5) & 0x01;
                uint16_t nSteps1 = (Packet[7]<<8)|Packet[8];

                bool directionForward2 = (Packet[14]>>4) & 0x01;
                uint8_t stepMode2 = Packet[14] & 0x0F;
                bool rgbON2 = (Packet[14]>>5) & 0x01;
                uint16_t nSteps2 = (Packet[12]<<8)|Packet[13];

                // Parameters: uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON
                stepperMotor1_step((Packet[5]<<8)|Packet[6],nSteps1,directionForward1,stepMode1,rgbON1,false);
                // Parameters: uint16_t freqHz, uint16_t nSteps, bool directionForward, uint8_t stepMode, bool rgbON
                stepperMotor2_step((Packet[10]<<8)|Packet[11],nSteps2,directionForward2,stepMode2,rgbON2,false);
                
                stepperMotor1_startStepping(nSteps1);
                stepperMotor2_startStepping(nSteps2);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[7]),0,1,false,true);
                return 0;
            }
            break;
        }
        // RGBLEDs
        case 0x02:
        {
            // SetAllRed
            if(Packet[4]==0x00)
            {
                TLC5947_SetAllRed((Packet[5]<<8)|Packet[6]);

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[8]),0,1,false,true);
                return 0;
            }
            // SetAllGreen
            else if(Packet[4]==0x01)
            {
                TLC5947_SetAllGreen((Packet[5]<<8)|Packet[6]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[9]),0,1,false,true);
                return 0;
            }         
            // SetAllBlue
            else if(Packet[4]==0x02)
            {
                TLC5947_SetAllBlue((Packet[5]<<8)|Packet[6]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[10]),0,1,false,true);
                return 0;
            }
            // SetAll
            else if(Packet[4]==0x03)
            {
                TLC5947_SetAll((Packet[5]<<8)|Packet[6]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[11]),0,1,false,true);
                return 0;
            }
            // SetAllOff
            else if(Packet[4]==0x04)
            {
                TLC5947_SetAllOff();
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[12]),0,1,false,true);
                return 0;
            }
            // SetRed
            else if(Packet[4]==0x05)
            {
                TLC5947_SetRed(Packet[5], (Packet[6]<<8)|Packet[7]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[13]),0,1,false,true);
                return 0;
            }
            // SetGreen
            else if(Packet[4]==0x06)
            {
                TLC5947_SetGreen(Packet[5], (Packet[6]<<8)|Packet[7]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[14]),0,1,false,true);
                return 0;
            }
            // SetBlue
            else if(Packet[4]==0x07)
            {
                TLC5947_SetBlue(Packet[5], (Packet[6]<<8)|Packet[7]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[15]),0,1,false,true);
                return 0;
            }
            // SetMultiple
            else if(Packet[4]==0x08)
            {
                TLC5947_SetMultiple((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8],(Packet[9]<<8)|Packet[10],(Packet[11]<<8)|Packet[12]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[16]),0,1,false,true);
                return 0;
            }
            // SetMultipleHold
            else if(Packet[4]==0x09)
            {
                TLC5947_SetMultipleHold((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8],(Packet[9]<<8)|Packet[10],(Packet[11]<<8)|Packet[12],(Packet[13]<<8)|Packet[14]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[17]),0,1,false,true);
                return 0;
            }
            // SetAllHold
            else if(Packet[4]==0x0A)
            {
                TLC5947_SetAllHold((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[18]),0,1,false,true);
                return 0;
            }
            // SetAllRGB
            else if(Packet[4]==0x0B)
            {
                TLC5947_SetAllRGB((Packet[5]<<8)|Packet[6],(Packet[7]<<8)|Packet[8],(Packet[9]<<8)|Packet[10]);
                
                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[19]),0,1,false,true);
                return 0;
            }                 
            break;
        }
        // ADC
        case 0x03:
        {
            // LM35GetTemperature
            if(Packet[4]==0x00)
            {
                uint16_t value=adcb_read(0,1);
                // Create header: Copy certain header fields from eBug Packet
                ResponsePacket[0]=Packet[0];
                ResponsePacket[1]=5; // Length (5 bytes)
                ResponsePacket[2]=(Packet[2]&0x80)|(0x1<<3)|(Packet[2]&0x07); // Options: Class=0x1 [eBug Response]
                ResponsePacket[3]=Packet[3];
                ResponsePacket[4]=Packet[4];
                // Payload (2 bytes)
                ResponsePacket[5]=(value>>8)&0xFF;
                ResponsePacket[6]=(value)&0xFF;
                // Checksum
                if((ResponsePacket[2]&0x80)==0x80)
                    ResponsePacket[7]=CalculateEBugPacketChecksum(ResponsePacket);
                else
                    ResponsePacket[7]=0x00;

                lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[20]),0,1,false,true);
                return 8;
            }
            break;
        }
        // ADD OTHER API FUNCTIONS HERE
    }

    lcd_puts_ucos((char*)pgm_read_word(&APIStringsPtrs[1]),0,1,false,true);
    return -1;
} // HandleEBugPacket()

void HandleXBeeATCommandResponse(uint8_t *XBeeRXFrame)
{
    // NODE DISCOVERY
    if((XBeeRXFrame[1]==0x4e)&&(XBeeRXFrame[2]==0x44))
    {
        // Local variables
        uint8_t n=0;
        // Get mutex
        OSMutexPend(&XBeeRXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
        // Only save data if XBeeNodeTableIndex value is valid (save data for MAX_NODES number of nodes only)
        if(XBeeNodeTableIndex<MAX_NODES)
        {
            // Save command status in xBeeNodeTable structure regardless of value
            xBeeNodeTable[XBeeNodeTableIndex].commandStatus=XBeeRXFrame[3];
            // Command Status
            switch(XBeeRXFrame[3])
            {
                // OK
                case 0x00:
                    // Save node addresses in xBeeNodeTable structure
                    xBeeNodeTable[XBeeNodeTableIndex].MY=(XBeeRXFrame[4]<<8)|XBeeRXFrame[5];
                    xBeeNodeTable[XBeeNodeTableIndex].SH_high=(XBeeRXFrame[6]<<8)|XBeeRXFrame[7];
                    xBeeNodeTable[XBeeNodeTableIndex].SH_low=(XBeeRXFrame[8]<<8)|XBeeRXFrame[9];
                    xBeeNodeTable[XBeeNodeTableIndex].SL_high=(XBeeRXFrame[10]<<8)|XBeeRXFrame[11];
                    xBeeNodeTable[XBeeNodeTableIndex].SL_low=(XBeeRXFrame[12]<<8)|XBeeRXFrame[13];
                    // Save NI string in xBeeNodeTable structure: [Default: (n=1) XBeeRXFrame[14]=0x20, XBeeRXFrame[15]=0x00]
                    while(XBeeRXFrame[14+n]!=0x00)
                    {
                        xBeeNodeTable[XBeeNodeTableIndex].NI[n]=XBeeRXFrame[14+n];
                        n++;
                    }
                    // Save remaining data in xBeeNodeTable structure
                    xBeeNodeTable[XBeeNodeTableIndex].parentNetworkAddress=(XBeeRXFrame[14+n+1]<<8)|XBeeRXFrame[14+n+2];
                    xBeeNodeTable[XBeeNodeTableIndex].deviceType=XBeeRXFrame[14+n+3];
                    xBeeNodeTable[XBeeNodeTableIndex].status=XBeeRXFrame[14+n+4];
                    xBeeNodeTable[XBeeNodeTableIndex].profileID=(XBeeRXFrame[14+n+5]<<8)|XBeeRXFrame[14+n+6];
                    xBeeNodeTable[XBeeNodeTableIndex].manufacturerID=(XBeeRXFrame[14+n+7]<<8)|XBeeRXFrame[14+n+8];
                    break;
                // ERROR
                case 0x01:
                    break;
                // INVALID COMMAND
                case 0x02:
                    break;
                // INVALID PARAMETER
                case 0x03: 
                    break;
                // TX FAILURE
                case 0x04: 
                    break;
            }
            // Increment index
            XBeeNodeTableIndex++;
            // Release mutex
            OSMutexPost(&XBeeRXDataProtect,OS_OPT_POST_NONE,&err);
        }
    }
} // HandleXBeeATCommandResponse()

void SendXBeeATCommand_ND(void)
{
    // Get mutex
    OSMutexPend(&XBeeRXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
    // Reset index
    XBeeNodeTableIndex=0;
    // Release mutex
    OSMutexPost(&XBeeRXDataProtect,OS_OPT_POST_NONE,&err);
    // Get mutex
    OSMutexPend(&XBeeTXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
    // Create packet
    XBeeTXData[0]=0x7e; // Delimiter
    XBeeTXData[1]=0x00; // Length (MSB)
    XBeeTXData[2]=0x04; // Length (LSB)
    XBeeTXData[3]=0x08; // AT Command API frame type
    XBeeTXData[4]=0x01; // Frame ID (set to non-zero value)
    XBeeTXData[5]=0x4e; // AT command ('N')
    XBeeTXData[6]=0x44; // AT command ('D')
    // Calculate checksum and save in TX data array
    XBeeTXData[7]=CalculateXBeePacketChecksum(XBeeTXData);
    // Release mutex
    OSMutexPost(&XBeeTXDataProtect,OS_OPT_POST_NONE,&err);
    // Post semaphore
    OSTaskSemPost(&AppTaskTCB[XBEE_TX_PACKET_HANDLER_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
} // SendXBeeATCommand_ND()

void SendXBeeTXRequestPacket(uint16_t SH_high, uint16_t SH_low, uint16_t SL_high, uint16_t SL_low, uint16_t MY, uint8_t *payload, int payloadLength)
{
    // Get mutex
    OSMutexPend(&XBeeTXDataProtect,0,OS_OPT_PEND_BLOCKING,NULL,&err);
    // Create packet
    XBeeTXData[0]=0x7e; // Delimiter
    // LENGTH {MSB,LSB]
    if((14+payloadLength)>255)
    {
        XBeeTXData[1]=((14+payloadLength)>>8)&0xFF;
        XBeeTXData[2]=(14+payloadLength)&0xFF;
    }
    else
    {
        XBeeTXData[1]=0x00;
        XBeeTXData[2]=14+payloadLength;
    }
    XBeeTXData[3]=0x10; // AT Command API frame type
    XBeeTXData[4]=0x00; // Frame ID (set to zero: no Transmit Status packet sent from destination)
    // Serial Number (64-bit destination address)
    XBeeTXData[5]=(SH_high>>8)&0xFF;
    XBeeTXData[6]=(SH_high)&0xFF;
    XBeeTXData[7]=(SH_low>>8)&0xFF;
    XBeeTXData[8]=(SH_low)&0xFF;
    XBeeTXData[9]=(SL_high>>8)&0xFF;
    XBeeTXData[10]=(SL_high)&0xFF;
    XBeeTXData[11]=(SL_low>>8)&0xFF;
    XBeeTXData[12]=(SL_low)&0xFF;
    // Network Address (16-bit)
    XBeeTXData[13]=(MY>>8)&0xFF;
    XBeeTXData[14]=(MY)&0xFF;
    // Broadcast Radius
    XBeeTXData[15]=0x00;
    // Options
    XBeeTXData[16]=0x00;
    // Payload
    for(uint8_t i=0;i<(payloadLength);i++)
        XBeeTXData[17+i]=payload[i];
    // Calculate checksum and save in TX data array
    XBeeTXData[17+payloadLength]=CalculateXBeePacketChecksum(XBeeTXData);
    // Release mutex
    OSMutexPost(&XBeeTXDataProtect,OS_OPT_POST_NONE,&err);
    // Post semaphore
    OSTaskSemPost(&AppTaskTCB[XBEE_TX_PACKET_HANDLER_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
} // SendXBeeTXRequestPacket()

uint8_t CalculateXBeePacketChecksum(uint8_t *PacketIn)
{
    // Local variables
    uint16_t packetLength;
    uint8_t checksumTotal=0;
    // Save packet length in local 16-bit variable
    packetLength=(PacketIn[1]<<8)+(PacketIn[2]);
    // Calculate checksum
    for(uint8_t i=0;i<packetLength;i++)
        checksumTotal+=PacketIn[3+i];
    // Return checksum value
    return (0xFF-checksumTotal);
} // CalculateXBeePacketChecksum()

uint8_t CalculateEBugPacketChecksum(uint8_t *PacketIn)
{
    // Local variables
    int packetLength;
    int checksumTotal=0;
    // Save packet length
    packetLength=PacketIn[1];
    // Calculate checksum (covers from Options to end of Payload)
    for(uint8_t i=0;i<packetLength;i++)
        checksumTotal+=PacketIn[2+i];
    // Return checksum value
    return (0xFF-checksumTotal);
} // CalculateEBugPacketChecksum()

void enableIRReceiversOdd(bool en, bool isOdd)
{
    // Local variables
    uint8_t mask;

    // Set mask
    if(isOdd)
        mask=0x55;
    else
        mask=0xAA;

    // ENABLE
    if(en)
    {
        /* Set as INPUTS */
        // IR Pairs 1,2;3,4;5,6
        PORTC.DIRCLR=(mask<<1)&0x0E;
        // IR Pairs 7,8;9,10;11,12
        PORTE.DIRCLR=(mask>>2)&0x0E;
        // IR Pairs 13,14;15,16
        PORTF.DIRCLR=(mask>>2)&0x30;

        // Enable ODD IR Receivers (1,3,5,7)
        if(isOdd)
        {
            // Enable Capture Channel interrupts
            TCD0.INTCTRLB = TC_CCCINTLVL_LO_gc;
            TCD1.INTCTRLB = TC_CCAINTLVL_LO_gc;
            TCF0.INTCTRLB = TC_CCAINTLVL_LO_gc | TC_CCCINTLVL_LO_gc;

            // Event System Channel 2 source: Port D, Pin2
            EVSYS.CH2MUX=EVSYS_CHMUX_PORTD_PIN2_gc;
            // Event System Channel 3 source: Port D, Pin4
            EVSYS.CH3MUX=EVSYS_CHMUX_PORTD_PIN4_gc;
            // Event System Channel 4 source: Port F, Pin0
            EVSYS.CH4MUX=EVSYS_CHMUX_PORTF_PIN0_gc;
            // Event System Channel 5 source: OFF
            EVSYS.CH5MUX=EVSYS_CHMUX_OFF_gc;
            // Event System Channel 6 source: Port F, Pin2
            EVSYS.CH6MUX=EVSYS_CHMUX_PORTF_PIN2_gc;
            // Event System Channel 7 source: OFF
            EVSYS.CH7MUX=EVSYS_CHMUX_OFF_gc;
        }
        // Enable EVEN IR Receivers (2,4,6,8)
        else
        {
            // Enable Capture Channel interrupts
            TCD0.INTCTRLB = TC_CCDINTLVL_LO_gc;
            TCD1.INTCTRLB = TC_CCBINTLVL_LO_gc;
            TCF0.INTCTRLB = TC_CCBINTLVL_LO_gc | TC_CCDINTLVL_LO_gc;

            // Event System Channel 2 source: OFF
            EVSYS.CH2MUX=EVSYS_CHMUX_OFF_gc;
            // Event System Channel 3 source: Port D, Pin3
            EVSYS.CH3MUX=EVSYS_CHMUX_PORTD_PIN3_gc;
            // Event System Channel 4 source: Port D, Pin5
            EVSYS.CH4MUX=EVSYS_CHMUX_PORTD_PIN5_gc;
            // Event System Channel 5 source: Port F, Pin1
            EVSYS.CH5MUX=EVSYS_CHMUX_PORTF_PIN1_gc;
            // Event System Channel 6 source: OFF
            EVSYS.CH6MUX=EVSYS_CHMUX_OFF_gc;
            // Event System Channel 7 source: Port F, Pin3
            EVSYS.CH7MUX=EVSYS_CHMUX_PORTF_PIN3_gc;
        }
    }
    // DISABLE
    else
    {
        /* Set as OUTPUTS */
        // IR Pairs 1,2;3,4;5,6
        PORTC.DIRSET=(mask<<1)&0x0E;
        // IR Pairs 7,8;9,10;11,12
        PORTE.DIRSET=(mask>>2)&0x0E;
        // IR Pairs 13,14;15,16
        PORTF.DIRSET=(mask>>2)&0x30;
    }
} // enableIRReceiversOdd()

void XBee_resetRXbuffer(bool intdis)
{
    CPU_SR_ALLOC();
    if(intdis)
        CPU_CRITICAL_ENTER();
    // Reset global variables
    rx_wr_index_XBee=0;
    rx_rd_index_XBee=0;
    rx_counter_XBee=0;
    if(intdis)
        CPU_CRITICAL_EXIT();
} // XBee_resetRXbuffer()

void usarte1_resetRXbuffer(bool intdis)
{
    CPU_SR_ALLOC();
    if(intdis)
        CPU_CRITICAL_ENTER();
    // Reset global variables
    rx_wr_index_usarte1=0;
    rx_rd_index_usarte1=0;
    rx_counter_usarte1=0;
    if(intdis)
        CPU_CRITICAL_EXIT();
} // usarte1_resetRXbuffer()

void checkForAndHandleGGPermanentFailure(void)
{
    // Local variables
    char lcdString[17];
    uint8_t byte0;
    uint8_t byte1;
    uint16_t pfStatus=0;
    uint16_t pfStatus2=0;
    bool doGGPFClear=false;
    CPU_SR_ALLOC();
    
    // PFStatus [0x53]
    sendBuffer[0]=0x53;
    if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
        handleTWIError();
    byte0 = twiMaster.readData[0]; 
    byte1 = twiMaster.readData[1];
    pfStatus=(byte1<<8)|byte0;
    
    // PFStatus2 [0x6b]
    sendBuffer[0]=0x6b;
    if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
        handleTWIError();
    byte0 = twiMaster.readData[0]; 
    byte1 = twiMaster.readData[1];
    pfStatus2=(byte1<<8)|byte0; 
    
    // Check for permanent failure(s)
    if(pfStatus!=0x0000 || pfStatus2!=0x0000)
    {
        // Set RED STATUS LED
        PORTH.OUTCLR=0x02;
        PORTH.OUTSET=0x10;
        /* Display message on LCD */
        // Clear top line
        LCD_ClearLineAndSetXPos(0,0);
        // Display string on LCD
        lcd_puts_p((char*)pgm_read_word(&LiPoStringsPtrs[2]));
        // Show permanent failure error codes on LCD
        if(pfStatus!=0x0000)
        {
            // Create string
            sprintf(lcdString,"PFStatus=0x%04x",pfStatus);
            // Clear bottom line
            LCD_ClearLineAndSetXPos(1,0);
            // Display string on LCD
            lcd_puts(lcdString);
            // Display strings for 3 seconds
            _delay_ms(3000);
        }
        if(pfStatus2!=0x0000)
        {
            // Create string
            sprintf(lcdString,"PFStatus2=0x%04x",pfStatus2);
            // Clear bottom line
            LCD_ClearLineAndSetXPos(1,0);
            // Display string on LCD
            lcd_puts(lcdString);
            // Display strings for 3 seconds
            _delay_ms(3000);
            // Clear LCD
            lcd_clrscr();
        }
        // Clear RED LED
        PORTH.OUTCLR=0x10;
        // Enable interrupts on TEST switch
        //PORTK.INT0MASK=0x80;    
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[0]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[1]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Go to (0,1) on LCD
        lcd_gotoxy(0,1);
        // Display string on LCD
        lcd_puts_p((char*) pgm_read_word(&ResetCountdownStringsPtrs[2]));
        // Display strings for 1 second
        _delay_ms(1000);
        // Check if TEST button is held down (clear PF flags if so)
        if(!(PORTK.IN & 0x80))
        {
            doGGPFClear=true;
            // Go to (0,1) on LCD
            lcd_gotoxy(0,1);
            // Show string on LCD
            lcd_puts_p((char*) pgm_read_word(&LiPoStringsPtrs[3]));
            // Display until button is released
            while(!(PORTK.IN & 0x80));
        }
        // Clear permanent failure flags before resetting
        if(doGGPFClear)
            clearGGPermanentFailure(true);
        // Reset XMEGA
        CPU_CRITICAL_ENTER();
            CPU_CCP=CCP_IOREG_gc; 
            RST.CTRL=RST_SWRST_bm;
        CPU_CRITICAL_EXIT();
    }
} // checkForAndHandleGGPermanentFailure()

void clearGGPermanentFailure(bool doReset)
{
    // Local variables
    uint8_t byte0;
    uint8_t byte1;
    uint8_t byte2;
    uint8_t byte3;
    uint8_t byte4;
    
    // PFKey [0x62]
    sendBuffer[0]=0x62;
    if(!TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,5)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
        handleTWIError();
    byte0 = twiMaster.readData[0]; // Number of bytes in block read
    byte1 = twiMaster.readData[1];
    byte2 = twiMaster.readData[2];
    byte3 = twiMaster.readData[3];
    byte4 = twiMaster.readData[4];

    // Reset PF flags using PFKey (first word)
    sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
    sendBuffer[1]=byte1;
    sendBuffer[2]=byte2;
    if(!TWI_MasterWrite(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
        handleTWIError();
    // Reset PF flags using PFKey (second word)
    sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
    sendBuffer[1]=byte3;
    sendBuffer[2]=byte4;
    if(!TWI_MasterWrite(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
        handleTWIError();
        
    // Perform gas gauge reset
    if(doReset)
    {
        sendBuffer[0]=0x00; // ManufacturerAccess [0x00]
        sendBuffer[1]=0x41; // lower byte of command
        sendBuffer[2]=0x00; // upper byte of command
        if(!TWI_MasterWrite(&twiMaster,BQ20Z65_WRITE>>1,sendBuffer,3)||!waitForTWIReady_Timeout(TWI_READY_TIMEOUT))
            handleTWIError();
    }
} // clearGGPermanentFailure()

void setIREmitters(uint16_t mask, bool enable)
{   
    // PF4=nG
    // PC1=SRCK
    // PC2=SER IN
    // PC3=RCK
    // PF5=nSRCLR
    
    // LSB of MASK is OUTPUT 0 of Shift Register 1 (i.e. first register in cascade)
    // MSB of MASK is OUTPUT 7 of Shift Register 2 (i.e. last register in cascade)
        
    // Local variables
    int count=0;
    
    // Invert bits to disable IR emitters in mask
    if(!enable)
        mask^=0xFFFF;

    // Put 16 bits on SER IN line (MSB first)
    while(count < 16)
    {
        if(mask & (0x8000 >> count))
        {
            // SER IN=1
            PORTC.OUTSET=0x04;
            // Pulse SRCK
            PORTC.OUTCLR=0x02;
            PORTC.OUTSET=0x02;
            PORTC.OUTCLR=0x02;
        }
        else
        {
            // SER IN=0
            PORTC.OUTCLR=0x04;
            // Pulse SRCK
            PORTC.OUTCLR=0x02;
            PORTC.OUTSET=0x02;
            PORTC.OUTCLR=0x02;
        }
        count++;
    }

    // Pulse RCK to transfer data to output buffers
    PORTC.OUTCLR=0x08;
    PORTC.OUTSET=0x08;
    PORTC.OUTCLR=0x08;
}

/*
*********************************************************************************************************
*                                       SETUP THE uC/OS-III TICK TIMER
*********************************************************************************************************
*/

void uCTickTimerStart(void)
{
    // Local variables
    unsigned char s;
    uint32_t  clk_per_freq;
    uint32_t  period;
    // Save interrupts enabled/disabled state
    s=SREG;
    // Disable interrupts
    asm("cli");
    // Disable and reset the timer/counter just to be sure
    tc1_disable(&TCC1);
    // Clock source: Peripheral Clock/256
    TCC1.CTRLA=(TCC1.CTRLA & (~TC1_CLKSEL_gm)) | TC_CLKSEL_DIV256_gc;
    // Mode: Normal Operation, Overflow Int./Event on TOP
    // Compare/Capture on channel A: Off
    // Compare/Capture on channel B: Off
    TCC1.CTRLB=(TCC1.CTRLB & (~(TC1_CCAEN_bm | TC1_CCBEN_bm | TC1_WGMODE_gm))) |
        TC_WGMODE_NORMAL_gc;
    // Capture event source: None
    // Capture event action: None
    TCC1.CTRLD=(TCC1.CTRLD & (~(TC1_EVACT_gm | TC1_EVSEL_gm))) |
        TC_EVACT_OFF_gc | TC_EVSEL_OFF_gc;
    // Overflow interrupt: High Level
    // Error interrupt: Disabled
    TCC1.INTCTRLA=(TCC1.INTCTRLA & (~(TC1_ERRINTLVL_gm | TC1_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_HI_gc;
    // Compare/Capture channel A interrupt: Disabled
    // Compare/Capture channel B interrupt: Disabled
    TCC1.INTCTRLB=(TCC1.INTCTRLB & (~(TC1_CCBINTLVL_gm | TC1_CCAINTLVL_gm))) |
        TC_CCBINTLVL_OFF_gc | TC_CCAINTLVL_OFF_gc;
    // High resolution extension: Off
    HIRESC.CTRLA&= ~HIRES_HREN1_bm;
    // Clear the interrupt flags
    TCC1.INTFLAGS=TCC1.INTFLAGS;
    // Set counter register
    TCC1.CNT=0x0000;
    // Calculate period
    // Note: The following equation to calculate the period assumes prescaler=CLK/256
    clk_per_freq   =  F_CPU;
    period         =  (uint32_t)(clk_per_freq)
                   /  (uint32_t)(256 * (uint32_t)(OS_CFG_TICK_RATE_HZ));
    // Set timer period register (maximum value=(2^16)-1=65535)
    TCC1.PER=(uint16_t)(period-1);
    // Set channel A Compare/Capture register
    TCC1.CCA=0x0000;
    // Set channel B Compare/Capture register
    TCC1.CCB=0x0000;
    // Restore interrupts enabled/disabled state
    SREG=s;
} // uCTickTimerStart()

/*
*********************************************************************************************************
*                                              ISR HANDLERS
*
*
* Note(s):  1)  Below are the ISR handlers written in C - these handlers are initially called by
*               handlers written in assembly located in isr.s. It is those handlers which perform the
*               register saving and restoring.
*           2)  See below for instructions on how to create new interrupts.
*
* Creating a new interrupt (for example, named TestISR):
*           1)  Add the new interrupt (TestISR) to the interrupt vector table in app_vect.s at the
*               appropriate interrupt source location (address).
*           2)  Create an external reference to TestISR in app_vect.s: .extern   TestISR
*           3)  Add the assembly ISR handler to isr.s - use an existing ISR as a template.
*               The following changes need to be made:
*               -   Within the section of the ISR handler which saves the registers, change the BRNE
*                   instruction to branch to TestISR_1. Also change the label of this section to TestISR.
*               -   Within the section of the ISR handler which calls the C-based ISR handler and
*                   restores the registers, change the CALL instruction to call the appropriate C-based
*                   handler which will be added below in Step 4. Also change the label of this section to
*                   TestISR_1.
*               -   Create the following references near the beginning of the file (isr.s):
*                       .global  TestISR
*                       .extern  TestISRHandler
*           4)  Add the ISR handler in C below (ebug.c).
*           5)  Ensure the appropriate XMEGA registers are configured for the interrupt source.
*
*********************************************************************************************************
*/

////////////////////
// USARTD1 RX ISR //
////////////////////
void usartd1_rx_isr_handler(void)
{
    // Local variables
    unsigned char status;
    char data;
    uint16_t rx_wr_index_XBee_temp;
    // Static variables
    static uint16_t startXBeePacketBufferIndex=0;
    static uint8_t XBeePacketLengthMSB=0;
    static uint8_t XBeePacketLengthLSB=0;
    static uint16_t XBeePacketLength=0;
    static bool frameStartDetected=false;

    // Get byte from USART
    status=USARTD1.STATUS;
    data=USARTD1.DATA;

    // Check for errors before saving byte into buffer
    if ((status & (USART_FERR_bm | USART_PERR_bm | USART_BUFOVF_bm)) == 0)
    {
        // Check for frame start
        if(data==XBEE_START_DELIMITER)
        {
            // New packet start received before previous packet was posted, reset buffer variables
            if(frameStartDetected)
            {
                rx_rd_index_XBee=rx_wr_index_XBee;
                rx_counter_XBee=0;
            }
            startXBeePacketBufferIndex=rx_wr_index_XBee;
            frameStartDetected=true;
        }
        // Save length (MSB)
        else if(rx_wr_index_XBee==(startXBeePacketBufferIndex+1))
            XBeePacketLengthMSB=data;
        // Save length (LSB)
        else if(rx_wr_index_XBee==(startXBeePacketBufferIndex+2))
        {
            XBeePacketLengthLSB=data;
            // Store length in 16-bit variable
            XBeePacketLength=(XBeePacketLengthMSB<<8)|(XBeePacketLengthLSB);
        }

        // Save received data into buffer
        if(frameStartDetected)
        {
            rx_buffer_XBee[rx_wr_index_XBee++]=data;

            // Check for buffer overflow, reset buffer
            if (++rx_counter_XBee == RX_BUFFER_SIZE_XBEE)
            {
                // Reset buffer to recover
                XBee_resetRXbuffer(false);  
            }
            // Reset buffer index if at end (i.e. buffer wraps around)
            if (rx_wr_index_XBee == RX_BUFFER_SIZE_XBEE)
                rx_wr_index_XBee=0;

            // Adjust index
            if(rx_wr_index_XBee<startXBeePacketBufferIndex)
                rx_wr_index_XBee_temp = rx_wr_index_XBee + RX_BUFFER_SIZE_XBEE;
            else
                rx_wr_index_XBee_temp = rx_wr_index_XBee;
            // Post semaphore when ENTIRE packet has been received
            if((rx_wr_index_XBee_temp-startXBeePacketBufferIndex) == (XBeePacketLength+4))
            {
                XBeeRXCount++;
                frameStartDetected=false;
                OSTaskSemPost(&AppTaskTCB[XBEE_RX_PACKET_HANDLER_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
            }
        }
    }
}

////////////////////
// USARTE0 RX ISR //
////////////////////
void usarte0_rx_isr_handler(void)
{
    // Local variables
    unsigned char status;
    char data;
    uint16_t rx_wr_index_XBee_temp;
    // Static variables
    static uint16_t startXBeePacketBufferIndex=0;
    static uint8_t XBeePacketLengthMSB=0;
    static uint8_t XBeePacketLengthLSB=0;
    static uint16_t XBeePacketLength=0;
    static bool frameStartDetected=false;

    // Get byte from USART
    status=USARTE0.STATUS;
    data=USARTE0.DATA;

    // Check for errors before saving byte into buffer
    if ((status & (USART_FERR_bm | USART_PERR_bm | USART_BUFOVF_bm)) == 0)
    {
        // Check for frame start
        if(data==XBEE_START_DELIMITER)
        {
            // New packet start received before previous packet was posted, reset buffer variables
            if(frameStartDetected)
            {
                rx_rd_index_XBee=rx_wr_index_XBee;
                rx_counter_XBee=0;
            }
            startXBeePacketBufferIndex=rx_wr_index_XBee;
            frameStartDetected=true;
        }
        // Save length (MSB)
        else if(rx_wr_index_XBee==(startXBeePacketBufferIndex+1))
            XBeePacketLengthMSB=data;
        // Save length (LSB)
        else if(rx_wr_index_XBee==(startXBeePacketBufferIndex+2))
        {
            XBeePacketLengthLSB=data;
            // Store length in 16-bit variable
            XBeePacketLength=(XBeePacketLengthMSB<<8)|(XBeePacketLengthLSB);
        }

        // Save received data into buffer
        if(frameStartDetected)
        {
            rx_buffer_XBee[rx_wr_index_XBee++]=data;

            // Check for buffer overflow, reset buffer
            if (++rx_counter_XBee == RX_BUFFER_SIZE_XBEE)
            {
                // Reset buffer to recover
                XBee_resetRXbuffer(false);
            }
            // Reset buffer index if at end (i.e. buffer wraps around)
            if (rx_wr_index_XBee == RX_BUFFER_SIZE_XBEE)
                rx_wr_index_XBee=0;

            // Adjust index
            if(rx_wr_index_XBee<startXBeePacketBufferIndex)
                rx_wr_index_XBee_temp = rx_wr_index_XBee + RX_BUFFER_SIZE_XBEE;
            else
                rx_wr_index_XBee_temp = rx_wr_index_XBee;
            // Post semaphore when ENTIRE packet has been received
            if((rx_wr_index_XBee_temp-startXBeePacketBufferIndex) == (XBeePacketLength+4))
            {
                XBeeRXCount++;
                frameStartDetected=false;
                OSTaskSemPost(&AppTaskTCB[XBEE_RX_PACKET_HANDLER_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
            }
        }
    }
}

///////////////////////////////////////////////
// Timer/counter TCC0 Overflow/Underflow ISR //
///////////////////////////////////////////////
void tcc0_overflow_isr_handler(void)
{
    // Increment count
    stepperMotor1StepCount++;
    // Check if we have stepped the desired number of times
    // Note: stepperMotor1NSteps=0 indicates that the motor steps continuously
    if((stepperMotor1StepCount==stepperMotor1NSteps)&&(stepperMotor1NSteps!=0))
    {
        // Disable output of STEP signal
        TCC0.CTRLB=(TCC0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
                    TC_WGMODE_FRQ_gc;
        // Disable stepper motor
        PORTJ.OUTSET=0x10;
        // Set signal low
        if(operatingMode==0)
            PORTB.OUTCLR=0x08;
        // Disable overflow interrupt
        TCC0.INTCTRLA=(TCC0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;
        // Turn OFF LEDs if they were ON
        if(stepper1rgbOn)
            TLC5947_SetAllHold(0,0x7F00);
        // Reset count
        stepperMotor1StepCount=0;
    }
} // tcc0_overflow_isr_handler()

///////////////////////////////////////////////
// Timer/counter TCE0 Overflow/Underflow ISR //
///////////////////////////////////////////////
void tce0_overflow_isr_handler(void)
{
    // Increment count
    stepperMotor2StepCount++;
    // Check if we have stepped the desired number of times
    // Note: stepperMotor2NSteps=0 indicates that the motor steps continuously
    if((stepperMotor2StepCount==stepperMotor2NSteps)&&(stepperMotor2NSteps!=0))
    {
        // Disable output of STEP signal
        TCE0.CTRLB=(TCE0.CTRLB & (~(TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm | TC0_WGMODE_gm))) |
                    TC_WGMODE_FRQ_gc;
        // Disable stepper motor
        PORTK.OUTSET=0x08;
        // Set signal low
        if(operatingMode==0)
            PORTE.OUTCLR=0x10;
        // Disable overflow interrupt
        TCE0.INTCTRLA=(TCE0.INTCTRLA & (~(TC0_ERRINTLVL_gm | TC0_OVFINTLVL_gm))) |
        TC_ERRINTLVL_OFF_gc | TC_OVFINTLVL_OFF_gc;
        // Turn OFF LEDs if they were ON
        if(stepper2rgbOn)
            TLC5947_SetAllHold(0,0x007F);
        // Reset count
        stepperMotor2StepCount=0;
    }
} // tce0_overflow_isr_handler()

/////////////////
// DMA CH0 ISR //
/////////////////
void dma_ch0_isr_handler(void)
{
    // ERROR:
    if (DMA.CH0.CTRLB & DMA_CH_ERRIF_bm)
    {
        DMA.CH0.CTRLB |= DMA_CH_ERRIF_bm;
    }
    // NO ERROR:
    // Only latch shifted bits to RGB LEDs if there were no errors in DMA transaction
    else
    {
        DMA.CH0.CTRLB |= DMA_CH_TRNIF_bm;
        // Set BLANK low (Constant current outputs are controlled by the grayscale PWM timing controller)
        SET_RGB_BLANK_LOW;
        // Set XLAT high
        SET_RGB_XLAT_HIGH;
    }
} // dma_ch0_isr_handler()

/////////////////
// DMA CH1 ISR //
/////////////////
void dma_ch1_isr_handler(void)
{
    // Local variables
    static unsigned long int dmaCH1TransactionCount=0;
    // ERROR:
    if (DMA.CH1.CTRLB & DMA_CH_ERRIF_bm)
    {
        DMA.CH1.CTRLB |= DMA_CH_ERRIF_bm;
    }
    // NO ERROR:
    else
    {
        DMA.CH1.CTRLB |= DMA_CH_TRNIF_bm;
        // Increment count
        dmaCH1TransactionCount++;
        // Disable/stop any further DMA transactions
        if(dmaCH1TransactionCount==dmaCH1TransactionCountLimit)
        {
            dmaCH1TransactionCount=0;
            DMA_DisableChannel(SPEAKER_DMA);
        }
    }
} // dma_ch1_isr_handler()

////////////////////////////////
// PORTK External Interrupt 0 //
////////////////////////////////
void PortKExtInt0ISR_handler(void)
{
    // Local variables
    static uint8_t fallingEdgeTime_s=0;
    static uint8_t risingEdgeTime_s=0;
        
    // FALLING EDGE
    if(!(PORTK.IN & 0x80))
    {   
        fallingEdgeTime_s = timeSeconds;
    }
    // RISING EDGE
    else
    {
        risingEdgeTime_s = timeSeconds;
        // TEST button functionality during Startup
        if(operatingState==STARTUP)
        {
            // Toggle state
            testButtonState^=0x01;
            // Toggle STATUS LED (GREEN)
            PORTH.OUTTGL=0x02;
        }
        // TEST button functionality in Test state
        else if(operatingState==TEST)
        {
            if(risingEdgeTime_s<fallingEdgeTime_s)
                risingEdgeTime_s+=60;
                
            // Measure time between edges (second resolution)
            if((abs(risingEdgeTime_s - fallingEdgeTime_s) >= TEST_REPEAT_BUTTON_HOLD_TIME_S) && !doDemo)
            {
                testRepeat^=0x01;
                // Set STATUS LEDs
                PORTH.OUTCLR=0x12;
                if(!testRepeat)
                    PORTH.OUTSET=0x02;
                else
                    PORTH.OUTSET=0x10;
            }
            else
            {
                displayIndex++;
                // Wrap index
                if(displayIndex==(nLCDStatItems-1))
                {
                    displayIndex=0;
                    backlightIndex++;
                    // Wrap index
                    // Note: Automatic LCD backlight brightness is not available in the TEST state
                    if(backlightIndex==2)
                        backlightIndex=0;
                }
            }
        }
        // TEST button functionality in Normal state or Charge state
        else if(operatingState==NORMAL || operatingState==CHARGE)
        {
            displayIndex++;
            // Wrap index
            if(displayIndex==(nLCDStatItems-1))
            {
                displayIndex=0;
                backlightIndex++;
                // Set light sensor gain
                if(backlightIndex==2)
                    setLightSensorHighGain();
                // Wrap index
                else if(backlightIndex==3)
                    backlightIndex=0;
            }
        }
    }
} // PortKExtInt0ISR_handler()

///////////////////////////
// TWID Master Interrupt //
///////////////////////////
void twid_master_isr_handler(void)
{
    TWI_MasterInterruptHandler(&twiMaster);
} // twid_master_isr_handler()

///////////////////////////////////////
// USARTE1 (Expansion Header) RX ISR //
///////////////////////////////////////
void usarte1_rx_isr_handler(void)
{
    // Local variables
    unsigned char status;
    char data;
    uint16_t rx_wr_index_usarte1_temp;
    // Static variables
    static uint16_t startEBugPacketBufferIndex=0;
    static uint8_t EBugPacketLength=0;
    static bool frameStartDetected=false;

    // Get byte from USART
    status=USARTE1.STATUS;
    data=USARTE1.DATA;

    // Check for errors before saving byte into buffer
    if ((status & (USART_FERR_bm | USART_PERR_bm | USART_BUFOVF_bm)) == 0)
    {
        // Check for frame start
        if(data==XBEE_START_DELIMITER)
        {
            // New packet start received before previous packet was posted, reset buffer variables
            if(frameStartDetected)
            {
                rx_rd_index_usarte1=rx_wr_index_usarte1;
                rx_counter_usarte1=0;
            }
            startEBugPacketBufferIndex=rx_wr_index_usarte1;
            frameStartDetected=true;
        }
        // Save length
        else if(rx_wr_index_usarte1==(startEBugPacketBufferIndex+1))
            EBugPacketLength=data;

        // Save received data into buffer
        if(frameStartDetected)
        {
            rx_buffer_usarte1[rx_wr_index_usarte1++]=data;

            // Check for buffer overflow, reset buffer
            if (++rx_counter_usarte1 == RX_BUFFER_SIZE_USARTE1)
            {
                // Reset buffer to recover
                usarte1_resetRXbuffer(false); 
            }
            // Reset buffer index if at end (i.e. buffer wraps around)
            if (rx_wr_index_usarte1 == RX_BUFFER_SIZE_USARTE1)
                rx_wr_index_usarte1=0;

            // Adjust index
            if(rx_wr_index_usarte1<startEBugPacketBufferIndex)
                rx_wr_index_usarte1_temp = rx_wr_index_usarte1 + RX_BUFFER_SIZE_USARTE1;
            else
                rx_wr_index_usarte1_temp = rx_wr_index_usarte1;
            // Post semaphore when ENTIRE packet has been received
            if((rx_wr_index_usarte1_temp-startEBugPacketBufferIndex) == (EBugPacketLength+3))
            {
                usarte1RXCount++;
                frameStartDetected=false;
                OSTaskSemPost(&AppTaskTCB[USARTE1RXTASK_TCB_ARRAY_INDEX],OS_OPT_POST_NONE,&err);
            }
        }
    }
} // usarte1_rx_isr_handler()

////////////////////////////////
// PORTC External Interrupt 0 //
////////////////////////////////
void PortCExtInt0ISR_handler(void)
{
    // RESET XMEGA
    CPU_CCP=CCP_IOREG_gc; 
    RST.CTRL=RST_SWRST_bm;
} // PortCExtInt0ISR_handler()

void rtc_overflow_isr_handler(void)
{
    // Note: This ISR is called every SECOND
    if((++timeSeconds)>59)
    {
        timeSeconds=0;
        if((++timeMinutes)>59)
        {
            timeMinutes=0;
            timeHours++;
        }
    }
} // rtc_overflow_isr_handler()

// IR RX 1
void TCD0_CCC_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCD0.CCC>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR1 = TCD0.CCC;
        // Disable Compare Channel interrupt
        TCD0.INTCTRLB = (TCD0.INTCTRLB & ~TC0_CCCINTLVL_gm) | TC_CCCINTLVL_OFF_gc;
    }
} // TCD0_CCC_isr_handler()

// IR RX 2
void TCD0_CCD_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCD0.CCD>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR2 = TCD0.CCD;
        // Disable Compare Channel interrupt
        TCD0.INTCTRLB = (TCD0.INTCTRLB & ~TC0_CCDINTLVL_gm) | TC_CCDINTLVL_OFF_gc;
    }
} // TCD0_CCD_isr_handler()

// IR RX 3
void TCD1_CCA_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCD1.CCA>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR3 = TCD1.CCA;
        // Disable Compare Channel interrupt
        TCD1.INTCTRLB = (TCD1.INTCTRLB & ~TC1_CCAINTLVL_gm) | TC_CCAINTLVL_OFF_gc;
    }
} // TCD1_CCA_isr_handler()

// IR RX 4
void TCD1_CCB_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCD1.CCB>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR4 = TCD1.CCB;
        // Disable Compare Channel interrupt
        TCD1.INTCTRLB = (TCD1.INTCTRLB & ~TC1_CCBINTLVL_gm) | TC_CCBINTLVL_OFF_gc;
    }
} // TCD1_CCB_isr_handler()

// IR RX 5
void TCF0_CCA_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCF0.CCA>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR5 = TCF0.CCA;
        // Disable Compare Channel interrupt
        TCF0.INTCTRLB = (TCF0.INTCTRLB & ~TC0_CCAINTLVL_gm) | TC_CCAINTLVL_OFF_gc;
    }
} // TCF0_CCA_isr_handler()

// IR RX 6
void TCF0_CCB_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCF0.CCB>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR6 = TCF0.CCB;
        // Disable Compare Channel interrupt
        TCF0.INTCTRLB = (TCF0.INTCTRLB & ~TC0_CCBINTLVL_gm) | TC_CCBINTLVL_OFF_gc;
    }
} // TCF0_CCB_isr_handler()

// IR RX 7
void TCF0_CCC_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCF0.CCC>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR7 = TCF0.CCC;
        // Disable Compare Channel interrupt
        TCF0.INTCTRLB = (TCF0.INTCTRLB & ~TC0_CCCINTLVL_gm) | TC_CCCINTLVL_OFF_gc;
    }
} // TCF0_CCC_isr_handler()

// IR RX 8
void TCF0_CCD_isr_handler(void)
{
    // Save width of pulse in global variable
    // Note: Pulse has to be longer than a pre-defined value to be recognized and saved
    if(TCF0.CCD>IRRX_MIN_PULSE_WIDTH)
    {
        pulseWidthIR8 = TCF0.CCD;
        // Disable Compare Channel interrupt
        TCF0.INTCTRLB = (TCF0.INTCTRLB & ~TC0_CCDINTLVL_gm) | TC_CCDINTLVL_OFF_gc;
    }
} // TCF0_CCD_isr_handler()

/*
*********************************************************************************************************
*********************************************************************************************************
**                                         uC/OS-III APP HOOKS
*********************************************************************************************************
*********************************************************************************************************
*/

/*
************************************************************************************************************************
*                                              SET ALL APPLICATION HOOKS
*
* Description: Set ALL application hooks.
*
* Arguments  : none.
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_SetAllHooks (void)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    CPU_SR_ALLOC();


    CPU_CRITICAL_ENTER();
    OS_AppTaskCreateHookPtr = App_OS_TaskCreateHook;
    OS_AppTaskDelHookPtr    = App_OS_TaskDelHook;
    OS_AppTaskReturnHookPtr = App_OS_TaskReturnHook;

    OS_AppIdleTaskHookPtr   = App_OS_IdleTaskHook;
    OS_AppStatTaskHookPtr   = App_OS_StatTaskHook;
    OS_AppTaskSwHookPtr     = App_OS_TaskSwHook;
    OS_AppTimeTickHookPtr   = App_OS_TimeTickHook;
    CPU_CRITICAL_EXIT();
#endif
}

/*
************************************************************************************************************************
*                                             CLEAR ALL APPLICATION HOOKS
*
* Description: Clear ALL application hooks.
*
* Arguments  : none.
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_ClrAllHooks (void)
{
#if OS_CFG_APP_HOOKS_EN > 0u
    CPU_SR_ALLOC();


    CPU_CRITICAL_ENTER();
    OS_AppTaskCreateHookPtr = (OS_APP_HOOK_TCB)0;
    OS_AppTaskDelHookPtr    = (OS_APP_HOOK_TCB)0;
    OS_AppTaskReturnHookPtr = (OS_APP_HOOK_TCB)0;

    OS_AppIdleTaskHookPtr   = (OS_APP_HOOK_VOID)0;
    OS_AppStatTaskHookPtr   = (OS_APP_HOOK_VOID)0;
    OS_AppTaskSwHookPtr     = (OS_APP_HOOK_VOID)0;
    OS_AppTimeTickHookPtr   = (OS_APP_HOOK_VOID)0;
    CPU_CRITICAL_EXIT();
#endif
}

/*
************************************************************************************************************************
*                                            APPLICATION TASK CREATION HOOK
*
* Description: This function is called when a task is created.
*
* Arguments  : p_tcb   is a pointer to the task control block of the task being created.
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_TaskCreateHook (OS_TCB  *p_tcb)
{
    (void)&p_tcb;
}

/*
************************************************************************************************************************
*                                            APPLICATION TASK DELETION HOOK
*
* Description: This function is called when a task is deleted.
*
* Arguments  : p_tcb   is a pointer to the task control block of the task being deleted.
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_TaskDelHook (OS_TCB  *p_tcb)
{
    (void)&p_tcb;
}

/*
************************************************************************************************************************
*                                             APPLICATION TASK RETURN HOOK
*
* Description: This function is called if a task accidentally returns.  In other words, a task should either be an
*              infinite loop or delete itself when done.
*
* Arguments  : p_tcb     is a pointer to the OS_TCB of the task that is returning.
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_TaskReturnHook (OS_TCB  *p_tcb)
{
    (void)&p_tcb;
}

/*
************************************************************************************************************************
*                                              APPLICATION IDLE TASK HOOK
*
* Description: This function is called by the idle task.  This hook has been added to allow you to do such things as
*              STOP the CPU to conserve power.
*
* Arguments  : none
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_IdleTaskHook (void)
{
}

/*
************************************************************************************************************************
*                                          APPLICATION OS INITIALIZATION HOOK
*
* Description: This function is called by OSInit() at the beginning of OSInit().
*
* Arguments  : none
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_InitHook (void)
{
}

/*
************************************************************************************************************************
*                                           APPLICATION STATISTIC TASK HOOK
*
* Description: This function is called every second by uC/OS-III's statistics task.  This allows your application to add
*              functionality to the statistics task.
*
* Arguments  : none
*
* Note(s)    : none
************************************************************************************************************************
*/

void  App_OS_StatTaskHook (void)
{
    // Local variables
    static char dispString[17];
    static bool firstRun=true;
    static uint16_t averageTimeToEmpty=0;
    static uint8_t rsoc=0;
    static uint16_t remainingCapacity=0;
    static double batteryTemperature=0;
    static int16_t current=0;
    static int16_t averageCurrent=0;
    static uint16_t lipoPackVoltage=0;
    static uint8_t byte0=0;
    static uint8_t byte1=0;
    static unsigned int lightSensorReadingSum=0;
    static uint8_t getReadingIndex=0;
    static uint8_t changeTaskCount=0;
    static uint8_t taskPriorityIndex=0;
    static CPU_STK_SIZE free;
    static CPU_STK_SIZE used;
    static OS_ERR LCDDataProtectErr;
    static uint16_t chargeOption=0;
    static uint16_t remainingChargeTime=0;
    static uint16_t fullChargeCapacity=0;
    static uint16_t chargerVoltage=0;
    static uint16_t chargerCurrent=0;
    static double powerSourceVoltage=0;
    static uint16_t cell1Voltage=0;
    static uint16_t cell2Voltage=0;
    static uint16_t cell3Voltage=0;

    // Toggle MCU LED (PQ2)
    PORTQ.OUTTGL=0x04;

    // Backlight
    switch(backlightIndex)
    {
        // BL off
        case 1:
            PORTA.OUTCLR=0x80;
            break;
        // BL dependent on light sensor
        case 2:
            // Get light sensor reading
            lightSensorReadingSum += getLightSensorReading();
            if(getReadingIndex==(LCD_BL_NSAMPLES-1))
            {
                if((lightSensorReadingSum>>3)<LCD_BL_THRESHOLD)
                    PORTA.OUTSET=0x80;
                else
                    PORTA.OUTCLR=0x80;
                lightSensorReadingSum=0;
                getReadingIndex=0;
            }
            else
                getReadingIndex++;
            break;
        // BL on
        default:
            PORTA.OUTSET=0x80;
            break;
    }

    // Get mutex (non-blocking)
    OSMutexPend(&LCDDataProtect,0,OS_OPT_PEND_NON_BLOCKING,NULL,&LCDDataProtectErr);
    if(LCDDataProtectErr==OS_ERR_NONE)
    {
        // Display
        switch(displayIndex)
        {
            case 1:
            {
                // AverageTimeToEmpty [0x12]
                sendBuffer[0]=0x12;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0]; 
                    byte1=twiMaster.readData[1];
                    averageTimeToEmpty=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"RunTime=%dmin",averageTimeToEmpty);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 2:
            {
                // RemainingCapacity [0x0f]
                sendBuffer[0]=0x0f;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0];
                    byte1=twiMaster.readData[1]; 
                    remainingCapacity=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"RemCap=%dmAh",remainingCapacity);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 3:
            {
                // Temperature [0x08]
                sendBuffer[0]=0x08;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0];
                    byte1=twiMaster.readData[1]; 
                    batteryTemperature=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"BTemp=%0.1f degC",(batteryTemperature/10)-273);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 4:
            {
                // Current [0x0a]
                sendBuffer[0]=0x0a;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0]; 
                    byte1=twiMaster.readData[1];
                    current=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"Current=%dmA",current);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 5:
            {
                // AverageCurrent [0x0b]
                sendBuffer[0]=0x0b;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0];
                    byte1=twiMaster.readData[1];
                    averageCurrent=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"AvgCur=%dmA",averageCurrent);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 6:
            {
                // Voltage [0x09]
                sendBuffer[0]=0x09;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0=twiMaster.readData[0]; 
                    byte1=twiMaster.readData[1];
                    lipoPackVoltage=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"BVoltage=%dmV",lipoPackVoltage);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                // Connect BAT signal to ADC in preparation for next display item (gives time for signal to settle)
                PORTQ.OUTSET=0x08;
                break;
            }
            case 7:
            {
                // Read power source voltage from ADC
                powerSourceVoltage=16*((double)adcb_read(2,1)/4096);
                // Create string containing power source voltage
                sprintf(dispString,"PSrcVolt=%0.2fV",powerSourceVoltage);
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }       
            case 8:
            {
                // Disconnect BAT signal from ADC
                PORTQ.OUTCLR=0x08;              
                // AverageTimeToFull [0x13]
                sendBuffer[0]=0x13;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    remainingChargeTime=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"ChgTime=%umin",remainingChargeTime);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 9:
            {
                // FullChargeCapacity [0x10]
                sendBuffer[0]=0x10;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    fullChargeCapacity=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"FChgCap=%dmAh",fullChargeCapacity);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 10:
            {
                // Cell 1 Voltage [0x3f]
                sendBuffer[0]=0x3f;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    cell1Voltage=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"C1Voltage=%dmV",cell1Voltage);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 11:
            {
                // Cell 2 Voltage [0x3e]
                sendBuffer[0]=0x3e;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    cell2Voltage=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"C2Voltage=%dmV",cell2Voltage);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 12:
            {
                // Cell 3 Voltage [0x3d]
                sendBuffer[0]=0x3d;
                if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    cell3Voltage=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"C3Voltage=%dmV",cell3Voltage);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 13:
            {
                // uC/OS-III running time
                CPU_SR_ALLOC();
                // Create string
                CPU_CRITICAL_ENTER();
                    sprintf(dispString,"uCTime=%02d:%02d:%02d",timeHours,timeMinutes,timeSeconds);
                CPU_CRITICAL_EXIT();
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }
            case 14:
            {
                // XBee RX Count
                // Create string
                sprintf(dispString,"XBeeRX=%ld",XBeeRXCount);
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }
            case 15:
            {
                // USARTE1 (Expansion Header) RX Count
                // Create string
                sprintf(dispString,"E1RX=%ld",usarte1RXCount);
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }
            case 16:
            {
                if((changeTaskCount++)==N_CHANGETASKCOUNT_LCD_STACK)
                {
                    // Get stack usage
                    OSTaskStkChk(&AppTaskTCB[taskPriorityIndex],&free,&used,&err);
                    // Create string
                    sprintf(dispString,"i=%d:U=%d,F=%d",taskPriorityIndex, used, free);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                    // Reset count
                    changeTaskCount=0;
                    // Change index
                    taskPriorityIndex++;
                    if(taskPriorityIndex==APP_CFG_N_TASKS)
                        taskPriorityIndex=0;
                }
                break;
            }
            case 17:
            {
                // Create string containing XBee USART data
                sprintf(dispString,"XB: %3d %3d %3d", rx_wr_index_XBee,rx_rd_index_XBee,rx_counter_XBee);
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Go to (0,0) on LCD
                lcd_gotoxy(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }
            case 18:
            {
                // Create string containing USARTE1 data
                sprintf(dispString,"UE1: %3d %3d %3d", rx_wr_index_usarte1,rx_rd_index_usarte1,rx_counter_usarte1);
                // Clear top line and go to x=0 on LCD
                LCD_ClearLineAndSetXPos(0,0);
                // Go to (0,0) on LCD
                lcd_gotoxy(0,0);
                // Display string on LCD
                lcd_puts(dispString);
                break;
            }
            //////////////////////////
            // Charge display items //
            //////////////////////////
            case 19:
            {
                // Charge Option [0x12]
                sendBuffer[0]=0x12;
                if((TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    chargeOption=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"ChgOption=0x%04x",chargeOption);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 20:
            {
                // Charger Set Voltage [0x15]
                sendBuffer[0]=0x15;
                if((TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    chargerVoltage=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"CVoltage=%dmV",chargerVoltage);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }
            case 21:
            {
                // Charger Set Current [0x14]
                sendBuffer[0]=0x14;
                if((TWI_MasterWriteRead(&twiMaster,BQ24725_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                {
                    byte0 = twiMaster.readData[0];
                    byte1 = twiMaster.readData[1];
                    chargerCurrent=(byte1<<8)|byte0;
                    // Create string
                    sprintf(dispString,"CCurrent=%dmA",chargerCurrent);
                    // Clear top line and go to x=0 on LCD
                    LCD_ClearLineAndSetXPos(0,0);
                    // Display string on LCD
                    lcd_puts(dispString);
                }
                break;
            }                                               
            // Show CPU USAGE and RSOC on top row of LCD (except on first run)
            default:
            {
                if(!firstRun)
                {
                    // RSOC [0x0d]
                    sendBuffer[0]=0x0d;
                    if((TWI_MasterWriteRead(&twiMaster,BQ20Z65_WRITE>>1,&sendBuffer[0],1,2))&&(waitForTWIReady_Timeout(TWI_READY_TIMEOUT)))
                    {
                        byte0=twiMaster.readData[0]; 
                        rsoc=byte0;
                        // Create string containing CPU usage
                        sprintf(dispString,"CPU=%3d%% B=%3d%%", OSStatTaskCPUUsage/100,rsoc);
                        // Clear top line and go to x=0 on LCD
                        LCD_ClearLineAndSetXPos(0,0);
                        // Go to (0,0) on LCD
                        lcd_gotoxy(0,0);
                        // Display string on LCD
                        lcd_puts(dispString);
                    }
                }
                else
                    firstRun=false;
                break;
            }
        }
        // Release mutex
        OSMutexPost(&LCDDataProtect,OS_OPT_POST_NONE,&err);
    }   
}

/*
************************************************************************************************************************
*                                             APPLICATION TASK SWITCH HOOK
*
* Description: This function is called when a task switch is performed.  This allows you to perform other operations
*              during a context switch.
*
* Arguments  : none
*
* Note(s)    : 1) Interrupts are disabled during this call.
*              2) It is assumed that the global pointer 'OSTCBHighRdyPtr' points to the TCB of the task that will be
*                 'switched in' (i.e. the highest priority task) and, 'OSTCBCurPtr' points to the task being switched out
*                 (i.e. the preempted task).
************************************************************************************************************************
*/

void  App_OS_TaskSwHook (void)
{
}

/*
************************************************************************************************************************
*                                                APPLICATION TICK HOOK
*
* Description: This function is called every tick.
*
* Arguments  : none
*
* Note(s)    : 1) This function is assumed to be called from the Tick ISR.
************************************************************************************************************************
*/

void  App_OS_TimeTickHook (void)
{
}