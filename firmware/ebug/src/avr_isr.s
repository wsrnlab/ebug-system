;********************************************************************************************************
;                                               uC/OS-III
;                                         The Real-Time Kernel
;
;                                       ATXmega128  Specific code
;                                           GNU AVR Compiler
;
;
; File     : avr_isr.s
; By       : JJL
;          : FT
;          : [with modifications by Nick D'Ademo]
;********************************************************************************************************

#include  <os_cpu_i.h>


;********************************************************************************************************
;                                         PUBLIC DECLARATIONS
;********************************************************************************************************

	.global  tcc1_overflow_isr

    .global  usartd1_rx_isr
    .global  tcc0_overflow_isr
    .global  tce0_overflow_isr
    .global  dma_ch0_isr
    .global  dma_ch1_isr
    .global  PortKExtInt0ISR
    .global  twid_master_isr
    .global  usarte1_rx_isr
    .global  PortCExtInt0ISR
    .global  rtc_overflow_isr
    .global  TCF0_CCA_isr
    .global  TCF0_CCB_isr 
    .global  TCF0_CCC_isr 
    .global  TCF0_CCD_isr 
    .global  TCD0_CCC_isr 
    .global  TCD0_CCD_isr 
    .global  TCD1_CCA_isr 
    .global  TCD1_CCB_isr
	.global  usarte0_rx_isr

;********************************************************************************************************
;                                         EXTERNAL DECLARATIONS
;********************************************************************************************************

    .extern  OSIntExit
    .extern  OSIntNestingCtr
    .extern  OSTCBCurPtr

    .extern  usartd1_rx_isr_handler
    .extern  tcc0_overflow_isr_handler
    .extern  tce0_overflow_isr_handler
    .extern  dma_ch0_isr_handler
    .extern  dma_ch1_isr_handler
    .extern  PortKExtInt0ISR_handler
    .extern  twid_master_isr_handler
    .extern  usarte1_rx_isr_handler
    .extern  PortCExtInt0ISR_handler
    .extern  rtc_overflow_isr_handler
    .extern  TCF0_CCA_isr_handler
    .extern  TCF0_CCB_isr_handler 
    .extern  TCF0_CCC_isr_handler 
    .extern  TCF0_CCD_isr_handler 
    .extern  TCD0_CCC_isr_handler 
    .extern  TCD0_CCD_isr_handler 
    .extern  TCD1_CCA_isr_handler 
    .extern  TCD1_CCB_isr_handler
	.extern  usarte0_rx_isr_handler

        .text

;********************************************************************************************************
;                                           SYSTEM TICK ISR
;
; Description : This function is the Tick ISR.
;
;               The following C-like pseudo-code describe the operation being performed in the code below.
;
;               - Save all registers on the current task's stack:
;                      Use the PUSH_ALL macro
;               - OSIntNestingCtr++;
;               if (OSIntNestingCtr == 1) {
;                  OSTCBCurPtr->OSTCBStkPtr = SP
;               }
;               Clear the interrupt;                  Not needed for the timer we used.
;               OSTimeTick();                         Notify uC/OS-III that a tick has occured
;               OSIntExit();                          Notify uC/OS-III about end of ISR
;               Restore all registers that were save on the current task's stack:
;                      Use the POP_ALL macro to restore the remaining registers
;               Return from interrupt
;********************************************************************************************************
            
tcc1_overflow_isr:       
        PUSH_ALL                                                ; Save all registers and status register        	
        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    tcc1_overflow_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }

tcc1_overflow_isr_1:
        CALL    OSTimeTick                                  	; Handle the tick ISR

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR
                
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                   USART D1 RxComplete ISR
;*
;* Description: This function is invoked when USARTD1 receives a character.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 usartd1_rx_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
usartd1_rx_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    usartd1_rx_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


usartd1_rx_isr_1:
        CALL    usartd1_rx_isr_handler                          ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                   USART E0 RxComplete ISR
;*
;* Description: This function is invoked when USARTE0 receives a character.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 usarte0_rx_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
usarte0_rx_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    usarte0_rx_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


usarte0_rx_isr_1:
        CALL    usarte0_rx_isr_handler                          ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                      Timer C0 Overflow
;*
;* Description: This function is invoked when Timer/Counter C0 overflows.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 tcc0_overflow_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
tcc0_overflow_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    tcc0_overflow_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
     
        ST      Y+,R26
        ST      Y+,R27                                          ; }


tcc0_overflow_isr_1:
        CALL    tcc0_overflow_isr_handler                       ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                      Timer E0 Overflow
;*
;* Description: This function is invoked when Timer/Counter E0 overflows.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 tce0_overflow_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
tce0_overflow_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    tce0_overflow_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


tce0_overflow_isr_1:
        CALL    tce0_overflow_isr_handler                       ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                    DMA Channel 0
;*
;* Description: This function is invoked when the DMA CH0 transaction is complete.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 dma_ch0_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
dma_ch0_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    dma_ch0_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


dma_ch0_isr_1:
        CALL    dma_ch0_isr_handler                             ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                    DMA Channel 1
;*
;* Description: This function is invoked when the DMA CH1 transaction is complete.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 dma_ch1_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
dma_ch1_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    dma_ch1_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


dma_ch1_isr_1:
        CALL    dma_ch1_isr_handler                             ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                               PortK External Interrupt 0
;*
;* Description: This function is invoked when an interrupt is triggered on PORTK.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 PortKExtInt0ISR_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
PortKExtInt0ISR:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    PortKExtInt0ISR_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


PortKExtInt0ISR_1:
        CALL    PortKExtInt0ISR_handler                         ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                     TWID Master Interrupt
;*
;* Description: This function is invoked when an interrupt is triggered on TWID.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 twid_master_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
twid_master_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    twid_master_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


twid_master_isr_1:
        CALL    twid_master_isr_handler                         ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                   USART E1 RxComplete ISR
;*
;* Description: This function is invoked when USARTE1 receives a character.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 usarte1_rx_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
usarte1_rx_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    usarte1_rx_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


usarte1_rx_isr_1:
        CALL    usarte1_rx_isr_handler                          ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                               PortC External Interrupt 0
;*
;* Description: This function is invoked when an interrupt is triggered on PORTC.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 PortCExtInt0ISR_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
PortCExtInt0ISR:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    PortCExtInt0ISR_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


PortCExtInt0ISR_1:
        CALL    PortCExtInt0ISR_handler                         ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                        RTC Overflow
;*
;* Description: This function is invoked when the RTC overflows.
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 rtc_overflow_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
rtc_overflow_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    rtc_overflow_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


rtc_overflow_isr_1:
        CALL    rtc_overflow_isr_handler                        ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer D0 Compare or Capture C
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCD0_CCC_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCD0_CCC_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCD0_CCC_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCD0_CCC_isr_1:
        CALL    TCD0_CCC_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer D0 Compare or Capture D
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCD0_CCD_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCD0_CCD_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCD0_CCD_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCD0_CCD_isr_1:
        CALL    TCD0_CCD_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer D1 Compare or Capture A
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCD1_CCA_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCD1_CCA_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCD1_CCA_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCD1_CCA_isr_1:
        CALL    TCD1_CCA_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer D1 Compare or Capture B
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCD1_CCB_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCD1_CCB_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCD1_CCB_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCD1_CCB_isr_1:
        CALL    TCD1_CCB_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer F0 Compare or Capture A
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCF0_CCA_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCF0_CCA_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCF0_CCA_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCF0_CCA_isr_1:
        CALL    TCF0_CCA_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer F0 Compare or Capture B
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCF0_CCB_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCF0_CCB_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCF0_CCB_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCF0_CCB_isr_1:
        CALL    TCF0_CCB_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer F0 Compare or Capture C
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCF0_CCC_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCF0_CCC_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCF0_CCC_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCF0_CCC_isr_1:
        CALL    TCF0_CCC_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI

;**********************************************************************************************
;*                                 Timer F0 Compare or Capture D
;*
;* Description: 
;*
;* Arguments  : none
;*
;* Note(s)    : 1) Pseudo code:
;*                 Disable Interrupts
;*                 Save all registers
;*                 OSIntNestingCtr++
;*                 if (OSIntNestingCtr == 1) {
;*                     OSTCBCurPtr->OSTCBStkPtr = SP
;*                 }
;*                 TCF0_CCD_isr_handler();
;*                 OSIntExit();
;*                 Restore all registers
;*                 Return from interrupt;
;**********************************************************************************************
        
TCF0_CCD_isr:        
        PUSH_ALL                                                ; Save all registers and status register        

        LDS     R16,OSIntNestingCtr                             ; Notify uC/OS-III of ISR
        INC     R16                                             ;
        STS     OSIntNestingCtr,R16                             ;

        CPI     R16,1                                           ; if (OSIntNestingCtr == 1) {
        BRNE    TCF0_CCD_isr_1

        SAVE_SP				                                    ; X = SP 		
        LDS     R28,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R29,OSTCBCurPtr+1                               ;    
        
        ST      Y+,R26
        ST      Y+,R27                                          ; }


TCF0_CCD_isr_1:
        CALL    TCF0_CCD_isr_handler                            ; Call Handler written in C

        CALL    OSIntExit                                       ; Notify uC/OS-III about end of ISR

        LDS     R26,OSTCBCurPtr                                 ; OSTCBCurPtr->OSTCBStkPtr = X
        LDS     R27,OSTCBCurPtr+1                               ;                        X = Y = SP
        
        POP_ALL                                                 ; Restore all registers
        
        RETI
