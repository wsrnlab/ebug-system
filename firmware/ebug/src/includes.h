/*
*********************************************************************************************************
*                                               uC/OS-III
*                                          The Real-Time Kernel
*
*                              (c) Copyright 2003-2008, Micrium, Weston, FL
*                                           All Rights Reserved
*
*                                           MASTER INCLUDE FILE
*********************************************************************************************************
*/

#ifndef INCLUDES_H
#define INCLUDES_H

// uC/OS-III header files
#include <cpu_def.h>
#include <cpu.h>
#include <app_cfg.h>
#include <os_cfg_app.h>
#include <os.h>
#include <os_app_hooks.h>

// AVR device-specific IO definitions
#include <avr/io.h>

// AVR header files
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <math.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

// AVR drivers
#include <dma_driver.h>
#include <twi_master_driver.h>
//#include <twi_slave_driver.h>

// External (3rd-party) libraries
#include <lcd.h>
#include <tlc5947.h>
#include <i2cmaster.h>

// Enumerations
typedef enum
{
    STARTUP,
    TEST,
    NORMAL,
    CHARGE
}OperatingState;

typedef enum
{
    NOT_CHARGING,
    CHARGE_TERMINATED,
    CHARGING
}ChargingState;

/*
*********************************************************************************************************
*                                             CONFIGURATION
*********************************************************************************************************
*/

// Note: CPU clock frequency can be changed in Atmel Studio 6 via Project Properties (in the AVR/GNU C Compiler and AVR/GNU Assembler "Symbols" sections).
// Valid frequencies = [32000000,29491200] <-> [internal osc., ext. crystal (2x PLL)]

// eBug Firmware Version
#define FW_VERSION 118

// (Slave) device address of TI bq20z65-R1 LiPo Gas Gauge
#define BQ20Z65_WRITE   0x16

// (Slave) device address of TI bq24725 LiPo Charger
#define BQ24725_WRITE   0x12

// Boot countdown length (seconds)
#define STARTUP_TIMEOUT_SECS 5

// Number of statistics items available on LCD (NORMAL state)
#define N_LCD_STAT_ITEMS_NORMAL 20
// Number of statistics items available on LCD (CHARGE state)
#define N_LCD_STAT_ITEMS_CHARGE N_LCD_STAT_ITEMS_NORMAL + 3

// Number of steps in self-test procedure (TEST state)
#define N_TEST_STEPS 8

// Number of App_TaskStatHook() calls to show stack usage of each uC/OS-III task
#define N_CHANGETASKCOUNT_LCD_STACK 10

// BQ24725 Device ID
#define BQ24725_DEVICE_ID 0x0008

// BQ20Z65 Device ID
#define BQ20Z65_DEVICE_ID 0x0650

// Storage Charge RSOC (%)
#define STORAGE_CHARGE_RSOC 50

// Stepper Motors Enable Storage Charge RSOC Difference Threshold (%)
// Note: If (Current RSOC - STORAGE_CHARGE_RSOC) is greater than this value, the stepper motors will be enabled to accelerate battery discharge.
#define STORAGE_CHARGE_RSOC_SM_EN_THRESH 5

// TEST push-button must be held for at least this value for current test to be repeated
#define TEST_REPEAT_BUTTON_HOLD_TIME_S 2

/////////
// DMA //
/////////
#define RGB_DMA     &DMA.CH0
#define SPEAKER_DMA &DMA.CH1

//////////
// XBee //
//////////
#define XBEE_START_DELIMITER 0x7E
#define API_ID_INDEX 3 // Position of API ID field in XBee packet (where index=0 corresponds to the Start Delimiter)

// The RF Data contained in the ZigBee API frame is the eBug Packet
// Note: XBee ZB firmware includes a command (ATNP) that returns the maximum number of RF payload bytes
//       that can be sent in a unicast transmission (API TX Request Frame).
// The following assumes: Security not enabled, source routing and fragmentation not used
#define MAX_EBUG_PACKET_LENGTH 84

// Maximum size (in bytes) of the ZigBee Receive Packet (API ID=0x90)
// 16 bytes = header + checksum
#define MAX_XBEE_RXPACKET_LENGTH (16+MAX_EBUG_PACKET_LENGTH)

// Maximum size (in bytes) of the ZigBee Transmit Request (API ID=0x10)
// 16 bytes = header + checksum
#define MAX_XBEE_TXPACKET_LENGTH (16+MAX_EBUG_PACKET_LENGTH)

// Maximum number of XBee nodes in Network (including coordinator node)
#define MAX_NODES 16+1

///////////////////
// USART Buffers //
///////////////////
// XBee Receiver buffer
#define RX_BUFFER_SIZE_XBEE MAX_XBEE_RXPACKET_LENGTH*6
// USARTE1 Receiver buffer
#define RX_BUFFER_SIZE_USARTE1 MAX_EBUG_PACKET_LENGTH*6

////////////////////////////////
// CodeVision AVR definitions //
////////////////////////////////
// USART.BAUDCTRLB
#define USART_BSCALE_gm 0xF0	// Baud Rate Scale group mask
#define USART_BSCALE_bp 4		// Baud Rate Scale group position

//////////////////////////
// ADCB Clock Frequency //
//////////////////////////
// ADC Clock Frequency=(MCU Clock Frequency/Prescaler Division)
// Note: The prescaler division is selected below.
#define ADCB_CLK_DIV 512
//#define ADCB_CLK_DIV 256
//#define ADCB_CLK_DIV 128
//#define ADCB_CLK_DIV 64
//#define ADCB_CLK_DIV 32
//#define ADCB_CLK_DIV 16
//#define ADCB_CLK_DIV 8
//#define ADCB_CLK_DIV 4

/////////
// TWI //
/////////
#define TWI_SEND_BUFFER_SIZE    6
#define BAUDRATE                61500   // 61.5kHz is the minimum allowable TWI clock speed @ F_CPU=32MHz
#define TWI_BAUDSETTING         TWI_BAUD(F_CPU, BAUDRATE)
#define TWI_READY_TIMEOUT       100000

///////////////////
// LCD BACKLIGHT //
///////////////////
#define LCD_BL_NSAMPLES 8
#define LCD_BL_THRESHOLD 1024

//////////////////
// IR RECEIVERS //
//////////////////
#define IRRX_MIN_PULSE_WIDTH 0x00FF // IRRX_MIN_PULSE_WIDTH*8us (for 32 MHz clock)

/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/

///////////////
// uC/OS-III //
///////////////
void uCTickTimerStart(void);
void AppTaskStart(void *p_arg);
void AppTaskStart_CreateTasks(OperatingState);

////////////////////
// Initialization //
////////////////////
void AVRInit(void);
// PORTS
void PortsInit(void);
// CLOCKS
void SystemClocksInit(void);
// TIMER/COUNTER
void tcc0_init(void);
void tcd0_init(void);
void tcd1_init(void);
void tce0_init(void);
void tcf0_init(void);
void tcf1_init(void);
// USART
void usartc1_init(void);
void usartd1_init(void);
void usarte0_init(void);
void usarte1_init(void);
// ADC
void adcb_init(int);
// DAC
void dacb_init(void);
// VIRTUAL PORTS
void vports_init(void);
// RTC
void rtcxm_init(void);
// IR RECEIVERS
void setupIRReceivers(void);
// EVENT SYSTEM
void event_system_init(void);

/////////////////
// Peripherals //
/////////////////
// TIMER/COUNTER
void tc0_disable(TC0_t *);
void tc1_disable(TC1_t *);
// USART
void putchar_usarte1(char);
uint8_t getchar_usarte1(void);
void ReadEBugPacket(void);
int HandleEBugPacket(uint8_t *, uint8_t *);
uint8_t CalculateEBugPacketChecksum(uint8_t *);
void usarte1_resetRXbuffer(bool);
// DMA
void TLC5947_SPI_DMA_init(volatile DMA_CH_t *);
// RGB LEDs
void TLC5947_SetAllRed(uint16_t);
void TLC5947_SetAllGreen(uint16_t);
void TLC5947_SetAllBlue(uint16_t);
void TLC5947_SetAll(uint16_t);
void TLC5947_SetAllOff(void);
void TLC5947_SetRed(uint8_t led, uint16_t);
void TLC5947_SetGreen(uint8_t led, uint16_t);
void TLC5947_SetBlue(uint8_t led, uint16_t);
void TLC5947_SetMultiple(uint16_t, uint16_t, uint16_t, uint16_t);
void TLC5947_SetMultipleHold(uint16_t, uint16_t, uint16_t, uint16_t, uint16_t);
void TLC5947_SetAllHold(uint16_t, uint16_t);
void TLC5947_SetAllRGB(uint16_t r_value, uint16_t g_value, uint16_t b_value);
// LCD
void LCD_ClearLineAndSetXPos(int, int);
void LCD_ClearLineFromXPos_ucos(int, int);
void lcd_puts_ucos(char *, int, int, bool, bool);
// ROTARY SWITCH
uint8_t getRotarySwitchState(void);
// CALIBRATION BYTE
uint8_t ReadCalibrationByte(uint8_t);
// ADC
unsigned int adcb_read(unsigned char, int);
// LIGHT SENSOR
void shutdownLightSensor(void);
unsigned int getLightSensorReading(void);
void setLightSensorLowGain(void);
void setLightSensorMediumGain(void);
void setLightSensorHighGain(void);
// TEMPERATURE SENSOR
double getTemperature(void);
// SPEAKER
void LoadSineWave(int);
void GenerateArbWave12_Speaker(volatile int *, int, unsigned long int, unsigned long int);
void shutdownAudioAmplifier(bool);
// EXPANSION HEADER
void setupExpansionHeader(uint8_t);
// STEPPER MOTORS
void stepperMotor1_step(uint16_t, uint16_t, bool, uint8_t, bool, bool);
void stepperMotor2_step(uint16_t, uint16_t, bool, uint8_t, bool, bool);
void stepperMotor1_stopDisable(bool);
void stepperMotor2_stopDisable(bool);
void stepperMotor1_startStepping(uint16_t);
void stepperMotor2_startStepping(uint16_t);
// TWI
bool waitForTWIReady_Timeout(uint32_t);
void handleTWIError(void);
// IR RECEIVERS
void enableIRReceiversOdd(bool, bool);
// IR EMIITERS
void setIREmitters(uint16_t, bool);
// XBEE
void putchar_XBee(char);
uint8_t getchar_XBee(void);
void XBee_resetRXbuffer(bool);
void SendXBeeATCommand_ND(void);
void SendXBeeTXRequestPacket(uint16_t, uint16_t, uint16_t, uint16_t, uint16_t, uint8_t *, int);
void ReadXBeePacket(void);
void HandleXBeeATCommandResponse(uint8_t *);
uint8_t CalculateXBeePacketChecksum(uint8_t *);
// GAS GAUGE
void checkForAndHandleGGPermanentFailure(void);
void clearGGPermanentFailure(bool);

#endif