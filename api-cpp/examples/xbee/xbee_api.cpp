// Written by Nick D'Ademo, Alastair Young, Alexandre Proust

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <fcntl.h>

#include "eBugAPI.h" 

#define BAUDRATE    B115200
#define UART        "/dev/tty01"
#define USB         "/dev/ttyUSB0"
#define DEVICE      USB                // Choose device here

static const char svn_id[] = "$Revision: 345 $ $LastChangedBy: nickdademo $";

int main()
{
    // Local variable(s)
    int srl_handle;
    struct termios options;
    std::vector<char> packetToSend;

    // Configure serial port
    srl_handle = open(DEVICE, O_RDWR | O_NOCTTY | O_NDELAY);
    if(srl_handle < 0)
    {
        perror("Serial port open");
        exit(-1);
    }
    tcgetattr(srl_handle, &options);
    cfsetospeed(&options, BAUDRATE);
    options.c_cflag |= (CLOCAL | CREAD);
    options.c_cflag &= ~PARENB;
    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;
    tcsetattr(srl_handle, TCSANOW, &options);

    // Create eBugAPI instance and set interface type: XBee
    eBugAPI ebugapi(XBEE);
    // Set receiver XBee node addresses (16-bit network address and 64-bit serial number)
    struct XBeeNodeData xBeeNodeData;
    xBeeNodeData.MY         = 0xFFFE; // Disable 16-bit addressing
    xBeeNodeData.SH_high    = 0x0013;
    xBeeNodeData.SH_low     = 0xA200;
    xBeeNodeData.SL_high    = 0x4086;
    xBeeNodeData.SL_low     = 0xDDD8;
    ebugapi.setXBeeNodeData(xBeeNodeData);
    // Create packet to send to eBug
    packetToSend = ebugapi.RGBLEDs_SetAllRed(4095, 0x00);
    // Send packet to the serial port
    write(srl_handle, &packetToSend[0], packetToSend.size());

    // Print success message and exit
    printf("Turned on all RGB LEDs red.\n");
    return(0);
}
