/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* TrackingSetupDialog.cpp                                              */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "TrackingSetupDialog.h"

// Qt header files
#include <QtGui>
// Configuration header file
#include "Config.h"

TrackingSetupDialog::TrackingSetupDialog(QWidget *parent) : QDialog(parent)
{
    // Setup dialog
    setupUi(this);
    // Connect GUI signals and slots
    connect(resetToDefaultsButton,SIGNAL(released()),this,SLOT(resetAllDialogToDefaults()));
    connect(thresholdModeGroup,SIGNAL(buttonReleased(QAbstractButton*)),this,SLOT(thresholdModeChange(QAbstractButton*)));
    connect(applyButton,SIGNAL(released()),this,SLOT(updateStoredSettingsFromDialog()));
    connect(vignettingCompCheckBox,SIGNAL(toggled(bool)),this,SLOT(setVCOptionsState(bool)));
    // Setup (fill) combo boxes
    setupComboBoxes();
    // Setup input validators
    setupInputValidators();
    // Set dialog values to defaults
    resetAllDialogToDefaults();
    // Initialize ARTKP structure (to defaults)
    initializeARTKPStructure();
} // TrackingSetupDialog constructor

bool TrackingSetupDialog::updateStoredSettingsFromDialog()
{
    // Validate values in dialog before storing
    if(validateDialog())
    {
        // Store settings in dialog in ATKPSettings structure
        artkpSettings.pattWidth=pattWidthEdit->text().toInt();
        artkpSettings.pattHeight=pattHeightEdit->text().toInt();
        artkpSettings.pattSamples=pattSamplesEdit->text().toInt();
        artkpSettings.maxLoadPatterns=maxLoadPatternsEdit->text().toInt();
        artkpSettings.maxImagePatterns=maxImagePatternsEdit->text().toInt();
        artkpSettings.trackingHistory=trackingHistoryCheckBox->isChecked();
        artkpSettings.borderWidth=markerBorderWidthEdit->text().toDouble();
        artkpSettings.autoThreshold=thresholdModeAutoButton->isChecked();
        artkpSettings.numAutoThreshRetries=autoThreshRetriesEdit->text().toInt();
        artkpSettings.threshold=thresholdEdit->text().toInt();
        artkpSettings.vignettingCompensation=vignettingCompCheckBox->isChecked();
        artkpSettings.vcnCorners=vcnCornersEdit->text().toInt();
        artkpSettings.vcnLeftRight=vcnLeftRightEdit->text().toInt();
        artkpSettings.vcnTopBottom=vcnTopBottomEdit->text().toInt();
        if(pixelFormatComboBox->currentIndex()==0)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_ABGR;
        else if(pixelFormatComboBox->currentIndex()==1)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_BGRA;
        else if(pixelFormatComboBox->currentIndex()==2)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_BGR;
        else if(pixelFormatComboBox->currentIndex()==3)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_RGBA;
        else if(pixelFormatComboBox->currentIndex()==4)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_RGB;
        else if(pixelFormatComboBox->currentIndex()==5)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_RGB565;
        else if(pixelFormatComboBox->currentIndex()==6)
            artkpSettings.pixelFormat=ARToolKitPlus::PIXEL_FORMAT_LUM;
        if(undistModeComboBox->currentIndex()==0)
            artkpSettings.undistortionMode=ARToolKitPlus::UNDIST_NONE;
        else if(undistModeComboBox->currentIndex()==1)
            artkpSettings.undistortionMode=ARToolKitPlus::UNDIST_STD;
        else if(undistModeComboBox->currentIndex()==2)
            artkpSettings.undistortionMode=ARToolKitPlus::UNDIST_LUT;
        if(poseEstimatorComboBox->currentIndex()==0)
            artkpSettings.poseEstimator=ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL;
        else if(poseEstimatorComboBox->currentIndex()==1)
            artkpSettings.poseEstimator=ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL_CONT;
        else if(poseEstimatorComboBox->currentIndex()==2)
            artkpSettings.poseEstimator=ARToolKitPlus::POSE_ESTIMATOR_RPP;
        if(imageProcessingModeComboBox->currentIndex()==0)
            artkpSettings.imageProcessingMode=ARToolKitPlus::IMAGE_HALF_RES;
        else if(imageProcessingModeComboBox->currentIndex()==1)
            artkpSettings.imageProcessingMode=ARToolKitPlus::IMAGE_FULL_RES;
        if(markerModeComboBox->currentIndex()==0)
            artkpSettings.markerMode=ARToolKitPlus::MARKER_TEMPLATE;
        else if(markerModeComboBox->currentIndex()==1)
            artkpSettings.markerMode=ARToolKitPlus::MARKER_ID_SIMPLE;
        else if(markerModeComboBox->currentIndex()==2)
            artkpSettings.markerMode=ARToolKitPlus::MARKER_ID_BCH;
        artkpSettings.cameraCalibrationFilename=cameraCalibrationFilenameEdit->text();
        artkpSettings.nEBug=nEBugEdit->text().toInt();
        // Update settings in processingThread
        emit setupARTKP(artkpSettings);
        return true;
    }
    // Settings not updated in processingThread
    else
    {
        QMessageBox::warning(this->parentWidget(),"ERROR:","Settings were not applied.");
        return false;
    }
} // updateStoredSettingsFromDialog()

void TrackingSetupDialog::updateDialogSettingsFromStored()
{
    pattWidthEdit->setText(QString::number(artkpSettings.pattWidth));
    pattHeightEdit->setText(QString::number(artkpSettings.pattHeight));
    maxImagePatternsEdit->setText(QString::number(artkpSettings.maxImagePatterns));
    pattSamplesEdit->setText(QString::number(artkpSettings.pattSamples));
    maxLoadPatternsEdit->setText(QString::number(artkpSettings.maxLoadPatterns));
    markerBorderWidthEdit->setText(QString::number(artkpSettings.borderWidth));
    trackingHistoryCheckBox->setChecked(artkpSettings.trackingHistory);
    thresholdModeAutoButton->setChecked(artkpSettings.autoThreshold);
    autoThreshRetriesEdit->setText(QString::number(artkpSettings.numAutoThreshRetries));
    thresholdEdit->setText(QString::number(artkpSettings.threshold));
    vignettingCompCheckBox->setChecked(artkpSettings.vignettingCompensation);
    setVCOptionsState(vignettingCompCheckBox->isChecked());
    vcnCornersEdit->setText(QString::number(artkpSettings.vcnCorners));
    vcnLeftRightEdit->setText(QString::number(artkpSettings.vcnLeftRight));
    vcnTopBottomEdit->setText(QString::number(artkpSettings.vcnTopBottom));
    if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_ABGR)
        pixelFormatComboBox->setCurrentIndex(0);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_BGRA)
        pixelFormatComboBox->setCurrentIndex(1);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_BGR)
        pixelFormatComboBox->setCurrentIndex(2);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_RGBA)
        pixelFormatComboBox->setCurrentIndex(3);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_RGB)
        pixelFormatComboBox->setCurrentIndex(4);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_RGB565)
        pixelFormatComboBox->setCurrentIndex(5);
    else if(artkpSettings.pixelFormat==ARToolKitPlus::PIXEL_FORMAT_LUM)
        pixelFormatComboBox->setCurrentIndex(6);
    if(artkpSettings.undistortionMode==ARToolKitPlus::UNDIST_NONE)
        undistModeComboBox->setCurrentIndex(0);
    else if(artkpSettings.undistortionMode==ARToolKitPlus::UNDIST_STD)
        undistModeComboBox->setCurrentIndex(1);
    else if(artkpSettings.undistortionMode==ARToolKitPlus::UNDIST_LUT)
        undistModeComboBox->setCurrentIndex(2);
    if(artkpSettings.poseEstimator==ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL)
        poseEstimatorComboBox->setCurrentIndex(0);
    else if(artkpSettings.poseEstimator==ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL_CONT)
        poseEstimatorComboBox->setCurrentIndex(1);
    else if(artkpSettings.poseEstimator==ARToolKitPlus::POSE_ESTIMATOR_RPP)
        poseEstimatorComboBox->setCurrentIndex(2);
    if(artkpSettings.imageProcessingMode==ARToolKitPlus::IMAGE_HALF_RES)
        imageProcessingModeComboBox->setCurrentIndex(0);
    else if(artkpSettings.imageProcessingMode==ARToolKitPlus::IMAGE_FULL_RES)
        imageProcessingModeComboBox->setCurrentIndex(1);
    if(artkpSettings.markerMode==ARToolKitPlus::MARKER_TEMPLATE)
        markerModeComboBox->setCurrentIndex(0);
    else if(artkpSettings.markerMode==ARToolKitPlus::MARKER_ID_SIMPLE)
        markerModeComboBox->setCurrentIndex(1);
    else if(artkpSettings.markerMode==ARToolKitPlus::MARKER_ID_BCH)
        markerModeComboBox->setCurrentIndex(2);
    cameraCalibrationFilenameEdit->setText(artkpSettings.cameraCalibrationFilename);
} // updateDialogSettingsFromStored()

void TrackingSetupDialog::resetAllDialogToDefaults()
{
    // Tracker settings
    pattWidthEdit->setText(QString::number(DEFAULT_ARTKP_PATTWIDTH));
    pattHeightEdit->setText(QString::number(DEFAULT_ARTKP_PATTHEIGHT));
    maxImagePatternsEdit->setText(QString::number(DEFAULT_ARTKP_MAXIMAGEPATTERNS));
    pattSamplesEdit->setText(QString::number(DEFAULT_ARTKP_PATTSAMPLES));
    maxLoadPatternsEdit->setText(QString::number(DEFAULT_ARTKP_MAXLOADPATTERNS));
    // Tracking history
    trackingHistoryCheckBox->setChecked(DEFAULT_ARTKP_TRACKINGHISTORY);
    // Marker border width
    markerBorderWidthEdit->setText(QString::number(DEFAULT_ARTKP_BORDERWIDTH));
    // Threshold mode
    autoThreshRetriesEdit->setText(QString::number(DEFAULT_ARTKP_NUMAUTOTHRESHRETRIES));
    thresholdEdit->setText(QString::number(DEFAULT_ARTKP_THRESHOLD));
    if(DEFAULT_ARTKP_AUTOTHRESHOLD)
        thresholdModeAutoButton->setChecked(true);
    else
        thresholdModeManualButton->setChecked(true);
    thresholdModeChange(thresholdModeGroup->checkedButton());
    // Vignetting compensation
    vignettingCompCheckBox->setChecked(DEFAULT_ARTKP_VC);
    setVCOptionsState(vignettingCompCheckBox->isChecked());
    vcnCornersEdit->setText(QString::number(DEFAULT_ARTKP_VC_NCORNERS));
    vcnLeftRightEdit->setText(QString::number(DEFAULT_ARTKP_VC_NLEFTRIGHT));
    vcnTopBottomEdit->setText(QString::number(DEFAULT_ARTKP_VC_NTOPBOTTOM));
    // Pixel format
    pixelFormatComboBox->setCurrentIndex(DEFAULT_ARTKP_PIXELFORMAT-1);
    // Undistortion mode
    if(DEFAULT_ARTKP_UNDISTMODE==ARToolKitPlus::UNDIST_NONE)
        undistModeComboBox->setCurrentIndex(0);
    else if(DEFAULT_ARTKP_UNDISTMODE==ARToolKitPlus::UNDIST_STD)
        undistModeComboBox->setCurrentIndex(1);
    else if(DEFAULT_ARTKP_UNDISTMODE==ARToolKitPlus::UNDIST_LUT)
        undistModeComboBox->setCurrentIndex(2);
    // Pose estimator
    if(DEFAULT_ARTKP_POSEESTIMATOR==ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL)
        poseEstimatorComboBox->setCurrentIndex(0);
    else if(DEFAULT_ARTKP_POSEESTIMATOR==ARToolKitPlus::POSE_ESTIMATOR_ORIGINAL_CONT)
        poseEstimatorComboBox->setCurrentIndex(1);
    else if(DEFAULT_ARTKP_POSEESTIMATOR==ARToolKitPlus::POSE_ESTIMATOR_RPP)
        poseEstimatorComboBox->setCurrentIndex(2);
    // Image processing mode
    if(DEFAULT_ARTKP_IMAGEPROCMODE==ARToolKitPlus::IMAGE_HALF_RES)
        imageProcessingModeComboBox->setCurrentIndex(0);
    else if(DEFAULT_ARTKP_IMAGEPROCMODE==ARToolKitPlus::IMAGE_FULL_RES)
        imageProcessingModeComboBox->setCurrentIndex(1);
    // Marker mode
    if(DEFAULT_ARTKP_MARKERMODE==ARToolKitPlus::MARKER_TEMPLATE)
        markerModeComboBox->setCurrentIndex(0);
    else if(DEFAULT_ARTKP_MARKERMODE==ARToolKitPlus::MARKER_ID_SIMPLE)
        markerModeComboBox->setCurrentIndex(1);
    else if(DEFAULT_ARTKP_MARKERMODE==ARToolKitPlus::MARKER_ID_BCH)
        markerModeComboBox->setCurrentIndex(2);
    // Camera calibration file
    cameraCalibrationFilenameEdit->setText(DEFAULT_ARTKP_CAMERACALIBRATIONFILE);
} // resetAllDialogToDefaults()

void TrackingSetupDialog::setupComboBoxes()
{
    // Local variables
    QStringList pixelFormat,undistMode,poseEstimator,imageProcessingMode,markerMode;
    // Fill combo boxes
    pixelFormat<<"ABGR"<<"BGRA"<<"BGR"<<"RGBA"<<"RGB"<<"RGB565"<<"LUM";
    pixelFormatComboBox->addItems(pixelFormat);
    undistMode<<"None"<<"Standard"<<"Look-up Table";
    undistModeComboBox->addItems(undistMode);
    poseEstimator<<"Original"<<"Original (cont)"<<"RPP";
    poseEstimatorComboBox->addItems(poseEstimator);
    imageProcessingMode<<"Half Resolution"<<"Full Resolution";
    imageProcessingModeComboBox->addItems(imageProcessingMode);
    markerMode<<"Template"<<"ID (Simple)"<<"ID (BCH)";
    markerModeComboBox->addItems(markerMode);
} // setupComboBoxes()

void TrackingSetupDialog::setupInputValidators()
{
    // pattWidthEdit input string validation
    QRegExp rx1("[1-9]\\d{0,1}"); // Integers 1 to 99
    QRegExpValidator *validator1 = new QRegExpValidator(rx1, 0);
    pattWidthEdit->setValidator(validator1);
    // pattHeightEdit input string validation
    QRegExp rx2("[1-9]\\d{0,1}"); // Integers 1 to 99
    QRegExpValidator *validator2 = new QRegExpValidator(rx2, 0);
    pattHeightEdit->setValidator(validator2);
    // pattSamplesEdit input string validation
    QRegExp rx3("[1-9]\\d{0,2}"); // Integers 1 to 999
    QRegExpValidator *validator3 = new QRegExpValidator(rx3, 0);
    pattSamplesEdit->setValidator(validator3);
    // maxLoadPatternsEdit input string validation
    QRegExp rx4("[0-9]\\d{0,2}"); // Integers 0 to 999
    QRegExpValidator *validator4 = new QRegExpValidator(rx4, 0);
    maxLoadPatternsEdit->setValidator(validator4);
    // maxImagePatternsEdit input string validation
    QRegExp rx5("[1-9]\\d{0,1}"); // Integers 1 to 99
    QRegExpValidator *validator5 = new QRegExpValidator(rx5, 0);
    maxImagePatternsEdit->setValidator(validator5);
    // markerBorderWidthEdit input string validation
    QDoubleValidator *validator6 = new QDoubleValidator(this);
    validator6->setDecimals(3);
    validator6->setNotation(QDoubleValidator::StandardNotation);
    markerBorderWidthEdit->setValidator(validator6);
    // autoThreshRetriesEdit input string validation
    QRegExp rx7("[1-9]\\d{0,2}"); // Integers 1 to 999
    QRegExpValidator *validator7 = new QRegExpValidator(rx7, 0);
    autoThreshRetriesEdit->setValidator(validator7);
    // thresholdEdit input string validation
    QRegExp rx8("[1-9]\\d{0,2}"); // Integers 1 to 999
    QRegExpValidator *validator8 = new QRegExpValidator(rx8, 0);
    thresholdEdit->setValidator(validator8);
    // nEBugEdit input string validation
    QRegExp rx9("[1-9]\\d{0,1}"); // Integers 1 to 99
    QRegExpValidator *validator9 = new QRegExpValidator(rx9, 0);
    nEBugEdit->setValidator(validator9);
    // vcnCornersEdit input string validation
    QRegExp rx10("[1-9]\\d{0,2}"); // Integers 0 to 999
    QRegExpValidator *validator10 = new QRegExpValidator(rx10, 0);
    vcnCornersEdit->setValidator(validator10);
    // vcnLeftRightEdit input string validation
    QRegExp rx11("[1-9]\\d{0,2}"); // Integers 0 to 999
    QRegExpValidator *validator11 = new QRegExpValidator(rx11, 0);
    vcnLeftRightEdit->setValidator(validator11);
    // vcnTopBottomEdit input string validation
    QRegExp rx12("[1-9]\\d{0,2}"); // Integers 0 to 999
    QRegExpValidator *validator12 = new QRegExpValidator(rx12, 0);
    vcnTopBottomEdit->setValidator(validator12);
} // setupInputValidators()

void TrackingSetupDialog::thresholdModeChange(QAbstractButton *input)
{
    if(input==(QAbstractButton*)thresholdModeAutoButton)
    {
       autoThreshRetriesEdit->setEnabled(true);
       thresholdEdit->setEnabled(false);
    }
    else if(input==(QAbstractButton*)thresholdModeManualButton)
    {
        autoThreshRetriesEdit->setEnabled(false);
        thresholdEdit->setEnabled(true);
    }
} // thresholdModeChange()

void TrackingSetupDialog::setVCOptionsState(bool input)
{
    vcnCornersEdit->setEnabled(input);
    vcnLeftRightEdit->setEnabled(input);
    vcnTopBottomEdit->setEnabled(input);
} // setVCOptionsState()

bool TrackingSetupDialog::validateDialog()
{
    // Local variables
    bool inputEmpty=false;
    // pattSamples must be a multiple of pattWidth and pattHeight
    if((pattSamplesEdit->text().toInt()%pattWidthEdit->text().toInt()!=0)||(pattSamplesEdit->text().toInt()%pattHeightEdit->text().toInt()!=0))
    {
        QMessageBox::warning(this->parentWidget(),"ERROR:","pattSamples must be a multiple of pattWidth and pattHeight.");
        return false;
    }
    // Check if nEBugEdit field is empty (if empty, function returns FALSE - i.e. no default value!)
    if(nEBugEdit->text().isEmpty())
    {
        QMessageBox::warning(this->parentWidget(),"ERROR:","'Number of eBugs to be tracked' field empty.");
        return false;
    }
    // Check if nEBugEdit is above MAX_NEBUGTRACK
    if(nEBugEdit->text().toInt()>MAX_NEBUGTRACK)
    {
        QMessageBox::warning(this->parentWidget(),"ERROR:","'Number of eBugs to be tracked' is greater than maximum allowable value ("+QString::number(MAX_NEBUGTRACK)+QString(")"));
        return false;
    }
    // Check is markerBorthWidth is in correct range
    if(!((markerBorderWidthEdit->text().toFloat()>0.000)&&(markerBorderWidthEdit->text().toFloat()<0.500)))
    {
        markerBorderWidthEdit->setText(QString::number(DEFAULT_ARTKP_BORDERWIDTH));
        QMessageBox::warning(this->parentWidget(),"ERROR:","Ensure 0<Marker Border Width<0.5\n\nAutomatically set to default value.");
    }
    // Check for empty inputs: if empty, set to default values
    if(pattWidthEdit->text().isEmpty())
    {
        pattWidthEdit->setText(QString::number(DEFAULT_ARTKP_PATTWIDTH));
        inputEmpty=true;
    }
    if(pattHeightEdit->text().isEmpty())
    {
        pattHeightEdit->setText(QString::number(DEFAULT_ARTKP_PATTHEIGHT));
        inputEmpty=true;
    }
    if(maxImagePatternsEdit->text().isEmpty())
    {
        maxImagePatternsEdit->setText(QString::number(DEFAULT_ARTKP_MAXIMAGEPATTERNS));
        inputEmpty=true;
    }
    if(pattSamplesEdit->text().isEmpty())
    {
        pattSamplesEdit->setText(QString::number(DEFAULT_ARTKP_PATTSAMPLES));
        inputEmpty=true;
    }
    if(maxLoadPatternsEdit->text().isEmpty())
    {
        maxLoadPatternsEdit->setText(QString::number(DEFAULT_ARTKP_MAXLOADPATTERNS));
        inputEmpty=true;
    }
    if(markerBorderWidthEdit->text().isEmpty())
    {
        markerBorderWidthEdit->setText(QString::number(DEFAULT_ARTKP_BORDERWIDTH));
        inputEmpty=true;
    }
    if(vcnCornersEdit->text().isEmpty())
    {
        vcnCornersEdit->setText(QString::number(DEFAULT_ARTKP_VC_NCORNERS));
        inputEmpty=true;
    }
    if(vcnLeftRightEdit->text().isEmpty())
    {
        vcnLeftRightEdit->setText(QString::number(DEFAULT_ARTKP_VC_NLEFTRIGHT));
        inputEmpty=true;
    }
    if(vcnTopBottomEdit->text().isEmpty())
    {
        vcnTopBottomEdit->setText(QString::number(DEFAULT_ARTKP_VC_NTOPBOTTOM));
        inputEmpty=true;
    }
    if(autoThreshRetriesEdit->text().isEmpty())
    {
        autoThreshRetriesEdit->setText(QString::number(DEFAULT_ARTKP_NUMAUTOTHRESHRETRIES));
        inputEmpty=true;
    }
    if(thresholdEdit->text().isEmpty())
    {
        thresholdEdit->setText(QString::number(DEFAULT_ARTKP_THRESHOLD));
        inputEmpty=true;
    }
    // Check if any of the inputs were empty
    if(inputEmpty)
        QMessageBox::warning(this->parentWidget(),"WARNING:","One or more inputs empty.\n\nAutomatically set to default values.");
    return true;
} // validateDialog()

void TrackingSetupDialog::setDialogState(bool trackingOn)
{
    // Set init flag
    artkpSettings.init=!trackingOn;
    // If tracking is currently ON
    if(trackingOn)
    {
        // Enable/Disable appropriate GUI items
        pattWidthEdit->setEnabled(false);
        pattHeightEdit->setEnabled(false);
        pattSamplesEdit->setEnabled(false);
        maxLoadPatternsEdit->setEnabled(false);
        maxImagePatternsEdit->setEnabled(false);
        nEBugEdit->setEnabled(false);
        applyButton->setEnabled(true);
    }
    // If tracking is currently OFF
    else
    {
        // Enable/Disable appropriate GUI items
        pattWidthEdit->setEnabled(true);
        pattHeightEdit->setEnabled(true);
        pattSamplesEdit->setEnabled(true);
        maxLoadPatternsEdit->setEnabled(true);
        maxImagePatternsEdit->setEnabled(true);
        nEBugEdit->setEnabled(true);
        applyButton->setEnabled(false);
    }
} // setDialogState()

void TrackingSetupDialog::initializeARTKPStructure()
{
    artkpSettings.pattWidth=DEFAULT_ARTKP_PATTWIDTH;
    artkpSettings.pattHeight=DEFAULT_ARTKP_PATTHEIGHT;
    artkpSettings.maxImagePatterns=DEFAULT_ARTKP_MAXIMAGEPATTERNS;
    artkpSettings.pattSamples=DEFAULT_ARTKP_PATTSAMPLES;
    artkpSettings.maxLoadPatterns=DEFAULT_ARTKP_MAXLOADPATTERNS;
    artkpSettings.trackingHistory=DEFAULT_ARTKP_TRACKINGHISTORY;
    artkpSettings.borderWidth=DEFAULT_ARTKP_BORDERWIDTH;
    artkpSettings.autoThreshold=DEFAULT_ARTKP_AUTOTHRESHOLD;
    artkpSettings.numAutoThreshRetries=DEFAULT_ARTKP_NUMAUTOTHRESHRETRIES;
    artkpSettings.threshold=DEFAULT_ARTKP_THRESHOLD;
    artkpSettings.vignettingCompensation=DEFAULT_ARTKP_VC;
    artkpSettings.vcnCorners=DEFAULT_ARTKP_VC_NCORNERS;
    artkpSettings.vcnLeftRight=DEFAULT_ARTKP_VC_NLEFTRIGHT;
    artkpSettings.vcnTopBottom=DEFAULT_ARTKP_VC_NTOPBOTTOM;
    artkpSettings.pixelFormat=DEFAULT_ARTKP_PIXELFORMAT;
    artkpSettings.undistortionMode=DEFAULT_ARTKP_UNDISTMODE;
    artkpSettings.poseEstimator=DEFAULT_ARTKP_POSEESTIMATOR;
    artkpSettings.imageProcessingMode=DEFAULT_ARTKP_IMAGEPROCMODE;
    artkpSettings.markerMode=DEFAULT_ARTKP_MARKERMODE;
    artkpSettings.cameraCalibrationFilename=DEFAULT_ARTKP_CAMERACALIBRATIONFILE;
} // initializeARTKPStructure()

int TrackingSetupDialog::getnEBugEdit()
{
    return artkpSettings.nEBug;
} // getnEBugEdit()
