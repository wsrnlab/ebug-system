/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* Controller.cpp                                                       */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "Controller.h"
#include "ImageBuffer.h"

// Qt header files
#include <QtGui>
// Qt header files
#include <QDebug>

Controller::Controller()
{
    // Initialize flags
    isCreatedProcessingThread=false;
    isCreatedControlThread=false;
} // Controller constructor

Controller::~Controller()
{
} // Controller destructor

bool Controller::connectToCamera(int deviceNumber, int imageBufferSize)
{
    // Local variables
    bool isOpened=false;
    // Store imageBufferSize in private member
    imageBufferSizeStore=imageBufferSize;
    // Create image buffer with user-defined size
    imageBuffer = new ImageBuffer(imageBufferSize);
    // Create capture thread
    captureThread = new CaptureThread(imageBuffer);
    // Attempt to connect to camera
    if((isOpened=captureThread->connectToCamera(deviceNumber)))
    {
        // Create processing thread
        processingThread = new ProcessingThread(imageBuffer,captureThread->getInputSourceWidth(),captureThread->getInputSourceHeight());
        isCreatedProcessingThread=true;
        // Create connections (1)
        if(isCreatedControlThread)
            connect(processingThread,SIGNAL(newEBugData(struct EBugData*, cv::Rect)),controlThread,SLOT(updateEBugData(struct EBugData*, cv::Rect)),Qt::UniqueConnection);
        // Start capturing frames from camera
        captureThread->start(QThread::IdlePriority);
        // Start processing captured frames
        processingThread->start(QThread::LowPriority);
    }
    else
    {
        // Delete capture thread
        deleteCaptureThread();
        // Delete image buffer
        deleteImageBuffer();
    }
    // Return camera open result
    return isOpened;
} // connectToCamera()

void Controller::disconnectCamera()
{
    // Stop processing thread
    if(processingThread->isRunning())
        stopProcessingThread();
    // Stop capture thread
    if(captureThread->isRunning())
        stopCaptureThread();
    // Disconnect connections (1)
    if(isCreatedControlThread)
        disconnect(processingThread,SIGNAL(newEBugData(struct EBugData*, cv::Rect)),controlThread,SLOT(updateEBugData(struct EBugData*, cv::Rect)));
    // Clear image buffer
    clearImageBuffer();
    // Disconnect camera
    captureThread->disconnectCamera();
    // Delete threads
    deleteCaptureThread();
    deleteProcessingThread();
    // Delete image buffer
    deleteImageBuffer();
} // disconnectCamera()

void Controller::stopCaptureThread()
{
    qDebug() << "About to stop capture thread...";
    captureThread->stopCaptureThread();
    // Take one frame off a FULL queue to allow the capture thread to finish
    if(imageBuffer->getSizeOfImageBuffer()==imageBufferSizeStore)
    {
        Mat temp;
        temp=imageBuffer->getFrame();
    }
    captureThread->wait();
    qDebug() << "Capture thread successfully stopped.";
} // stopCaptureThread()

void Controller::stopProcessingThread()
{
    qDebug() << "About to stop processing thread...";
    processingThread->stopProcessingThread();
    processingThread->wait();
    qDebug() << "Processing thread successfully stopped.";
} // stopProcessingThread()

void Controller::deleteCaptureThread()
{
    // Delete thread
    delete captureThread;
} // deleteCaptureThread()

void Controller::deleteProcessingThread()
{
    // Delete thread
    delete processingThread;
    // Set flag
    isCreatedProcessingThread=false;
} // deleteProcessingThread()

void Controller::clearImageBuffer()
{
    imageBuffer->clearBuffer();
} // clearImageBuffer()

void Controller::deleteImageBuffer()
{
    // Delete image buffer
    delete imageBuffer;
} // deleteImageBuffer()

void Controller::stopXBeeRXTXThread()
{
    qDebug() << "About to stop XBee RX/TX thread...";
    xBeeRXTXThread->stopXBeeRXTXThread();
    // Add empty packet to EMPTY queue to allow thread to finish
    if(xBee->getSizeOfPacketBuffer()==0)
        xBee->addPacket(QByteArray());
    xBeeRXTXThread->wait();
    qDebug() << "XBee RX/TX thread successfully stopped.";
} // stopXBeeRXTXThread()

void Controller::deleteXBeeRXTXThread()
{
    // Delete thread
    delete xBeeRXTXThread;
} // deleteXBeeRXTXThread()

void Controller::stopControlThread()
{
    qDebug() << "About to stop control thread...";
    controlThread->stopControlThread();
    controlThread->wait();
    qDebug() << "Control thread successfully stopped.";
} // stopControlThread()

void Controller::deleteControlThread()
{
    // Delete thread
    delete controlThread;
    // Set flag
    isCreatedControlThread=false;
} // deleteControlThread()

bool Controller::connectToXBee(int packetBufferSize, SerialPortData serialPortData)
{
    // Local variables
    bool isOpened;
    // Create XBee class with user-defined packet buffer size
    xBee = new XBee(packetBufferSize);
    // Set device name
    xBee->port->setDeviceName(serialPortData.portName);
    // Attempt to  open port
    if((isOpened=xBee->port->open(AbstractSerial::ReadWrite|AbstractSerial::Unbuffered)))
    {
        // Set serial device parameters
        xBee->port->setBaudRate(serialPortData.baudRate);
        xBee->port->setDataBits(serialPortData.dataBits);
        xBee->port->setStopBits(serialPortData.stopBits);
        xBee->port->setParity(serialPortData.parity);
        xBee->port->setFlowControl(serialPortData.flowControl);
        // Create XBeeRXTX thread
        xBeeRXTXThread = new XBeeRXTXThread(xBee);
        // Create Control thread
        controlThread = new ControlThread(xBee);
        // Set flag
        isCreatedControlThread=true;
        // Start XBeeRXTX thread
        xBeeRXTXThread->start(QThread::LowPriority);
        // Start Control thread
        controlThread->start(QThread::LowPriority);
        // Create connections (2)
        if(isCreatedProcessingThread)
            connect(processingThread,SIGNAL(newEBugData(struct EBugData*,cv::Rect)),controlThread,SLOT(updateEBugData(struct EBugData*,cv::Rect)),Qt::UniqueConnection);
        connect(xBeeRXTXThread,SIGNAL(newXBeeNodeData(XBeeNodeTable*,int)),controlThread,SLOT(updateXBeeNodeData(XBeeNodeTable*,int)),Qt::UniqueConnection);
    }
    else
    {
        // Delete xBee class
        delete xBee;
    }
    return isOpened;
} // connectToXBee()

void Controller::disconnectXBee()
{
    // Stop XBeeRXTX thread
    if(xBeeRXTXThread->isRunning())
        stopXBeeRXTXThread();
    // Stop Control thread
    if(controlThread->isRunning())
        stopControlThread();
    // Disconnect connections (2)
    disconnect(xBeeRXTXThread,SIGNAL(newXBeeNodeData(XBeeNodeTable*,int)),controlThread,SLOT(updateXBeeNodeData(XBeeNodeTable*,int)));
    if(isCreatedProcessingThread)
        disconnect(processingThread,SIGNAL(newEBugData(struct EBugData*,cv::Rect)),controlThread,SLOT(updateEBugData(struct EBugData*,cv::Rect)));
    // Close serial port (if open)
    if(xBee->port->isOpen())
        xBee->port->close();
    // Delete Control thread
    deleteControlThread();
    // Delete XBeeRXTX thread
    deleteXBeeRXTXThread();
    // Delete xBee class
    delete xBee;
} // disconnectXBee()
