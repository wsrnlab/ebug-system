/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* ControlThread.cpp                                                    */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "ControlThread.h"
// Control algorithms
#include <control_algorithms/RandomPosition.h>
#include <control_algorithms/LineFormation.h>
#include <control_algorithms/TriangleFormation.h>
#include <control_algorithms/FormationControl.h>
#include <control_algorithms/CircleFormation.h>
#include <control_algorithms/DiagonalFormation1.h>
#include <control_algorithms/DiagonalFormation2.h>
#include <control_algorithms/DominosFormation.h>
#include <control_algorithms/ECSEFormation.h>
#include <control_algorithms/PathFinding.h>
#include <control_algorithms/TargetTracking.h>

static int temp;
static int seq;
int lineOrDiagParam=21;
int circleParam=120;
#include <qapplication.h>

ControlThread::ControlThread(XBee *xBee) : QThread(), xBee(xBee)
{
    // Initialize variables
    stopped=false;
    wasStopped=false;
    threadDataReady=false;
    storeInitialAngle=true;
    controlStop=false;
    mappingXBeeNodeTableIndex=0;
    angleInitialSampleCount=0;
    clearTrail=0;
    resetCumulativeSumArrays();
    timeStep=DEFAULT_CONTROL_TIME_STEP_MS;
    initializeMappingVector();
    initializeControlDataRandomPosition();
    initializeControlDataLineFormation();
    initializeControlDataTriangleFormation();
    initializeControlDataFormationControl();
    initializeControlDataCircleFormation();
    initializeControlDataDiagonalFormation1();
    initializeControlDataDiagonalFormation2();
    initializeControlDataDominosFormation();
    initializeControlDataECSEFormation();
    initializeControlDataPathFinding();
    initializeControlDataTargetTracking();

} // ControlThread constructor
ControlThread::~ControlThread()
{
} // ControlThread destructor

void ControlThread::run()
{
    while(1)
    {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////
        /////////////////////////////////

        /////////////////////
        // PERFORM CONTROL //
        /////////////////////
        if(threadDataReady)
        {
            updateMembersMutex.lock();
            if(controlOn)
            {
                clearTrail=0;
                // Store validity of eBugData structure
                bool isEBugDataValid=validateEBugDataStructure(nTracked);
                // Create map of valid marker IDs
                for(int i=0;i<validMarkerIDs.length();i++)
                    IDs.insert(validMarkerIDs.at(i),i);

                // Clearing trackedIDs
                trackedIDs.clear();

                // Perform control on each eBug (i)
                for(int i=0;i<nNodes;i++)
                {
                    // Get index of eBug in validMarkerIDs array
                    int validIDIndex=getIndexOfValidID(eBugData[i].id,nNodes);

                    // Check validity of eBugData
                    if(isEBugDataValid&&isValidID(eBugData[i].id))
                    {
                        //qDebug("currentIndex: %d ENTER", validIDIndex);

                        // Remove tracked ID from map
                        IDs.remove(eBugData[i].id);
                        // Add tracked ID
                        trackedIDs.append(eBugData[i].id);
                        ///////////////////////////////////
                        // INSERT CONTROL ROUTINES BELOW //
                        ///////////////////////////////////
                        // Perform user-specified control
                        // (Starting with eBug with lowest TRACKED marker ID)

                        // This code line is hidden for Leon's usage.
                        if (controlIndex!=4)
                        {
                            qDebug("controlIndex=%d",controlIndex);
                        }

                        switch(controlIndex)
                        {
                        // Random Position
                        case 0:
                            randomPosition(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                           eBugData,i,validIDIndex,nTracked,currentROI,
                                           controlDataRandomPosition,
                                           controlParameters,resetControlFlag[validIDIndex]);
                            break;
                            // Line Formation
                        case 1:
                        {
                            temp=lineFormation(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                               eBugData,i,validIDIndex,nTracked,currentROI,
                                               controlDataLineFormation,
                                               controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                //ledChaser(xBee, nNodes);
                                if(seq==1)
                                {
                                    controlParameters[20]=lineOrDiagParam;
                                    controlIndex=5; //diag1
                                    seq=2;
                                }
                                if(seq==3)
                                {
                                    controlParameters[20]=lineOrDiagParam;
                                    controlIndex=6; //diag2
                                    seq=4;
                                }
                                if(seq==5)
                                {
                                    controlParameters[20]=circleParam;
                                    controlIndex=3; //circle
                                }
                                wasStopped=true;
                                for(int i=0;i<MAX_NEBUGTRACK;i++)
                                {
                                    resetControlFlag[i]=true;
                                }
                                temp=0;
                                //sleep(0.001);
                            }
                        }
                            break;
                            // Triangle Formation
                        case 2:
                        {
                            temp=triangleFormation(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                   eBugData,i,validIDIndex,nTracked,currentROI,
                                                   controlDataTriangleFormation,
                                                   controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                controlParameters[20]=lineOrDiagParam;
                                controlIndex=3;
                                wasStopped=true;
                                for(int i=0;i<MAX_NEBUGTRACK;i++)
                                {
                                    resetControlFlag[i]=true;
                                }
                                temp=0;
                                //sleep(0.001);
                            }
                        }
                            break;
                            // <user-defined control routine>
                        case 3:
                        {
                            temp=circleFormation(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                 eBugData,i,validIDIndex,nTracked,currentROI,
                                                 controlDataCircleFormation,
                                                 controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                //ledChaser(xBee, nNodes);
                                seq=1;
                                controlParameters[20]=lineOrDiagParam; // line
                                //controlParameters[20]=250; -> For triangle
                                controlIndex=1;
                                wasStopped=true;
                                for(int i=0;i<MAX_NEBUGTRACK;i++)
                                {
                                    resetControlFlag[i]=true;
                                }
                                temp=0;
                                //sleep(0.001);
                            }
                        }
                            break;
                            // <user-defined control routine>
                        case 4:
                        {
                            int returnVal = formationControl(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                             eBugData,i,validIDIndex,nTracked,
                                             controlDataFormationControl,
                                             controlParameters,resetControlFlag[validIDIndex],&clearTrail);

                            if (returnVal == 1)
                            {
                                initializeControlDataFormationControl();

                                for (int z=0;z<MAX_NEBUGTRACK;z++)
                                {
                                    resetControlFlag[z] = false;
                                }
                            }

                            break;
                        }
                            // <user-defined control routine> .etc
                        case 5:
                        {
                            temp=diagonalFormation1(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                    eBugData,i,validIDIndex,nTracked,currentROI,
                                                    controlDataDiagonalFormation1,
                                                    controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                //ledChaser(xBee, nNodes);
                                controlParameters[20]=lineOrDiagParam;
                                //controlParameters[20]=110; //Circle
                                controlIndex=1;
                                wasStopped=true;
                                for(int i=0;i<MAX_NEBUGTRACK;i++)
                                {
                                    resetControlFlag[i]=true;
                                }
                                temp=0;
                                seq=3;
                                //sleep(0.001);
                            }
                        }
                            break;
                            // <user-defined control routine>
                        case 6:
                        {
                            temp=diagonalFormation2(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                    eBugData,i,validIDIndex,nTracked,currentROI,
                                                    controlDataDiagonalFormation2,
                                                    controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                //ledChaser(xBee, nNodes);
                                controlParameters[20]=lineOrDiagParam; //For Line/Diagonal
                                // controlParameters[20]=100; // For Circle
                                controlIndex=1;
                                wasStopped=true;
                                for(int i=0;i<MAX_NEBUGTRACK;i++)
                                {
                                    resetControlFlag[i]=true;
                                }
                                temp=0;
                                seq=5;
                                //sleep(0.001);
                            }
                        }
                            break;
                            // <user-defined control routine>
                        case 7:
                        {
                            qDebug("Initial Clear ");
                            controlDataDominosFormation[1].SSD.clear();
                            qDebug("Initial Cleared ");

                            temp=dominosFormation(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                  eBugData,i,validIDIndex,nTracked,currentROI,
                                                  controlDataDominosFormation,
                                                  controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                        }
                            break;
                            // Path Finding
                        case 8:
                        {
                            if (temp == 99) break;
                            temp=pathFinding(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                        eBugData,i,validIDIndex,nTracked,currentROI,
                                        controlDataPathFinding,
                                        controlParameters,resetControlFlag[validIDIndex]);

                        }
                            break;
                        case 9:
                        {
                            temp=ecseFormation(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                               eBugData,i,validIDIndex,nTracked,currentROI,
                                               controlDataECSEFormation,
                                               controlParameters,resetControlFlag[validIDIndex],wasStopped);
                            wasStopped=false;
                            if(temp==88)
                            {
                                //ledChaser(xBee, nNodes);
                                temp=0;
                            }
                        }
                            break;
                        case 10:
                        {
                            temp=targetTracking(xBee,xBeeNodeTable[mappingVector[validIDIndex]],
                                                  eBugData,i,validIDIndex,nTracked,currentROI,
                                                  controlDataTargetTracking,
                                                  controlParameters,resetControlFlag[validIDIndex],&clearTrail);
                            wasStopped=false;
                        }
                            break;
                        }
                        ///////////////////////////////////
                        // INSERT CONTROL ROUTINES ABOVE //
                        ///////////////////////////////////
                    }
                    //////////////////////////////
                    // Draw control data on GUI //
                    //////////////////////////////
                    switch(controlIndex)
                    {
                    // Random Position
                    case 0:
                        emit drawControlDataRandomPosition(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataRandomPosition);
                        break;
                        // Line Formation
                    case 1:
                        emit drawControlDataLineFormation(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataLineFormation);
                        break;
                        // Triangle Formation
                    case 2:
                        emit drawControlDataTriangleFormation(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataTriangleFormation);
                        break;
                        // <user-defined control routine>
                    case 3:
                        emit drawControlDataCircleFormation(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataCircleFormation);
                        break;
                        // <user-defined control routine>
                    case 4:
                        emit drawControlDataFormationControl(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataFormationControl,&clearTrail);
                        break;
                        // <user-defined control routine> .etc
                    case 5:
                        emit drawControlDataDiagonalFormation1(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataDiagonalFormation1);
                        break;
                        // <user-defined control routine> .etc
                    case 6:
                        emit drawControlDataDiagonalFormation2(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataDiagonalFormation2);
                        break;
                        // <user-defined control routine> .etc
                    case 7:
                        emit drawControlDataDominosFormation(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataDominosFormation);
                        break;
                        // <user-defined control routine> .etc
                    case 8:
                        emit drawControlDataPathFinding(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataPathFinding);
                        break;
                        // <user-defined control routine> .etc
                    case 9:
                        emit drawControlDataECSEFormation(currentROI,nTracked,nNodes,trackedIDs,validMarkerIDs,controlDataECSEFormation);
                        break;
                        // <user-defined control routine> .etc
                    case 10:
                        emit drawControlDataTargetTracking(currentROI, nTracked, nNodes, trackedIDs, validMarkerIDs, controlDataTargetTracking, &clearTrail);
                        break;
                        // Target Tracking routine
                    }
                }
                /////////////////////////////////////////////////
                // Stop all non-tracked eBugs (with valid IDs) //
                /////////////////////////////////////////////////
                QList<int> indexList=IDs.values();
                for(int j=0;j<indexList.size();j++)
                {
                    // This code line is hidden for Leon's usage.
                    if (controlIndex!=4)
                    {
                        qDebug("Stop non tracked for j=%d",j);
                    }
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[indexList.at(j)]],StepperMotorStopBoth(true,true,true,0x00)));
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[indexList.at(j)]],TLC5947_SetAllRed(0xFF,0xFF)));
                }
            }
            //////////////////
            // STOP CONTROL //
            //////////////////
            else if(controlStop)
            {
                for(int i=0;i<nNodes;i++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[i]],StepperMotorStopBoth(true,true,true,0x00)));
                }
                // Reset flag
                controlStop=false;
                wasStopped=true;
                clearTrail=1;
            }
            ///////////////////
            // PERFORM TASKS //
            ///////////////////
            // Mapping
            else if(mapping&&validateEBugDataStructure(nNodes))
            {
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingXBeeNodeTableIndex],StepperMotorLeftStep(250,0,true,0x00,true,true,0x00)));
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingXBeeNodeTableIndex],StepperMotorStopBoth(true, true, true, 0x00)));
                // Rotate eBug (with index=mappingXBeeNodeTableIndex in XBee node table)
                // i.e. We rotate node with 1st entry in xBeeNodeTable, then 2nd entry, then 3rd entry etc.
                if(!storeInitialAngle)
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingXBeeNodeTableIndex],StepperMotorCWStep(250,50,true,0x00,true,true,0x00)));
                // Observe each eBug (starting from eBug with LOWEST marker ID)
                for(int i=0;i<nNodes;i++)
                {
                    // Store absolute value of angle
                    angleNormalized[i]=abs(eBugData[i].angle);

                    // Store initial angle of each eBug (get average)
                    if(storeInitialAngle)
                    {
                        // Initialize arrays
                        if((i==0)&&(angleInitialSampleCount==0))
                            resetCumulativeSumArrays();

                        // Add current angle to running total
                        if(angleInitialSampleCount<DEFAULT_MAPPING_INITIAL_ANGLE_SAMPLES)
                            angleInitialSum[i]+=angleNormalized[i];
                        // All samples taken: calculate average and store in angleInitial[] array
                        else
                        {
                            angleInitial[i]=angleInitialSum[i]/DEFAULT_MAPPING_INITIAL_ANGLE_SAMPLES;
                            // End store initial angle routine
                            if(i==(nNodes-1))
                            {
                                // Reset variables
                                angleInitialSampleCount=0;
                                storeInitialAngle=false;
                            }
                        }

                        // Increment sample count
                        if((i==(nNodes-1))&&storeInitialAngle)
                            angleInitialSampleCount++;
                    }
                    // Perform mapping
                    else
                    {
                        // Store difference between current and initial angle
                        angleDifference[i]=angleNormalized[i]-angleInitial[i];
                        // Calculate and store cumulative absolute difference
                        angleDifferenceMagSum[i]+=abs(angleDifference[i]);
                        // Check if sum has exceeded threshold
                        if(angleDifferenceMagSum[i]>DEFAULT_MAPPING_ANGLE_DIFF_MAG_SUM_THRESH)
                        {
                            // Initialize mapping vector before storing first entry
                            if(mappingXBeeNodeTableIndex==0)
                                initializeMappingVector();
                            // Update mapping vector
                            // e.g. mappingVector=[2 0 1] -> eBug with LOWEST marker ID is 3rd (last) entry in node table; eBug with HIGHEST marker ID is 2nd entry in node table etc.
                            mappingVector[i]=mappingXBeeNodeTableIndex;
                            // Stop stepping
                            for(int j=0; j<5; j++)
                            {
                                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingXBeeNodeTableIndex],StepperMotorStopBoth(true,true,true,0x00)));
                            }

                            // Mapping done
                            if(mappingXBeeNodeTableIndex==(nNodes-1))
                            {
                                // Reset flags/variables
                                storeInitialAngle=true;
                                mappingXBeeNodeTableIndex=0;
                                mapping=false;
                                // Reset cumulative sum arrays
                                resetCumulativeSumArrays();
                                // Emit signal to GUI
                                emit newMappingVector(mappingVector);
                            }
                            // Map next entry in node table
                            else
                            {
                                // Set flag (get initial angles)
                                storeInitialAngle=true;
                                // Reset cumulative sum arrays
                                resetCumulativeSumArrays();
                                // Increment index
                                mappingXBeeNodeTableIndex++;
                            }
                        }
                    }
                }
            } // Else If
            updateMembersMutex.unlock();
            // Sleep
            msleep(controlTimeStep);
        }
    }
    qDebug() << "Stopping control thread...";
} // run()

void ControlThread::stopControlThread()
{
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
} // stopControlThread()

void ControlThread::updateControlThreadData(struct ControlThreadData controlThreadDataIn)
{
    QMutexLocker locker(&updateMembersMutex);
    this->nNodes=controlThreadDataIn.nNodes;
    // Set control stop flag
    if(this->controlOn&&!controlThreadDataIn.controlOn)
        controlStop=true;
    this->controlOn=controlThreadDataIn.controlOn;
    this->controlIndex=controlThreadDataIn.controlIndex;
    this->controlTimeStep.fetchAndStoreAcquire(controlThreadDataIn.controlTimeStep);
    this->mapping=controlThreadDataIn.mapping;
    this->validMarkerIDs.clear();
    // Clear valid marker ID array before storing
    this->validMarkerIDs.clear();
    for(int i=0;i<nNodes;i++)
        this->validMarkerIDs.append(controlThreadDataIn.validMarkerIDs.at(i));
    for(int j=0;j<MAX_CONTROL_PARAMETERS;j++)
        controlParameters[j]=controlThreadDataIn.controlParams[j];
    // Set flags
    for(int i=0;i<nNodes;i++)
        resetControlFlag[i]=true;
    // Set flag to TRUE on first data update
    if(!threadDataReady)
        threadDataReady=true;
} // updateControlThreadData()

void ControlThread::updateXBeeNodeData(struct XBeeNodeTable *xBeeNodeTableData, int xBeeNodeTableIndex)
{
    QMutexLocker locker(&updateMembersMutex);
    // Save number of discovered nodes
    nNodes=xBeeNodeTableIndex;
    // Save table
    for(int i=0;i<nNodes;i++)
    {
        // Save XBee node table
        xBeeNodeTable[i].commandStatus=xBeeNodeTableData[i].commandStatus;
        xBeeNodeTable[i].MY=xBeeNodeTableData[i].MY;
        xBeeNodeTable[i].SH_high=xBeeNodeTableData[i].SH_high;
        xBeeNodeTable[i].SH_low=xBeeNodeTableData[i].SH_low;
        xBeeNodeTable[i].SL_high=xBeeNodeTableData[i].SL_high;
        xBeeNodeTable[i].SL_low=xBeeNodeTableData[i].SL_low;
        for(int j=0;j<xBeeNodeTableData[i].NI.length();j++)
            xBeeNodeTable[i].NI[j]=xBeeNodeTableData[i].NI.at(j);
        xBeeNodeTable[i].parentNetworkAddress=xBeeNodeTableData[i].parentNetworkAddress;
        xBeeNodeTable[i].deviceType=xBeeNodeTableData[i].deviceType;
        xBeeNodeTable[i].status=xBeeNodeTableData[i].status;
        xBeeNodeTable[i].profileID=xBeeNodeTableData[i].profileID;
        xBeeNodeTable[i].manufacturerID=xBeeNodeTableData[i].manufacturerID;
    }
} // updateXBeeNodeData()

void ControlThread::updateEBugData(struct EBugData *eBugDataIn, Rect currentROIIn)
{
    QMutexLocker locker(&updateMembersMutex);
    // Initialize variables
    nTracked=0;
    // Save tracking data (data is only saved after nodes are discovered)
    for(int i=0;i<nNodes;i++)
    {
        this->eBugData[i].id=eBugDataIn[i].id;
        this->eBugData[i].x=eBugDataIn[i].x;
        this->eBugData[i].y=eBugDataIn[i].y;
        this->eBugData[i].angle=eBugDataIn[i].angle;
        this->eBugData[i].cf=eBugDataIn[i].cf;
        this->eBugData[i].isTracked=eBugDataIn[i].isTracked;
        // Is eBug being tracked and has a valid ID?
        if((this->eBugData[i].isTracked)&&(isValidID(eBugData[i].id)))
            nTracked++;
    }
    // Save current ROI
    currentROI.width=currentROIIn.width;
    currentROI.height=currentROIIn.height;
    currentROI.x=currentROIIn.x;
    currentROI.y=currentROIIn.y;
} // updateEBugData()

void ControlThread::resetCumulativeSumArrays()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        angleInitialSum[i]=0;
        angleDifferenceMagSum[i]=0;
    }
} // resetCumulativeSumArrays()

void ControlThread::initializeMappingVector()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
        mappingVector[i]=-1;
} // initializeMappingVector()

bool ControlThread::validateEBugDataStructure(int n)
{
    // Local variables
    int nValidCount=0;
    // This function only returns TRUE if n CORRECT markers (specified in validMarkerIDs array) are being tracked
    for(int i=0;i<nNodes;i++)
    {
        for(int j=0;j<n;j++)
        {
            if(validMarkerIDs.at(i)==eBugData[j].id)
                nValidCount++;
        }
        // nTracked valid IDs
        if(nValidCount==n)
            return true;
    }
    return false;
} // validateEBugDataStructure()

void ControlThread::initializeControlDataRandomPosition()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataRandomPosition[i].stage=0;
        controlDataRandomPosition[i].xDesired=0;
        controlDataRandomPosition[i].yDesired=0;
        controlDataRandomPosition[i].xCurrent=0;
        controlDataRandomPosition[i].yCurrent=0;
        controlDataRandomPosition[i].angleCurrent=0;
        controlDataRandomPosition[i].angleErrTermCount=0;
        controlDataRandomPosition[i].distanceSquaredCurrent=0;
        controlDataRandomPosition[i].collisionDistanceSquaredSumCurrent=0;
        controlDataRandomPosition[i].collisionDistanceSquaredSumPrev=0;
        controlDataRandomPosition[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataRandomPosition()

void ControlThread::initializeControlDataLineFormation()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataLineFormation[i].stage=0;
        controlDataLineFormation[i].xDesired=0;
        controlDataLineFormation[i].yDesired=0;
        controlDataLineFormation[i].xCurrent=0;
        controlDataLineFormation[i].yCurrent=0;
        controlDataLineFormation[i].angleCurrent=0;
        controlDataLineFormation[i].angleErrTermCount=0;
        controlDataLineFormation[i].distanceSquaredCurrent=0;
        controlDataLineFormation[i].collisionDistanceSquaredSumCurrent=0;
        controlDataLineFormation[i].collisionDistanceSquaredSumPrev=0;
        controlDataLineFormation[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlLineFormation()

void ControlThread::initializeControlDataTriangleFormation()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataTriangleFormation[i].stage=0;
        controlDataTriangleFormation[i].xDesired=0;
        controlDataTriangleFormation[i].yDesired=0;
        controlDataTriangleFormation[i].xCurrent=0;
        controlDataTriangleFormation[i].yCurrent=0;
        controlDataTriangleFormation[i].angleCurrent=0;
        controlDataTriangleFormation[i].angleErrTermCount=0;
        controlDataTriangleFormation[i].distanceSquaredCurrent=0;
        controlDataTriangleFormation[i].collisionDistanceSquaredSumCurrent=0;
        controlDataTriangleFormation[i].collisionDistanceSquaredSumPrev=0;
        controlDataTriangleFormation[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlTriangleFormation()

void ControlThread::initializeControlDataCircleFormation()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataCircleFormation[i].stage=0;
        controlDataCircleFormation[i].xDesired=0;
        controlDataCircleFormation[i].yDesired=0;
        controlDataCircleFormation[i].xCurrent=0;
        controlDataCircleFormation[i].yCurrent=0;
        controlDataCircleFormation[i].angleCurrent=0;
        controlDataCircleFormation[i].angleErrTermCount=0;
        controlDataCircleFormation[i].distanceSquaredCurrent=0;
        controlDataCircleFormation[i].collisionDistanceSquaredSumCurrent=0;
        controlDataCircleFormation[i].collisionDistanceSquaredSumPrev=0;
        controlDataCircleFormation[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlCircleFormation()

void ControlThread::initializeControlDataDiagonalFormation1()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataDiagonalFormation1[i].stage=0;
        controlDataDiagonalFormation1[i].xDesired=0;
        controlDataDiagonalFormation1[i].yDesired=0;
        controlDataDiagonalFormation1[i].xCurrent=0;
        controlDataDiagonalFormation1[i].yCurrent=0;
        controlDataDiagonalFormation1[i].angleCurrent=0;
        controlDataDiagonalFormation1[i].angleErrTermCount=0;
        controlDataDiagonalFormation1[i].distanceSquaredCurrent=0;
        controlDataDiagonalFormation1[i].collisionDistanceSquaredSumCurrent=0;
        controlDataDiagonalFormation1[i].collisionDistanceSquaredSumPrev=0;
        controlDataDiagonalFormation1[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataDiagonalFormation1()

void ControlThread::initializeControlDataDiagonalFormation2()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataDiagonalFormation2[i].stage=0;
        controlDataDiagonalFormation2[i].xDesired=0;
        controlDataDiagonalFormation2[i].yDesired=0;
        controlDataDiagonalFormation2[i].xCurrent=0;
        controlDataDiagonalFormation2[i].yCurrent=0;
        controlDataDiagonalFormation2[i].angleCurrent=0;
        controlDataDiagonalFormation2[i].angleErrTermCount=0;
        controlDataDiagonalFormation2[i].distanceSquaredCurrent=0;
        controlDataDiagonalFormation2[i].collisionDistanceSquaredSumCurrent=0;
        controlDataDiagonalFormation2[i].collisionDistanceSquaredSumPrev=0;
        controlDataDiagonalFormation2[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataDiagonalFormation2()

void ControlThread::initializeControlDataDominosFormation()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataDominosFormation[i].stage=0;
        controlDataDominosFormation[i].xDesired=0;
        controlDataDominosFormation[i].yDesired=0;
        controlDataDominosFormation[i].xCurrent=0;
        controlDataDominosFormation[i].yCurrent=0;
        controlDataDominosFormation[i].angleCurrent=0;
        controlDataDominosFormation[i].angleErrTermCount=0;
        controlDataDominosFormation[i].distanceSquaredCurrent=0;
        controlDataDominosFormation[i].collisionDistanceSquaredSumCurrent=0;
        controlDataDominosFormation[i].collisionDistanceSquaredSumPrev=0;
        controlDataDominosFormation[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataDominosFormation()

void ControlThread::initializeControlDataFormationControl()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataFormationControl[i].stage=0;
        controlDataFormationControl[i].xDesired=0;
        controlDataFormationControl[i].yDesired=0;
        controlDataFormationControl[i].angleDesired=0;
        controlDataFormationControl[i].xCurrent=0;
        controlDataFormationControl[i].yCurrent=0;
        controlDataFormationControl[i].angleCurrent=0;
        controlDataFormationControl[i].ID = -1;
        controlDataFormationControl[i].distanceToTargetSquared=1000;
        controlDataFormationControl[i].leader1=-1;
        controlDataFormationControl[i].angleConstraint1=0;
        controlDataFormationControl[i].distanceConstraint1 = 0;
        controlDataFormationControl[i].leader2=-1;
        controlDataFormationControl[i].angleConstraint2=0;
        controlDataFormationControl[i].distanceConstraint2 = 0;
        controlDataFormationControl[i].hasStopped=0;
        controlDataFormationControl[i].isObstacle=0;
    }
} // initializeControlDataFormationControl()

void ControlThread::initializeControlDataPathFinding()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataPathFinding[i].stage=0;
        controlDataPathFinding[i].xDesired=0;
        controlDataPathFinding[i].yDesired=0;
        controlDataPathFinding[i].xCurrent=0;
        controlDataPathFinding[i].yCurrent=0;
        controlDataPathFinding[i].angleCurrent=0;
        controlDataPathFinding[i].angleErrTermCount=0;
        controlDataPathFinding[i].distanceSquaredCurrent=0;
        controlDataPathFinding[i].collisionDistanceSquaredSumCurrent=0;
        controlDataPathFinding[i].collisionDistanceSquaredSumPrev=0;
        controlDataPathFinding[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataPathFinding()

void ControlThread::initializeControlDataECSEFormation()
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        controlDataECSEFormation[i].stage=0;
        controlDataECSEFormation[i].xDesired=0;
        controlDataECSEFormation[i].yDesired=0;
        controlDataECSEFormation[i].xCurrent=0;
        controlDataECSEFormation[i].yCurrent=0;
        controlDataECSEFormation[i].angleCurrent=0;
        controlDataECSEFormation[i].angleErrTermCount=0;
        controlDataECSEFormation[i].distanceSquaredCurrent=0;
        controlDataECSEFormation[i].collisionDistanceSquaredSumCurrent=0;
        controlDataECSEFormation[i].collisionDistanceSquaredSumPrev=0;
        controlDataECSEFormation[i].collisionDistanceSquaredSumDifference=0;
    }
} // initializeControlDataECSEFormation()

void ControlThread::initializeControlDataTargetTracking()
{
    for(int i=0; i<MAX_NEBUGTRACK;i++){
        controlDataTargetTracking[i].xDesired=0;
    }
}

bool ControlThread::isValidID(int id)
{
    for(int i=0;i<MAX_NEBUGTRACK;i++)
    {
        if(id==validMarkerIDs.at(i))
            return true;
    }
    return false;
} // isValidID()

int ControlThread::getIndexOfValidID(int id, int n)
{
    for(int i=0;i<n;i++)
    {
        if(validMarkerIDs.at(i)==id)
            return i;
    }
    return -1;
} // getIndexOfValidID()

int ControlThread::ledChaser(XBee *xBee, int nNodes)
{
    int iIndex;
        for (int loop=0;loop<20;loop++)
        {
            for(int count=0;count<17;count++)
            {
                for(int j=0;j<nNodes;j++)
                {
                    iIndex=getIndexOfValidID(eBugData[j].id,nNodes);
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[iIndex]],TLC5947_SetRed(count,4095,true,0x00)));
                }

            }
        }
//    for(int loop=0;loop<2;loop++)
//    {
//        for(int count=0;count<50;count++)
//        {
//            for(int j=0;j<nNodes;j++)
//            {
//                iIndex=getIndexOfValidID(eBugData[j].id,nNodes);
//                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[iIndex]],TLC5947_SetAllRed(4095-80*count,true,0x00)));
//            }
//        }
//        for(int count=0;count<50;count++)
//        {
//            for(int j=0;j<nNodes;j++)
//            {
//                iIndex=getIndexOfValidID(eBugData[j].id,nNodes);
//                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[iIndex]],TLC5947_SetAllRed(80*count,true,0x00)));
//            }
//        }
//    }
    for (int count=0;count<10;count++)
    {
        for(int j=0;j<nNodes;j++)
        {
            iIndex=getIndexOfValidID(eBugData[j].id,nNodes);
            xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable[mappingVector[iIndex]],TLC5947_SetAllOff(true,0x00)));
        }
    }
    return 0;
}

void sleepThread(unsigned long msecs) {
  QWaitCondition w;
  QMutex sleepmutex;
  sleepmutex.lock();
  w.wait(&sleepmutex, msecs);
  sleepmutex.unlock();
}
