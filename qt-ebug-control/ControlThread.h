/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* ControlThread.h                                                      */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef CONTROLTHREAD_H
#define CONTROLTHREAD_H

#include "Structures.h"

// Qt header files
#include <QThread>
#include <QtGui>
// OpenCV header files
#include <opencv/cv.h>
#include <opencv/highgui.h>
// Configuration header file
#include "Config.h"
// eBug-II API
#include <eBugAPI.h>
// XBee class
#include <XBee.h>
// Control algorithms
#include <control_algorithms/RandomPosition.h>
#include <control_algorithms/LineFormation.h>
#include <control_algorithms/TriangleFormation.h>
#include <control_algorithms/FormationControl.h>
#include <control_algorithms/CircleFormation.h>
#include <control_algorithms/DiagonalFormation1.h>
#include <control_algorithms/DiagonalFormation2.h>
#include <control_algorithms/DominosFormation.h>
#include <control_algorithms/ECSEFormation.h>
#include <control_algorithms/PathFinding.h>
#include <control_algorithms/TargetTracking.h>


using namespace cv;

class XBee;

class ControlThread : public QThread
{
    Q_OBJECT

public:
    ControlThread(XBee *xBee);
    ~ControlThread();
    void stopControlThread();
private:
    XBee *xBee;
    volatile bool stopped;
    volatile bool threadDataReady;
    QMutex stoppedMutex;
    QMutex updateMembersMutex;
    int mappingVector[MAX_NEBUGTRACK];
    int angleNormalized[MAX_NEBUGTRACK];
    int angleInitial[MAX_NEBUGTRACK];
    int angleInitialSum[MAX_NEBUGTRACK];
    int angleDifference[MAX_NEBUGTRACK];
    int angleDifferenceMagSum[MAX_NEBUGTRACK];
    XBeeNodeTable xBeeNodeTable[MAX_NEBUGTRACK];
    EBugData eBugData[MAX_NEBUGTRACK];
    int nTracked;

    ControlDataRandomPosition controlDataRandomPosition[MAX_NEBUGTRACK];
    ControlDataLineFormation controlDataLineFormation[MAX_NEBUGTRACK];
    ControlDataTriangleFormation controlDataTriangleFormation[MAX_NEBUGTRACK];
    ControlDataFormationControl controlDataFormationControl[MAX_NEBUGTRACK];
    ControlDataCircleFormation controlDataCircleFormation[MAX_NEBUGTRACK];
    ControlDataDiagonalFormation1 controlDataDiagonalFormation1[MAX_NEBUGTRACK];
    ControlDataDiagonalFormation2 controlDataDiagonalFormation2[MAX_NEBUGTRACK];
    ControlDataDominosFormation controlDataDominosFormation[MAX_NEBUGTRACK];
    ControlDataECSEFormation controlDataECSEFormation[MAX_NEBUGTRACK];
    ControlDataPathFinding controlDataPathFinding[MAX_NEBUGTRACK];
    ControlDataTargetTracking controlDataTargetTracking[MAX_NEBUGTRACK];

    Rect currentROI;
    int mappingXBeeNodeTableIndex;
    bool storeInitialAngle;
    int angleInitialSampleCount;
    void resetCumulativeSumArrays();
    void initializeMappingVector();
    bool validateEBugDataStructure(int);
    bool resetControlFlag[MAX_NEBUGTRACK];
    bool isValidID(int);
    int getIndexOfValidID(int, int);
    int ledChaser(XBee *, int);

    void initializeControlDataRandomPosition();
    void initializeControlDataLineFormation();
    void initializeControlDataTriangleFormation();
    void initializeControlDataFormationControl();
    void initializeControlDataCircleFormation();
    void initializeControlDataDiagonalFormation1();
    void initializeControlDataDiagonalFormation2();
    void initializeControlDataDominosFormation();
    void initializeControlDataECSEFormation();
    void initializeControlDataPathFinding();
    void initializeControlDataTargetTracking();

    bool controlStop;
    bool wasStopped;
    // Control thread data
    int controlIndex;
    QAtomicInt controlTimeStep;
    double controlParameters[MAX_CONTROL_PARAMETERS];
    int nNodes;
    bool controlOn;
    bool eBugDiscovery;
    bool mapping;
    int timeStep;
    QList<int> validMarkerIDs;
    QList<int> trackedIDs;
    QMap<int,int> IDs;
    bool clearTrail;
protected:
    void run();
private slots:
    void updateControlThreadData(struct ControlThreadData);
    void updateXBeeNodeData(struct XBeeNodeTable*,int);
    void updateEBugData(struct EBugData*,cv::Rect);
signals:
    void newMappingVector(int *);
    void drawControlDataRandomPosition(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataRandomPosition*);
    void drawControlDataLineFormation(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataLineFormation*);
    void drawControlDataTriangleFormation(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataTriangleFormation*);
    void drawControlDataFormationControl(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataFormationControl*, bool*);
    void drawControlDataCircleFormation(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataCircleFormation*);
    void drawControlDataDiagonalFormation1(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataDiagonalFormation1*);
    void drawControlDataDiagonalFormation2(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataDiagonalFormation2*);
    void drawControlDataDominosFormation(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataDominosFormation*);
    void drawControlDataECSEFormation(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataECSEFormation*);
    void drawControlDataPathFinding(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataPathFinding*);
    void drawControlDataTargetTracking(cv::Rect, int, int, QList<int>, QList<int>, struct ControlDataTargetTracking*, bool*);


};

#endif // CONTROLTHREAD_H
