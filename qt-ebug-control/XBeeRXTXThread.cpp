/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* XBeeRXTXThread.cpp                                                   */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "XBeeRXTXThread.h"

// Qt header files
#include <QDebug>

XBeeRXTXThread::XBeeRXTXThread(XBee *xBee) : QThread(), xBee(xBee)
{
    // Initialize variables
    stopped=false;
    xBeeNodeTableIndex=0;
    // Connect serial readyRead() signal to slot
    connect(this->xBee->port, SIGNAL(readyRead()), this, SLOT(slotRead()));
} // XBeeRXTXThread constructor

XBeeRXTXThread::~XBeeRXTXThread()
{
} // XBeeRXTXThread destructor

void XBeeRXTXThread::run()
{
    while(1)
    {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////
        /////////////////////////////////
        // WAIT for packet from queue and send (blocking)
        xBee->port->write(xBee->getPacket());
        // Emit signal
        emit xBeeRXTXThreadLoopEnd(xBee->getSizeOfPacketBuffer());
        // Sleep
        // i.e. send one packet every 1ms
        msleep(1);
    }
    qDebug() << "Stopping XBee RX/TX thread...";
} // run()

void XBeeRXTXThread::stopXBeeRXTXThread()
{
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
} // stopXBeeRXTXThread()

void XBeeRXTXThread::slotRead()
{
    // Read all bytes available
    int bytesAvail=xBee->port->bytesAvailable();
    QByteArray temp = xBee->port->read(bytesAvail);
    // Take action
    for(int i=0;i<temp.length();i++)
        readXBeePacket((unsigned char)temp.at(i));
} // slotRead()

void XBeeRXTXThread::readXBeePacket(unsigned char data)
{
    // Local variables
    static unsigned char XBeeRXFrame[MAX_XBEE_RXPACKET_LENGTH-4];
    static int index=0;
    static int checksumTotal=0;
    static unsigned char lengthMSB=0;
    static unsigned char lengthLSB=0;
    static int packetLength=0;
    static unsigned char apiID=0;
    // Point pointer to start of eBug Packet
    //static unsigned char *ptr=&XBeeRXFrame[11];
    // New packet start before previous packeted completed: discard previous packet and start over
    if((index>0)&&(data==XBEE_START_DELIMITER))
    {
        index=0;
        checksumTotal=0;
    }
    // Checksum includes all bytes starting with API ID
    if(index>=API_ID_INDEX)
        checksumTotal+=data;
    // Read packet
    switch(index)
    {
        case 0:
            if(data==XBEE_START_DELIMITER)
                index++;
            break;
        case 1:
            lengthMSB=data;
            index++;
            break;
        case 2:
            lengthLSB=data;
            index++;
            // Save packet length
            packetLength=((lengthMSB<<8)&0xFF)+(lengthLSB&0xFF);
            break;
        case 3:
            apiID=data;
            index++;
            break;
        default:
            // ABORT: Maximum packet size exceeded
            if(index>MAX_XBEE_RXPACKET_LENGTH)
            {
                // Reset variables
                index=0;
                checksumTotal=0;
                break;
            }
            // END OF PACKET: Verify checksum
            if(index==(packetLength+3))
            {
                // Only take action if checksum passes
                if((checksumTotal&0xFF)==0xFF)
                {
                    // ZigBee Receive Packet
                    if(apiID==0x90)
                    {
                        // Handle eBug Packet
                    }
                    // AT Command Response
                    else if(apiID==0x88)
                    {
                        handleXBeeATCommandResponse(XBeeRXFrame);
                    }
                }
                else
                    qDebug()<<"ERROR: XBee checksum fail.";
                // Reset variables
                index=0;
                checksumTotal=0;
            }
            // SAVE PACKET: Starting from 64-bit address (MSB)
            else
            {
                XBeeRXFrame[index-4]=data;
                index++;
            }
            break;
    }
} // readXBeePacket()

void XBeeRXTXThread::handleXBeeATCommandResponse(unsigned char *XBeeRXFrame)
{
    // NODE DISCOVERY
    if((XBeeRXFrame[1]==0x4e)&&(XBeeRXFrame[2]==0x44))
    {
        // Acquire MUTEX
        protectXBeeNodeTable.lock();
        // Local variables
        int n=0;
        // Only save data if xBeeNodeTableIndex value is valid (save data for MAX_NEBUGTRACK number of nodes only)
        if(xBeeNodeTableIndex<MAX_NEBUGTRACK)
        {
            // Save command status in xBeeNodeTable structure regardless of value
            xBeeNodeTable[xBeeNodeTableIndex].commandStatus=XBeeRXFrame[3];
            // Command Status
            switch(XBeeRXFrame[3])
            {
                // OK
                case 0x00:
                    // Save node addresses in xBeeNodeTable structure
                    xBeeNodeTable[xBeeNodeTableIndex].MY=(XBeeRXFrame[4]<<8)|XBeeRXFrame[5];
                    xBeeNodeTable[xBeeNodeTableIndex].SH_high=(XBeeRXFrame[6]<<8)|XBeeRXFrame[7];
                    xBeeNodeTable[xBeeNodeTableIndex].SH_low=(XBeeRXFrame[8]<<8)|XBeeRXFrame[9];
                    xBeeNodeTable[xBeeNodeTableIndex].SL_high=(XBeeRXFrame[10]<<8)|XBeeRXFrame[11];
                    xBeeNodeTable[xBeeNodeTableIndex].SL_low=(XBeeRXFrame[12]<<8)|XBeeRXFrame[13];
                    // Save NI string in xBeeNodeTable structure: [Default: (n=1) XBeeRXFrame[14]=0x20, XBeeRXFrame[15]=0x00]
                    xBeeNodeTable[xBeeNodeTableIndex].NI.clear();
                    while(XBeeRXFrame[14+n]!=0x00)
                    {
                        xBeeNodeTable[xBeeNodeTableIndex].NI.append(XBeeRXFrame[14+n]);
                        n++;
                    }
                    // Save remaining data in xBeeNodeTable structure
                    xBeeNodeTable[xBeeNodeTableIndex].parentNetworkAddress=(XBeeRXFrame[14+n+1]<<8)|XBeeRXFrame[14+n+2];
                    xBeeNodeTable[xBeeNodeTableIndex].deviceType=XBeeRXFrame[14+n+3];
                    xBeeNodeTable[xBeeNodeTableIndex].status=XBeeRXFrame[14+n+4];
                    xBeeNodeTable[xBeeNodeTableIndex].profileID=(XBeeRXFrame[14+n+5]<<8)|XBeeRXFrame[14+n+6];
                    xBeeNodeTable[xBeeNodeTableIndex].manufacturerID=(XBeeRXFrame[14+n+7]<<8)|XBeeRXFrame[14+n+8];
                    break;
                // ERROR
                case 0x01:
                    break;
                // INVALID COMMAND
                case 0x02:
                    break;
                // INVALID PARAMETER
                case 0x03:
                    break;
                // TX FAILURE
                case 0x04:
                    break;
            }
            // Increment index
            xBeeNodeTableIndex++;
            // Release MUTEX
            protectXBeeNodeTable.unlock();
        }
    }
} // handleXBeeATCommandResponse()

void XBeeRXTXThread::xBeeNDTimeout()
{
    // Wait for current node discovery iteration to finish (if in progress)
    QMutexLocker locker(&protectXBeeNodeTable);
    // Send data to controlThread and GUI thread
    emit newXBeeNodeData(xBeeNodeTable,xBeeNodeTableIndex);
    // Reset index
    xBeeNodeTableIndex=0;
} // xBeeNDTimeout()
