/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* ProcessingThread.cpp                                                 */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "ImageBuffer.h"
#include "ProcessingThread.h"
#include "MatToQImage.h"

// Qt header files
#include <QDebug>
// OpenCV header files
#include <opencv/cv.h>
#include <opencv/highgui.h>
// ARToolKitPlus
#include <ARToolKitPlus/TrackerSingleMarker.h>
#include <ARToolKitPlus/TrackerMultiMarker.h>

using ARToolKitPlus::TrackerMultiMarker;
using ARToolKitPlus::TrackerSingleMarker;

int struct_cmp_by_markerID(const void *, const void *);

ProcessingThread::ProcessingThread(ImageBuffer *imageBuffer, int inputSourceWidth, int inputSourceHeight)
                                   : QThread(), imageBuffer(imageBuffer), inputSourceWidth(inputSourceWidth),
                                   inputSourceHeight(inputSourceHeight)
{
    // Initialize variables
    stopped=false;
    sampleNo=0;
    fpsSum=0;
    avgFPS=0;
    fps.clear();
    // Initialize currentROI variable
    currentROI=Rect(0,0,inputSourceWidth,inputSourceHeight);
    // Store original ROI
    originalROI=currentROI;
} // ProcessingThread constructor

ProcessingThread::~ProcessingThread()
{
} // ProcessingThread destructor

void ProcessingThread::run()
{
    while(1)
    {
        /////////////////////////////////
        // Stop thread if stopped=TRUE //
        /////////////////////////////////
        stoppedMutex.lock();
        if (stopped)
        {
            stopped=false;
            stoppedMutex.unlock();
            break;
        }
        stoppedMutex.unlock();
        /////////////////////////////////
        /////////////////////////////////

        // Save processing time
        processingTime=t.elapsed();
        // Start timer (used to calculate processing rate)
        t.start();
        // Get frame from queue, store in currentFrame, set ROI
        currentFrame=Mat(imageBuffer->getFrame(),currentROI);

        updateMembersMutex.lock();
        ///////////////////
        // PERFORM TASKS //
        ///////////////////
        if(resetROIFlag)
            resetROI();
        else if(setROIFlag)
            setROI();
        ////////////////////////////////////
        // PERFORM IMAGE PROCESSING BELOW //
        ////////////////////////////////////
        else
        {
            // Grayscale conversion
            if(grayscaleOn)
                cvtColor(currentFrame,currentFrameGrayscale,CV_BGR2GRAY);
            // Smooth (in-place operations)
            if(smoothOn)
            {
                if(grayscaleOn)
                {
                    switch(smoothType)
                    {
                        // BLUR
                        case 0:
                            blur(currentFrameGrayscale,currentFrameGrayscale,Size(smoothParam1,smoothParam2));
                            break;
                        // GAUSSIAN
                        case 1:
                            GaussianBlur(currentFrameGrayscale,currentFrameGrayscale,Size(smoothParam1,smoothParam2),smoothParam3,smoothParam4);
                            break;
                        // MEDIAN
                        case 2:
                            medianBlur(currentFrameGrayscale,currentFrameGrayscale,smoothParam1);
                            break;
                    }
                }
                else
                {
                    switch(smoothType)
                    {
                        // BLUR
                        case 0:
                            blur(currentFrame,currentFrame,Size(smoothParam1,smoothParam2));
                            break;
                        // GAUSSIAN
                        case 1:
                            GaussianBlur(currentFrame,currentFrame,Size(smoothParam1,smoothParam2),smoothParam3,smoothParam4);
                            break;
                        // MEDIAN
                        case 2:
                            medianBlur(currentFrame,currentFrame,smoothParam1);
                            break;
                    }
                }
            }
            // Dilate
            if(dilateOn)
            {
                if(grayscaleOn)
                    dilate(currentFrameGrayscale,currentFrameGrayscale,Mat(),Point(-1,-1),dilateNumberOfIterations);
                else
                    dilate(currentFrame,currentFrame,Mat(),Point(-1,-1),dilateNumberOfIterations);
            }
            // Erode
            if(erodeOn)
            {
                if(grayscaleOn)
                    erode(currentFrameGrayscale,currentFrameGrayscale,Mat(),Point(-1,-1),erodeNumberOfIterations);
                else
                    erode(currentFrame,currentFrame,Mat(),Point(-1,-1),erodeNumberOfIterations);
            }
            //////////////////////////////////////
            // Find markers using ARToolKitPlus //
            //////////////////////////////////////
            if(trackingOn)
            {
                // Reset count
                validMarkerCount=0;
                // Pre-fill eBugData structure
                for(int i=0;i<nEBug;i++)
                {
                    eBugData[i].isTracked=false;
                    eBugData[i].id=4096;
                }
                if(!grayscaleOn)
                {
                    // Copy current frame to resized image for input into ARToolKitPlus
                    currentFrame.copyTo(currentFrameResizedToROI);
                    // Note: arDetectMarker() and arDetectMarkerLite() return 0 if any markers are detected
                    if(trackingHistory)
                        arDetectReturnValue=tracker->arDetectMarker((unsigned char*)currentFrameResizedToROI.data,tracker->getThreshold(),&marker_info,&marker_num);
                    else
                        arDetectReturnValue=tracker->arDetectMarkerLite((unsigned char*)currentFrameResizedToROI.data,tracker->getThreshold(),&marker_info,&marker_num);
                    // If markers were found...
                    if(arDetectReturnValue==0)
                    {
                        for(int i=0;i<marker_num;i++)
                        {
                            // Check if BCH marker ID is valid
                            if((marker_info[i].id>=0)&&(marker_info[i].id<=4095))
                            {
                                // Copy tracking data into EBugData structure
                                eBugData[validMarkerCount].id=marker_info[i].id;
                                eBugData[validMarkerCount].x=(int)(marker_info[i].pos[0]+0.5);
                                eBugData[validMarkerCount].y=(int)(marker_info[i].pos[1]+0.5);
                                eBugData[validMarkerCount].angle=ARTKPGetMarkerAngle(marker_info[i]);
                                eBugData[validMarkerCount].cf=marker_info[i].cf;
                                eBugData[validMarkerCount].isTracked=true;
                                // Draw box around marker
                                ARTKPDrawBox(currentFrame,marker_info[i]);
                                // Draw marker ID
                                sprintf(idString,"%d",marker_info[i].id);
                                putText(currentFrame,
                                        idString,Point(marker_info[i].pos[0],marker_info[i].pos[1]),
                                        FONT_HERSHEY_PLAIN,2.0,Scalar(0,255,0),2);
                                // Increment count
                                validMarkerCount++;
                            }
                        }
                    }
                }
                else
                {
                    // Copy current frame to resized image for input into ARToolKitPlus
                    currentFrameGrayscale.copyTo(currentFrameResizedToROIGrayscale);
                    // Note: arDetectMarker() and arDetectMarkerLite() return 0 if any markers are detected
                    if(trackingHistory)
                        arDetectReturnValue=tracker->arDetectMarker((unsigned char*)currentFrameResizedToROIGrayscale.data,tracker->getThreshold(),&marker_info,&marker_num);
                    else
                        arDetectReturnValue=tracker->arDetectMarkerLite((unsigned char*)currentFrameResizedToROIGrayscale.data,tracker->getThreshold(),&marker_info,&marker_num);
                    // If markers were found...
                    if(arDetectReturnValue==0)
                    {
                        for(int i=0;i<marker_num;i++)
                        {
                            // Check if BCH marker ID is valid
                            if((marker_info[i].id>=0)&&(marker_info[i].id<=4095))
                            {
                                // Copy tracking data into EBugData structure
                                eBugData[validMarkerCount].id=marker_info[i].id;
                                eBugData[validMarkerCount].x=(int)(marker_info[i].pos[0]+0.5);
                                eBugData[validMarkerCount].y=(int)(marker_info[i].pos[1]+0.5);
                                eBugData[validMarkerCount].angle=ARTKPGetMarkerAngle(marker_info[i]);
                                eBugData[validMarkerCount].cf=marker_info[i].cf;
                                // Draw box around marker
                                ARTKPDrawBox(currentFrameGrayscale,marker_info[i]);
                                // Draw marker ID
                                sprintf(idString,"%d",marker_info[i].id);
                                putText(currentFrameGrayscale,
                                        idString,Point(marker_info[i].pos[0],marker_info[i].pos[1]),
                                        FONT_HERSHEY_PLAIN,2.0,Scalar(0,255,0),2);
                                // Increment count
                                validMarkerCount++;
                            }
                        }
                    }
                }
                // Only sort data if valid markers are found
                if(validMarkerCount>0)
                {
                    // Sort eBugData array in ascending marker ID order
                    size_t structs_len = (validMarkerCount*sizeof(eBugData[0])) / sizeof(struct EBugData);
                    qsort(eBugData, structs_len, sizeof(struct EBugData), struct_cmp_by_markerID);
                }
                // Emit data
                emit newEBugData(eBugData,currentROI);
            }
            // Flip
            if(flipOn)
            {
                if(grayscaleOn)
                    flip(currentFrameGrayscale,currentFrameGrayscale,flipCode);
                else
                    flip(currentFrame,currentFrame,flipCode);
            }
        }
        ////////////////////////////////////
        // PERFORM IMAGE PROCESSING ABOVE //
        ////////////////////////////////////

        //// Convert Mat to QImage: Show grayscale frame [if Grayscale mode is ON]
        if(grayscaleOn)
            frame=MatToQImage(currentFrameGrayscale);
        //// Convert Mat to QImage: Show BGR frame
        else
            frame=MatToQImage(currentFrame);
        updateMembersMutex.unlock();

        // Update statistics
        updateFPS(processingTime);
        currentSizeOfBuffer=imageBuffer->getSizeOfImageBuffer();
        // Inform controller of new frame (QImage)
        emit newFrame(frame);
    }
    qDebug() << "Stopping processing thread...";
}

void ProcessingThread::updateFPS(int timeElapsed)
{
    // Add instantaneous FPS value to queue
    if(timeElapsed>0)
    {
        fps.enqueue((int)1000/timeElapsed);
        // Increment sample number
        sampleNo++;
    }
    // Maximum size of queue is DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH
    if(fps.size()>DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH)
        fps.dequeue();
    // Update FPS value every DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH samples
    if((fps.size()==DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH)&&(sampleNo==DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH))
    {
        // Empty queue and store sum
        while(!fps.empty())
            fpsSum+=fps.dequeue();
        avgFPS=fpsSum/DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH; // Calculate average FPS
        fpsSum=0; // Reset sum
        sampleNo=0; // Reset sample number
    }
} // updateFPS()

void ProcessingThread::stopProcessingThread()
{
    stoppedMutex.lock();
    stopped=true;
    stoppedMutex.unlock();
} // stopProcessingThread()

void ProcessingThread::setROI()
{
    // Save selection as new (current) ROI
    currentROI=selectionBox;
    qDebug() << "ROI successfully SET.";
    // Resize images
    currentFrameResizedToROI.create(Size(currentROI.width,currentROI.height),CV_8UC3);
    currentFrameResizedToROIGrayscale.create(Size(currentROI.width,currentROI.height),CV_8UC1);
    // Reset setROIOn flag to FALSE
    setROIFlag=false;
} // setROI()

void ProcessingThread::resetROI()
{
    // Reset ROI to original
    currentROI=originalROI;
    qDebug() << "ROI successfully RESET.";
    // Resize images
    currentFrameResizedToROI.create(Size(currentROI.width,currentROI.height),CV_8UC3);
    currentFrameResizedToROIGrayscale.create(Size(currentROI.width,currentROI.height),CV_8UC1);
    // Reset resetROIOn flag to FALSE
    resetROIFlag=false;
} // resetROI()

void ProcessingThread::updateProcessingThreadData(struct ProcessingThreadData processingThreadData)
{
    QMutexLocker locker(&updateMembersMutex);
    this->trackingOn=processingThreadData.trackingOn;
    this->grayscaleOn=processingThreadData.grayscaleOn;
    this->smoothOn=processingThreadData.smoothOn;
    this->dilateOn=processingThreadData.dilateOn;
    this->erodeOn=processingThreadData.erodeOn;
    this->flipOn=processingThreadData.flipOn;
} // updateProcessingThreadData()

void ProcessingThread::updateImageProcessingSettings(struct ImageProcessingSettings imageProcessingSettings)
{
    QMutexLocker locker(&updateMembersMutex);
    this->smoothType=imageProcessingSettings.smoothType;
    this->smoothParam1=imageProcessingSettings.smoothParam1;
    this->smoothParam2=imageProcessingSettings.smoothParam2;
    this->smoothParam3=imageProcessingSettings.smoothParam3;
    this->smoothParam4=imageProcessingSettings.smoothParam4;
    this->dilateNumberOfIterations=imageProcessingSettings.dilateNumberOfIterations;
    this->erodeNumberOfIterations=imageProcessingSettings.erodeNumberOfIterations;
    this->flipCode=imageProcessingSettings.flipCode;
} // updateImageProcessingSettings()

void ProcessingThread::updateTaskData(struct TaskData taskData)
{
    QMutexLocker locker(&updateMembersMutex);
    this->setROIFlag=taskData.setROIFlag;
    this->resetROIFlag=taskData.resetROIFlag;
    this->selectionBox.x=taskData.selectionBox.left();
    this->selectionBox.y=taskData.selectionBox.top();
    this->selectionBox.width=taskData.selectionBox.width();
    this->selectionBox.height=taskData.selectionBox.height();
} // updateTaskData()

int ProcessingThread::getAvgFPS()
{
    return avgFPS;
} // getAvgFPS()

int ProcessingThread::getCurrentSizeOfBuffer()
{
    return currentSizeOfBuffer;
} // getCurrentSizeOfBuffer()

Rect ProcessingThread::getCurrentROI()
{
    return currentROI;
} // getCurrentROI();

double ProcessingThread::ARTKPGetMarkerAngle(ARToolKitPlus::ARMarkerInfo &marker_info)
{
    switch(marker_info.dir)
    {
        case 0:
            return (-180/PI)*atan2(marker_info.vertex[1][1]-marker_info.vertex[2][1],marker_info.vertex[1][0]-marker_info.vertex[2][0]);
        case 1:
            return (-180/PI)*atan2(marker_info.vertex[0][1]-marker_info.vertex[1][1],marker_info.vertex[0][0]-marker_info.vertex[1][0]);
        case 2:
            return (-180/PI)*atan2(marker_info.vertex[3][1]-marker_info.vertex[0][1],marker_info.vertex[3][0]-marker_info.vertex[0][0]);
        case 3:
            return (-180/PI)*atan2(marker_info.vertex[2][1]-marker_info.vertex[3][1],marker_info.vertex[2][0]-marker_info.vertex[3][0]);
        default:
            return 0;
    }
} // ARTKPGetMarkerAngle()

void ProcessingThread::ARTKPDrawBox(Mat& mat, ARToolKitPlus::ARMarkerInfo &marker_info)
{
    switch(marker_info.dir)
    {
        case 0:
            line(mat,Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),
                                  Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),
                                  Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),Scalar(255,0,0),2);
            line(mat,Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),
                                  Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),
                                  Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),Scalar(0,0,255),2);
            break;
        case 1:
            line(mat,Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),
                                  Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),
                                  Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),Scalar(255,0,0),2);
            line(mat,Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),
                                  Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),
                                  Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),Scalar(0,0,255),2);
            break;
        case 2:
            line(mat,Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),
                                  Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),
                                  Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),Scalar(255,0,0),2);
            line(mat,Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),
                                  Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),
                                  Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),Scalar(0,0,255),2);
            break;
        case 3:
            line(mat,Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),
                                  Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[1][0],marker_info.vertex[1][1]),
                                  Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),Scalar(255,0,0),2);
            line(mat,Point(marker_info.vertex[2][0],marker_info.vertex[2][1]),
                                  Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),Scalar(0,0,255),2);
            line(mat,Point(marker_info.vertex[3][0],marker_info.vertex[3][1]),
                                  Point(marker_info.vertex[0][0],marker_info.vertex[0][1]),Scalar(0,0,255),2);
            break;
    }
} // ARTKPDrawBox()

void ProcessingThread::setupARTKP(struct ARTKPSettings artkpSettings)
{
    QMutexLocker locker(&updateMembersMutex);
    // Initialization    
    if(artkpSettings.init)
    {
        // Create single marker tracker
        /**
         * These parameters control the way the toolkit warps a found
         * marker to a perfect square. The square has size
         * pattWidth * pattHeight, the projected
         * square in the image is subsampled at a min of
         * pattWidth/pattHeight and a max of pattSamples
         * steps in both x and y direction
         *  @param imWidth width of the source image in px
         *  @param imHeight height of the source image in px
         *  @param maxImagePatterns describes the maximum number of patterns that can be analyzed in a camera image.
         *  @param pattWidth describes the pattern image width (must be 6 for binary markers)
         *  @param pattHeight describes the pattern image height (must be 6 for binary markers)
         *  @param pattSamples describes the maximum resolution at which a pattern is sampled from the camera image
         *  (6 by default, must a a multiple of pattWidth and pattHeight).
         *  @param maxLoadPatterns describes the maximum number of pattern files that can be loaded.
         *  Reduce maxLoadPatterns and maxImagePatterns to reduce memory footprint.
         */
        tracker = new TrackerSingleMarker(currentROI.width, currentROI.height, artkpSettings.maxImagePatterns,
                                          artkpSettings.pattWidth, artkpSettings.pattHeight,
                                          artkpSettings.pattSamples, artkpSettings.maxLoadPatterns);
        // Load camera calibration file
        if(!tracker->init(artkpSettings.cameraCalibrationFilename.toLatin1(), 1.0f, 1000.0f))
            qDebug("ERROR: ARToolKitPlus init() failed.");
    }

    /////////////////////
    // UPDATE SETTINGS //
    /////////////////////
    // Load camera calibration file
    tracker->loadCameraFile(artkpSettings.cameraCalibrationFilename.toLatin1(), 1.0f, 1000.0f);
    // Save tracking history value
    this->trackingHistory=artkpSettings.trackingHistory;
    // Set marker border width
    tracker->setBorderWidth(artkpSettings.borderWidth);
    // Set thresholding
    tracker->setThreshold(artkpSettings.threshold);
    tracker->activateAutoThreshold(artkpSettings.autoThreshold);
    tracker->setNumAutoThresholdRetries(artkpSettings.numAutoThreshRetries);
    // Vignetting compensation
    tracker->activateVignettingCompensation(artkpSettings.vignettingCompensation,
                                            artkpSettings.vcnCorners,artkpSettings.vcnLeftRight,artkpSettings.vcnTopBottom);
    // Set pixel format
    tracker->setPixelFormat(artkpSettings.pixelFormat);
    // Set undistortion mode
    tracker->setUndistortionMode(artkpSettings.undistortionMode);
    // Set pose estimator
    tracker->setPoseEstimator(artkpSettings.poseEstimator);
    // Set image processing mode
    tracker->setImageProcessingMode(artkpSettings.imageProcessingMode);
    // Set marker mode
    tracker->setMarkerMode(artkpSettings.markerMode);    
    // Save local copy of nEBug
    this->nEBug=artkpSettings.nEBug;
} // setupARTKP()

void ProcessingThread::deleteARTKPTracker()
{
    QMutexLocker locker(&updateMembersMutex);
    // Delete tracker
    delete tracker;
} // deleteARTKPTracker()

int struct_cmp_by_markerID(const void *a, const void *b)
{
    struct EBugData *ia = (struct EBugData *)a;
    struct EBugData *ib = (struct EBugData *)b;
    if((ia->id)<(ib->id))
        return -1;
    else if((ia->id)==(ib->id))
        return 0;
    else if((ia->id)>(ib->id))
        return 1;
    return 0; // Suppress compiler warning
} // struct_cmp_by_markerID()
