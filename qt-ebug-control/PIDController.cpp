#include "PIDController.h"

PIDController::PIDController()
{
}

void PIDController::Initialize(double kp, double ki, double kd, double error_thresh, double step_time)
{
    // Initialize controller parameters
    m_kp = kp;
    m_ki = ki;
    m_kd = kd;
    m_error_thresh = error_thresh;

    // Controller step time and its inverse
    m_h = step_time;
    m_inv_h = 1 / step_time;

    // Initialize integral and derivative calculations
    m_integral = 0;
    m_started = false;
} // Initialize()

double PIDController::Update(double error)
{
    // Set q to 1 if the error magnitude is below
    // the threshold and 0 otherwise
    double q;
    if (fabs(error) > m_error_thresh)
        q = 1;
    else
        q = 0;

    // Update the error integral
    m_integral += m_h*q*error;

    // Compute the error derivative
    double deriv;
    if (!m_started)
    {
        m_started = true;
        deriv = 0;
    }
    else
        deriv = (error - m_prev_error) * m_inv_h;

    m_prev_error = error;

    // Return the PID controller actuator command
    return m_kp*(error + m_ki*m_integral + m_kd*deriv);
} // Update()

void PIDController::updateParameters(double kp, double ki, double kd, double error_thresh)
{
    m_kp = kp;
    m_ki = ki;
    m_kd = kd;
    m_error_thresh = error_thresh;
}

void PIDController::reset()
{
    m_integral = 0;
    m_started = false;
} // updateParameters()
