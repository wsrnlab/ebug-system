/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* XBee.h                                                               */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef XBEE_H
#define XBEE_H

#define XBEE_START_DELIMITER 0x7E
#define API_ID_INDEX 3
// Maximum size (in bytes) of the ZigBee Receive Packet (API ID=0x90)
// 16 bytes (i.e. header & checksum) + RF data
#define MAX_XBEE_RXPACKET_LENGTH (16+MAX_EBUG_RXPACKET_LENGTH)

#include "Structures.h"

// Qt header files
#include <QWaitCondition>
#include <QMutex>
#include <QQueue>
#include <QSemaphore>
// QSerialDevice header file
#include <qserialdevice/abstractserial.h>

class XBee
{
public:
    XBee(int bufferSize);
    ~XBee();
    AbstractSerial *port;
    void addPacket(const QByteArray &);
    QByteArray getPacket();
    void clearPacketBuffer();
    int getSizeOfPacketBuffer();
    char calculateXBeeChecksum(QByteArray *);
    QByteArray makePacket_XBeeATCommand_ND();
    QByteArray makePacket_eBugPacketToXBeePacket(struct XBeeNodeTable, const QByteArray &);
private:
    QMutex mutex;
    QQueue<QByteArray> packetQueue;
    QSemaphore *freeSlots;
    QSemaphore *usedSlots;
    QSemaphore *clearBuffer1;
    QSemaphore *clearBuffer2;
    int bufferSize;
};

#endif // XBEE_H
