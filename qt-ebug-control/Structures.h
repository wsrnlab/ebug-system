/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* Structures.h                                                         */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef STRUCTURES_H
#define STRUCTURES_H

// Qt header files
#include <QtGui>
// ARToolKitPlus
#include <ARToolKitPlus/TrackerSingleMarker.h>
#include <ARToolKitPlus/TrackerMultiMarker.h>
// QSerialDevice header file
#include <qserialdevice/abstractserial.h>
// OpenCV header files
#include <opencv/highgui.h>
// Configuration header file
#include "Config.h"

// eBugData structure definition
struct EBugData{
    int id;
    int x;
    int y;
    double angle;
    double cf;
    bool isTracked;
};

// SerialPortData structure definition
struct SerialPortData{
    QString portName;
    AbstractSerial::BaudRate baudRate;
    AbstractSerial::Flow flowControl;
    AbstractSerial::DataBits dataBits;
    AbstractSerial::Parity parity;
    AbstractSerial::StopBits stopBits;
};

// ARTKPSettings structure definition
struct ARTKPSettings{
    bool init;
    int pattWidth;
    int pattHeight;
    int pattSamples;
    int maxLoadPatterns;
    int maxImagePatterns;
    bool trackingHistory;
    float borderWidth;
    bool autoThreshold;
    int numAutoThreshRetries;
    int threshold;
    bool vignettingCompensation;
    int vcnCorners;
    int vcnLeftRight;
    int vcnTopBottom;
    ARToolKitPlus::PIXEL_FORMAT pixelFormat;
    ARToolKitPlus::UNDIST_MODE undistortionMode;
    ARToolKitPlus::POSE_ESTIMATOR poseEstimator;
    ARToolKitPlus::IMAGE_PROC_MODE imageProcessingMode;
    ARToolKitPlus::MARKER_MODE markerMode;
    QString cameraCalibrationFilename;
    int nEBug;
};

// XBeeNodeTable structure definition
struct XBeeNodeTable{
    unsigned char commandStatus;
    unsigned int MY;
    unsigned int SH_high;
    unsigned int SH_low;
    unsigned int SL_high;
    unsigned int SL_low;
    QString NI;
    unsigned int parentNetworkAddress;
    unsigned char deviceType;
    unsigned char status;
    unsigned int profileID;
    unsigned int manufacturerID;
};

// ImageProcessingSettings structure definition
struct ImageProcessingSettings{
    int smoothType;
    int smoothParam1;
    int smoothParam2;
    double smoothParam3;
    double smoothParam4;
    int dilateNumberOfIterations;
    int erodeNumberOfIterations;
    int flipCode;
};

// ProcessingThreadData structure definition
struct ProcessingThreadData{
    bool trackingOn;
    bool grayscaleOn;
    bool smoothOn;
    bool dilateOn;
    bool erodeOn;
    bool flipOn;
};

// ControlThreadData structure definition
struct ControlThreadData{
    int controlIndex;
    QAtomicInt controlTimeStep;
    double controlParams[MAX_CONTROL_PARAMETERS];
    int nNodes;
    QList<int> validMarkerIDs;
    bool controlOn;
    bool mapping;
};

// TaskData structure definition
struct TaskData{
    QRect selectionBox;
    bool setROIFlag;
    bool resetROIFlag;
};

// MouseData structure definition
struct MouseData{
    QRect selectionBox;
    bool leftButtonRelease;
    bool rightButtonRelease;
};

#endif // STRUCTURES_H
