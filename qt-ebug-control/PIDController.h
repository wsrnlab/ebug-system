#ifndef PIDCONTROLLER_H
#define PIDCONTROLLER_H

class PIDController
{
public:
    PIDController();
    void Initialize(double kp, double ki, double kd, double error_thresh, double step_time);
    double Update(double error);
    void updateParameters(double kp, double ki, double kd, double error_thresh);
    void reset();
private:
    bool m_started;
    double m_kp, m_ki, m_kd, m_h, m_inv_h, m_prev_error, m_error_thresh, m_integral;
};

#endif // PIDCONTROLLER_H
