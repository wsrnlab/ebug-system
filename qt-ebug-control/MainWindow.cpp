/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* MainWindow.cpp                                                       */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

////////////
// TO DO: //
////////////
// 1. Verify/test ARToolKitPlus tracker settings which can *actually* be set while tracking is progress.
// 2. Implement log feature.
// 3. Prevent errors associated with user switching to grayscale while tracking with PixelFormat *NOT* set to LUM.
// 4. Dilate and Erode image processing functions produce unpredictable results: may cause program to crash -> need to fix.

//////////////////
// BUGS/ISSUES: //
//////////////////
// 1. Program crashes when the camera calibration (.CAL) file is not present -> program should instead handle error!

#include "CameraConnectDialog.h"
#include "XBeeConnectDialog.h"
#include "ImageProcessingSettingsDialog.h"
#include "TrackingSetupDialog.h"
#include "Controller.h"
#include "MainWindow.h"

// Qt header files
#include <QDebug>
// Configuration header file
#include "Config.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{  
    // Setup user interface
    setupUi(this);
    // Create controller
    controller = new Controller;
    // Save application version in QString variable
    appVersion=QUOTE(APP_VERSION);
    // Initialize data structures
    initializeProcessingThreadDataStructure();
    initializeTaskDataStructure();
    initializeControlThreadDataStructure();
    // Set initial GUI state
    setInitGUIState();
    // Connect signals to slots
    signalSlotsInit();
    // Register meta types
    qRegisterMetaType<struct ProcessingThreadData>("ProcessingThreadData");
    qRegisterMetaType<struct ImageProcessingSettings>("ImageProcessingSettings");
    qRegisterMetaType<struct ControlThreadData>("ControlThreadData");
    qRegisterMetaType<struct TaskData>("TaskData");
    qRegisterMetaType<struct EBugData>("EBugData");
    qRegisterMetaType<struct ARTKPSettings>("ARTKPSettings");
    qRegisterMetaType<struct MouseData>("MouseData");
    qRegisterMetaType<cv::Rect>("cv::Rect");
    qRegisterMetaType< QList<int> >("QList<int>");
    // Initialize flag
    isCameraConnected=false;
    isXBeeConnected=false;
    isTrackingStopped=true;
    // Create timer
    nodeDiscoveryTimer = new QTimer(this);
    // Set timer as single shot
    nodeDiscoveryTimer->setSingleShot(true);
} // MainWindow constructor

MainWindow::~MainWindow()
{
    // Disconnect camera if connected
    if(isCameraConnected)
        controller->disconnectCamera();
    // Disconnect XBee if connected
    if(isXBeeConnected)
        controller->disconnectXBee();
} // MainWindow destructor

void MainWindow::connectToCamera()
{
    // Create cameraConnectDialog
    cameraConnectDialog = new CameraConnectDialog(this);
    // Create processingSettingsDialog
    imageProcessingSettingsDialog = new ImageProcessingSettingsDialog(this);
    // Create trackingSetupDialog
    trackingSetupDialog = new TrackingSetupDialog(this);
    // PROMPT USER:
    // If user presses OK button on dialog, connect to camera; else do nothing
    if(cameraConnectDialog->exec()==1)
    {
        // Set private member variables in cameraConnectDialog to values in dialog
        cameraConnectDialog->setDeviceNumber();
        cameraConnectDialog->setImageBufferSize();
        // Store image buffer size in local variable
        imageBufferSize=cameraConnectDialog->getImageBufferSize();
        // Store device number in local variable
        deviceNumber=cameraConnectDialog->getDeviceNumber();
        // Connect to camera
        if((isCameraConnected=controller->connectToCamera(deviceNumber,imageBufferSize)))
        {
            // Create connection between processing thread (emitter) and GUI thread (receiver/listener)
            connect(controller->processingThread,SIGNAL(newFrame(QImage)),this,SLOT(updateFrame(QImage)),Qt::UniqueConnection);
            // Create connections (4) between GUI thread (emitter) and processing thread (receiver/listener)
            connect(this,SIGNAL(newProcessingThreadData(struct ProcessingThreadData)),controller->processingThread,SLOT(updateProcessingThreadData(struct ProcessingThreadData)),Qt::UniqueConnection);
            connect(this->imageProcessingSettingsDialog,SIGNAL(newImageProcessingSettings(struct ImageProcessingSettings)),controller->processingThread,SLOT(updateImageProcessingSettings(struct ImageProcessingSettings)),Qt::UniqueConnection);
            connect(this,SIGNAL(newTaskData(struct TaskData)),controller->processingThread,SLOT(updateTaskData(struct TaskData)),Qt::UniqueConnection);
            connect(controller->processingThread,SIGNAL(newEBugData(struct EBugData*,cv::Rect)),this,SLOT(updateTrackingTable(struct EBugData*,cv::Rect)),Qt::UniqueConnection);
            // Create connection between GUI thread (emitter) and processing thread (receiver/listener)
            connect(this->trackingSetupDialog,SIGNAL(setupARTKP(struct ARTKPSettings)),controller->processingThread,SLOT(setupARTKP(struct ARTKPSettings)),Qt::UniqueConnection);
            // Initialize data structures
            initializeProcessingThreadDataStructure();
            initializeTaskDataStructure();
            initializeControlThreadDataStructure();
            // Set data to defaults in processingThread
            emit newProcessingThreadData(processingThreadData);
            emit newTaskData(taskData);
            // Setup imageBufferBar in main window with minimum and maximum values
            imageBufferBar->setMinimum(0);
            imageBufferBar->setMaximum(imageBufferSize);
            // Enable/Disable appropriate GUI items
            connectToCameraAction->setEnabled(false);
            disconnectCameraAction->setEnabled(true);
            imageProcessingSettingsAction->setEnabled(true);
            imageProcessingMenu->setEnabled(true);
            startTrackingButton->setEnabled(true);
            // Enable "Clear Image Buffer" push button in main window
            clearImageBufferButton->setEnabled(true);
            // Get input stream properties
            sourceWidth=controller->captureThread->getInputSourceWidth();
            sourceHeight=controller->captureThread->getInputSourceHeight();
            // Set text in labels in main window
            deviceNumberLabel->setNum(deviceNumber);
            cameraResolutionLabel->setText(QString::number(sourceWidth)+QString("x")+QString::number(sourceHeight));
        }
        // Display error dialog if camera connection is unsuccessful
        else
        {
            QMessageBox::warning(this,"ERROR:","Could not connect to camera.");
            // Delete dialogs
            delete cameraConnectDialog;
            delete imageProcessingSettingsDialog;
            delete trackingSetupDialog;
        }
    }
} // connectToCamera()

void MainWindow::disconnectCamera()
{
    // Check if camera is connected
    if(controller->captureThread->isCameraConnected())
    {
        // Stop tracking (if ON)
        if(processingThreadData.trackingOn)
            stopTracking();
        // Disconnect connections (6)
        disconnect(controller->processingThread,SIGNAL(newFrame(QImage)),0,0);
        disconnect(this,SIGNAL(newProcessingThreadData(struct ProcessingThreadData)),controller->processingThread,SLOT(updateProcessingThreadData(struct ProcessingThreadData)));
        disconnect(this->imageProcessingSettingsDialog,SIGNAL(newImageProcessingSettings(struct ImageProcessingSettings)),controller->processingThread,SLOT(updateImageProcessingSettings(struct ImageProcessingSettings)));
        disconnect(this,SIGNAL(newTaskData(struct TaskData)),controller->processingThread,SLOT(updateTaskData(struct TaskData)));
        disconnect(this->trackingSetupDialog,SIGNAL(setupARTKP(struct ARTKPSettings)),controller->processingThread,SLOT(setupARTKP(struct ARTKPSettings)));
        disconnect(controller->processingThread,SIGNAL(newEBugData(struct EBugData*,cv::Rect)),this,SLOT(updateTrackingTable(struct EBugData*,cv::Rect)));
        // Delete dialogs
        delete cameraConnectDialog;
        delete imageProcessingSettingsDialog;
        delete trackingSetupDialog;
        // Reset flag
        isCameraConnected=false;
        // Enable/Disable appropriate GUI items
        connectToCameraAction->setEnabled(true);
        disconnectCameraAction->setEnabled(false);
        trackingSetupAction->setEnabled(false);
        imageProcessingSettingsAction->setEnabled(false);
        imageProcessingMenu->setEnabled(false);
        // Set GUI in main window
        grayscaleAction->setChecked(false);
        smoothAction->setChecked(false);
        dilateAction->setChecked(false);
        erodeAction->setChecked(false);
        flipAction->setChecked(false);
        frameLabel->setText("No camera connected.");
        imageBufferBar->setValue(0);
        imageBufferLabel->setText("[000/000]");
        captureRateLabel->setText("");
        processingRateLabel->setText("");
        deviceNumberLabel->setText("");
        cameraResolutionLabel->setText("");
        roiLabel->setText("");
        mouseCursorPosLabel->setText("");
        clearImageBufferButton->setEnabled(false);
        // Disconnect camera
        controller->disconnectCamera();
        // Clear tracking table
        trackingTable->clearContents();
        // Clear mapping vector
        mappingVectorEdit->clear();
    }
    // Display error dialog
    else
        QMessageBox::warning(this,"ERROR:","Camera already disconnected.");
} // disconnectCamera()

void MainWindow::about()
{
    QMessageBox::information(this,"About",QString("Created by Nick D'Ademo\n\nContact: nickdademo@gmail.com\nWebsite: www.nickdademo.com\n\nVersion: ")+appVersion);
} // about()

void MainWindow::clearImageBuffer()
{
    controller->clearImageBuffer();
} // clearImageBuffer()

void MainWindow::setGrayscale(bool input)
{
    // Not checked
    if(!input)
        processingThreadData.grayscaleOn=false;
    // Checked
    else if(input)
        processingThreadData.grayscaleOn=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // setGrayscale()

void MainWindow::setSmooth(bool input)
{
    // Not checked
    if(!input)
        processingThreadData.smoothOn=false;
    // Checked
    else if(input)
        processingThreadData.smoothOn=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // setSmooth()

void MainWindow::setDilate(bool input)
{
    // Not checked
    if(!input)
        processingThreadData.dilateOn=false;
    // Checked
    else if(input)
        processingThreadData.dilateOn=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // setDilate()

void MainWindow::setErode(bool input)
{
    // Not checked
    if(!input)
        processingThreadData.erodeOn=false;
    // Checked
    else if(input)
        processingThreadData.erodeOn=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // setErode()

void MainWindow::setFlip(bool input)
{
    // Not checked
    if(!input)
        processingThreadData.flipOn=false;
    // Checked
    else if(input)
        processingThreadData.flipOn=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // setFlip()

void MainWindow::updateFrame(const QImage &frame)
{
    // Show [number of images in buffer / image buffer size] in imageBufferLabel in main window
    imageBufferLabel->setText(QString("[")+QString::number(controller->processingThread->getCurrentSizeOfBuffer())+
                              QString("/")+QString::number(imageBufferSize)+QString("]"));
    // Show percentage of image bufffer full in imageBufferBar in main window
    imageBufferBar->setValue(controller->processingThread->getCurrentSizeOfBuffer());
    // Show processing rate in captureRateLabel in main window
    captureRateLabel->setNum(controller->captureThread->getAvgFPS());
    captureRateLabel->setText(captureRateLabel->text()+" fps");
    // Show processing rate in processingRateLabel in main window
    processingRateLabel->setNum(controller->processingThread->getAvgFPS());
    processingRateLabel->setText(processingRateLabel->text()+" fps");
    // Show ROI information in roiLabel in main window
    roiLabel->setText(QString("(")+QString::number(controller->processingThread->getCurrentROI().x)+QString(",")+
                      QString::number(controller->processingThread->getCurrentROI().y)+QString(") ")+
                      QString::number(controller->processingThread->getCurrentROI().width)+
                      QString("x")+QString::number(controller->processingThread->getCurrentROI().height));
    // Display frame in main window
    frameLabel->setPixmap(QPixmap::fromImage(frame));
} // updateFrame()

void MainWindow::setImageProcessingSettings()
{
    // Prompt user:
    // If user presses OK button on dialog, update image processing settings
    if(imageProcessingSettingsDialog->exec()==1)
        imageProcessingSettingsDialog->updateStoredSettingsFromDialog();
    // Else, restore dialog state
    else
       imageProcessingSettingsDialog->updateDialogSettingsFromStored();
} // setImageProcessingSettings()

void MainWindow::updateMouseCursorPosLabel()
{
    // Update mouse cursor position in mouseCursorPosLabel in main window
    mouseCursorPosLabel->setText(QString("(")+QString::number(frameLabel->getMouseCursorPos().x())+
                                 QString(",")+QString::number(frameLabel->getMouseCursorPos().y())+
                                 QString(")"));
    // Show ROI-adjusted cursor position if camera is connected
    if(isCameraConnected)
        mouseCursorPosLabel->setText(mouseCursorPosLabel->text()+
                                     QString(" [")+QString::number(frameLabel->getMouseCursorPos().x()-(frameLabel->width()-controller->processingThread->getCurrentROI().width)/2)+
                                     QString(",")+QString::number(frameLabel->getMouseCursorPos().y()-(frameLabel->height()-controller->processingThread->getCurrentROI().height)/2)+
                                     QString("]"));
} // updateMouseCursorPosLabel()

void MainWindow::newMouseData(struct MouseData mouseData)
{
    // Local variables
    int x_temp, y_temp, width_temp, height_temp;
    // Mouse events on frameLabel are only valid if camera is connected
    if(isCameraConnected)
    {
        // Set ROI
        if(mouseData.leftButtonRelease)
        {
            // Copy box dimensions from mouseData to taskData
            taskData.selectionBox.setX(mouseData.selectionBox.x());
            taskData.selectionBox.setY(mouseData.selectionBox.y());
            taskData.selectionBox.setWidth(mouseData.selectionBox.width());
            taskData.selectionBox.setHeight(mouseData.selectionBox.height());
            // Check if selection box has NON-ZERO dimensions
            if((taskData.selectionBox.width()!=0)&&((taskData.selectionBox.height())!=0))
            {
                // Selection box can also be drawn from bottom-right to top-left corner
                if(taskData.selectionBox.width()<0)
                {
                    x_temp=taskData.selectionBox.x();
                    width_temp=taskData.selectionBox.width();
                    taskData.selectionBox.setX(x_temp+taskData.selectionBox.width());
                    taskData.selectionBox.setWidth(width_temp*-1);
                }
                if(taskData.selectionBox.height()<0)
                {
                    y_temp=taskData.selectionBox.y();
                    height_temp=taskData.selectionBox.height();
                    taskData.selectionBox.setY(y_temp+taskData.selectionBox.height());
                    taskData.selectionBox.setHeight(height_temp*-1);
                }
                // Check if selection box is not outside window
                if((taskData.selectionBox.x()<0)||(taskData.selectionBox.y()<0)||
                   ((taskData.selectionBox.x()+taskData.selectionBox.width())>sourceWidth)||
                   ((taskData.selectionBox.y()+taskData.selectionBox.height())>sourceHeight))
                {
                    // Display error message
                    QMessageBox::warning(this,"ERROR:","Selection box outside range. Please try again.");
                }
                else
                {
                    if(!isTrackingStopped)
                        QMessageBox::warning(this,"ERROR:","Setting/resetting of the ROI is not permitted while tracking is NOT stopped.");
                    else
                    {
                        // Set setROIFlag to TRUE
                        taskData.setROIFlag=true;
                        // Update task data in processingThread
                        emit newTaskData(taskData);
                        // Set setROIFlag to FALSE
                        taskData.setROIFlag=false;
                    }
                }
            }
        }
        // Reset ROI
        else if(mouseData.rightButtonRelease)
        {
            if(!isTrackingStopped)
                QMessageBox::warning(this,"ERROR:","Setting/resetting of the ROI is not permitted while tracking is running.");
            else
            {
                // Set resetROIFlag to TRUE
                taskData.resetROIFlag=true;
                // Update task data in processingThread
                emit newTaskData(taskData);
                // Set resetROIFlag to FALSE
                taskData.resetROIFlag=false;
            }
        }
    }
} // newMouseData()

void MainWindow::initializeProcessingThreadDataStructure()
{
    processingThreadData.trackingOn=false;
    processingThreadData.grayscaleOn=false;
    processingThreadData.smoothOn=false;
    processingThreadData.dilateOn=false;
    processingThreadData.erodeOn=false;
    processingThreadData.flipOn=false;
} // initializeProcessingThreadDataStructure()

void MainWindow::initializeTaskDataStructure()
{
    taskData.setROIFlag=false;
    taskData.resetROIFlag=false;
} // initializeTaskDataStructure()

void MainWindow::initializeControlThreadDataStructure()
{
    controlThreadData.nNodes=0;
    controlThreadData.controlOn=false;
    controlThreadData.controlIndex=0;
    controlThreadData.mapping=false;
    controlThreadData.controlTimeStep=DEFAULT_CONTROL_TIME_STEP_MS;
    for(int i=0;i<MAX_CONTROL_PARAMETERS;i++)
        controlThreadData.controlParams[i]=0.00;
} // initializeControlThreadData()

void MainWindow::setInitGUIState()
{
    // Setup tables
    setupTables();
    //////////////////////////////////////////
    // Enable/disable appropriate GUI items //
    //////////////////////////////////////////
    // MENU
    connectToCameraAction->setEnabled(true);
    disconnectCameraAction->setEnabled(false);
    trackingSetupAction->setEnabled(false);
    imageProcessingMenu->setEnabled(false);
    imageProcessingSettingsAction->setEnabled(false);
    grayscaleAction->setChecked(false);
    smoothAction->setChecked(false);
    dilateAction->setChecked(false);
    erodeAction->setChecked(false);
    flipAction->setChecked(false);
    xBeeConnectAction->setEnabled(true);
    xBeeDisconnectAction->setEnabled(false);
    // TRACKING TAB
    frameLabel->setText("No camera connected.");
    imageBufferBar->setValue(0);
    imageBufferLabel->setText("[000/000]");
    captureRateLabel->setText("");
    processingRateLabel->setText("");
    deviceNumberLabel->setText("");
    cameraResolutionLabel->setText("");
    roiLabel->setText("");
    mouseCursorPosLabel->setText("");
    clearImageBufferButton->setEnabled(false);
    // CONTROL TAB
    clearPacketBufferButton->setEnabled(false);
    packetBufferLabel->setText("[000/000]");
    startStopControlButton->setEnabled(false);
    // SIDE AREA
    startTrackingButton->setEnabled(false);
    stopTrackingButton->setEnabled(false);
    pauseResumeTrackingButton->setEnabled(false);
    discoverEBugsButton->setEnabled(false);
    performMappingButton->setEnabled(false);
    xBeeDiscoveryDurationLineEdit->setText(QString::number(DEFAULT_XBEE_ND_DURATION));
} // setInitGUIState()

void MainWindow::signalSlotsInit()
{
    connect(connectToCameraAction, SIGNAL(triggered()), this, SLOT(connectToCamera()));
    connect(disconnectCameraAction, SIGNAL(triggered()), this, SLOT(disconnectCamera()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
    connect(trackingSetupAction, SIGNAL(triggered()),this,SLOT(setTrackingSettings()));
    connect(grayscaleAction, SIGNAL(toggled(bool)), this, SLOT(setGrayscale(bool)));
    connect(smoothAction, SIGNAL(toggled(bool)), this, SLOT(setSmooth(bool)));
    connect(dilateAction, SIGNAL(toggled(bool)), this, SLOT(setDilate(bool)));
    connect(erodeAction, SIGNAL(toggled(bool)), this, SLOT(setErode(bool)));
    connect(flipAction, SIGNAL(toggled(bool)), this, SLOT(setFlip(bool)));
    connect(imageProcessingSettingsAction, SIGNAL(triggered()), this, SLOT(setImageProcessingSettings()));
    connect(xBeeConnectAction, SIGNAL(triggered()),this,SLOT(connectToXBee()));
    connect(xBeeDisconnectAction, SIGNAL(triggered()),this,SLOT(disconnectXBee()));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(about()));
    connect(clearImageBufferButton, SIGNAL(released()), this, SLOT(clearImageBuffer()));
    connect(frameLabel, SIGNAL(onMouseMoveEvent()), this, SLOT(updateMouseCursorPosLabel()));
    connect(startTrackingButton,SIGNAL(released()),this,SLOT(startTracking()));
    connect(stopTrackingButton,SIGNAL(released()),this,SLOT(stopTracking()));
    connect(pauseResumeTrackingButton,SIGNAL(released()),this,SLOT(pauseResumeTracking()));
    connect(discoverEBugsButton,SIGNAL(released()),this,SLOT(performXBeeDiscovery()));
    connect(performMappingButton,SIGNAL(released()),this,SLOT(performMapping()));
    connect(loadControlParameterFileButton,SIGNAL(released()),this,SLOT(loadControlParametersFromFile()));
    connect(browseControlParameterFileButton,SIGNAL(released()),this,SLOT(setControlParameterFileName()));
    connect(startStopControlButton,SIGNAL(released()),this,SLOT(startStopControl()));
    // Create connection between frameLabel (emitter) and GUI thread (receiver/listener)
    connect(this->frameLabel,SIGNAL(newMouseData(struct MouseData)),this,SLOT(newMouseData(struct MouseData)));
} // signalSlotsInit()

void MainWindow::startTracking()
{
    // Setup tracker
    if(setTrackingSettings())
    {
        // Enable/Disable appropriate GUI items
        trackingSetupAction->setEnabled(true);
        startTrackingButton->setEnabled(false);
        stopTrackingButton->setEnabled(true);
        pauseResumeTrackingButton->setEnabled(true);
        // Turn tracking ON
        processingThreadData.trackingOn=true;
        // Set flag
        isTrackingStopped=false;
        // Update settings in processingThread
        emit newProcessingThreadData(processingThreadData);
    }
} // startTracking()

void MainWindow::stopTracking()
{
    // Enable/Disable appropriate GUI items
    trackingSetupAction->setEnabled(false);
    startTrackingButton->setEnabled(true);
    stopTrackingButton->setEnabled(false);
    pauseResumeTrackingButton->setEnabled(false);
    // Clear tracking table
    trackingTable->clearContents();
    // Clear mapping vector
    mappingVectorEdit->clear();
    // Turn tracking OFF
    processingThreadData.trackingOn=false;
    // Set flag
    isTrackingStopped=true;
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
    // Delete ARTKP tracker
    controller->processingThread->deleteARTKPTracker();
} // stopTracking()

void MainWindow::pauseResumeTracking()
{
    // Set tracking ON/OFF
    if(processingThreadData.trackingOn)
    {
        trackingSetupAction->setEnabled(false);
        pauseResumeTrackingButton->setText("Resume");
        processingThreadData.trackingOn=false;
        // Clear tracking table
        trackingTable->clearContents();
    }
    else if(!processingThreadData.trackingOn)
    {
        trackingSetupAction->setEnabled(true);
        pauseResumeTrackingButton->setText("Pause");
        processingThreadData.trackingOn=true;
    }
    // Update settings in processingThread
    emit newProcessingThreadData(processingThreadData);
} // pauseResumeTracking()

bool MainWindow::setTrackingSettings()
{
    // Inform dialog of tracking state
    trackingSetupDialog->setDialogState(processingThreadData.trackingOn);
    // Ensure dialog represents stored settings
    trackingSetupDialog->updateDialogSettingsFromStored();
    // Prompt user:
    // If user presses OK button on dialog, attempt to update tracking settings
    if(trackingSetupDialog->exec()==1)
        return trackingSetupDialog->updateStoredSettingsFromDialog();
    // Else, restore dialog state
    else
    {
        trackingSetupDialog->updateDialogSettingsFromStored();
        return false;
    }
} // setTrackingSettings()

void MainWindow::updateTrackingTable(struct EBugData *eBugDataIn, cv::Rect currentROI)
{
    // Only show tracking info if tracking is ON
    if(processingThreadData.trackingOn)
    {
        // Clear tracking table
        trackingTable->clearContents();
        // Rows
        for(int i=0;i<trackingSetupDialog->getnEBugEdit();i++)
        {
            // Save data to local copy
            eBugData[i].angle=eBugDataIn[i].angle;
            eBugData[i].x=eBugDataIn[i].x;
            eBugData[i].y=eBugDataIn[i].y;
            eBugData[i].id=eBugDataIn[i].id;
            eBugData[i].isTracked=eBugDataIn[i].isTracked;
            eBugData[i].cf=eBugDataIn[i].cf;
            // Only display tracking info for VALID markers
            if(eBugDataIn[i].isTracked)
            {
                // Columns
                for(int j=0;j<trackingTable->columnCount();j++)
                {
                    switch(j)
                    {
                        // ID
                        case 0:
                        trackingTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(eBugDataIn[i].id,0,10)));
                        break;
                        // X
                        case 1:
                            trackingTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(eBugDataIn[i].x,0,10)));
                            break;
                        // Y
                        case 2:
                            trackingTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(eBugDataIn[i].y,0,10)));
                            break;
                        // Angle
                        case 3:
                        trackingTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(eBugDataIn[i].angle,0,'f',2)));
                            break;
                        // Confidence Factor
                        case 4:
                        trackingTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(100*eBugDataIn[i].cf,0,'f',2)));
                            break;
                     }
                }
            }
        }
    }
} // updateTrackingTable()

void MainWindow::setupTables()
{
    // Tracking table
        // Set column count
        trackingTable->setColumnCount(5);
        // Set row count
        trackingTable->setRowCount(MAX_NEBUGTRACK);
        // Set horizontal header label
        trackingTableHeaderList<<"ID"<<"X"<<"Y"<<"Angle (degs)"<<"Confidence Factor (%)";
        trackingTable->setHorizontalHeaderLabels(trackingTableHeaderList);
        // Set longest possible length values to set appropriate column width
        trackingTable->setItem(0, 0, new QTableWidgetItem(("9999")));
        trackingTable->setItem(0, 1, new QTableWidgetItem(("9999")));
        trackingTable->setItem(0, 2, new QTableWidgetItem(("9999")));
        trackingTable->setItem(0, 3, new QTableWidgetItem(("-360.00")));
        trackingTable->setItem(0, 4, new QTableWidgetItem(("100.00")));
        trackingTable->resizeColumnsToContents();
        trackingTable->clearContents();
    // XBee Node table
        // Set dimensions
        xBeeNodeTable->setRowCount(MAX_NEBUGTRACK);
        xBeeNodeTable->setColumnCount(8);
        // Set horizontal header label
        xBeeNodeTableHeaderList<<"Serial Number"<<"Network Address"<<"Node ID"<<"Parent Network Address"<<"Device Type"<<"Status"<<"Profile ID"<<"Manufacturer ID";
        xBeeNodeTable->setHorizontalHeaderLabels(xBeeNodeTableHeaderList);
        // Resize table
        xBeeNodeTable->resizeColumnsToContents();
} // setupTables()

void MainWindow::onXBeeRXTXThreadLoop(int currentPacketBufferSize)
{
    // Show [number of packets in buffer / packet buffer size] in packeteBufferLabel in main window
    packetBufferLabel->setText(QString("[")+QString::number(currentPacketBufferSize)+
                              QString("/")+QString::number(packetBufferSize)+QString("]"));
    // Show percentage of packet bufffer full in packetBufferBar in main window
    packetBufferBar->setValue(currentPacketBufferSize);
} // onXBeeRXTXThreadLoop()

void MainWindow::updateXBeeNodeTable(struct XBeeNodeTable *xBeeNodeTableData, int nNodes)
{
    // Clear table
    xBeeNodeTable->clearContents();
    // Save number of nodes
    controlThreadData.nNodes=nNodes;
    // Check if any nodes were found
    if(nNodes==0)
    {
        // Display error message
        QMessageBox::warning(this,"ERROR:","No XBee nodes found.\n\n1. Ensure all nodes are powered on and associated (ASSOC LED flashing).\n2. Increase the discovery duration and try again.");
    }
    else
    {
        // Rows
        for(int i=0;i<nNodes;i++)
        {
            // Copy NI string to QString
            QString NI;
            int k=0;
            while((xBeeNodeTableData[i].NI[k+1]!=0x00)&&(k<20))
            {
                NI[k]=xBeeNodeTableData[i].NI[k+1];
                k++;
            }
            // Columns
            for(int j=0;j<xBeeNodeTable->columnCount();j++)
            {
                switch(j)
                {
                    // Serial Number
                    case 0:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1%2%3%4").arg(xBeeNodeTableData[i].SH_high,0,16).arg(xBeeNodeTableData[i].SH_low,0,16).arg(xBeeNodeTableData[i].SL_high,0,16).arg(xBeeNodeTableData[i].SL_low,0,16)));
                        break;
                    // Network Address
                    case 1:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].MY,0,16)));
                        break;
                    // Node ID
                    case 2:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(NI,0));
                        break;
                    // Parent Network Address
                    case 3:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].parentNetworkAddress,0,16)));
                        break;
                    // Device Type
                    case 4:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].deviceType,0,16)));
                        break;
                    // Status
                    case 5:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].status,0,16)));
                        break;
                    // Profile ID
                    case 6:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].profileID,0,16)));
                        break;
                    // Manufacturer ID
                    case 7:
                        xBeeNodeTable->setItem(i, j, new QTableWidgetItem(QString("%1").arg(xBeeNodeTableData[i].manufacturerID,0,16)));
                        break;
                 }
            }
        }
        // Resize table
        xBeeNodeTable->resizeColumnsToContents();
    }
    // Set GUI
    xBeeDiscoveryDurationLineEdit->setEnabled(true);
    discoverEBugsButton->setEnabled(true);
    discoverEBugsButton->setText("Perform");
    xBeeDisconnectAction->setEnabled(true);
} // updateXBeeNodeTable()

void MainWindow::connectToXBee()
{
    // Create XBeeConnectDialog
    xBeeConnectDialog = new XBeeConnectDialog(this);
    // PROMPT USER:
    // If user presses OK button on dialog, connect to XBee
    if(xBeeConnectDialog->exec()==1)
    {
        // Store settings
        if(xBeeConnectDialog->updateStoredSettingsFromDialog())
        {
            // Open XBee
            if((isXBeeConnected=controller->connectToXBee(xBeeConnectDialog->packetBufferSize,xBeeConnectDialog->serialPortData)))
            {
                // Create queued connection between timer timeout signal and xBeeRXTX thread
                connect(this->nodeDiscoveryTimer,SIGNAL(timeout()),controller->xBeeRXTXThread,SLOT(xBeeNDTimeout()),Qt::UniqueConnection);
                // Create queued connection beween XBee node table update signal in xBeeRXTX thread and table update function in GUI thread
                connect(controller->xBeeRXTXThread,SIGNAL(newXBeeNodeData(XBeeNodeTable*,int)),this,SLOT(updateXBeeNodeTable(XBeeNodeTable*,int)),Qt::UniqueConnection);
                // Create queued connections between GUI thread (emitter) and control thread (receiver/listener)
                connect(this,SIGNAL(newControlThreadData(struct ControlThreadData)),controller->controlThread,SLOT(updateControlThreadData(struct ControlThreadData)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and GUI thread (receiver/listener)
                connect(controller->controlThread,SIGNAL(newMappingVector(int*)),this,SLOT(mappingDone(int*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataRandomPosition(cv::Rect,int,int,QList<int>,QList<int>,ControlDataRandomPosition*)),this->controlMainLabel,SLOT(drawControlDataRandomPosition(cv::Rect,int,int,QList<int>,QList<int>,ControlDataRandomPosition*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataLineFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataLineFormation*)),this->controlMainLabel,SLOT(drawControlDataLineFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataLineFormation*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataTriangleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTriangleFormation*)),this->controlMainLabel,SLOT(drawControlDataTriangleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTriangleFormation*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataFormationControl(cv::Rect,int,int,QList<int>,QList<int>,ControlDataFormationControl*,bool*)),this->controlMainLabel,SLOT(drawControlDataFormationControl(cv::Rect,int,int,QList<int>,QList<int>,ControlDataFormationControl*,bool*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataCircleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataCircleFormation*)),this->controlMainLabel,SLOT(drawControlDataCircleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataCircleFormation*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataDiagonalFormation1(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation1*)),this->controlMainLabel,SLOT(drawControlDataDiagonalFormation1(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation1*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataDiagonalFormation2(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation2*)),this->controlMainLabel,SLOT(drawControlDataDiagonalFormation2(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation2*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataDominosFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDominosFormation*)),this->controlMainLabel,SLOT(drawControlDataDominosFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDominosFormation*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataECSEFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataECSEFormation*)),this->controlMainLabel,SLOT(drawControlDataECSEFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataECSEFormation*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataPathFinding(cv::Rect,int,int,QList<int>,QList<int>,ControlDataPathFinding*)),this->controlMainLabel,SLOT(drawControlDataPathFinding(cv::Rect,int,int,QList<int>,QList<int>,ControlDataPathFinding*)),Qt::UniqueConnection);
                // Create queued connection between control thread (emitter) and QLabel in Control tab
                connect(controller->controlThread,SIGNAL(drawControlDataTargetTracking(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTargetTracking*)),this->controlMainLabel,SLOT(drawControlDataTargetTracking(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTargetTracking*)),Qt::UniqueConnection);
                // Update data in controlThread
                emit newControlThreadData(controlThreadData);
                // Enable/disable appropriate GUI items
                xBeeConnectAction->setEnabled(false);
                xBeeDisconnectAction->setEnabled(true);
                discoverEBugsButton->setEnabled(true);
                performMappingButton->setEnabled(true);
            }
            // If open was unsuccessful:
            else
            {
                // Display error message
                QMessageBox::warning(this,"ERROR:","Serial device could not be opened on "+QString(xBeeConnectDialog->serialPortData.portName)+QString("."));
                // Delete XBeeConnectDialog
                delete xBeeConnectDialog;
            }
        }
        // Do not store settings
        else
        {
            // Delete XBeeConnectDialog
            delete xBeeConnectDialog;
        }
    }
    // Else, do nothing
    else
    {
        // Delete XBeeConnectDialog
        delete xBeeConnectDialog;
    }
} // connectToXBee()

void MainWindow::disconnectXBee()
{
    // Delete XBeeConnectDialog
    delete xBeeConnectDialog;
    // Disconnect queued connections (5)
    disconnect(this->nodeDiscoveryTimer,SIGNAL(timeout()),controller->xBeeRXTXThread,SLOT(xBeeNDTimeout()));
    disconnect(controller->xBeeRXTXThread,SIGNAL(newXBeeNodeData(XBeeNodeTable*,int)),this,SLOT(updateXBeeNodeTable(XBeeNodeTable*,int)));
    disconnect(this,SIGNAL(newControlThreadData(struct ControlThreadData)),controller->controlThread,SLOT(updateControlThreadData(struct ControlThreadData)));
    disconnect(controller->controlThread,SIGNAL(newMappingVector(int*)),this,SLOT(mappingDone(int*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataRandomPosition(cv::Rect,int,int,QList<int>,QList<int>,ControlDataRandomPosition*)),this->controlMainLabel,SLOT(drawControlDataRandomPosition(cv::Rect,int,int,QList<int>,QList<int>,ControlDataRandomPosition*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataLineFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataLineFormation*)),this->controlMainLabel,SLOT(drawControlDataLineFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataLineFormation*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataTriangleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTriangleFormation*)),this->controlMainLabel,SLOT(drawControlDataTriangleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataTriangleFormation*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataFormationControl(cv::Rect,int,int,QList<int>,QList<int>,ControlDataFormationControl*,bool*)),this->controlMainLabel,SLOT(drawControlDataFormationControl(cv::Rect,int,int,QList<int>,QList<int>,ControlDataFormationControl*, bool*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataCircleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataCircleFormation*)),this->controlMainLabel,SLOT(drawControlDataCircleFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataCircleFormation*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataDiagonalFormation1(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation1*)),this->controlMainLabel,SLOT(drawControlDataDiagonalFormation1(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation1*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataDiagonalFormation2(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation2*)),this->controlMainLabel,SLOT(drawControlDataDiagonalFormation2(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDiagonalFormation2*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataDominosFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDominosFormation*)),this->controlMainLabel,SLOT(drawControlDataDominosFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataDominosFormation*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataECSEFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataECSEFormation*)),this->controlMainLabel,SLOT(drawControlDataECSEFormation(cv::Rect,int,int,QList<int>,QList<int>,ControlDataECSEFormation*)));
    disconnect(controller->controlThread,SIGNAL(drawControlDataPathFinding(cv::Rect,int,int,QList<int>,QList<int>,ControlDataPathFinding*)),this->controlMainLabel,SLOT(drawControlDataPathFinding(cv::Rect,int,int,QList<int>,QList<int>,ControlDataPathFinding*)));

    // Enable/disable appropriate GUI items
    xBeeConnectAction->setEnabled(true);
    xBeeDisconnectAction->setEnabled(false);
    discoverEBugsButton->setEnabled(false);
    performMappingButton->setEnabled(false);
    // Reset number of nodes to zero
    controlThreadData.nNodes=0;
    // Clear XBee node table
    xBeeNodeTable->clearContents();
    // Clear mapping vector
    mappingVectorEdit->clear();
    // Disconnect XBee
    controller->disconnectXBee();
} // disconnectXBee()

void MainWindow::performXBeeDiscovery()
{
    // Validate duration
    if(xBeeDiscoveryDurationLineEdit->text().isEmpty())
    {
        // Set to default
        xBeeDiscoveryDurationLineEdit->setText(QString::number(DEFAULT_XBEE_ND_DURATION));
        // Display error message
        QMessageBox::warning(this, "ERROR:","Discovery duration field blank.\nAutomatically set to default value.");
    }
    // Get duration from GUI
    int duration=xBeeDiscoveryDurationLineEdit->text().toInt();
    // Perform check
    if(duration<DEFAULT_MIN_XBEE_ND_DURATION)
    {
        // Display error message
        QMessageBox::warning(this,"ERROR:","ERROR: XBee node discovery duration must be >="+QString::number(DEFAULT_MIN_XBEE_ND_DURATION)+QString(" seconds."));
    }
    else
    {
        // Send Node Discovery packet
        controller->xBee->addPacket(controller->xBee->makePacket_XBeeATCommand_ND());
        // Start timer
        nodeDiscoveryTimer->start(1000*xBeeDiscoveryDurationLineEdit->text().toInt());
        // Set GUI
        xBeeDiscoveryDurationLineEdit->setEnabled(false);
        discoverEBugsButton->setEnabled(false);
        discoverEBugsButton->setText("Wait...");
        xBeeDisconnectAction->setEnabled(false);
    }
} // performXBeeDiscovery()

void MainWindow::performMapping()
{
    // Local variables
    bool mappingOKToStart=true;
    QString detectedMarkerIDs;
    int markerCount=0;
    ////////////////////
    // Perform checks //
    ////////////////////
    // XBee device must be connected
    if(!controller->xBee->port->isOpen())
        mappingOKToStart=false;
    // Tracking must be ON
    if(!processingThreadData.trackingOn)
        mappingOKToStart=false;
    // Number of objects being tracked and number of nodes discovered must be NON-ZERO
    if((trackingSetupDialog->getnEBugEdit()==0)||(controlThreadData.nNodes==0))
        mappingOKToStart=false;
    // Number of objects being tracked must equal the number of nodes discovered
    if(trackingSetupDialog->getnEBugEdit()!=controlThreadData.nNodes)
        mappingOKToStart=false;
    // Check flag
    if(!mappingOKToStart)
    {
        // Show dialog
        QMessageBox::warning(this,
                             "ERROR:","Mapping could not be started.\n\nPossible causes:\n1. No XBee nodes have been discovered (and/or XBee device is NOT connected).\n2. Tracking is NOT currently in progress.\n3. The number of objects being tracked DOES NOT EQUAL the number of XBee nodes discovered.");
    }
    // Start mapping
    else
    {
        // Reset list
        controlThreadData.validMarkerIDs.clear();
        // Create a string with detected markers and count number of detected markers
        for(int i=0;i<trackingSetupDialog->getnEBugEdit();i++)
        {
            if(eBugData[i].isTracked)
            {
                controlThreadData.validMarkerIDs.append(eBugData[i].id);
                detectedMarkerIDs.append(QString::number(controlThreadData.validMarkerIDs.at(markerCount))+QString("\n"));
                markerCount++;
            }
        }
        // Check that ALL markers are being tracked
        if(markerCount!=trackingSetupDialog->getnEBugEdit())
        {
            QMessageBox::warning(this,
                                 "ERROR:","Mapping was not started. Please ensure that ALL the markers are being tracked before performing mapping.");
        }
        else
        {
            // Sort previously stored valid marker IDs in ASCENDING order
            qSort(controlThreadData.validMarkerIDs.begin(), controlThreadData.validMarkerIDs.end());
            // Show dialog
            QMessageBox msgBox;
            msgBox.setText("Marker ID Confirmation:");
            msgBox.setInformativeText("The following marker IDs were detected:\n"+detectedMarkerIDs+QString("\nDo you wish to proceed?"));
            msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            msgBox.setDefaultButton(QMessageBox::Yes);
            if(msgBox.exec()==0x00004000)
            {
                // Set GUI
                discoverEBugsButton->setEnabled(false);
                performMappingButton->setEnabled(false);
                performMappingButton->setText("Wait...");
                // Update ControlThreadData
                controlThreadData.mapping=true;
                // Update data in controlThread (i.e. start mapping)
                emit newControlThreadData(controlThreadData);
            }
            else
                QMessageBox::warning(this,
                                     "ERROR:","Mapping was not started. Please ensure the CORRECT markers are being tracked before performing mapping.");
        }
    }
} // performMapping()

void MainWindow::mappingDone(int *mappingVector)
{
    // Update GUI
    mappingVectorEdit->clear();
    mappingVectorEdit->setText(QString("[ "));
    for(int i=0;i<controlThreadData.nNodes;i++)
        mappingVectorEdit->setText(mappingVectorEdit->text()+QString::number(mappingVector[i])+QString(" "));
    mappingVectorEdit->setText(mappingVectorEdit->text()+QString("]"));
    // Set label view to start of vector
    mappingVectorEdit->setCursorPosition(0);
    // Reset flag
    controlThreadData.mapping=false;
    // Set GUI
    discoverEBugsButton->setEnabled(true);
    performMappingButton->setEnabled(true);
    performMappingButton->setText("Perform Mapping");
    xBeeDisconnectAction->setEnabled(true);
} // mappingDone()

void MainWindow::loadControlParametersFromFile()
{
    // Local variables
    int i=0;
    int nIgnore=0;
    // Check if file has been selected (i.e. not empty)
    if(!controlParameterFilenameEdit->text().isEmpty())
    {
        // Create file
        QFile file(controlParameterFilenameEdit->text());
        // Attempt to open file
        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
            QMessageBox::warning(this,"ERROR:","File could not be opened.\nCheck filename and/or path.");
        // If file opened successfully:
        else
        {
            // Create text stream
            QTextStream textStream(&file);
            // Clear text edit
            controlFileParametersEdit->clear();
            // Clear string list
            controlParameterStringList.clear();
            // Read text
            while (1)
            {
                // Read line
                QString line = textStream.readLine();
                // Check if at end of file
                if (line.isNull())
                {
                    // Close file
                    file.close();
                    // Store number of parameters read
                    nControlParam=i-2-nIgnore;
                    // Set GUI
                    startStopControlButton->setEnabled(true);
                    break;
                }
                else
                {
                    // Set controlIndexEdit
                    if(i==0)
                        controlIndexEdit->setText(line);
                    // Set controlTimeStepEdit
                    else if(i==1)
                        controlTimeStepEdit->setText(line);
                    // File contains too many parameters (exceeds allowable limit)
                    if(i>(MAX_CONTROL_PARAMETERS+1))
                    {
                        QMessageBox::warning(this,"ERROR:","Number of allowable parameters exceeded.\n"+QString("Parameter ")+QString::number(i)+QString(" will not be ignored."));
                        nIgnore++;
                    }
                    /////////////////////////////////
                    // SAVE and DISPLAY parameters //
                    /////////////////////////////////
                    if(i==2)
                    {
                        // Add to string list
                        controlParameterStringList.append(line);
                        // Display first parameter
                        controlFileParametersEdit->setText(controlFileParametersEdit->text()+line);
                    }
                    else if(i>2)
                    {
                        // Add to string list
                        controlParameterStringList.append(line);
                        // Include comma in between remaining parameters
                        controlFileParametersEdit->setText(controlFileParametersEdit->text()+QString(",")+line);
                    }
                    // Increment counter
                    i++;
                }
            }
            // Show dialog
            QMessageBox::information(this, "Control Parameters:", QString::number(nControlParam)+QString(" parameter(s) read."));
        }
    }
    else
        QMessageBox::warning(this,"ERROR:","Control parameter file not specified.");
} // loadControlParametersFromFile()

void MainWindow::setControlParameterFileName()
{
    // Create, show dialog, save chosen filename
    controlParameterFileName = QFileDialog::getOpenFileName(this,
                                                            tr("Open File"),
                                                            "",
                                                            tr("Control Parameter Files (*.param)"));
    // Set text edit to chosen filename
    controlParameterFilenameEdit->setText(controlParameterFileName);
} // setControlParameterFileName()

void MainWindow::startStopControl()
{
    // STOP CONTROL
    if(controlThreadData.controlOn)
    {
        // Set GUI
        browseControlParameterFileButton->setEnabled(true);
        loadControlParameterFileButton->setEnabled(true);
        startStopControlButton->setText("Start");
        controlMainLabel->clear();
        // Set control to OFF
        controlThreadData.controlOn=false;
        // Update data in controlThread (i.e. STOP control)
        emit newControlThreadData(controlThreadData);
    }
    // START CONTROL
    else
    {
        // Check if we are ready to begin control
        if(!mappingVectorEdit->text().isEmpty()&&processingThreadData.trackingOn)
        {
            // Set control index
            controlThreadData.controlIndex=controlIndexEdit->text().toInt();
            // Set control time step
            controlThreadData.controlTimeStep=controlTimeStepEdit->text().toInt();
            // Set control parameters
            for(int i=0;i<nControlParam;i++)
                controlThreadData.controlParams[i]=controlParameterStringList.at(i).toDouble();
            // Set GUI
            browseControlParameterFileButton->setEnabled(false);
            loadControlParameterFileButton->setEnabled(false);
            startStopControlButton->setText("Stop");
            // Set control to ON
            controlThreadData.controlOn=true;
            // Update data in controlThread (i.e. START control)
            emit newControlThreadData(controlThreadData);
        }
        // Display error dialog
        else
            QMessageBox::warning(this,"ERROR:","Mapping vector is empty AND/OR tracking NOT in progress. Control cannot be started.\n\nRequired steps:\n1. Start tracking\n2. Discover eBugs\n3. Perform mapping");
    }
} // startStopControl()
