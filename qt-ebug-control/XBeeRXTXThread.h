/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* XBeeRXTXThread.h                                                     */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef XBEERXTXTHREAD_H
#define XBEERXTXTHREAD_H

#include "Structures.h"

// Configuration header file
#include "Config.h"
// Qt header files
#include <QThread>
#include <QtGui>
// XBee class
#include <XBee.h>

class XBee;

class XBeeRXTXThread : public QThread
{
    Q_OBJECT

public:
    XBeeRXTXThread(XBee *xBee);
    ~XBeeRXTXThread();
    void stopXBeeRXTXThread();
private:
    XBee *xBee;
    bool flag;
    XBeeNodeTable xBeeNodeTable[MAX_NEBUGTRACK];
    volatile bool stopped;
    QMutex stoppedMutex;
    QMutex protectXBeeNodeTable;
    void readXBeePacket(unsigned char);
    void handleXBeeATCommandResponse(unsigned char *);
    int xBeeNodeTableIndex;
protected:
    void run();
public slots:
    void xBeeNDTimeout();
    void slotRead();
signals:
    void newXBeeNodeData(struct XBeeNodeTable *xBeeNodeTable, int xBeeNodeTableIndex);
    void xBeeRXTXThreadLoopEnd(int currentPacketBufferSize);
};

#endif // XBEERXTXTHREAD_H
