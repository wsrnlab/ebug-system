#ifndef VIDEO_STITCHER_H
#define VIDEO_STITCHER_H

#include <vector>

#include "opencv2/core/core.hpp"
#include "opencv2/nonfree/features2d.hpp"

/* class VideoStitcher
 *
 * Handles calculation of image correlation between two cameras, and stitching of the cameras'
 * frames. */
class VideoStitcher
{
public:
    VideoStitcher();
    bool matchFeatures( cv::Mat& img1,
                        cv::Mat& img2,
                        double minHessian = 1000. );
    bool calcHomography(void);
    bool correlate(cv::Mat& cam0_frame, cv::Mat& cam1_frame, uint match_threshold);
    cv::Mat stitchFrames(cv::Mat& img1, cv::Mat& img2);
    std::vector<cv::Point2f> getMatches(int img_num);
    void clearMatches(void);
    void setVideoInfo(cv::Mat& frame0, cv::Mat& frame1);
    bool cameraCorrelationKnown(void);
    void printInfo(void);

private:
    bool _Hcalculated, _videoInfoSet;
    cv::Mat H, H_inv;
    std::vector<cv::Point2f> cam0_frame_matches, cam1_frame_matches;
    cv::Size frames0_size, frames1_size, stitched_size;
    cv::Point2f frame0_origin;
    int frame_img_type;
};

#endif
