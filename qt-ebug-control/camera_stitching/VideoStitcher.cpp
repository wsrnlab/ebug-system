#include <iostream>

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "VideoStitcher.h"

using namespace std;
using namespace cv;

/* Put the coordinates of the corners of the given Size object into the corners vector */
void getImageCorners(Size sz, vector<Point2f>& corners)
{
    corners.push_back(Point2f( 0, 0));
    corners.push_back(Point2f( sz.width, 0 ));
    corners.push_back(Point2f( sz.width, sz.height ));
    corners.push_back(Point2f( 0, sz.height ));
}

VideoStitcher::VideoStitcher()
{
    this->frames0_size = Size(0,0);
    this->frames1_size = Size(0,0);
    this->stitched_size = Size(0,0);
    this->_Hcalculated = false;
    this->_videoInfoSet = false;
}

/* Print variables from the VideoStitcher object. Useful for debugging */
void VideoStitcher::printInfo(void)
{
    cout << "Video stitcher created:" << endl;
    cout << "Camera 0 res: " << this->frames0_size.width << "x" << this->frames0_size.height << endl;
    cout << "Camera 1 res: " << this->frames1_size.width << "x" << this->frames1_size.height << endl;
    cout << this->_Hcalculated << endl;
    cout << this->_videoInfoSet << endl;
}

/* Returns whether or not the camera correlation has been calculated */
bool VideoStitcher::cameraCorrelationKnown(void) { return this->_Hcalculated; }

/* Clears the image match vectors within the VideoStitcher object */
void VideoStitcher::clearMatches()
{
    this->cam0_frame_matches.clear();
    this->cam1_frame_matches.clear();
}

/* Saves information about the given camera frames into the VideoStitcher object */
void VideoStitcher::setVideoInfo(Mat& frame0, Mat& frame1)
{
    this->frames0_size = Size(frame0.cols, frame0.rows);
    this->frames1_size = Size(frame1.cols, frame1.rows);
    this->frame_img_type = frame0.type();
    this->_videoInfoSet = true;
}

/* Calculates a crude camera relationship by calculating the homography of a plane visible
 * within both images. This will result in a good camera correlation if the plane takes up a
 * large portion of each camera's field of view.
 */
bool VideoStitcher::calcHomography()
{
    // need at least 4 matching points to calculate a homography
    if (this->cam0_frame_matches.size() < 4)
        return false;
    if (this->cam0_frame_matches.size() != this->cam1_frame_matches.size())
    {
        cout << "calcHomography error: pt arrays size mismatch" << endl;
        return false;
    }
    try
    {
        //save the homography and its inverse
        this->H = findHomography( cam0_frame_matches, cam1_frame_matches, CV_RANSAC );
        this->H_inv = this->H.inv();

        // calculate the size of the stitched image by projecting the corners of frame1 into
        // frame0's coordinate system, then finding the max x&y coordinates.
        vector<Point2f> frame0_corners;
        vector<Point2f> frame1_corners;
        getImageCorners(this->frames0_size, frame0_corners);
        perspectiveTransform(frame0_corners, frame1_corners, this->H_inv);
        float xmin=9999, xmax=-9999, ymin=9999, ymax=-9999;
        for (uint i = 0; i < frame1_corners.size(); i++)
        {
            xmin = min(frame0_corners[i].x, xmin);
            xmin = min(frame1_corners[i].x, xmin);
            xmax = max(frame0_corners[i].x, xmax);
            xmax = max(frame1_corners[i].x, xmax);
            ymin = min(frame0_corners[i].y, ymin);
            ymin = min(frame1_corners[i].y, ymin);
            ymax = max(frame0_corners[i].y, ymax);
            ymax = max(frame1_corners[i].y, ymax);
        }
        //save the origin of frame0 in output image
        this->frame0_origin = Point2f(floor(0 - xmin), floor(0 - ymin));
        this->stitched_size = Size(ceil(xmax - xmin), ceil(ymax - ymin));
        this->_Hcalculated = true;
    }
    catch (exception& e)
    {
        cout << "Exception caught within findHomography." <<  endl;
        return false;
    }
    return true;
}

/* Detect, extract and match SURF features from each image. Store the matches within the
 * VideoStitcher object.
 */
bool VideoStitcher::matchFeatures(  Mat& img1,
                                    Mat& img2,
                                    double minHessian )
{
    if (this->_videoInfoSet == false) this->setVideoInfo(img1, img2);
    // detect keypoints
    SurfFeatureDetector detector( minHessian );
    vector<KeyPoint> img1_kp, img2_kp;
    detector.detect(img1, img1_kp);
    detector.detect(img2, img2_kp);

    // extract descriptors
    SurfDescriptorExtractor extractor;
    Mat img1_desc, img2_desc;
    extractor.compute( img1, img1_kp, img1_desc );
    extractor.compute( img2, img2_kp, img2_desc );

    // match features
    FlannBasedMatcher matcher;
    vector<DMatch> pre_matches;
    try {   matcher.match( img1_desc, img2_desc, pre_matches ); }
    catch (exception& e)
    {
        cout << "Exception caught in flann matcher" << endl;
        return false;
    }

    // Calculation of max and min distances between keypoints
    double max_dist = 0; double min_dist = 100;
    for( int i = 0; i < img1_desc.rows; i++ )
    {
        double dist = pre_matches[i].distance;
        if( dist < min_dist ) min_dist = dist;
        if( dist > max_dist ) max_dist = dist;
    }

    // get only "good" matches (i.e. whose distance is less than 3*min_dist )
    vector<DMatch> matches;
    for( int i = 0; i < img1_desc.rows; i++ )
    {
        if( pre_matches[i].distance < 3*min_dist )
            matches.push_back( pre_matches[i]);
    }

    // Get the coordinates of the good matches
    for( uint i = 0; i < matches.size(); i++ )
    {
        this->cam0_frame_matches.push_back( img1_kp[ matches[i].queryIdx ].pt );
        this->cam1_frame_matches.push_back( img2_kp[ matches[i].trainIdx ].pt );
    }
    return true;
}

/* Find the correlation of a plane visible to both cameras. If the plane takes up a large
 * portion of each camera's field of view, the correlation should be accurate enough to track
 * objects on the plane as they travel throughout each camera's field of view.
 *
 * This function will return true once enough information has been extracted from the camera
 * frames. This may take a number of frames. The number of frames can be lowered by lowering
 * the match_threshold, but this may result in a worse camera correlation.
 */
bool VideoStitcher::correlate(Mat& cam0_frame, Mat& cam1_frame, uint match_threshold)
{
    matchFeatures(cam0_frame, cam1_frame);
    if (this->cam0_frame_matches.size() >= match_threshold)
    {
        if (calcHomography())
        {
            clearMatches();
            return true;
        }
        else
        {
            cout << "Error during VideoStitcher::correlate" << endl;
            return false;
        }
    }
    else
        cout << "Accumulating feature matches. Remaining: " << match_threshold -
            this->cam0_frame_matches.size() << endl;
    return false;
}

/* Returns an image made up of the two input images. This can only be performed once the camera
 * correlation is known. frame1 is warped according to the camera correlation and pasted over the
 * top of frame0.
 */
Mat VideoStitcher::stitchFrames(Mat& frame0, Mat& frame1)
{
    Mat output(this->stitched_size.height, this->stitched_size.width, this->frame_img_type);
    if (this->H.empty())
    {
        cout << "Error in stitchImages: No H matrix calculated." << endl;
        return output;
    }
    // placeholder for frame1 manipulations
    Mat frame1_temp(this->stitched_size.height, this->stitched_size.width, this->frame_img_type);
    Mat frame1_mask_temp(this->stitched_size.height, this->stitched_size.width, this->frame_img_type);
    Rect roi = Rect(this->frame0_origin.x, this->frame0_origin.y, frame1.cols, frame1.rows);
    // copy frame1 to the placeholder
    Mat sub_frame1_temp = frame1_temp(roi);
    frame1.copyTo(sub_frame1_temp);
    sub_frame1_temp = frame1_mask_temp(roi);
    sub_frame1_temp.setTo(Scalar(255,255,255));

    // now warp frame1 to frame0 coordinates. The mask is used to only copy the relevant area of
    // frame1 to frame0.
    Mat frame1_warped, frame1_mask;
    warpPerspective(frame1_temp, frame1_warped, this->H_inv, Size(frame1_temp.cols, frame1_temp.rows));
    warpPerspective(frame1_mask_temp, frame1_mask, this->H_inv, Size(frame1_temp.cols, frame1_temp.rows));

    //paste frame0 and frame1 into a final output image
    Mat sub_output = output(roi);
    frame0.copyTo(sub_output);
    frame1_warped.copyTo(output,frame1_mask);

    return output;
}
