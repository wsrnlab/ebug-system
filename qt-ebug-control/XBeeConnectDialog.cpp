/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* XBeeConnectDialog.cpp                                                */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "XBeeConnectDialog.h"

// Qt header files
#include <QtGui>
// Configuration header file
#include "Config.h"

XBeeConnectDialog::XBeeConnectDialog(QWidget *parent) : QDialog(parent)
{
    // Setup dialog
    setupUi(this);
    // Setup combo boxes
    setupComboBoxes();
    // Connect signals to slots
    connect(detectXBeeButton,SIGNAL(released()),this,SLOT(detectConnectedSerialDevices()));
    connect(resetToDefaultsButton,SIGNAL(released()),this,SLOT(resetAllDialogToDefaults()));
    // Set dialog values to defaults
    resetAllDialogToDefaults();
    // Detect devices
    detectConnectedSerialDevices();
} // XBeeConnectDialog constructor

void XBeeConnectDialog::detectConnectedSerialDevices()
{
    // Local variables
    globbuf.gl_offs = 1;
    // Detect devices
    glob("/dev/ttyUSB*", GLOB_DOOFFS, NULL, &globbuf);
    globbuf.gl_pathv[0] = (char*)"ls";
    // Clear contents of combo box
    XBeeDeviceComboBox->clear();
    // Add detected devices to combo box
    for(unsigned int i=0;i<globbuf.gl_pathc;i++)
        XBeeDeviceComboBox->addItem(globbuf.gl_pathv[i+1]);
} // detectConnectedSerialDevices()

void XBeeConnectDialog::setupComboBoxes()
{
    // BAUD RATE
    QStringList baudRates;
    baudRates<<"1200"<<"2400"<<"4800"<<"9600"<<"19200"<<"38400"<<"57600"<<"115200";
    XBeeBaudRateComboBox->addItems(baudRates);
    // DATA BITS
    QStringList dataBits;
    dataBits<<"5"<<"6"<<"7"<<"8";
    XBeeDataBitsComboBox->addItems(dataBits);
    // STOP BITS
    QStringList stopBits;
    stopBits<<"1"<<"2";
    XBeeStopBitsComboBox->addItems(stopBits);
    // PARITY
    QStringList parity;
    parity<<"None"<<"Odd"<<"Even"<<"Space";
    XBeeParityComboBox->addItems(parity);
    // FLOW CONTROL
    QStringList flowControl;
    flowControl<<"Off"<<"Hardware"<<"XonXoff";
    XBeeFlowControlComboBox->addItems(flowControl);
} // setupComboBoxes()

void XBeeConnectDialog::resetAllDialogToDefaults()
{
    // Clear device combo box
    XBeeDeviceComboBox->clear();
    // BAUD RATE
    if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate1200)
        XBeeBaudRateComboBox->setCurrentIndex(0);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate2400)
        XBeeBaudRateComboBox->setCurrentIndex(1);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate4800)
        XBeeBaudRateComboBox->setCurrentIndex(2);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate9600)
        XBeeBaudRateComboBox->setCurrentIndex(3);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate19200)
        XBeeBaudRateComboBox->setCurrentIndex(4);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate38400)
        XBeeBaudRateComboBox->setCurrentIndex(5);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate57600)
        XBeeBaudRateComboBox->setCurrentIndex(6);
    else if(DEFAULT_BAUDRATE==AbstractSerial::BaudRate115200)
        XBeeBaudRateComboBox->setCurrentIndex(7);
    // DATA BITS
    if(DEFAULT_DATABITS==AbstractSerial::DataBits5)
        XBeeDataBitsComboBox->setCurrentIndex(0);
    else if(DEFAULT_DATABITS==AbstractSerial::DataBits6)
        XBeeDataBitsComboBox->setCurrentIndex(1);
    else if(DEFAULT_DATABITS==AbstractSerial::DataBits7)
        XBeeDataBitsComboBox->setCurrentIndex(2);
    else if(DEFAULT_DATABITS==AbstractSerial::DataBits8)
        XBeeDataBitsComboBox->setCurrentIndex(3);
    // STOP BITS
    if(DEFAULT_STOPBITS==AbstractSerial::StopBits1)
        XBeeStopBitsComboBox->setCurrentIndex(0);
    else if(DEFAULT_STOPBITS==AbstractSerial::StopBits2)
        XBeeStopBitsComboBox->setCurrentIndex(1);
    // PARITY
    if(DEFAULT_PARITY==AbstractSerial::ParityNone)
        XBeeParityComboBox->setCurrentIndex(0);
    else if(DEFAULT_PARITY==AbstractSerial::ParityOdd)
        XBeeParityComboBox->setCurrentIndex(1);
    else if(DEFAULT_PARITY==AbstractSerial::ParityEven)
        XBeeParityComboBox->setCurrentIndex(2);
    else if(DEFAULT_PARITY==AbstractSerial::ParitySpace)
        XBeeParityComboBox->setCurrentIndex(3);
    // FLOW CONTROL
    if(DEFAULT_FLOWCONTROL==AbstractSerial::FlowControlOff)
        XBeeFlowControlComboBox->setCurrentIndex(0);
    else if(DEFAULT_FLOWCONTROL==AbstractSerial::FlowControlHardware)
        XBeeFlowControlComboBox->setCurrentIndex(1);
    else if(DEFAULT_FLOWCONTROL==AbstractSerial::FlowControlXonXoff)
        XBeeFlowControlComboBox->setCurrentIndex(2);
    // Set buffer size
    xBeePacketBufferSizeEdit->setText(QString::number(DEFAULT_XBEE_PACKET_BUFFER_SIZE));
} // resetAllDialogToDefaults()

bool XBeeConnectDialog::updateStoredSettingsFromDialog()
{
    // Validate values in dialog before storing
    if(validateDialog())
    {
        // Device name
        serialPortData.portName=XBeeDeviceComboBox->itemText(XBeeDeviceComboBox->currentIndex());
        // BAUD RATE
        if(XBeeBaudRateComboBox->currentIndex()==0)
            serialPortData.baudRate=AbstractSerial::BaudRate1200;
        else if(XBeeBaudRateComboBox->currentIndex()==1)
            serialPortData.baudRate=AbstractSerial::BaudRate2400;
        else if(XBeeBaudRateComboBox->currentIndex()==2)
            serialPortData.baudRate=AbstractSerial::BaudRate4800;
        else if(XBeeBaudRateComboBox->currentIndex()==3)
            serialPortData.baudRate=AbstractSerial::BaudRate9600;
        else if(XBeeBaudRateComboBox->currentIndex()==4)
            serialPortData.baudRate=AbstractSerial::BaudRate19200;
        else if(XBeeBaudRateComboBox->currentIndex()==5)
            serialPortData.baudRate=AbstractSerial::BaudRate38400;
        else if(XBeeBaudRateComboBox->currentIndex()==6)
            serialPortData.baudRate=AbstractSerial::BaudRate57600;
        else if(XBeeBaudRateComboBox->currentIndex()==7)
            serialPortData.baudRate=AbstractSerial::BaudRate115200;
        // DATA BITS
        if(XBeeDataBitsComboBox->currentIndex()==0)
            serialPortData.dataBits=AbstractSerial::DataBits5;
        else if(XBeeDataBitsComboBox->currentIndex()==1)
            serialPortData.dataBits=AbstractSerial::DataBits6;
        else if(XBeeDataBitsComboBox->currentIndex()==2)
            serialPortData.dataBits=AbstractSerial::DataBits7;
        else if(XBeeDataBitsComboBox->currentIndex()==3)
            serialPortData.dataBits=AbstractSerial::DataBits8;
        // STOP BITS
        if(XBeeStopBitsComboBox->currentIndex()==0)
            serialPortData.stopBits=AbstractSerial::StopBits1;
        else if(XBeeStopBitsComboBox->currentIndex()==1)
            serialPortData.stopBits=AbstractSerial::StopBits2;
        // PARITY
        if(XBeeParityComboBox->currentIndex()==0)
            serialPortData.parity=AbstractSerial::ParityNone;
        else if(XBeeParityComboBox->currentIndex()==1)
            serialPortData.parity=AbstractSerial::ParityOdd;
        else if(XBeeParityComboBox->currentIndex()==2)
            serialPortData.parity=AbstractSerial::ParityEven;
        else if(XBeeParityComboBox->currentIndex()==3)
            serialPortData.parity=AbstractSerial::ParitySpace;
        // FLOW CONTROL
        if(XBeeFlowControlComboBox->currentIndex()==0)
            serialPortData.flowControl=AbstractSerial::FlowControlOff;
        else if(XBeeFlowControlComboBox->currentIndex()==1)
            serialPortData.flowControl=AbstractSerial::FlowControlHardware;
        else if(XBeeFlowControlComboBox->currentIndex()==2)
            serialPortData.flowControl=AbstractSerial::FlowControlXonXoff;
        // Buffer size
        packetBufferSize=xBeePacketBufferSizeEdit->text().toInt();
        return true;
    }
    else
        return false;
} // updateStoredSettingsFromDialog()

void XBeeConnectDialog::updateDialogSettingsFromStored()
{
    // Device name (clear and set to stored)
    for(int i=0;i<XBeeDeviceComboBox->count();i++)
        XBeeDeviceComboBox->removeItem(i);
    XBeeDeviceComboBox->setItemText(0,serialPortData.portName);
    // BAUD RATE
    if(serialPortData.baudRate==AbstractSerial::BaudRate1200)
        XBeeBaudRateComboBox->setCurrentIndex(0);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate2400)
        XBeeBaudRateComboBox->setCurrentIndex(1);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate4800)
        XBeeBaudRateComboBox->setCurrentIndex(2);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate9600)
        XBeeBaudRateComboBox->setCurrentIndex(3);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate19200)
        XBeeBaudRateComboBox->setCurrentIndex(4);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate38400)
        XBeeBaudRateComboBox->setCurrentIndex(5);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate57600)
        XBeeBaudRateComboBox->setCurrentIndex(6);
    else if(serialPortData.baudRate==AbstractSerial::BaudRate115200)
        XBeeBaudRateComboBox->setCurrentIndex(7);
    // DATA BITS
    if(serialPortData.dataBits==AbstractSerial::DataBits5)
        XBeeDataBitsComboBox->setCurrentIndex(0);
    else if(serialPortData.dataBits==AbstractSerial::DataBits6)
        XBeeDataBitsComboBox->setCurrentIndex(1);
    else if(serialPortData.dataBits==AbstractSerial::DataBits7)
        XBeeDataBitsComboBox->setCurrentIndex(2);
    else if(serialPortData.dataBits==AbstractSerial::DataBits8)
        XBeeDataBitsComboBox->setCurrentIndex(3);
    // STOP BITS
    if(serialPortData.stopBits==AbstractSerial::StopBits1)
        XBeeStopBitsComboBox->setCurrentIndex(0);
    else if(serialPortData.stopBits==AbstractSerial::StopBits2)
        XBeeStopBitsComboBox->setCurrentIndex(1);
    // PARITY
    if(serialPortData.parity==AbstractSerial::ParityNone)
        XBeeParityComboBox->setCurrentIndex(0);
    else if(serialPortData.parity==AbstractSerial::ParityOdd)
        XBeeParityComboBox->setCurrentIndex(1);
    else if(serialPortData.parity==AbstractSerial::ParityEven)
        XBeeParityComboBox->setCurrentIndex(2);
    else if(serialPortData.parity==AbstractSerial::ParitySpace)
        XBeeParityComboBox->setCurrentIndex(3);
    // FLOW CONTROL
    if(serialPortData.flowControl==AbstractSerial::FlowControlOff)
        XBeeFlowControlComboBox->setCurrentIndex(0);
    else if(serialPortData.flowControl==AbstractSerial::FlowControlHardware)
        XBeeFlowControlComboBox->setCurrentIndex(1);
    else if(serialPortData.flowControl==AbstractSerial::FlowControlXonXoff)
        XBeeFlowControlComboBox->setCurrentIndex(2);
    // Set buffer size
    packetBufferSize=xBeePacketBufferSizeEdit->text().toInt();
} // updateDialogSettingsFromStored()

bool XBeeConnectDialog::validateDialog()
{
    // Check if packet buffer size field is empty
    if(xBeePacketBufferSizeEdit->text().isEmpty())
    {
        xBeePacketBufferSizeEdit->setText(QString::number(DEFAULT_XBEE_PACKET_BUFFER_SIZE));
        QMessageBox::warning(this->parentWidget(),"WARNING:","Packet buffer size field empty.\n\nAutomatically set to default value.");
    }
    // Check if device name field is empty
    if(XBeeDeviceComboBox->currentText().isEmpty())
    {
        QMessageBox::warning(this->parentWidget(),"ERROR:","Device name field empty. Ensure that the device is connected.");
        return false;
    }
    else
        return true;
} // validateDialog()
