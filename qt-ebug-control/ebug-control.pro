QT       += core gui

TARGET = ebug-control
TEMPLATE = app

VERSION = 1.00

DEFINES += APP_VERSION=$$VERSION

FORMS = CameraConnectDialog.ui MainWindow.ui ImageProcessingSettingsDialog.ui TrackingSetupDialog.ui XBeeConnectDialog.ui

SOURCES += main.cpp\
        MainWindow.cpp \
    CaptureThread.cpp \
    Controller.cpp \
    ImageBuffer.cpp \
    CameraConnectDialog.cpp \
    ProcessingThread.cpp \
    FrameLabel.cpp \
    MatToQImage.cpp \
    TrackingSetupDialog.cpp \
    ImageProcessingSettingsDialog.cpp \
    pidcontroller.cpp \
    XBee.cpp \
    XBeeRXTXThread.cpp \
    ControlThread.cpp \
    XBeeConnectDialog.cpp \
    eBugAPI.cpp \
    control_algorithms/RandomPosition.cpp \
    control_algorithms/LineFormation.cpp \
    control_algorithms/TriangleFormation.cpp \
    control_algorithms/FormationControl.cpp \
    control_algorithms/CircleFormation.cpp \
    control_algorithms/DiagonalFormation1.cpp \
    control_algorithms/DiagonalFormation2.cpp \
    control_algorithms/DominosFormation.cpp \
    control_algorithms/PathFinding.cpp \
    control_algorithms/ECSEFormation.cpp \
    control_algorithms/TargetTracking.cpp \
    PIDController.cpp

HEADERS  += MainWindow.h \
    CaptureThread.h \
    Controller.h \
    ImageBuffer.h \
    CameraConnectDialog.h \
    ProcessingThread.h \
    FrameLabel.h \
    Structures.h \
    Config.h \
    MatToQImage.h \
    TrackingSetupDialog.h \
    ImageProcessingSettingsDialog.h \
    pidcontroller.h \
    XBee.h \
    XBeeRXTXThread.h \
    ControlThread.h \
    XBeeConnectDialog.h \
    eBugAPI.h \
    control_algorithms/RandomPosition.h \
    control_algorithms/LineFormation.h \
    control_algorithms/TriangleFormation.h \
    control_algorithms/FormationControl.h \
    control_algorithms/CircleFormation.h \
    control_algorithms/DiagonalFormation1.h \
    control_algorithms/DiagonalFormation2.h \
    control_algorithms/DominosFormation.h \
    control_algorithms/PathFinding.h \
    control_algorithms/ECSEFormation.h \
    control_algorithms/TargetTracking.h \
    PIDController.h

LIBS += -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_ml -lopencv_video -lopencv_features2d -lopencv_calib3d -lopencv_objdetect -lopencv_contrib -lopencv_legacy -lopencv_flann \
        -lARToolKitPlus -lqserialdevice







































