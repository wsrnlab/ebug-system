/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* MainWindow.h                                                         */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "MainWindow.ui"
#include "Structures.h"

// Configuration header file
#include "Config.h"

#define QUOTE_(x) #x
#define QUOTE(x) QUOTE_(x)

class CameraConnectDialog;
class XBeeConnectDialog;
class ImageProcessingSettingsDialog;
class TrackingSetupDialog;
class Controller;

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private:
    void initializeProcessingThreadDataStructure();
    void initializeTaskDataStructure();
    void initializeControlThreadDataStructure();
    void setInitGUIState();
    void signalSlotsInit();
    void setupTables();
    CameraConnectDialog *cameraConnectDialog;
    XBeeConnectDialog *xBeeConnectDialog;
    ImageProcessingSettingsDialog *imageProcessingSettingsDialog;
    TrackingSetupDialog *trackingSetupDialog;
    Controller *controller;
    ProcessingThreadData processingThreadData;
    TaskData taskData;
    EBugData eBugData[MAX_NEBUGTRACK];
    ControlThreadData controlThreadData;
    ARTKPSettings artkpSettings;
    QString appVersion;
    QStringList trackingTableHeaderList;
    QStringList xBeeNodeTableHeaderList;
    int sourceWidth;
    int sourceHeight;
    int deviceNumber;
    int imageBufferSize;
    bool isCameraConnected;
    bool isXBeeConnected;
    bool isTrackingStopped;
    int packetBufferSize;
    QTimer* nodeDiscoveryTimer;
    QString controlParameterFileName;
    QStringList controlParameterStringList;
    int nControlParam;
public slots:
    void connectToCamera();
    void disconnectCamera();
    void about();
    void clearImageBuffer();
    void setGrayscale(bool);
    void setSmooth(bool);
    void setDilate(bool);
    void setErode(bool);
    void setFlip(bool);
    void setImageProcessingSettings();
    void updateMouseCursorPosLabel();
    void newMouseData(struct MouseData);
    void startTracking();
    void stopTracking();
    void pauseResumeTracking();
    bool setTrackingSettings();
    void connectToXBee();
    void disconnectXBee();
    void performXBeeDiscovery();
    void performMapping();
    void loadControlParametersFromFile();
    void setControlParameterFileName();
    void startStopControl();
private slots:
    void updateFrame(const QImage &frame);
    void updateTrackingTable(struct EBugData*, cv::Rect);
    void onXBeeRXTXThreadLoop(int);
    void updateXBeeNodeTable(struct XBeeNodeTable *xBeeNodeTableData, int nNodes);
    void mappingDone(int *);
signals:
    void newProcessingThreadData(struct ProcessingThreadData processingThreadData);
    void newTaskData(struct TaskData taskData);
    void newControlThreadData(struct ControlThreadData controlThreadData);
};

#endif // MAINWINDOW_H
