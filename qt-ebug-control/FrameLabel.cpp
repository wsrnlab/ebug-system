/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* FrameLabel.cpp                                                       */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                              150                                        */
/************************************************************************/

#include "FrameLabel.h"

static bool clearTrailFlag = 0;
static bool lastClearTrail = 0;

FrameLabel::FrameLabel(QWidget *parent) : QLabel(parent)
{
    // Initialize variables
    startPoint.setX(0);
    startPoint.setY(0);
    mouseCursorPos.setX(0);
    mouseCursorPos.setY(0);
    drawBox=false;
    drawControlDataRandomPositionFlag=false;
    drawControlDataLineFormationFlag=false;
    drawControlDataTriangleFormationFlag=false;
    drawControlDataFormationControlFlag=0;
    drawControlDataCircleFormationFlag=false;
    drawControlDataDiagonalFormation1Flag=false;
    drawControlDataDiagonalFormation2Flag=false;
    drawControlDataDominosFormationFlag=false;
    drawControlDataECSEFormationFlag=false;
    drawControlDataPathFindingFlag=false;
    drawControlDataTargetTrackingFlag=false;
    // Initialize MouseData structure
    mouseData.leftButtonRelease=false;
    mouseData.rightButtonRelease=false;
    //
} // FrameLabel constructor

void FrameLabel::mouseMoveEvent(QMouseEvent *ev)
{
    // Save mouse cursor position
    setMouseCursorPos(ev->pos());
    // Update box width and height if box drawing is in progress
    if(drawBox)
    {
        box->setWidth(getMouseCursorPos().x()-startPoint.x());
        box->setHeight(getMouseCursorPos().y()-startPoint.y());
    }
    // Inform main window of mouse move event
    emit onMouseMoveEvent();
} // mouseMoveEvent()

void FrameLabel::setMouseCursorPos(QPoint input)
{
    mouseCursorPos=input;
} // setMouseCursorPos()

QPoint FrameLabel::getMouseCursorPos()
{
    return mouseCursorPos;
} // getMouseXPos()

void FrameLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    // Update cursor position
    setMouseCursorPos(ev->pos());
    // On left mouse button release
    if(ev->button()==Qt::LeftButton)
    {
        // Set leftButtonRelease flag to TRUE
        mouseData.leftButtonRelease=true;
        if(drawBox)
        {
            // Stop drawing box
            drawBox=false;
            // Save box dimensions
            mouseData.selectionBox.setX(box->left());
            mouseData.selectionBox.setY(box->top());
            mouseData.selectionBox.setWidth(box->width());
            mouseData.selectionBox.setHeight(box->height());
            // Set leftButtonRelease flag to TRUE
            mouseData.leftButtonRelease=true;
            // Inform main window of event
            emit newMouseData(mouseData);
        }
        // Set leftButtonRelease flag to FALSE
        mouseData.leftButtonRelease=false;
    }
    // On right mouse button release
    else if(ev->button()==Qt::RightButton)
    {
        // If user presses (and then releases) the right mouse button while drawing box, stop drawing box
        if(drawBox)
            drawBox=false;
        else
        {
            // Set rightButtonRelease flag to TRUE
            mouseData.rightButtonRelease=true;
            // Inform main window of event
            emit newMouseData(mouseData);
            // Set rightButtonRelease flag to FALSE
            mouseData.rightButtonRelease=false;
        }
    }
} // mouseReleaseEvent()

void FrameLabel::mousePressEvent(QMouseEvent *ev)
{
    // Update cursor position
    setMouseCursorPos(ev->pos());;
    if(ev->button()==Qt::LeftButton)
    {
        // Start drawing box
        startPoint=ev->pos();
        box=new QRect(startPoint.x(),startPoint.y(),0,0);
        drawBox=true;
    }
} // mousePressEvent()

void FrameLabel::paintEvent(QPaintEvent *ev)
{
    QLabel::paintEvent(ev);
    QPainter painter(this);
    // Draw box
    if(drawBox)
    {
        painter.setPen(Qt::blue);
        painter.drawRect(*box);
    }
    // Draw control data (RANDOM POSITION)
    else if(drawControlDataRandomPositionFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataRandomPosition[i].xCurrent+(this->width()-currentROI.width)/2,controlDataRandomPosition[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);
            // Draw eBug outline
            if(controlDataRandomPosition[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataRandomPosition[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataRandomPosition[i].stage==2)
                painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataRandomPosition[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataRandomPosition[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataRandomPosition[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataRandomPosition[i].angleCurrent*(PI/180)));
            // Change sign of dy controlDataRandomPosition(as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataRandomPosition[i].xCurrent+(this->width()-currentROI.width)/2,controlDataRandomPosition[i].yCurrent+(this->height()-currentROI.height)/2,controlDataRandomPosition[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataRandomPosition[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataRandomPosition[i].xCurrent+(this->width()-currentROI.width)/2,controlDataRandomPosition[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataRandomPosition[i].xDesired,((this->height()-currentROI.height)/2)+controlDataRandomPosition[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataRandomPositionFlag=false;
    }

    // Draw control data (LINE FORMATION)
    else if(drawControlDataLineFormationFlag)
    {
        //        // Draw goal points on shape (red)
        //        for(int j=0; j<(controlDataLineFormation[0].xArrayShape).size(); j++)
        //        {
        //            shape.insert(j,QPoint((int)controlDataLineFormation[0].xArrayShape[j]+(this->width()-currentROI.width)/2, (int)controlDataLineFormation[0].yArrayShape[j]+(this->height()-currentROI.height)/2));
        //            painter.setPen(Qt::red);
        //        }
        //        painter.drawPoints(shape.data(),shape.size());

        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataLineFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataLineFormation[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);
            // Draw eBug outline
            if(controlDataLineFormation[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataLineFormation[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataLineFormation[i].stage==2)
                painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataLineFormation[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataLineFormation[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataLineFormation[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataLineFormation[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataLineFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataLineFormation[i].yCurrent+(this->height()-currentROI.height)/2,controlDataLineFormation[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataLineFormation[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataLineFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataLineFormation[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataLineFormation[i].xDesired,((this->height()-currentROI.height)/2)+controlDataLineFormation[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataLineFormationFlag=false;
    }

    // Draw control data (TRIANGLE FORMATION)
    else if(drawControlDataTriangleFormationFlag)
    {
        //        // Draw goal points on shape (red)
        //        for(int j=0; j<(controlDataTriangleFormation[0].xArrayShape).size(); j++)
        //        {
        //            shape.insert(j,QPoint((int)controlDataTriangleFormation[0].xArrayShape[j]+(this->width()-currentROI.width)/2, (int)controlDataTriangleFormation[0].yArrayShape[j]+(this->height()-currentROI.height)/2));
        //            painter.setPen(Qt::red);
        //        }
        //        painter.drawPoints(shape.data(),shape.size());

        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataTriangleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTriangleFormation[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);
            // Draw eBug outline
            if(controlDataTriangleFormation[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataTriangleFormation[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataTriangleFormation[i].stage==2)
                painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataTriangleFormation[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataTriangleFormation[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataTriangleFormation[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataTriangleFormation[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataTriangleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTriangleFormation[i].yCurrent+(this->height()-currentROI.height)/2,controlDataTriangleFormation[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataTriangleFormation[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataTriangleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTriangleFormation[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataTriangleFormation[i].xDesired,((this->height()-currentROI.height)/2)+controlDataTriangleFormation[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataTriangleFormationFlag=false;
    }
    // Draw control data (FORMATION CONTROL)
    else if(drawControlDataFormationControlFlag)
    {
        // Draw goal points on shape (red)
        for(int j=0; j<(controlDataFormationControl[0].xArrayShape).size(); j++)
        {
            shape.insert(j,QPoint((int)controlDataFormationControl[0].xArrayShape[j]+(this->width()-currentROI.width)/2, (int)controlDataFormationControl[0].yArrayShape[j]+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::red);
        }
        painter.drawPoints(shape.data(),shape.size());

        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            //qDebug("drawControlDataFormationControlFlag: %d", drawControlDataFormationControlFlag);

            // clear all trails
            if (clearTrailFlag)
            {
                // need a loop here to prevent clearTrailFlag (separate thread) from being set to 0 before second trail can be cleared
                for(int j=0;j<nTracked;j++)
                {
                    //qDebug("Clearing trail %d", j);
                    trail[j].clear();
                    //qDebug("Cleared trail %d", j);
                    clearTrailFlag = 0;
                }
            }            
            else if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataFormationControl[i].xCurrent+(this->width()-currentROI.width)/2,controlDataFormationControl[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());

            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            // Draw eBug outline
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataFormationControl[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataFormationControl[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataFormationControl[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataFormationControl[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataFormationControl[i].xCurrent+(this->width()-currentROI.width)/2,controlDataFormationControl[i].yCurrent+(this->height()-currentROI.height)/2,controlDataFormationControl[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataFormationControl[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataFormationControl[i].xCurrent+(this->width()-currentROI.width)/2,controlDataFormationControl[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataFormationControl[i].xDesired,((this->height()-currentROI.height)/2)+controlDataFormationControl[i].yDesired);

            /*
            // obtain reference angle
            static double refAngle = 0;

            if ((controlDataFormationControl[i].leader1 == -1) && (controlDataFormationControl[i].leader2 == -1))
            {
                refAngle = controlDataFormationControl[i].angleCurrent - 180;

                while (refAngle < -180)
                {
                    refAngle += 360;
                }

                while (refAngle > 180)
                {
                    refAngle -= 360;
                }
            }

            // plotting reference angle on all eBugs
            int refX = controlDataFormationControl[i].xCurrent + 50*cos(PI/180*refAngle);
            int refY = controlDataFormationControl[i].yCurrent - 50*sin(PI/180*refAngle);

            painter.setPen(Qt::blue);
            painter.drawLine(controlDataFormationControl[i].xCurrent+(this->width()-currentROI.width)/2,controlDataFormationControl[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+refX,((this->height()-currentROI.height)/2)+refY);
            */
        }
        // Set flag to disable drawing
        drawControlDataFormationControlFlag=0;
    }

    // Draw control data (CIRCLE FORMATION)
    else if(drawControlDataCircleFormationFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataCircleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataCircleFormation[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            /*
            // Draw eBug outline
            if(controlDataCircleFormation[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataCircleFormation[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataCircleFormation[i].stage==2)
                painter.setPen(Qt::darkGreen);
            */
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataCircleFormation[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataCircleFormation[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataCircleFormation[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataCircleFormation[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataCircleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataCircleFormation[i].yCurrent+(this->height()-currentROI.height)/2,controlDataCircleFormation[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataCircleFormation[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataCircleFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataCircleFormation[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataCircleFormation[i].xDesired,((this->height()-currentROI.height)/2)+controlDataCircleFormation[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataCircleFormationFlag=false;
    }
    // Draw control data (DIAGONAL FORMATION1)
    else if(drawControlDataDiagonalFormation1Flag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataDiagonalFormation1[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation1[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            /*
              // Draw eBug outline
              if(controlDataDiagonalFormation1[i].stage==0)
                  painter.setPen(QColor(255,127,0));
              else if(controlDataDiagonalFormation1[i].stage==1)
                  painter.setPen(Qt::darkRed);
              else if(controlDataDiagonalFormation1[i].stage==2)
                  painter.setPen(Qt::darkGreen);
              */
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataDiagonalFormation1[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataDiagonalFormation1[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataDiagonalFormation1[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataDiagonalFormation1[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataDiagonalFormation1[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation1[i].yCurrent+(this->height()-currentROI.height)/2,controlDataDiagonalFormation1[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataDiagonalFormation1[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataDiagonalFormation1[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation1[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataDiagonalFormation1[i].xDesired,((this->height()-currentROI.height)/2)+controlDataDiagonalFormation1[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataDiagonalFormation1Flag=false;
    }
    // Draw control data (DIAGONAL FORMATION2)
    else if(drawControlDataDiagonalFormation2Flag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataDiagonalFormation2[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation2[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            /*
              // Draw eBug outline
              if(controlDataDiagonalFormation2[i].stage==0)
                  painter.setPen(QColor(255,127,0));
              else if(controlDataDiagonalFormation2[i].stage==1)
                  painter.setPen(Qt::darkRed);
              else if(controlDataDiagonalFormation2[i].stage==2)
                  painter.setPen(Qt::darkGreen);
              */
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataDiagonalFormation2[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataDiagonalFormation2[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataDiagonalFormation2[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataDiagonalFormation2[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw lineRandomPosition
            painter.drawLine(controlDataDiagonalFormation2[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation2[i].yCurrent+(this->height()-currentROI.height)/2,controlDataDiagonalFormation2[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataDiagonalFormation2[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataDiagonalFormation2[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDiagonalFormation2[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataDiagonalFormation2[i].xDesired,((this->height()-currentROI.height)/2)+controlDataDiagonalFormation2[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataDiagonalFormation2Flag=false;
    }
    else if(drawControlDataDominosFormationFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataDominosFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDominosFormation[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            /*
            // Draw eBug outline
            if(controlDataDominosFormation[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataDominosFormation[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataDominosFormation[i].stage==2)
                painter.setPen(Qt::darkGreen);
            */
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataDominosFormation[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataDominosFormation[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataDominosFormation[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataDominosFormation[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataDominosFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDominosFormation[i].yCurrent+(this->height()-currentROI.height)/2,controlDataDominosFormation[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataDominosFormation[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataDominosFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataDominosFormation[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataDominosFormation[i].xDesired,((this->height()-currentROI.height)/2)+controlDataDominosFormation[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataDominosFormationFlag=false;
    }
    else if(drawControlDataECSEFormationFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataECSEFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataECSEFormation[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataECSEFormation[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataECSEFormation[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataECSEFormation[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataECSEFormation[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataECSEFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataECSEFormation[i].yCurrent+(this->height()-currentROI.height)/2,controlDataECSEFormation[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataECSEFormation[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataECSEFormation[i].xCurrent+(this->width()-currentROI.width)/2,controlDataECSEFormation[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataECSEFormation[i].xDesired,((this->height()-currentROI.height)/2)+controlDataECSEFormation[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataECSEFormationFlag=false;
    }
    // Draw control data (PATH FINDING)
    else if(drawControlDataPathFindingFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataPathFinding[i].xCurrent+(this->width()-currentROI.width)/2,controlDataPathFinding[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);
            // Draw eBug outline
            if(controlDataPathFinding[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataPathFinding[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataPathFinding[i].stage==2)
                painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataPathFinding[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataPathFinding[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataPathFinding[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataPathFinding[i].angleCurrent*(PI/180)));
            // Change sign of dy controlDataPathFinding(as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataPathFinding[i].xCurrent+(this->width()-currentROI.width)/2,controlDataPathFinding[i].yCurrent+(this->height()-currentROI.height)/2,controlDataPathFinding[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataPathFinding[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataPathFinding[i].xCurrent+(this->width()-currentROI.width)/2,controlDataPathFinding[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataPathFinding[i].xDesired,((this->height()-currentROI.height)/2)+controlDataPathFinding[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataPathFindingFlag=false;
    }
    else if(drawControlDataTargetTrackingFlag)
    {
        for(int i=0;i<nTracked;i++)
        {
            // Draw path with single points
            if(trail[i].size()==DRAW_TRAIL_NO_OF_POINTS)
                trail[i].remove(DRAW_TRAIL_NO_OF_POINTS-1);
            else
                trail[i].insert(0,QPoint(controlDataTargetTracking[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTargetTracking[i].yCurrent+(this->height()-currentROI.height)/2));
            painter.setPen(Qt::gray);
            painter.drawPoints(trail[i].data(),trail[i].size());
            // Draw ROI outline (red)
            painter.setPen(Qt::red);
            painter.drawRect((this->width()-currentROI.width)/2,(this->height()-currentROI.height)/2,currentROI.width,currentROI.height);

            /*
            // Draw eBug outline
            if(controlDataCircleFormation[i].stage==0)
                painter.setPen(QColor(255,127,0));
            else if(controlDataCircleFormation[i].stage==1)
                painter.setPen(Qt::darkRed);
            else if(controlDataCircleFormation[i].stage==2)
                painter.setPen(Qt::darkGreen);
            */
            painter.setPen(Qt::darkGreen);
            painter.drawEllipse(controlDataTargetTracking[i].xCurrent-(EBUG_SIZE_PIXELS/2)+(this->width()-currentROI.width)/2, controlDataTargetTracking[i].yCurrent-(EBUG_SIZE_PIXELS/2)+(this->height()-currentROI.height)/2, EBUG_SIZE_PIXELS, EBUG_SIZE_PIXELS);
            ///////////////////////////////////////////
            // Draw angle tracking pattern direction //
            ///////////////////////////////////////////
            // Calculate line end-points
            int dx=(int)roundNumber((EBUG_SIZE_PIXELS/2)*cos(controlDataTargetTracking[i].angleCurrent*(PI/180)));
            int dy=(int)roundNumber((EBUG_SIZE_PIXELS/2)*sin(controlDataTargetTracking[i].angleCurrent*(PI/180)));
            // Change sign of dy (as ORIGIN=TOP-LEFT CORNER of label)
            dy*=-1;
            // Draw line
            painter.drawLine(controlDataTargetTracking[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTargetTracking[i].yCurrent+(this->height()-currentROI.height)/2,controlDataTargetTracking[i].xCurrent+((this->width()-currentROI.width)/2)+dx,controlDataTargetTracking[i].yCurrent+((this->height()-currentROI.height)/2)+dy);
            ///////////////////////////////////////////////////////
            // Draw line from centre of eBug to desired position //
            ///////////////////////////////////////////////////////
            painter.setPen(Qt::black);
            painter.drawLine(controlDataTargetTracking[i].xCurrent+(this->width()-currentROI.width)/2,controlDataTargetTracking[i].yCurrent+(this->height()-currentROI.height)/2,((this->width()-currentROI.width)/2)+controlDataTargetTracking[i].xDesired,((this->height()-currentROI.height)/2)+controlDataTargetTracking[i].yDesired);
        }
        // Set flag to disable drawing
        drawControlDataTargetTrackingFlag=false;
    }

} // paintEvent()

double FrameLabel::roundNumber(double r)
{
    // This function rounds a double to a whole number (also double)
    // e.g: roundNumber(4.5)=5.0, roundNumber(-4.5)=-5.0
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
} // roundNumber()

void FrameLabel::drawControlDataRandomPosition(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataRandomPosition *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataRandomPosition[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataRandomPosition[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataRandomPosition[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataRandomPosition[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataRandomPosition[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataRandomPosition[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataRandomPositionFlag=true;
    // Draw
    update();
} // drawControlDataRandomPosition()

void FrameLabel::drawControlDataLineFormation(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataLineFormation *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataLineFormation[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataLineFormation[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataLineFormation[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataLineFormation[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataLineFormation[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataLineFormation[i].stage=controlDataIn[j].stage;
                //                this->controlDataLineFormation[i].xArrayShape=controlDataIn[j].xArrayShape;
                //                this->controlDataLineFormation[i].yArrayShape=controlDataIn[j].yArrayShape;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataLineFormationFlag=true;
    // Draw
    update();
} // drawControlDataLineFormation()

void FrameLabel::drawControlDataTriangleFormation(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataTriangleFormation *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataTriangleFormation[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataTriangleFormation[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataTriangleFormation[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataTriangleFormation[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataTriangleFormation[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataTriangleFormation[i].stage=controlDataIn[j].stage;
                //                this->controlDataTriangleFormation[i].xArrayShape=controlDataIn[j].xArrayShape;
                //                this->controlDataTriangleFormation[i].yArrayShape=controlDataIn[j].yArrayShape;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataTriangleFormationFlag=true;
    // Draw
    update();
} // drawControlDataTriangleFormation()

void FrameLabel::drawControlDataFormationControl(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataFormationControl *controlDataIn, bool *clearTrail)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataFormationControl[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataFormationControl[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataFormationControl[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataFormationControl[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataFormationControl[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataFormationControl[i].stage=controlDataIn[j].stage;
                this->controlDataFormationControl[i].xArrayShape=controlDataIn[j].xArrayShape;
                this->controlDataFormationControl[i].yArrayShape=controlDataIn[j].yArrayShape;
                this->controlDataFormationControl[i].leader1=controlDataIn[j].leader1;
                this->controlDataFormationControl[i].leader2=controlDataIn[j].leader2;
            }
        }
    }

    // rising edge to clear trail
    if ((lastClearTrail == 0)&&(*clearTrail == 1))
    {
        //qDebug("Set ClearTrailFlag");
        clearTrailFlag = 1;
    }
    // still pending for clear trail
    else if ((lastClearTrail == 1)&&(*clearTrail == 1))
    {
        // trail has been cleared
        if (clearTrailFlag == 0)
        {
            //qDebug("Trail cleared");
            *clearTrail = 0;
        }
    }

    //qDebug("set draw formation control flag");
    lastClearTrail = *clearTrail;
    drawControlDataFormationControlFlag=1;

    // Draw
    update();
} // drawControlDataFormationControl()

void FrameLabel::drawControlDataCircleFormation(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataCircleFormation *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataCircleFormation[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataCircleFormation[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataCircleFormation[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataCircleFormation[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataCircleFormation[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataCircleFormation[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataCircleFormationFlag=true;
    // Draw
    update();
} // drawControlDataCircleFormation()

void FrameLabel::drawControlDataDiagonalFormation1(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataDiagonalFormation1 *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataDiagonalFormation1[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataDiagonalFormation1[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataDiagonalFormation1[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataDiagonalFormation1[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataDiagonalFormation1[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataDiagonalFormation1[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataDiagonalFormation1Flag=true;
    // Draw
    update();
} // drawControlDataDiagonalFormation1()

void FrameLabel::drawControlDataDiagonalFormation2(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataDiagonalFormation2 *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataDiagonalFormation2[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataDiagonalFormation2[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataDiagonalFormation2[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataDiagonalFormation2[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataDiagonalFormation2[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataDiagonalFormation2[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataDiagonalFormation2Flag=true;
    // Draw
    update();
} // drawControlDataDiagonalFormation2()

void FrameLabel::drawControlDataDominosFormation(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataDominosFormation *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataDominosFormation[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataDominosFormation[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataDominosFormation[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataDominosFormation[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataDominosFormation[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataDominosFormation[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataDominosFormationFlag=true;
    // Draw
    update();
} // drawControlDataDominosFormation()

void FrameLabel::drawControlDataECSEFormation(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataECSEFormation *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataECSEFormation[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataECSEFormation[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataECSEFormation[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataECSEFormation[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataECSEFormation[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataECSEFormation[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataECSEFormationFlag=true;
    // Draw
    update();
} // drawControlDataECSEFormation()

void FrameLabel::drawControlDataPathFinding(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataPathFinding *controlDataIn)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataPathFinding[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataPathFinding[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataPathFinding[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataPathFinding[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataPathFinding[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataPathFinding[i].stage=controlDataIn[j].stage;
            }
        }
    }
    // Set flag to enable drawing
    drawControlDataPathFindingFlag=true;
    // Draw
    update();
} // drawControlDataPathFinding()

void FrameLabel::drawControlDataTargetTracking(cv::Rect ROI, int nTrackedIn, int nNodesIn, QList<int> trackedIDs, QList<int> validIDs, struct ControlDataTargetTracking *controlDataIn, bool *clearTrail)
{
    // Save number of nodes
    this->nTracked=nTrackedIn;
    // Save ROI
    this->currentROI.x=ROI.x;
    this->currentROI.y=ROI.y;
    this->currentROI.width=ROI.width;
    this->currentROI.height=ROI.height;
    // Save local copy of controlData
    for(int i=0;i<nTracked;i++)
    {
        for(int j=0;j<nNodesIn;j++)
        {
            if(trackedIDs.at(i)==validIDs.at(j))
            {
                this->controlDataTargetTracking[i].xDesired=controlDataIn[j].xDesired;
                this->controlDataTargetTracking[i].yDesired=controlDataIn[j].yDesired;
                this->controlDataTargetTracking[i].xCurrent=controlDataIn[j].xCurrent;
                this->controlDataTargetTracking[i].yCurrent=controlDataIn[j].yCurrent;
                this->controlDataTargetTracking[i].angleCurrent=controlDataIn[j].angleCurrent;
                this->controlDataTargetTracking[i].isTarget=controlDataIn[j].isTarget;
            }
        }
    }

    // rising edge to clear trail
    if ((lastClearTrail == 0)&&(*clearTrail == 1))
    {
        //qDebug("Set ClearTrailFlag");
        clearTrailFlag = 1;
    }
    // still pending for clear trail
    else if ((lastClearTrail == 1)&&(*clearTrail == 1))
    {
        // trail has been cleared
        if (clearTrailFlag == 0)
        {
            //qDebug("Trail cleared");
            *clearTrail = 0;
        }
    }

    //qDebug("set draw formation control flag");
    lastClearTrail = *clearTrail;
    drawControlDataTargetTrackingFlag=1;

    // Draw
    update();
} // drawControlDataTargetTracking()
