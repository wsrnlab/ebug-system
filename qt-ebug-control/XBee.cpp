/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* XBee.cpp                                                             */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2012 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "XBee.h"

XBee::XBee(int bufferSize) : bufferSize(bufferSize)
{
    // Semaphore initializations
    freeSlots = new QSemaphore(bufferSize);
    usedSlots = new QSemaphore(0);
    clearBuffer1 = new QSemaphore(1);
    clearBuffer2 = new QSemaphore(1);
    // Create port
    port = new AbstractSerial();
} // XBee constructor

XBee::~XBee()
{
} // XBee destructor

void XBee::addPacket(const QByteArray &packetData)
{
    // Acquire semaphores
    clearBuffer1->acquire();
    freeSlots->acquire();
    // Add packet to queue
    mutex.lock();
    packetQueue.enqueue(packetData);
    mutex.unlock();
    clearBuffer1->release();
    usedSlots->release();
} // addPacket()

QByteArray XBee::getPacket()
{
    // Local variables
    QByteArray temp;
    // Resize array to zero
    temp.resize(0);
    // Acquire semaphore
    clearBuffer2->acquire();
    // Wait for packet to arrive if queue is empty
    usedSlots->acquire();
    // Take packet from queue
    mutex.lock();
    temp=packetQueue.dequeue();
    mutex.unlock();
    freeSlots->release();
    // Release semaphore
    clearBuffer2->release();
    // Return packet to caller
    return temp;
} // getPacket()

void XBee::clearPacketBuffer()
{
    // Check if buffer is not empty
    if(packetQueue.size()!=0)
    {
        // Stop adding packets to buffer
        clearBuffer1->acquire();
        // Stop taking packets from buffer
        clearBuffer2->acquire();
        // Release all remaining slots in queue
        freeSlots->release(packetQueue.size());
        // Acquire all queue slots
        freeSlots->acquire(bufferSize);
        // Reset usedSlots to zero
        usedSlots->acquire(packetQueue.size());
        // Clear buffer by dequeuing items
        while(packetQueue.size()!=0)
        {
            // Local temporary storage
            QByteArray temp;
            // Dequeue IplImage
            temp=packetQueue.dequeue();
        }
        // Release all slots
        freeSlots->release(bufferSize);
        // Allow getPacket() to resume
        clearBuffer2->release();
        // Allow addPacket() to resume
        clearBuffer1->release();
        qDebug() << "Packet buffer successfully cleared.";
    }
    else
        qDebug() << "WARNING: Could not clear packet buffer: already empty.";
} // clearPacketBuffer()

int XBee::getSizeOfPacketBuffer()
{
    return packetQueue.size();
} // getSizeOfPacketBuffer()

char XBee::calculateXBeeChecksum(QByteArray *packetIn)
{
    // Local variables
    int packetLength;
    int checksumTotal=0;
    // Save packet length in local 16-bit variable
    packetLength=((packetIn->at(1)<<8)&0xFF)+(packetIn->at(2)&0xFF);
    // Calculate checksum
    for(int i=0;i<packetLength;i++)
        checksumTotal+=packetIn->at(3+i);
    // Return checksum value
    return (0xFF-checksumTotal);
} // calculateXBeeChecksum()

QByteArray XBee::makePacket_XBeeATCommand_ND()
{
    // Local variables
    QByteArray packet;
    // Packet has a fixed size of 8 bytes
    packet.resize(8);
    // Create packet
    packet[0]=0x7e; // Delimiter
    packet[1]=0x00; // Length (MSB)
    packet[2]=0x04; // Length (LSB)
    packet[3]=0x08; // AT Command API frame type
    packet[4]=0x01; // Frame ID (set to non-zero value)
    packet[5]=0x4e; // AT command ('N')
    packet[6]=0x44; // AT command ('D')
    // Calculate checksum and save in packet array
    packet[7]=calculateXBeeChecksum(&packet);
    // Return packet to caller
    return packet;
} // makePacket_XBeeATCommand_ND()

QByteArray XBee::makePacket_eBugPacketToXBeePacket(struct XBeeNodeTable xBeeNodeTableData, const QByteArray &eBugPacket)
{
    // Local variables
    QByteArray packet;
    // Set packet size
    packet.resize(eBugPacket.size()+18);
    ///////////////////
    // Create packet //
    ///////////////////
    packet[0]=0x7e; // Delimiter
    // LENGTH {MSB,LSB]
    if((14+eBugPacket.size())>255)
    {
        packet[1]=((14+eBugPacket.size())>>8)&0xFF;
        packet[2]=(14+eBugPacket.size())&0xFF;
    }
    else
    {
        packet[1]=0x00;
        packet[2]=14+eBugPacket.size();
    }
    packet[3]=0x10; // ZigBee Transmit Request frame type
    packet[4]=0x00; // Frame ID (set to zero: no Transmit Status packet sent from destination)
    // Serial Number (64-bit destination address)
    packet[5]=(xBeeNodeTableData.SH_high>>8)&0xFF;
    packet[6]=xBeeNodeTableData.SH_high&0xFF;
    packet[7]=(xBeeNodeTableData.SH_low>>8)&0xFF;
    packet[8]=xBeeNodeTableData.SH_low&0xFF;
    packet[9]=(xBeeNodeTableData.SL_high>>8)&0xFF;
    packet[10]=xBeeNodeTableData.SL_high&0xFF;
    packet[11]=(xBeeNodeTableData.SL_low>>8)&0xFF;
    packet[12]=xBeeNodeTableData.SL_low&0xFF;
    // Network Address (16-bit)
    packet[13]=(xBeeNodeTableData.MY>>8)&0xFF;
    packet[14]=(xBeeNodeTableData.MY)&0xFF;
    // Broadcast Radius
    packet[15]=0x00;
    // Options
    packet[16]=0x00;
    ////////////////////////
    // Insert eBug Packet //
    ////////////////////////
    for(int i=0;i<eBugPacket.size();i++)
        packet[17+i]=eBugPacket.at(i);
    // Calculate checksum and save in packet array
    packet[17+eBugPacket.size()]=calculateXBeeChecksum(&packet);
    // Return packet to caller
    return packet;
} // makePacket_eBugPacketToXBeePacket()
