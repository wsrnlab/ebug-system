#ifndef FORMATIONCONTROL_H
#define FORMATIONCONTROL_H

// eBug-II API
#include <eBugAPI.h>
// XBee class
#include <XBee.h>

static const int MAX_STEPPER_MOTOR_FREQ = 5000;
static const int MAX_EBUG_AS_OBSTACLE = 10;
static const int MAX_FORMATION_TYPE = 1;
static const double V_TO_FREQ_CONSTANT = 25.2792;
static const double W_TO_FREQ_CONSTANT = 8.6972;
static const int MIN_STOPPING_DISTANCE = 200;
static const int EBUG_RADIUS = EBUG_SIZE_PIXELS/2;

enum nFormationIndex
{
    triangle = 0,
    square,
    circle,
    line
};

// sets the order in which stages are executed (must be 1 apart between adjacent stages)
enum stagesIndex
{
    initializeSystem = 0,
    assignFormationPos,
    moveFormation,
    buildFormation
};

struct FormationConstraint {
    double angle1;
    double angle2;
    int dist1;
    int dist2;
};

// ControlDataFormationControl structure definition
struct ControlDataFormationControl{
    int stage;
    int xDesired;
    int yDesired;
    int xCurrent;
    int yCurrent;
    int ID;
    double angleCurrent;
    double angleDesired; // angle between eBug and target coordinate
    double distanceToTargetSquared; // distance squared to target coordinate
    int hasStopped;
    int leader1;
    double angleConstraint1;
    int distanceConstraint1;
    int leader2;
    double angleConstraint2;
    int distanceConstraint2;
    int isObstacle;
    QVector<int> xArrayShape;
    QVector<int> yArrayShape;
};

int formationControl(XBee *, struct XBeeNodeTable, struct EBugData*, int, int, int, struct ControlDataFormationControl*, double *, bool &, bool *);

#endif // FORMATIONCONTROL_H
