
#include "TargetTracking.h"
#include "RandomPosition.h"

/* Changelog and TODO list
  26 July, 2012
  Wrote in basic functions, and pseudo code
  Planning path of target

  TODO
  Write target controller
  Write PSO controller
  Write Potential Field controller
*/

//Function Prototypes
double DistanceSquare(int, int, int, int);
double TargetPosition(IplImage *);
double PsoController(void); // PSO-based target tracking algorithm
double PfController(void); // Potential field-based target tracking algorithm


//Threads
/* Port streaming threads here to read data :) */

//Global Variables

int targetTracking(XBee *xBee, struct XBeeNodeTable xBeeNodeTable,
                   struct EBugData* ebugData, int ebugDataIndex, int currentEbugIndex, int nNodes, cv::Rect CurrentROI,
                   struct ControlDataTargetTracking* controlData,
                   double* parameters, bool &resetControlFlag, bool *clearTrail)
{
    // Setting target position
  /* Pseudo code:
     Choose controller to use
     Find target position
     Find next position for each robot
  */
    int nParameter;

    qDebug("nodes %d", nNodes);
    qDebug("%d\n", ebugDataIndex);

    return 0;
}//TargetTracking

double DistanceSquare(int x1, int x2, int y1, int y2){
    int x = pow((x2-x1),2);
    int y = pow((y2-y1),2);
    return x + y;
}//DistanceSquare
