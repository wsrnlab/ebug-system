#ifndef LINEFORMATION_H
#define LINEFORMATION_H

// eBug-II API
#include <eBugAPI.h>
// XBee class
#include <XBee.h>
// PID controller
#include "pidcontroller.h"

// ControlDataLineFormation structure definition
struct ControlDataLineFormation{
    int stage;
    int xDesired;
    int yDesired;
    int xCurrent;
    int yCurrent;
    int angleCurrent;
    int angleErrTermCount;
    int distanceSquaredCurrent;
    int collisionDistanceSquaredSumCurrent;
    int collisionDistanceSquaredSumPrev;
    int collisionDistanceSquaredSumDifference;
    int nNodes;
    int nTracked;
    int index;
    int sumSquaredDistanceToGoalPoint;
    int iSumSquaredDistanceToGoalPoint;
    int minSumSquaredDistanceToOtherEbugs;
    int iMinSumSquaredDistanceToOtherEbugs;
    int minAvoidanceAngleThreshold;
    int maxAvoidanceAngleThreshold;
    bool desiredGoalPointReached;
    bool avoidanceActive;
    bool nodeStop;
    QMap<int,int> SSD_iGP;
    int iGP;
    QVector<int> SSD;
    bool conflictedGP;
    int iSSDValue;
    bool collisionCleared;
    int collidingNodeIndex;
};

int lineFormation(XBee *, struct XBeeNodeTable, struct EBugData*, int, int, int, cv::Rect, struct ControlDataLineFormation*, double *, bool &, bool);

#endif // LINEFORMATION_H
