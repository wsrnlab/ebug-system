/************************************************************************/
/* ebug-control:                                                        */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* RandomPosition.cpp                                                   */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#include "RandomPosition.h"
#include "math.h"

static int angleFreq(double angleErr);
static quint8 angleStepMode(double angleErr);
static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs);
static int getNumberOfCollisions(bool *row, int nNodes);
static double roundNumber(double r);

int randomPosition(XBee *xBee, struct XBeeNodeTable xBeeNodeTable,
                    struct EBugData* eBugData, int eBugDataIndex, int currentIndex, int nNodes, cv::Rect currentROI,
                    struct ControlDataRandomPosition* controlData,
                    double* parameters, bool &resetControlFlag)
{
    ////////////////////////////
    // FUNCTION RETURN VALUES //
    ////////////////////////////
    // 0: NO CONTROL ACTION
    // 1: CONTROL ACTION: Stop eBug
    // 2: CONTROL ACTION: Move eBug

    ////////////////
    // PARAMETERS //
    ////////////////
    // parameters[0]:   PID Controller [angle] : Kp
    // parameters[1]:   PID Controller [angle] : Ki
    // parameters[2]:   PID Controller [angle] : Kd
    // parameters[3]:   PID Controller [angle] : Error threshold
    // parameters[4].   PID Controller [angle] : Step time (ms)
    // parameters[5]:   Deadzone (pixels)
    // parameters[6]:   STAGE 2 [Set Trajectory] : Angle error termination threshold (degrees)
    // parameters[7]:   STAGE 2 [Set Trajectory] : Angle error termination count
    // parameters[8]:   STAGE 2 [Set Trajectory] : Stepper motor MIN speed
    // parameters[9]:   STAGE 2 [Set Trajectory] : Stepper motor MAX speed
    // parameters[10]:  STAGE 3 [Move eBug] : Collision avoidance (ON=1,OFF=0)
    // parameters[11]:  STAGE 3 [Move eBug] : Collision avoidance threshold
    // parameters[12]:  STAGE 3 [Move eBug] : Reset trajectory threshold (degrees)
    // parameters[13]:  STAGE 3 [Move eBug] : Desired position threshold
    // parameters[14]:  STAGE 3 [Move eBug] : Get new random position threshold
    // parameters[15]:  STAGE 3 [Move eBug] : New random projection projection (pixels)
    // parameters[16]:  STAGE 3 [Move eBug] : Stepper motor MIN speed
    // parameters[17]:  STAGE 3 [Move eBug] : Stepper motor MAX speed
    // parameters[18]:  STAGE 3 [Move eBug] : Stepper motor BASE speed [recommended value: maxSpeed+((minSpeed-maxSpeed)/2)]
    // parameters[19]:  RGB LEDs (ON=1,OFF=0)

    ///////////////////////////
    // INTERNAL CONTROL DATA //
    ///////////////////////////
    static bool collisionMatrix[MAX_NEBUGTRACK][MAX_NEBUGTRACK]={{false}};
    static int collisionVector[MAX_NEBUGTRACK]={0};
    static bool stopVector[MAX_NEBUGTRACK]={false};
    static bool newRandPos[MAX_NEBUGTRACK]={false};

    // Create PID controller(s)
    // (one controller for each eBug)
    static class PIDController anglePID[MAX_NEBUGTRACK];

    ///////////
    // RESET //
    ///////////
    if(resetControlFlag)
    {
        // Reset internal control data to initial values
        for(int i=0;i<MAX_NEBUGTRACK;i++)
        {
            collisionVector[i]=0;
            stopVector[i]=false;
            newRandPos[i]=false;
            for(int j=0;j<MAX_NEBUGTRACK;j++)
                collisionMatrix[i][j]=false;
        }
        controlData[currentIndex].stage=0;
        resetControlFlag=false;
    }

    // Save current data
    controlData[currentIndex].xCurrent=eBugData[eBugDataIndex].x;
    controlData[currentIndex].yCurrent=eBugData[eBugDataIndex].y;
    controlData[currentIndex].angleCurrent=eBugData[eBugDataIndex].angle;

    ////////////////////////////////////////////////////////////////////////
    // STAGE 1: Initialization and calculation of random desired position //
    ////////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage==0)
    {
        // Initialize PID controller
        anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
        // Desired position range: [deadzone,deadzone]->[currentROI.width-deadzone,currentROI.height-deadzone]
        int xMin=(int)parameters[5];
        int yMin=(int)parameters[5];
        int xMax=currentROI.width-xMin;
        int yMax=currentROI.height-yMin;
        qDebug("xMin:%d yMin:%d xMax:%d xMin:%d", xMin, yMin, xMax, yMax);
        // Save desired position
        controlData[currentIndex].xDesired = xMin + rand() % (xMax - xMin + 1);
        controlData[currentIndex].yDesired = yMin + rand() % (yMax - yMin + 1);
        // Increment stage index
        controlData[currentIndex].stage++;
    }

    ////////////////////
    // PID CONTROLLER //
    ////////////////////
    // Calculate desired angle
    double angleDesired=(180/PI)*atan2(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired,controlData[currentIndex].xDesired-eBugData[eBugDataIndex].x);
    // Calculate angle error
    double angleErr=angleDesired-eBugData[eBugDataIndex].angle;
    // Adjust angle error (ensures appropriate rotation direction for angle errors above 180 degrees)
    if(fabs(angleErr)>180)
    {
        if(angleErr<0)
            angleErr=360-fabs(angleErr);
        else if(angleErr>0)
            angleErr=-(360-fabs(angleErr));
    }
    // Calculate PID output (command/control action)
    double command=anglePID[currentIndex].Update(angleErr);
    // qDebug("Command: %.2f, eBugDataIndex: %d, currentIndex: %d", command, eBugDataIndex, currentIndex);

    ///////////////////////////////////////////////////////////////////////
    // STAGE 2: Set eBug trajectory (no motion in the x or y directions) //
    ///////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage==1)
    {
        if(newRandPos[currentIndex])
            controlData[currentIndex].stage++;
        else
        {
            // Stop if at correct trajectory
            // fabs(): returns absolute double value of angleErr (i.e. removes sign)
            if(fabs(angleErr)<parameters[6])
            {
                // Increment counter
                controlData[currentIndex].angleErrTermCount++;
                // Stop stepper motors
                if(controlData[currentIndex].angleErrTermCount==(int)parameters[7])
                {
                    // Reset counter
                    controlData[currentIndex].angleErrTermCount=0;
                    // Increment stage index
                    controlData[currentIndex].stage++;
                    // Reset PID controller
                    anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
                    // Stop eBug
                    for (int n=0;n>5;n++)
                    {
                        xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                    }
                    // Turn ON RED LEDs
                    if((bool)parameters[19])
                        //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                    return 1;
                }
            }
            // Perform control
            else
            {
                // Turn clockwise
                if(command<0)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                    return 2;
                }
                // Turn counter-clockwise
                else if(command>0)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                    return 2;
                }
            }
        }
    }

    /////////////////////////////////////////////////
    // STAGE 3: Move eBug towards desired position //
    /////////////////////////////////////////////////
    else if(controlData[currentIndex].stage==2)
    {
        // Collision avoidance
        if((bool)parameters[10])
        {
            qDebug("Collision Avoidance is TRUE");
            // Reset sum
            controlData[currentIndex].collisionDistanceSquaredSumCurrent=0;
            // Initialize stopVector
            for(int i=0;i<nNodes;i++)
                stopVector[i]=false;
            // Get data
            for(int j=0;j<nNodes;j++)
            {
                // Store current distance squared
                controlData[currentIndex].distanceSquaredCurrent=(abs(eBugData[eBugDataIndex].x-eBugData[j].x)*abs(eBugData[eBugDataIndex].x-eBugData[j].x))+(abs(eBugData[eBugDataIndex].y-eBugData[j].y)*abs(eBugData[eBugDataIndex].y-eBugData[j].y));
                // Check current eBug position with all other eBug positions
                if(j!=eBugDataIndex)
                {
                    // If current eBug is too close to any other eBug:
                    qDebug("currentIndex.distSquared=%d",controlData[currentIndex].distanceSquaredCurrent);
                    qDebug("collisionAvoidanceThreshold=%d",(int)parameters[11]*(int)parameters[11]);
                    if((controlData[currentIndex].distanceSquaredCurrent<(((int)parameters[11])*((int)parameters[11]))))
                    {
                        // Add distance square to sum
                        controlData[currentIndex].collisionDistanceSquaredSumCurrent+=controlData[currentIndex].distanceSquaredCurrent;
                        // Set collision matrix elements
                        collisionMatrix[eBugDataIndex][j]=true;
                        collisionMatrix[j][eBugDataIndex]=true;
                        // Stop other eBug j involved in collision only if current eBug is NOT stopped
                        // (this prevents all eBugs involved in a collision stopping)
                        if(!stopVector[eBugDataIndex])
                            stopVector[j]=true;
                    }
                    // Set collision matrix elements
                    else
                    {
                        collisionMatrix[eBugDataIndex][j]=false;
                        collisionMatrix[j][eBugDataIndex]=false;
                    }
                }
                // Set collision matrix elements
                else
                   collisionMatrix[currentIndex][currentIndex]=false;
            }

            // Calculate collisionDistanceSquaredSumDifference
            controlData[currentIndex].collisionDistanceSquaredSumDifference=controlData[currentIndex].collisionDistanceSquaredSumCurrent-controlData[currentIndex].collisionDistanceSquaredSumPrev;
            // Store temporary copy of collisionDistanceSquaredSumPrev
            int collisionDistanceSquaredSumPrevTemp=controlData[currentIndex].collisionDistanceSquaredSumPrev;
            // Store current distance squared as previous
            controlData[currentIndex].collisionDistanceSquaredSumPrev=controlData[currentIndex].collisionDistanceSquaredSumCurrent;
            // Update collision vector
            collisionVector[currentIndex]=getNumberOfCollisions(collisionMatrix[currentIndex],nNodes);

            // Validate new random position
            if(newRandPos[currentIndex])
            {
                // Reset sum
                int collisionDistanceSquaredSumCurrentLocal=0;
                // Get data
                for(int j=0;j<nNodes;j++)
                {
                    // Store current distance squared
                    int distanceSquaredCurrentLocal=((abs((eBugData[eBugDataIndex].x+(int)roundNumber((int)parameters[15]*cos(angleDesired*(PI/180))))-eBugData[j].x))*(abs((eBugData[eBugDataIndex].x+(int)roundNumber((int)parameters[15]*cos(angleDesired*(PI/180))))-eBugData[j].x)))+
                                                        ((abs((eBugData[eBugDataIndex].y-(int)roundNumber((int)parameters[15]*sin(angleDesired*(PI/180))))-eBugData[j].y))*(abs((eBugData[eBugDataIndex].y-(int)roundNumber((int)parameters[15]*sin(angleDesired*(PI/180))))-eBugData[j].y)));
                    // Check current eBug position with all other eBug positions
                    if(j!=currentIndex)
                    {
                        // If current eBug is too close to any other eBug:
                        if((distanceSquaredCurrentLocal<(((int)parameters[11])*((int)parameters[11]))))
                        {
                            // Add distance square to sum
                            collisionDistanceSquaredSumCurrentLocal+=distanceSquaredCurrentLocal;
                        }
                    }
                }
                // Set trajectory for new random position
                if(collisionDistanceSquaredSumCurrentLocal==0)
                {
                    // Set flag
                    newRandPos[currentIndex]=false;
                    // Set index
                    controlData[currentIndex].stage=1;
                    return 0;
                }
                // Get another new random position
                else
                {
                    // Get new random position
                    newRandPos[currentIndex]=true;
                    // Set index
                    controlData[currentIndex].stage=0;
                    return 0;
                }
            }

            // In collision and set to STOPPED: stop eBug
            qDebug("stopVector[%d]=%d",currentIndex,stopVector[currentIndex]);
            qDebug("collisionVector[%d]=%d",currentIndex,collisionVector[currentIndex]);
            if((stopVector[currentIndex])&&(collisionVector[currentIndex]>0))
            {
                // Stop eBug
                qDebug("stopVector[%d] && collVector[%d]>0",currentIndex,currentIndex);
                qDebug("Stage 3: (Collision Avoidance) Stop eBug");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                // Turn ON RED LEDs
                if((bool)parameters[19])
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                return 1;
            }
            // Check if eBug has MOVED INTO (detect edge) OR is MOVING FURTHER into a collision, if so, get new random position
            // MOVING FURTHER INTO COLLISION: collisionDistanceSquaredSumDifference=NEGATIVE
            // MOVING OUT OF COLLISION: collisionDistanceSquaredSumDifference=POSITIVE
            else if((collisionVector[currentIndex]>0)&&(((collisionDistanceSquaredSumPrevTemp==0)&&(controlData[currentIndex].collisionDistanceSquaredSumCurrent!=0))
                    ||((controlData[currentIndex].collisionDistanceSquaredSumDifference)<parameters[14])))
            {
                // Get new random position
                newRandPos[currentIndex]=true;
                // Set index
                controlData[currentIndex].stage=0;
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
                // Turn ON RED LEDs
                if((bool)parameters[19])
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                return 1;
            }
            // Allow current eBug to move (no longer in collision)
            else
               stopVector[currentIndex]=false;

            // Store current distance squared as previous
            controlData[currentIndex].collisionDistanceSquaredSumPrev=controlData[currentIndex].collisionDistanceSquaredSumCurrent;
            // Reset flag
            newRandPos[currentIndex]=false;
        }

        // Desired position reached
        if((abs(eBugData[eBugDataIndex].x-controlData[currentIndex].xDesired)<parameters[13])&&(abs(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired)<parameters[13]))
        {
            // Reset index
            controlData[currentIndex].stage=0;
            // Turn ON RED LEDs
            if((bool)parameters[19])
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
            return 1;
        }
        // Reset trajectory if absolute angle error is above a certain threshold
        else if(fabs(angleErr)>parameters[12])
        {
            // Set index
            controlData[currentIndex].stage=1;
            // Turn ON RED LEDs
            if((bool)parameters[19])
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
            return 1;
        }
        // Perform control
        else
        {
            QVector<int> distanceSquaredToOtherEbugs;
            int minDistanceSquaredToOtherEbugs;
            distanceSquaredToOtherEbugs.clear();
            for (int j=0;j<nNodes;j++)
            {
                if(j!=eBugDataIndex)
                {
                    // Store current distance squared
                    distanceSquaredToOtherEbugs.append((int)((abs(eBugData[eBugDataIndex].x-eBugData[j].x))*(abs(eBugData[eBugDataIndex].x-eBugData[j].x)))+
                                                        (int)((abs(eBugData[eBugDataIndex].y-eBugData[j].y))*(abs(eBugData[eBugDataIndex].y-eBugData[j].y))));
                    qDebug("eBug[%d] to [%d] = %d", eBugDataIndex, j, distanceSquaredToOtherEbugs.last());
                }
            }

            minDistanceSquaredToOtherEbugs = 1000000;
            qDebug("initial min dist = %d", minDistanceSquaredToOtherEbugs);
            for (int k=0;k<nNodes-1;k++)
            {
                if(distanceSquaredToOtherEbugs.at(k)<minDistanceSquaredToOtherEbugs)
                {
                    qDebug("%d < %d",distanceSquaredToOtherEbugs.at(k),minDistanceSquaredToOtherEbugs );
                    minDistanceSquaredToOtherEbugs = distanceSquaredToOtherEbugs.at(k);
                }
                qDebug("minDistance of Ebug[%d]=%d",eBugDataIndex,minDistanceSquaredToOtherEbugs);
            }

            qDebug("xCurrent=%d yCurrent=%d", controlData[currentIndex].xCurrent, controlData[currentIndex].yCurrent);
            qDebug("xDesired=%d yDesired=%d", controlData[currentIndex].xDesired, controlData[currentIndex].yDesired);
            int xDifference = abs(controlData[currentIndex].xCurrent - controlData[currentIndex].xDesired);
            int xSquared = xDifference*xDifference;
            int yDifference = abs(controlData[currentIndex].yCurrent - controlData[currentIndex].yDesired);
            int ySquared = yDifference*yDifference;
            controlData[currentIndex].distanceSquaredToDesired = (xSquared+ySquared);
            qDebug("ssd=%d",controlData[currentIndex].distanceSquaredToDesired);
            // Steer right
            if(command<0)
            {
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs),0,true,0x08,true,
                                                                                                                round(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs)*0.9),0,true,0x08,true,true,0x00)));
                return 2;
            }
            // Steer left
            else if(command>0)
            {
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(round(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs)*0.9),0,true,0x08,true,
                                                                                                                speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs),0,true,0x08,true,true,0x00)));
                return 2;
            }
        }
    }
    return 0;
} // randomPosition()

static int angleFreq(double angleErr)
{
    int freqVal;

    if(fabs(angleErr)>100)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>60)
    {
        freqVal=250;
    }

    else if(fabs(angleErr)>30)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>10)
    {
        freqVal=100;
    }

    else
        freqVal=100;

    return freqVal;
} // angleFreq()

static quint8 angleStepMode(double angleErr)
{
    quint8 stepModeVal;

    if(fabs(angleErr)>100)
    {
        stepModeVal=0x00;
    }

    else if(fabs(angleErr)>60)
    {
        stepModeVal=0x01;
    }

    else if(fabs(angleErr)>10)
    {
        stepModeVal=0x02;
    }

    else if(fabs(angleErr)>5)
    {
        stepModeVal=0x03;
    }

    else
        stepModeVal=0x08;

    return stepModeVal;
} // angleStepMode()

static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs)
{
    int speedFreqVal;
    int minSquaredDistance;

    if (sumOfSquaredDistanceOtherEbugs*0.5 < sumOfSquaredDistance)
    {
        minSquaredDistance = sumOfSquaredDistanceOtherEbugs*0.5;
    }
    else
        minSquaredDistance = sumOfSquaredDistance;

    if(fabs(minSquaredDistance)>20000)
    {
        speedFreqVal=6000;
    }

    else if(fabs(minSquaredDistance)>10000)
    {
        speedFreqVal=5000;
    }

    else if(fabs(minSquaredDistance)>2000)
    {
        speedFreqVal=3000;
    }

    else if(fabs(minSquaredDistance)>500)
    {
        speedFreqVal=2000;
    }

    else
        speedFreqVal=1000;

    return speedFreqVal;
} // speedFreqVal()

static int getNumberOfCollisions(bool *row, int nNodes)
{
    // Local variables
    int count=0;
    // Read row vector
    for(int i=0;i<nNodes;i++)
    {
        if(row[i])
            count++;
    }
    return count;
} // getNumberOfCollisions()

static double roundNumber(double r)
{
    // This function rounds a double to a whole number (also double)
    // e.g: roundNumber(4.5)=5.0, roundNumber(-4.5)=-5.0
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
} // roundNumber()
