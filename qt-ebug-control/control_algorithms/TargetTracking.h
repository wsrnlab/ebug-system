#ifndef TARGETTRACKING_H
#define TARGETTRACKING_H

/*
  TARGETTRACKING.h

  Header file for all target tracking algorithms

 */

// eBug-II API
#include <eBugAPI.h>
// XBee class
#include <XBee.h>

#include <opencv/cv.h>
#include <math.h>
#include <QTime>
#include <pthread.h>

enum trackingAlgorithmIndex //Place any additional Target tracking algorithms here
{
    pso = 0,
    pf,
};

enum targetStage
{
    newPosition = 0,
    waiting,
};

struct trackingConstraints{
    double minDistToTarget;
    double maxDistToTarget;

};

struct ControlDataTargetTracking{
    int trackingAlgorithm;
    int xDesired;
    int yDesired;
    int xCurrent;
    int yCurrent;
    int angleCurrent;
    int angleErrTermCount;
    int distanceSquaredCurrent;
    int collisionDistanceSquaredSumCurrent;
    int collisionDistanceSquaredSumPrev;
    int collisionDistanceSquaredSumDifference;
    int nNodes;
    int distanceSquaredToDesired;
    int distanceSquaredToTarget;
    int isTarget;
};

int targetTracking(XBee *, struct XBeeNodeTable, struct EBugData*, int, int, int, cv::Rect, struct ControlDataTargetTracking*, double *, bool &, bool *);

#endif // TARGETTRACKING_H
