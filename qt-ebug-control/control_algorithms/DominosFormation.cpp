#include "DominosFormation.h"

void sleepThread(unsigned long msecs);
static int ledChaser(XBee *xBee, struct XBeeNodeTable xBeeNodeTable, int option);
static int getNumberOfCollisions(bool *, int);
static double roundNumber(double r);
static int angleFreq(double angleErr);
static quint8 angleStepMode(double angleErr);
static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs);
static int getNumberOfCollisions(bool *row, int nNodes);
static int minVal(int value1, int value2);
static int maxVal(int value1, int value2);
static int iMinVal(int value1, int value2);
static int iMaxVal(int value1, int value2);
static int normalizeAngle(int angle, int scope);
static int rectifiedXCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug);
static int rectifiedYCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug);
static int quadrantLocation(int xThisEbug, int yThisEbug, int xOtherEbug, int yOtherEbug);

int dominosFormation(XBee *xBee, struct XBeeNodeTable xBeeNodeTable,
                    struct EBugData* eBugData, int eBugDataIndex, int currentIndex, int nNodes, cv::Rect currentROI,
                    struct ControlDataDominosFormation* controlData,
                    double* parameters, bool &resetControlFlag, bool ctrlThreadStopped)
{
    ////////////////////////////
    // FUNCTION RETURN VALUES //
    ////////////////////////////
    // 0: NO CONTROL ACTION
    // 1: CONTROL ACTION: Stop eBug
    // 2: CONTROL ACTION: Move eBug

    ////////////////
    // PARAMETERS //
    ////////////////
    // parameters[0]:   PID Controller [angle] : Kp
    // parameters[1]:   PID Controller [angle] : Ki
    // parameters[2]:   PID Controller [angle] : Kd
    // parameters[3]:   PID Controller [angle] : Error threshold
    // parameters[4].   PID Controller [angle] : Step time (ms)
    // parameters[5]:   Deadzone (pixels)
    // parameters[6]:   STAGE 2 [Set Trajectory] : Angle error termination threshold (degrees)
    // parameters[7]:   STAGE 2 [Set Trajectory] : Angle error termination count
    // parameters[8]:   STAGE 2 [Set Trajectory] : Stepper motor MIN speed
    // parameters[9]:   STAGE 2 [Set Trajectory] : Stepper motor MAX speed
    // parameters[10]:  STAGE 3 [Move eBug] : Collision avoidance (ON=1,OFF=0)
    // parameters[11]:  STAGE 3 [Move eBug] : Collision avoidance threshold
    // parameters[12]:  STAGE 3 [Move eBug] : Reset trajectory threshold (degrees)
    // parameters[13]:  STAGE 3 [Move eBug] : Desired position threshold
    // parameters[14]:  STAGE 3 [Move eBug] : Get new random position threshold
    // parameters[15]:  STAGE 3 [Move eBug] : New random projection projection (pixels)
    // parameters[16]:  STAGE 3 [Move eBug] : Stepper motor MIN speed
    // parameters[17]:  STAGE 3 [Move eBug] : Stepper motor MAX speed
    // parameters[18]:  STAGE 3 [Move eBug] : Stepper motor BASE speed [recommended value: maxSpeed+((minSpeed-maxSpeed)/2)]
    // parameters[19]:  RGB LEDs (ON=1,OFF=0)
    // parameters[20]:  SQUARE FORMATION : ?
    // parameters[21]:  Avoidance Angle Clearance
    // parameters[22]:  Avoidance Distance Clearance

    ///////////////////////////
    // INTERNAL CONTROL DATA //
    ///////////////////////////
    //static bool collisionMatrix[MAX_NEBUGTRACK][MAX_NEBUGTRACK]={{false}};
    //static int collisionVector[MAX_NEBUGTRACK]={0};
    //static bool stopVector[MAX_NEBUGTRACK]={false};
    static bool completedFlag[MAX_NEBUGTRACK]={false}; // complete what?
    static bool allPositionsAllocated;
    static QVector<int> eBugIndex;
    static int nRun;
    static float xMid;
    static float yMid;
    static float xCoord;
    static float yCoord;
    static int radius;
    static bool conflictedGoalPoints=true;
    static int iIndex;
    static int jIndex;
    static int mIndex;
    static int finalLoopCount;
    static int count;

    // Create PID controller(s)
    // (one controller for each eBug)
    static class PIDController anglePID[MAX_NEBUGTRACK];

    // Saving Initial Conditions
    static int nNodesPrevious;
    //static bool firstRun = true;
    static bool nNodesChanged;
    static int nNodesPresent;
    static QVector<int> xPointsFormation;
    static QVector<int> yPointsFormation;
    static QVector<int> xGoalPoints;
    static QVector<int> yGoalPoints;
    static QVector<int> nodeIndex;
    static int iVectorFormation;
    static int sumSquaredDistance[MAX_NEBUGTRACK][MAX_NEBUGTRACK];
    static int sumSquaredDistanceToOtherEbugs[MAX_NEBUGTRACK][MAX_NEBUGTRACK];
    static int numberPointsFormation;
    static int iOtherNearestEbug;
    static bool allAvoidanceActive;
    static bool initializeGoalPointVector;

    static bool firstRun = true;

    if (ctrlThreadStopped)
    {
        qDebug("Control Thread Was Stopped");
        firstRun=true;
        ctrlThreadStopped=false;
        xPointsFormation.clear();
        yPointsFormation.clear();
        nodeIndex.clear();
    }

    nNodesPresent=nNodes;
    qDebug("firstRun=%d",firstRun);
    if(firstRun==true)
    {
        qDebug("firstRun HERE!");
        nNodesPrevious=nNodesPresent;
        nNodesChanged=false;
        (controlData[1].SSD).append(1);
        qDebug("TEST POINT 1");
        for(int i=0; i<MAX_NEBUGTRACK; i++)
        {
            qDebug("i=%d",i);
            controlData[i].index=0;
            qDebug("controlData[i].index=%d",i,controlData[i].index);
            controlData[i].stage=0;
            qDebug("controlData[i].stage=%d",i,controlData[i].stage);
            controlData[i].desiredGoalPointReached=false;
            qDebug("controlData[i].desiredGoalPointReached=%d",i,controlData[i].desiredGoalPointReached);
            controlData[i].avoidanceActive=false;
            qDebug("controlData[i].avoidanceActive=%d",i,controlData[i].avoidanceActive);
            controlData[i].nodeStop=false;
            qDebug("controlData[i].nodeStop=%d",i,controlData[i].nodeStop);
            controlData[i].angleErrTermCount=0;
            qDebug("controlData[i].angleErrTermCount=%d",i,controlData[i].angleErrTermCount);

            controlData[i].collisionCleared=false;
            qDebug("controlData[i].collisionCleared=%d",i,controlData[i].collisionCleared);

            allPositionsAllocated=false;
            qDebug("allPositionsAllocated=false");
            initializeGoalPointVector=true;
            qDebug("initializeGoalPointVector=true");
            controlData[i].SSD.append(1);
            qDebug("TEST!");
            controlData[i].SSD.clear();
            qDebug("SSD Cleared");

            controlData[i].iSSDValue=0;
            qDebug("controlData[i].iSSDValue=%d",i,controlData[i].iSSDValue);
            controlData[i].SSD_iGP.clear();
            controlData[i].iGP=0;
            qDebug("controlData[i].iGP=%d",i,controlData[i].iGP);
            controlData[i].conflictedGP=false;
            qDebug("controlData[i].conflictedGP=%d",i,controlData[i].conflictedGP);
            controlData[i].iSSDValue=0;
            qDebug("controlData[i].iSSDValue=%d",i,controlData[i].iSSDValue);
        }
        eBugIndex.clear();
        xGoalPoints.clear();
        yGoalPoints.clear();

        // Defining Points to describe DICE
        xMid = (int)currentROI.width/2;
        yMid = (int)currentROI.height/2;
//        radius = (float)parameters[20];
//        numberPointsFormation=0;
//        nRun=0;
//        finalLoopCount=0;
//        qDebug("allocating Formation Points HERE!");
//        for(int i=0;i<36;i++)
//        {
//            qDebug("in for loop %d",i);
//            if(i<9)
//            {
//                xCoord=xMid+radius*cos(i*10*PI/180);
//                xPointsFormation.append(round(xCoord));
//                yCoord=yMid-radius*sin(i*10*PI/180);
//                yPointsFormation.append(round(yCoord));
//                qDebug("xpoint[%d]=%d",i,xPointsFormation.at(i));
//                qDebug("ypoint[%d]=%d",i,yPointsFormation.at(i));
//            }
//            else if(i<18)
//            {
//                xCoord=xMid-radius*sin((i*10-90)*PI/180);
//                xPointsFormation.append(round(xCoord));
//                yCoord=yMid-radius*cos((i*10-90)*PI/180);
//                yPointsFormation.append(round(yCoord));
//                qDebug("xpoint[%d]=%d",i,xPointsFormation.at(i));
//                qDebug("ypoint[%d]=%d",i,yPointsFormation.at(i));
//            }
//            else if(i<27)
//            {
//                xCoord=xMid-radius*cos((i*10-180)*PI/180);
//                xPointsFormation.append(round(xCoord));
//                yCoord=yMid+radius*sin((i*10-180)*PI/180);
//                yPointsFormation.append(round(yCoord));
//                qDebug("xpoint[%d]=%d",i,xPointsFormation.at(i));
//                qDebug("ypoint[%d]=%d",i,yPointsFormation.at(i));
//            }
//            else
//            {
//                xCoord=xMid+radius*sin((i*10-270)*PI/180);
//                xPointsFormation.append(round(xCoord));
//                yCoord=yMid+radius*cos((i*10-270)*PI/180);
//                yPointsFormation.append(round(yCoord));
//                qDebug("xpoint[%d]=%d",i,xPointsFormation.at(i));
//                qDebug("ypoint[%d]=%d",i,yPointsFormation.at(i));
//            }
//            numberPointsFormation++;
//        }
    }
    else if((firstRun==false && (nNodesPrevious!=nNodesPresent)))
    {
        nNodesChanged=true;
        allPositionsAllocated=false;
        initializeGoalPointVector=true;
        controlData[currentIndex].avoidanceActive = false;
        controlData[currentIndex].desiredGoalPointReached==false;
        finalLoopCount=0;
        xGoalPoints.clear();
        yGoalPoints.clear();
        nodeIndex.clear();
        nNodesPrevious=nNodesPresent;
        nRun=0;
        qDebug("nRun set to zero");
        qDebug("***********nNodesChanged************");
    }

    qDebug("======= ======= DOMINOS FORMATION ======= =======");
    qDebug("======= CURRENT INDEX = [%d] =======", currentIndex);
    qDebug("nNodesPrevious= %d",nNodesPrevious);
    qDebug("nNodesPresent= %d", nNodesPresent);

    if(resetControlFlag==true || nNodesChanged==true)
    {
        allPositionsAllocated = false;
        resetControlFlag = false;
        nNodesChanged=false;
        for(int i=0; i<MAX_NEBUGTRACK; i++)
        {
            controlData[i].index=0;
            controlData[i].stage=0;
            controlData[i].desiredGoalPointReached=false;
            controlData[i].avoidanceActive=false;
            controlData[i].nodeStop=false;
            controlData[i].angleErrTermCount=0;
            controlData[i].collisionCleared=false;

            controlData[i].iSSDValue=0;
            controlData[i].SSD_iGP.clear();
            controlData[i].iGP=0;
            controlData[i].SSD.clear();
            controlData[i].conflictedGP=false;
            controlData[i].iSSDValue=0;
            eBugIndex.clear();
        }
        // Initialize PID controller
        anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
    }

    // Save current data
    controlData[currentIndex].xCurrent = eBugData[eBugDataIndex].x;
    controlData[currentIndex].yCurrent = eBugData[eBugDataIndex].y;
    controlData[currentIndex].angleCurrent = eBugData[eBugDataIndex].angle;
    qDebug("xCurrent[%d]=%d",currentIndex,controlData[currentIndex].xCurrent);
    qDebug("yCurrent[%d]=%d",currentIndex,controlData[currentIndex].yCurrent);

    if(nRun<nNodes)
    {
        nodeIndex.append(currentIndex);
        qDebug("nodeIndex[%d]=%d",nRun,nodeIndex.at(nRun));
        nRun++;
        firstRun=false;
        qDebug("nRun increased to %d",nRun);
    }
    if(nRun==nNodes)
    {
        controlData[currentIndex].stage == 0;
        qDebug("nRun==nNodes");
    }
    qDebug("nRun=%d",nRun);
    if(nRun!=nNodes)
    {
        qDebug("nRun!=nNodes");
        return 0;
    }

    allAvoidanceActive=false;
    for(int i=0;i<nNodes;i++)
    {
        iIndex=nodeIndex.at(i);
        qDebug("node[%d] avoidanceActive=%d",iIndex,controlData[iIndex].avoidanceActive);
        if(controlData[iIndex].avoidanceActive==true)
            allAvoidanceActive=true;
    }

    if(controlData[currentIndex].nodeStop)
    {
        // Stop eBug
        xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
        controlData[currentIndex].nodeStop=false;
        return 1;
    }

    if(controlData[currentIndex].collisionCleared)
    {
        qDebug("CollisionCleared");
        controlData[currentIndex].xDesired=xGoalPoints.at(controlData[currentIndex].iGP);
        controlData[currentIndex].yDesired=yGoalPoints.at(controlData[currentIndex].iGP);
        controlData[currentIndex].collisionCleared=false;
    }

    ////////////////////////////////////////////////////////////////////////
    // STAGE 0: Initialization and calculation of desired position        //
    ////////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage == 0)
    {
        if(allPositionsAllocated==false)
        {
            qDebug("*** Hello from STAGE 0 for currentIndex[%d] ***", currentIndex);

            // Storing goal points into matrix
            if (nNodes==1)
            {
                (controlData[currentIndex].xDesired)=round(xMid);
                (controlData[currentIndex].yDesired)=round(yMid);
                allPositionsAllocated = true;
                controlData[currentIndex].sumSquaredDistanceToGoalPoint = abs(controlData[currentIndex].xCurrent-controlData[currentIndex].xDesired)*abs(controlData[currentIndex].xCurrent-controlData[currentIndex].xDesired)+
                        abs(controlData[currentIndex].yCurrent-controlData[currentIndex].yDesired)*abs(controlData[currentIndex].yCurrent-controlData[currentIndex].yDesired);
                controlData[currentIndex].minSumSquaredDistanceToOtherEbugs = 999999;
            }
            else  if (nNodes >= 2)
            {
                if(initializeGoalPointVector)
                {
                    if (nNodes==2)
                    {
                        xGoalPoints.append(xMid-90);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid-90);
                        yGoalPoints.append(yMid+90);
                    }
                    else if (nNodes==3)
                    {
                        xGoalPoints.append(xMid-90);
                        yGoalPoints.append(yMid-90);
                        xGoalPoints.append(xMid);
                        yGoalPoints.append(yMid);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid+90);
                    }
                    else if (nNodes==4)
                    {
                        xGoalPoints.append(xMid-90);
                        yGoalPoints.append(yMid-90);
                        xGoalPoints.append(xMid-90);
                        yGoalPoints.append(yMid+90);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid-90);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid+90);
                    }
                    else if (nNodes==5)
                    {
                        xGoalPoints.append(xMid-90);
                        yGoalPoints.append(yMid-90);
                        xGoalPoints.append(xMid-90);
                        yGoalPoints.append(yMid+90);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid-90);
                        xGoalPoints.append(xMid+90);
                        yGoalPoints.append(yMid+90);
                        xGoalPoints.append(xMid);
                        yGoalPoints.append(yMid);
                    }
                    numberPointsFormation=nNodes;
                    initializeGoalPointVector=false;
                }
                // Creating Goal Points vector

                qDebug("-- Determining SSD --");
                // Allocating goal points to eBugs
                for(int i=0;i<nNodes;i++)
                {
                    iIndex=nodeIndex.at(i);
                    for(int j=0;j<xGoalPoints.size();j++)
                    {
                        controlData[iIndex].SSD.append(abs(controlData[iIndex].xCurrent-xGoalPoints.at(j))*abs(controlData[iIndex].xCurrent-xGoalPoints.at(j))+
                                                       abs(controlData[iIndex].yCurrent-yGoalPoints.at(j))*abs(controlData[iIndex].yCurrent-yGoalPoints.at(j)));
                        controlData[iIndex].SSD_iGP[controlData[iIndex].SSD.at(j)]=j;
                        qDebug("Goal Point %d: %d,%d",j,xGoalPoints.at(j),yGoalPoints.at(j));
                        qDebug("sumSquared for node[%d] to GoalPoint %d = %d",iIndex,j,controlData[iIndex].SSD.at(j));
                    }
                    //Sorting SSD Array of node iIndex Ebug
                    qStableSort(controlData[iIndex].SSD.begin(), controlData[iIndex].SSD.end());
                    //Pre-allocate each eBug to closest Goal Point
                    controlData[iIndex].iGP=controlData[iIndex].SSD_iGP.value(controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue));
                    controlData[iIndex].xDesired=xGoalPoints.at(controlData[iIndex].iGP);
                    controlData[iIndex].yDesired=yGoalPoints.at(controlData[iIndex].iGP);
                    qDebug("Closest Goal Point for node[%d]= x:%d y:%d (iGP:%d SSD:%d)",iIndex,controlData[iIndex].xDesired,
                           controlData[iIndex].yDesired,controlData[iIndex].iGP,controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue));
                }

                qDebug("-- Checking for conflicted GP and Reallocating Points --");
                conflictedGoalPoints=true;
                count=0;
                do{
                    for(int i=0;i<nNodes-1;i++)
                    {
                        iIndex=nodeIndex.at(i);
                        for(int j=i+1;j<nNodes;j++)
                        {
                            jIndex=nodeIndex.at(j);
                            if(controlData[iIndex].iGP==controlData[jIndex].iGP)
                            {
                                qDebug("Conflicted Goal Points!");
                                if(controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue)<=
                                        controlData[jIndex].SSD.at(controlData[jIndex].iSSDValue))
                                {
                                    if(controlData[jIndex].iSSDValue==xGoalPoints.size())
                                        controlData[jIndex].iSSDValue=0;
                                    else
                                        controlData[jIndex].iSSDValue++;

                                    controlData[jIndex].iGP=
                                            controlData[jIndex].SSD_iGP.value(controlData[jIndex].SSD.at(controlData[jIndex].iSSDValue));
                                    controlData[jIndex].xDesired=xGoalPoints.at(controlData[jIndex].iGP);
                                    controlData[jIndex].yDesired=yGoalPoints.at(controlData[jIndex].iGP);
                                    qDebug("New Goal Point for node[%d]= x:%d y:%d (iGP:%d SSD:%d)",jIndex,controlData[jIndex].xDesired,
                                           controlData[jIndex].yDesired,controlData[jIndex].iGP,controlData[jIndex].SSD.at(controlData[jIndex].iSSDValue));
                                }
                                else if(controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue)>
                                        controlData[jIndex].SSD.at(controlData[jIndex].iSSDValue))
                                {
                                    if(controlData[iIndex].iSSDValue==xGoalPoints.size())
                                        controlData[iIndex].iSSDValue=0;
                                    else
                                        controlData[iIndex].iSSDValue++;

                                    controlData[iIndex].iGP=
                                            controlData[iIndex].SSD_iGP.value(controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue));
                                    controlData[iIndex].xDesired=xGoalPoints.at(controlData[iIndex].iGP);
                                    controlData[iIndex].yDesired=yGoalPoints.at(controlData[iIndex].iGP);
                                    qDebug("New Goal Point for node[%d]= x:%d y:%d (iGP:%d SSD:%d)",iIndex,controlData[iIndex].xDesired,
                                           controlData[iIndex].yDesired,controlData[iIndex].iGP,controlData[iIndex].SSD.at(controlData[iIndex].iSSDValue));
                                }
                            }
                        }
                    }
                    //Checking Again After Reallocation
                    conflictedGoalPoints=false;
                    for(int i=0;i<nNodes-1;i++)
                    {
                        iIndex=nodeIndex.at(i);
                        for(int j=i+1;j<nNodes;j++)
                        {
                            jIndex=nodeIndex.at(j);
                            if(controlData[iIndex].iGP==controlData[jIndex].iGP)
                                conflictedGoalPoints=true;
                        }
                    }
                    count++;
                } while(conflictedGoalPoints==true && count<30);

                if(count==30)
                {
                    for(int i=0;i<nNodes;i++)
                    {
                        iIndex=nodeIndex.at(i);
                        controlData[iIndex].xDesired=xGoalPoints.at(i);
                        controlData[iIndex].yDesired=yGoalPoints.at(i);
                        controlData[iIndex].iGP=i;
                        qDebug("While Loop exceeded 50");
                        qDebug("Default Coord Node[%d]:  x=%d  y=%d  (iGP=%d)",iIndex,controlData[iIndex].xDesired,
                               controlData[iIndex].yDesired,controlData[iIndex].iGP);
                    }
                }
            }

            allPositionsAllocated=true;
        }

        if(allPositionsAllocated==true)
        {
            // Increment stage index
            controlData[currentIndex].stage++;
        }
        qDebug("Stage 0 has been incremented");
    }

    ////////////////////
    // PID CONTROLLER //
    ////////////////////
    // Calculate desired angle
    double angleDesired;
    double angleErr;
    if(controlData[currentIndex].desiredGoalPointReached==false)
    {
        angleDesired = (180/PI)*atan2(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired,controlData[currentIndex].xDesired-eBugData[eBugDataIndex].x);
        // Calculate angle error
        angleErr=angleDesired-eBugData[eBugDataIndex].angle;
    }
    else if(controlData[currentIndex].desiredGoalPointReached==true)
    {
        angleDesired = (180/PI)*atan2(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired,controlData[currentIndex].xDesired-eBugData[eBugDataIndex].x);
        //angleDesired = 90;
        // Calculate angle error
        angleErr=angleDesired-eBugData[eBugDataIndex].angle;
        completedFlag[currentIndex]=true;
    }
    // Adjust angle error (ensures appropriate rotation direction for angle errors above 180 degrees)
    if(fabs(angleErr)>180)
    {
        if(angleErr<0)
            angleErr=360-fabs(angleErr);
        else if(angleErr>0)
            angleErr=-(360-fabs(angleErr));
    }
    // Calculate PID output (command/ control action)
    double command=anglePID[currentIndex].Update(angleErr);
    qDebug("angleDesired=%f  angleCurrent=%d  angleErr=%f",angleDesired, controlData[currentIndex].angleCurrent, angleErr);
    qDebug("command=%f",command);

    ///////////////////////////////////////////////////////////////////////
    // STAGE 1: Set eBug trajectory (no motion in the x or y directions) //
    ///////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage==1 && (allPositionsAllocated||controlData[currentIndex].avoidanceActive))
    {
        qDebug("Hello from Stage 1 for [%d]", currentIndex);
        if(controlData[currentIndex].avoidanceActive)
            qDebug("avoidanceActive is TRUE");

        // Stop if at correct trajectory
        // fabs(): returns absolute double value of angleErr (i.e. removes sign)
        if(fabs(angleErr)<(int)parameters[6] ||
                (abs(controlData[currentIndex].xCurrent-xGoalPoints.at(controlData[currentIndex].iGP))<(int)parameters[13] &&
                 abs(controlData[currentIndex].yCurrent-yGoalPoints.at(controlData[currentIndex].iGP))<(int)parameters[13]))
        {
            // Increment counter
            controlData[currentIndex].angleErrTermCount++;
            qDebug("angleTermCount[%d]=%d",currentIndex, controlData[currentIndex].angleErrTermCount);
            // Stop stepper motors
            if(controlData[currentIndex].angleErrTermCount==(int)parameters[7])
            {
                qDebug("angleErroTermCount=%d",controlData[currentIndex].angleErrTermCount);
                // Reset counter
                controlData[currentIndex].angleErrTermCount=0;
                // Increment stage index
                controlData[currentIndex].stage++;
                // Reset PID controller
                anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
                // Stop eBug
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
                // Turn ON RED LEDs
                if((bool)parameters[19])
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                    return 1;
            }
        }
        // Perform control
        else
        {
            // Turn clockwise
            if(command<0)
            {
                qDebug("Turning CW");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                return 2;
            }
            // Turn counter-clockwise
            else if(command>0)
            {
                qDebug("Turning CCW");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                return 2;
            }
        }
    }

    /////////////////////////////////////////////////
    // STAGE 2: Move eBug towards desired position //
    /////////////////////////////////////////////////
    else if(controlData[currentIndex].stage==2 && allPositionsAllocated)
    {
        qDebug("Hello from Stage 2 for [%d]", currentIndex);
        controlData[currentIndex].sumSquaredDistanceToGoalPoint = abs(controlData[currentIndex].xCurrent-controlData[currentIndex].xDesired)*abs(controlData[currentIndex].xCurrent-controlData[currentIndex].xDesired)+
                abs(controlData[currentIndex].yCurrent-controlData[currentIndex].yDesired)*abs(controlData[currentIndex].yCurrent-controlData[currentIndex].yDesired);
        // Collision avoidance
        qDebug("--Testing for Collision--");
        for(int i=0; i<nNodes; i++)
        {
            iIndex=nodeIndex.at(i);
            qDebug("Node:%d",iIndex);
            for(int j=0; j<nNodes; j++)
            {
                jIndex=nodeIndex.at(j);
                if(iIndex!=jIndex)
                {
                    sumSquaredDistanceToOtherEbugs[iIndex][jIndex] = abs(controlData[iIndex].xCurrent-controlData[jIndex].xCurrent)*abs(controlData[iIndex].xCurrent-controlData[jIndex].xCurrent)+
                            abs(controlData[iIndex].yCurrent-controlData[jIndex].yCurrent)*abs(controlData[iIndex].yCurrent-controlData[jIndex].yCurrent);
                    qDebug("sumSquared between node[%d] and node[%d] = %d",iIndex,jIndex,sumSquaredDistanceToOtherEbugs[iIndex][jIndex]);
                }
                else
                {
                    sumSquaredDistanceToOtherEbugs[iIndex][jIndex]=999999;
                    qDebug("sumSquared between node[%d] and node[%d] = %d",iIndex,jIndex,sumSquaredDistanceToOtherEbugs[iIndex][jIndex]);
                }
            }
        }

        for(int i=0; i<nNodes; i++)
        {
            iIndex=nodeIndex.at(i);
            qDebug("**Node:%d**",iIndex);
            for(int j=0; j<nNodes; j++)
            {
                jIndex=nodeIndex.at(j);
                if(j==0)
                {
                    controlData[iIndex].minSumSquaredDistanceToOtherEbugs = sumSquaredDistanceToOtherEbugs[iIndex][jIndex];
                    controlData[iIndex].iMinSumSquaredDistanceToOtherEbugs = jIndex;
                }
                if(controlData[iIndex].minSumSquaredDistanceToOtherEbugs > sumSquaredDistanceToOtherEbugs[iIndex][jIndex] && iIndex!=jIndex)
                {
                    controlData[iIndex].minSumSquaredDistanceToOtherEbugs = sumSquaredDistanceToOtherEbugs[iIndex][jIndex];
                    controlData[iIndex].iMinSumSquaredDistanceToOtherEbugs = jIndex;
                }
            }
            qDebug("min SSD to other Node for Node[%d] has SSD=%d",iIndex,controlData[iIndex].minSumSquaredDistanceToOtherEbugs);
        }

        if(controlData[currentIndex].minSumSquaredDistanceToOtherEbugs<=(int)parameters[11]*(int)parameters[11] &&
                controlData[currentIndex].sumSquaredDistanceToGoalPoint>(int)parameters[13] &&
                !controlData[currentIndex].collisionCleared)
        {
            qDebug("minSSD = %d <= %d",controlData[currentIndex].minSumSquaredDistanceToOtherEbugs, (int)parameters[11]*(int)parameters[11]);
            qDebug("Checking if Node[%d] colliding into another eBug", currentIndex);
            controlData[currentIndex].minAvoidanceAngleThreshold=normalizeAngle(controlData[currentIndex].angleCurrent,-120);
            controlData[currentIndex].maxAvoidanceAngleThreshold=normalizeAngle(controlData[currentIndex].angleCurrent,120);
            qDebug("Node[%d]: minAvoidanceAngleThreshold=%d", currentIndex,controlData[currentIndex].minAvoidanceAngleThreshold);
            qDebug("Node[%d]: maxAvoidanceAngleThreshold=%d", currentIndex,controlData[currentIndex].maxAvoidanceAngleThreshold);

            iOtherNearestEbug=controlData[currentIndex].iMinSumSquaredDistanceToOtherEbugs;
            static int tempMinAngle;
            static int tempMaxAngle;
            qDebug("Check if falls within scope");
            tempMinAngle=minVal(controlData[currentIndex].minAvoidanceAngleThreshold, controlData[currentIndex].maxAvoidanceAngleThreshold);
            tempMaxAngle=maxVal(controlData[currentIndex].minAvoidanceAngleThreshold, controlData[currentIndex].maxAvoidanceAngleThreshold);
            controlData[currentIndex].minAvoidanceAngleThreshold=tempMinAngle;
            controlData[currentIndex].maxAvoidanceAngleThreshold=tempMaxAngle;

            static int otherEbugLocation;
            static int goalPointLocation;

            otherEbugLocation=quadrantLocation(controlData[currentIndex].xCurrent,controlData[currentIndex].yCurrent,
                                               controlData[iOtherNearestEbug].xCurrent,controlData[iOtherNearestEbug].yCurrent);
            goalPointLocation=quadrantLocation(controlData[currentIndex].xCurrent,controlData[currentIndex].yCurrent,
                                               controlData[currentIndex].xDesired,controlData[currentIndex].yDesired);
            qDebug("otherEbugLocation=%d", otherEbugLocation);
            qDebug("goalPointLocation=%d", goalPointLocation);

                        if(((otherEbugLocation==11)&&(goalPointLocation==11||goalPointLocation==12||goalPointLocation==42))||
                                ((otherEbugLocation==12)&&(goalPointLocation==12||goalPointLocation==21||goalPointLocation==11))||
                                ((otherEbugLocation==21)&&(goalPointLocation==21||goalPointLocation==22||goalPointLocation==12))||
                                ((otherEbugLocation==22)&&(goalPointLocation==22||goalPointLocation==21||goalPointLocation==31))||
                                ((otherEbugLocation==31)&&(goalPointLocation==31||goalPointLocation==22||goalPointLocation==32))||
                                ((otherEbugLocation==32)&&(goalPointLocation==32||goalPointLocation==31||goalPointLocation==41))||
                                ((otherEbugLocation==41)&&(goalPointLocation==41||goalPointLocation==32||goalPointLocation==42))||
                                ((otherEbugLocation==42)&&(goalPointLocation==42||goalPointLocation==11||goalPointLocation==41)))
//                            if(((otherEbugLocation==11)&&(goalPointLocation!=31))||
//                                    ((otherEbugLocation==12)&&(goalPointLocation!=32))||
//                                    ((otherEbugLocation==21)&&(goalPointLocation!=41))||
//                                    ((otherEbugLocation==22)&&(goalPointLocation!=42))||
//                                    ((otherEbugLocation==31)&&(goalPointLocation!=11))||
//                                    ((otherEbugLocation==32)&&(goalPointLocation!=12))||
//                                    ((otherEbugLocation==41)&&(goalPointLocation!=21))||
//                                    ((otherEbugLocation==42)&&(goalPointLocation!=22)))

            {
                controlData[currentIndex].avoidanceActive=true;
                controlData[iOtherNearestEbug].avoidanceActive=true;
                qDebug("Node[%d] is colliding into Node[%d]", currentIndex, iOtherNearestEbug);

//                if(otherEbugLocation==goalPointLocation)
//                {
//                    static int temp_xDesired;
//                    static int temp_yDesired;
//                    static int temp_ssdToGoalPoint;
//                    temp_xDesired=controlData[currentIndex].xDesired;
//                    temp_yDesired=controlData[currentIndex].yDesired;
//                    temp_ssdToGoalPoint=controlData[currentIndex].sumSquaredDistanceToGoalPoint;
//                    controlData[currentIndex].xDesired=controlData[iOtherNearestEbug].xDesired;
//                    controlData[currentIndex].yDesired=controlData[iOtherNearestEbug].yDesired;
//                    controlData[currentIndex].sumSquaredDistanceToGoalPoint=controlData[iOtherNearestEbug].sumSquaredDistanceToGoalPoint;
//                    controlData[iOtherNearestEbug].xDesired=temp_xDesired;
//                    controlData[iOtherNearestEbug].yDesired=temp_yDesired;
//                    controlData[iOtherNearestEbug].sumSquaredDistanceToGoalPoint=temp_ssdToGoalPoint;
//                }

                if(controlData[currentIndex].sumSquaredDistanceToGoalPoint>controlData[iOtherNearestEbug].sumSquaredDistanceToGoalPoint)
                {
                    controlData[currentIndex].xDesired=rectifiedXCoord(otherEbugLocation,goalPointLocation,controlData[currentIndex].angleCurrent,(int)parameters[21],(int)parameters[22],
                                                                       controlData[currentIndex].yCurrent,controlData[iOtherNearestEbug].yCurrent,
                                                                       controlData[currentIndex].xCurrent,controlData[iOtherNearestEbug].xCurrent);
                    controlData[currentIndex].yDesired=rectifiedYCoord(otherEbugLocation,goalPointLocation,controlData[currentIndex].angleCurrent,(int)parameters[21],(int)parameters[22],
                                                                       controlData[currentIndex].yCurrent,controlData[iOtherNearestEbug].yCurrent,
                                                                       controlData[currentIndex].xCurrent,controlData[iOtherNearestEbug].xCurrent);
                    qDebug("New Coord for Node[%d]: %d  %d", currentIndex, controlData[currentIndex].xDesired, controlData[currentIndex].yDesired);
                    controlData[currentIndex].stage=1;
                    controlData[iOtherNearestEbug].collidingNodeIndex=currentIndex;
                    controlData[iOtherNearestEbug].nodeStop=true;
                }
                else
                {
                    controlData[currentIndex].collidingNodeIndex=iOtherNearestEbug;
                    controlData[currentIndex].nodeStop=true;
                }
//                else if (controlData[iOtherE])
//                {
//                    controlData[iOtherNearestEbug].xDesired=rectifiedXCoord(otherEbugLocation,goalPointLocation,controlData[currentIndex].angleCurrent,(int)parameters[21],(int)parameters[22],
//                                                                       controlData[currentIndex].yCurrent,controlData[iOtherNearestEbug].yCurrent,
//                                                                       controlData[currentIndex].xCurrent,controlData[iOtherNearestEbug].xCurrent);
//                    controlData[iOtherNearestEbug].yDesired=rectifiedYCoord(otherEbugLocation,goalPointLocation,controlData[currentIndex].angleCurrent,(int)parameters[21],(int)parameters[22],
//                                                                       controlData[currentIndex].yCurrent,controlData[iOtherNearestEbug].yCurrent,
//                                                                       controlData[currentIndex].xCurrent,controlData[iOtherNearestEbug].xCurrent);
//                    qDebug("New Coord for Node[%d]: %d  %d", currentIndex, controlData[currentIndex].xDesired, controlData[currentIndex].yDesired);
//                }
                return 0;
            }
        }
        else if (allAvoidanceActive==false && controlData[currentIndex].desiredGoalPointReached)
        {
            if(controlData[currentIndex].avoidanceActive)
                controlData[currentIndex].collisionCleared=true;
            qDebug("avoidanceActive SET TO FALSE");
            controlData[currentIndex].avoidanceActive=false;
        }

        // Desired position reached
        if((abs(eBugData[eBugDataIndex].x-controlData[currentIndex].xDesired)<(int)parameters[13])&&(abs(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired)<(int)parameters[13]))
        {
            qDebug("Desired position reached");
            controlData[currentIndex].desiredGoalPointReached=true;
            controlData[currentIndex].stage=0;
            qDebug("stage set to %d for [%d]",controlData[currentIndex].stage, currentIndex);
            // Turn ON RED LEDs
            if((bool)parameters[19])
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                // Stop eBug
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
            if(controlData[currentIndex].avoidanceActive)
            {
                controlData[currentIndex].stage=0;
                controlData[currentIndex].avoidanceActive=false;
                controlData[currentIndex].collisionCleared=true;
            }
            else
                return 1;
        }
        // Reset trajectory if absolute angle error is above a certain threshold
        else if(fabs(angleErr)>parameters[12]||(controlData[currentIndex].avoidanceActive&&fabs(angleErr)>parameters[12]))
        {
            qDebug("Trajectory resetted");
            // Updating current number of eBugs in ROI
            // nNodesPrevious = nNodesPresent;
            // Set index
            //if((initialRunFlag||nNodesPresent!=nNodesPrevious))
            controlData[currentIndex].stage=1;
            qDebug("stage set to %d for [%d]",controlData[currentIndex].stage, currentIndex);
            // Turn ON RED LEDs
            //if((bool)parameters[19])
            //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
            // Stop eBug
            xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
            return 1;
        }
        // Perform control
        else
        {
            // Steer right
            if(command<0)
            {
                qDebug("Steer Right");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(speedFreq(controlData[currentIndex].sumSquaredDistanceToGoalPoint,controlData[currentIndex].minSumSquaredDistanceToOtherEbugs),0,true,0x08,true,
                                                                                                                round(speedFreq(controlData[currentIndex].sumSquaredDistanceToGoalPoint,controlData[currentIndex].minSumSquaredDistanceToOtherEbugs)*0.9),0,true,0x08,true,true,0x00)));
                return 2;
            }
            // Steer left
            else if(command>0)
            {
                qDebug("Steer left");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(round(speedFreq(controlData[currentIndex].sumSquaredDistanceToGoalPoint,controlData[currentIndex].minSumSquaredDistanceToOtherEbugs)*0.9),0,true,0x08,true,
                                                                                                                speedFreq(controlData[currentIndex].sumSquaredDistanceToGoalPoint,controlData[currentIndex].minSumSquaredDistanceToOtherEbugs),0,true,0x08,true,true,0x00)));
                return 2;
            }
        }
    }

    // Setting initialRunFlag to FALSE once first loop of STAGE 1, 2 and 3 have been completed.
    firstRun = false;

    static bool formationCompleted;
    formationCompleted=true;
    for (int i=0;i<nNodes;i++)
    {
        iIndex=nodeIndex.at(i);
        if(abs(controlData[iIndex].xCurrent-xGoalPoints.at(controlData[iIndex].iGP))>=(int)parameters[13] ||
                abs(controlData[iIndex].yCurrent-yGoalPoints.at(controlData[iIndex].iGP))>=(int)parameters[13])
        {
            qDebug("In here!");
            formationCompleted=false;
            finalLoopCount=0;
            break;
        }
        qDebug("FormationCompleted=%d",formationCompleted);
    }
    if (formationCompleted==true)
    {
        finalLoopCount++;
        controlData[currentIndex].nodeStop=true;
//        if(finalLoopCount>nNodes)
//        {
//            ledChaser(xBee,xBeeNodeTable,0);
//        }
        if(finalLoopCount==nNodes*2)
        {
            formationCompleted=false;
            sleep(0.001);
            qDebug("returning 88");
            return 88;
        }
    }
    return 0;
} // dominosFormation()

static int getNumberOfCollisions(bool *row, int nNodes)
{
    // Local variables
    int count=0;
    // Read row vector
    for(int i=0;i<nNodes;i++)
    {
        if(row[i])
            count++;
    }
    return count;
} // getNumberOfCollisions()

static double roundNumber(double r)
{
    // This function rounds a double to a whole number (also double)
    // e.g: roundNumber(4.5)=5.0, roundNumber(-4.5)=-5.0
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
} // roundNumber()

static int angleFreq(double angleErr)
{
    int freqVal;

    if(fabs(angleErr)>100)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>60)
    {
        freqVal=250;
    }

    else if(fabs(angleErr)>30)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>10)
    {
        freqVal=100;
    }

    else
        freqVal=100;

    return freqVal;
} // angleFreq()

static quint8 angleStepMode(double angleErr)
{
    quint8 stepModeVal;

    if(fabs(angleErr)>100)
    {
        stepModeVal=0x00;
    }

    else if(fabs(angleErr)>60)
    {
        stepModeVal=0x01;
    }

    else if(fabs(angleErr)>10)
    {
        stepModeVal=0x02;
    }

    else if(fabs(angleErr)>5)
    {
        stepModeVal=0x03;
    }

    else
        stepModeVal=0x08;

    return stepModeVal;
} // angleStepMode()

static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs)
{
    int speedFreqVal;
    int minSquaredDistance;

    if (sumOfSquaredDistanceOtherEbugs/2 < sumOfSquaredDistance)
    {
        minSquaredDistance = sumOfSquaredDistanceOtherEbugs*0.5;
    }
    else
        minSquaredDistance = sumOfSquaredDistance;

    if(fabs(minSquaredDistance)>20000)
    {
        speedFreqVal=6000;
    }

    else if(fabs(minSquaredDistance)>10000)
    {
        speedFreqVal=5000;
    }

    else if(fabs(minSquaredDistance)>2000)
    {
        speedFreqVal=3000;
    }

    else if(fabs(minSquaredDistance)>500)
    {
        speedFreqVal=2000;
    }

    else
        speedFreqVal=1000;
    qDebug("min SSD to Goal=%d",sumOfSquaredDistance);
    qDebug("min SSD to other eBugs=%d",sumOfSquaredDistanceOtherEbugs);
    qDebug("minSquaredDistance=%d",minSquaredDistance);
    qDebug("speedFreqVal=%d",speedFreqVal);
    return speedFreqVal;
} // speedFreqVal()

static int normalizeAngle(int angle, int scope)
{
    int normalizedAngleVal;
    int tempAngleVal;
    tempAngleVal=angle+scope;
    qDebug("tempAngleVal=%d + %d = %d",angle, scope, tempAngleVal);

    if(tempAngleVal>=360)
    {
        normalizedAngleVal=tempAngleVal-360;
    }
    else if(tempAngleVal<0)
    {
        normalizedAngleVal=tempAngleVal+360;
    }
    else
        normalizedAngleVal=tempAngleVal;
    qDebug("normalizedAngleVal=%d",normalizedAngleVal);
    return normalizedAngleVal;
} // normalizeAngle()

static int maxVal(int value1, int value2)
{
    if(value1>value2)
        return value1;
    else
        return value2;
} // maxVal()

static int iMaxVal(int value1, int value2)
{
    int index;
    if(value1>value2)
        index=1;
    else index=2;
    return index;
} // iMaxVal()

static int minVal(int value1, int value2)
{
    if(value1<value2)
        return value1;
    else
        return value2;
} // minVal()

static int iMinVal(int value1, int value2)
{
    int index;
    if(value1<value2)
        index=1;
    else index=2;
    return index;
} // iMinVal()

static int rectifiedXCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug)
{
    double angleTemp;
    double angleDiff;
    double newAngle;
    double newXCoord;

    if (angle<0)
        angle=angle+360;

    if (quadNode==11||quadNode==12)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=angleDiff;
    }
    else if (quadNode==21||quadNode==22)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180-angleDiff;
    }
    else if (quadNode==31||quadNode==32)
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180+angleDiff;
    }
    else
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=360-angleDiff;
    }
    qDebug("angleDiff between eBugs=%f",angleDiff);

    if (quadNode/10==4&&quadGP/10==1 || quadNode/10==1&&quadGP/10==4)
    {
        if(angle>angleTemp)
        {
            angleThreshold=-angleThreshold;
            qDebug("in quad 4,1 angle>angleTemp");
        }
    }
    else if (angle<angleTemp)
    {
        angleThreshold=-angleThreshold;

        qDebug("not in quad 4,1 angle<angleTemp");
    }

    qDebug("angleThresh=%d",angleThreshold);

    //newAngle=angleThreshold+angleTemp;
    newAngle=angle+angleThreshold;
    qDebug("raw newAngle=%f",newAngle);

    do {newAngle=newAngle-360;} while (newAngle>=360);
    if (newAngle>180) {newAngle=newAngle-360;}
    else if (newAngle<-180) {newAngle=newAngle+360;}
    qDebug("rectified newAngle=%f",newAngle);

    if (newAngle>=-90 && newAngle<90)
    {
        newXCoord=xThisEbug+distanceThreshold*cos(fabs(newAngle*PI/180));
    }
    else
        newXCoord=xThisEbug-distanceThreshold*cos((180-fabs(newAngle))*PI/180);
    qDebug("xCurrent=%d, newxCoord=%f",xThisEbug,newXCoord);
    qDebug("currentAngle=%d, newAngle=%f",angle,newAngle);
    return round(newXCoord);
} //rectifiedXCoord()

static int rectifiedYCoord(int quadNode,int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug)
{
    double angleTemp;
    double angleDiff;
    double newAngle;
    double newYCoord;

    if (angle<0)
        angle=angle+360;

    if (quadNode==11||quadNode==12)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=angleDiff;
    }
    else if (quadNode==21||quadNode==22)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180-angleDiff;
    }
    else if (quadNode==31||quadNode==32)
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180+angleDiff;
    }
    else
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=360-angleDiff;
    }

    if (quadNode/10==4&&quadGP/10==1 || quadNode/10==1&&quadGP/10==4)
    {
        if(angle>angleTemp)
        {
            angleThreshold=-angleThreshold;
            qDebug("in quad 4,1 angle>angleTemp");
        }
    }
    else if (angle<angleTemp)
    {
        angleThreshold=-angleThreshold;

        qDebug("not in quad 4,1 angle<angleTemp");
    }

    //newAngle=angleThreshold+angleTemp;
    newAngle=angle+angleThreshold;

    do {newAngle=newAngle-360;} while (newAngle>=360);
    if (newAngle>180) {newAngle=newAngle-360;}
    else if (newAngle<-180) {newAngle=newAngle+360;}

    if (newAngle>=0 && newAngle<180)
    {
        newYCoord=yThisEbug-distanceThreshold*sin(fabs(newAngle*PI/180));
    }
    else
        newYCoord=yThisEbug+distanceThreshold*sin((180-fabs(newAngle))*PI/180);
    qDebug("yCurrent=%d, newyCoord=%f",yThisEbug,newYCoord);
    qDebug("currentAngle=%d, newAngle=%f",angle,newAngle);
    return round(newYCoord);
} //rectifiedYCoord()

static int quadrantLocation(int xThisEbug, int yThisEbug, int xOtherEbug, int yOtherEbug)
{
    double angle;
    if(xThisEbug<=xOtherEbug && yThisEbug>=yOtherEbug)
    {
        qDebug("Part 1");
        angle = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        if(angle>45)
        {
            qDebug("Returning 12");
            return 12;
        }
        else
        {
            qDebug("Returning 11");
            return 11;
        }
    }
    else if(xThisEbug>=xOtherEbug && yThisEbug>=yOtherEbug)
    {
        qDebug("Part 2");
        angle = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        if(angle>45)
        {
            qDebug("Returning 21");
            return 21;
        }
        else
        {
            qDebug("Returning 22");
            return 22;
        }
    }
    else if(xThisEbug>=xOtherEbug && yThisEbug<=yOtherEbug)
    {
        qDebug("Part 3");
        angle = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        if(angle>45)
        {
            qDebug("Returning 32");
            return 32;
        }
        else
        {
            qDebug("Returning 31");
            return 31;
        }
    }
    else
    {
        qDebug("Part 4");
        angle = (180/PI)*atan(((double)xOtherEbug-(double)xThisEbug)/((double)yOtherEbug-(double)yThisEbug));
        if(angle>45)
        {
            qDebug("Returning 42");
            return 42;
        }
        else
        {
            qDebug("Returning 41");
            return 41;
        }
    }
} //quadrantLocation()

static int ledChaser(XBee *xBee, struct XBeeNodeTable xBeeNodeTable, int option)
{
    switch (option)
    {
    case 0:
    {
        for(int count=0;count<17;count++)
        {
            xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetRed(count,4095,true,0x00)));
            sleepThread(40);
        }
        xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
        break;
    }

    case 1:
    {

        xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllOff(true,0x00)));
    }
        break;
    }
    return 0;
}
