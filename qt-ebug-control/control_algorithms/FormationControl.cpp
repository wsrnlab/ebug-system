/* TODO:
=======
1. Collision avoidance
-fix ebug not moving at all when obstacle is in its path and at +/- 90 deg from the ebug
    1. instead of *cos(angle to target), try to have a minimum V (don't have ebug stop completely when 90 deg from target)
    2. in collision avoidance, try to reduce the impact of V reduction, have it scale according to distance, currently it is a big drop, maybe remove the multiplier which considers angle of object
  marking ebug as obstacles
-when stuck in local minima, need to be able to push itself away, make changes to V scaling (when too close, -ve v)
    eg: if detected more than 2 obstacles, try to group them so that ebug only avoid once for all obstacles, this will prevent ebug from getting stuck when sandwiched between 2 obstacles in front
        -how to detect if ebug is stuck in local minima:
            i) if ebug is avoiding obstacle
            ii) v ~= 0 & w~= 0 (use moving average to find average over a period of time: aveVal = a*lastAveVal + b*currVal where a >> b, reset when collisionAvoidance is no longer triggered) for a period
        -if stuck, bounce back (increase repulsive force, or make multiplier more negative), then make a random turn
-after avoiding an obstacle, remember the last correctionW and avoid turning back into the obstacle again, this is where you incorporate desired angle and correction angle

*/
#include "FormationControl.h"
#include <math.h>
#include <QTime>

static double calculateDistanceSquared(int, int, int , int );
static int convertVelocitiesToFreq(double, double, int *, int *, int *, int *);
static int correctDisplayAngle(double *);
static int calculateErrorVector(double , double , double , double , double *, double *);
static int readSensorInput(int , int , int , int , double , double *, double *);
static int checkFormationConstraint(int, int);
static double leaderWController(int , int , int , int , double , double );
static double followerVController(double , double );
static double followerWController(double , double , double , double ,double );
static double stdAngleToTarget(double , double );
static double normAngleToTarget(double , double );

int formationControl(XBee *xBee, struct XBeeNodeTable xBeeNodeTable,
                     struct EBugData* ebugData, int ebugDataIndex, int currentEbugIndex, int nNodes,
                     struct ControlDataFormationControl* controlData,
                     double* parameters, bool &resetControlFlag, bool *clearTrail)
{
    // PARAMETERS
    static int startingFormation; // Starting formation type
    static int xDesired; // Desired destination x coordinate
    static int yDesired; // Desired destination y coordinate
    static int ebugDistance; // Distance between adjacent ebugs in formation (in pixels)
    static double kDist; // kDist
    static double kAng; // kAng
    static double kV; // kV
    static double deadZone; // deadZone
    static double formationV; // formationV: speed of entire formation (leader)
    static int debug; // debug: no movement if set to 1
    static int initializeFormation; // set to 1 if user wants initial formation to be formed before moving
    static int collisionProxFactor; // multiplier applied to EBUG_RADIUS, dictating how close can ebugs be to obstacles before starting collision avoidance (default 3)
    static int generic;
    static int sideThreshold;
    static int nEbugObstacle; // number of ebugs which are obstacles (max of 10)
    static int obstacleID[MAX_EBUG_AS_OBSTACLE]; // obstacleID: array storing the IDs of ebugs which are obstacles

    // defines which leader to follow in each position within the formation
    static int leaderVector[MAX_FORMATION_TYPE][2][9] = {{{-1,0,0,1,1,2,3,3,4},{-1,-1,1,2,3,3,4,5,6}}}; // triangle

    // define the angle and distance constraints for various constraint types
    static FormationConstraint constraints[MAX_FORMATION_TYPE][4] = {{{210,-1,0,-1},{150,90,0,0},{210,270,0,0},{150,210,0,0}}}; // 4 types of constraints in triangle formation

    static int currentFormationPositionIndex = 0;
    static int currentFormation = -1;
    static double scaleFactor = 1;
    static int maxNodes;
    static int nActiveNodes;
    static double refAngle;
    static QList<int> uncheckedEbugs;
    static QList<int> positionAssigned;
    static int stageComplete = 0;

    /////////////////////////////////////////////////////
    // RESET: only triggered when you press start/stop,//
    /////////////////////////////////////////////////////
    if(resetControlFlag)
    {
        int nParameter = 0;
        qDebug("**INPUTS**");
        startingFormation = parameters[nParameter++];
        qDebug("startingFormation: %d", startingFormation);
        xDesired = parameters[nParameter++];
        qDebug("xDesired: %d", xDesired);
        yDesired = parameters[nParameter++];
        qDebug("yDesired: %d", yDesired);
        ebugDistance = parameters[nParameter++];
        qDebug("ebugDistance: %d", ebugDistance);
        kDist = parameters[nParameter++];
        qDebug("kDist: %.2f", kDist);
        kAng = parameters[nParameter++];
        qDebug("kAng: %.2f", kAng);
        kV = parameters[nParameter++];
        qDebug("kV: %.2f", kV);
        deadZone = parameters[nParameter++];
        qDebug("deadZone: %.2f", deadZone);
        formationV = parameters[nParameter++];
        qDebug("formationV: %.2f", formationV);
        debug = parameters[nParameter++];
        qDebug("debug: %d", debug);
        initializeFormation = parameters[nParameter++];
        qDebug("initializeFormation: %d", initializeFormation);
        collisionProxFactor = parameters[nParameter++];
        qDebug("collisionProxFactor: %d", collisionProxFactor);
        generic = parameters[nParameter++];
        qDebug("generic: %d", generic);
        sideThreshold = parameters[nParameter++];
        qDebug("sideThreshold: %d", sideThreshold);
        nEbugObstacle = parameters[nParameter++];
        qDebug("nEbugObstacle: %d", nEbugObstacle);

        if (nEbugObstacle > MAX_EBUG_AS_OBSTACLE)
        {
            nEbugObstacle = MAX_EBUG_AS_OBSTACLE;
            qDebug("nEbugObstacle is limited to %d", MAX_EBUG_AS_OBSTACLE);
        }

        // clear obstacleID
        for (int i=0;i<MAX_EBUG_AS_OBSTACLE;i++)
        {
            obstacleID[i] = -1;
        }

        // determine number of ebugs which will be part of the formation (ie not obstacles)
        nActiveNodes = nNodes;
        for (int i=0;i<nEbugObstacle;i++)
        {
            obstacleID[i] = parameters[nParameter++];
            qDebug("obstacleID[%d]: %d", i,obstacleID[i]);
            if (obstacleID[i] != -1)
            {
                nActiveNodes--;
            }
        }
        qDebug("**********");
        qDebug("nActiveNodes: %d", nActiveNodes);

        // update formation constraints for every formation
        for (int i=0;i<MAX_FORMATION_TYPE;i++)
        {
            // for every formation constraint
            switch(i)
            {
                case triangle:
                    for (int j=0;j<4;j++)
                    {
                        constraints[triangle][j].dist1 = ebugDistance;
                        constraints[triangle][j].dist2 = ebugDistance;
                    }
                    constraints[triangle][0].dist2 = 0;
                    break;
            }
        }

        maxNodes = nNodes; // set maximum # of nodes tracked in this run
        currentFormation = startingFormation;
        currentFormationPositionIndex = 0;
        scaleFactor = 1;
        stageComplete = 0;
        positionAssigned.clear();
        uncheckedEbugs.clear();
        *clearTrail = 1; // clear ebug trails on GUI

        for (int j=0;j<maxNodes;j++)
        {
            uncheckedEbugs.append(j);
        }

        qDebug("Running initializeControlDataFormationControl");
        return 1; // run ControlThread::initializeControlDataFormationControl() back in ControlThread.cpp
    }

    controlData[currentEbugIndex].xCurrent = ebugData[ebugDataIndex].x;
    controlData[currentEbugIndex].yCurrent = ebugData[ebugDataIndex].y;
    controlData[currentEbugIndex].angleCurrent = ebugData[ebugDataIndex].angle;
    controlData[currentEbugIndex].ID = ebugData[ebugDataIndex].id;

    if (!positionAssigned.isEmpty())
    {
        refAngle = controlData[positionAssigned.at(0)].angleCurrent - 180;
        correctDisplayAngle(&refAngle);
    }

    // marking ebugs as obstacles
    for (int i=0;i<nEbugObstacle;i++)
    {
        if (controlData[currentEbugIndex].ID == obstacleID[i])
        {
            //qDebug("eBug %d (ID: %d) marked as obstacle.", currentEbugIndex, controlData[currentEbugIndex].ID);

            controlData[currentEbugIndex].isObstacle = 1;
            if (uncheckedEbugs.contains(currentEbugIndex))
            {
                uncheckedEbugs.removeAll(currentEbugIndex);
            }
            // no desired position, just mark current position for drawing
            controlData[currentEbugIndex].xDesired = controlData[currentEbugIndex].xCurrent;
            controlData[currentEbugIndex].yDesired = controlData[currentEbugIndex].yCurrent;
            //qDebug("Qlist length: %d", uncheckedEbugs.length());

            return 0; // don't have to run the remaining code
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Initialize System: determining desired position, selecting leader //
    ///////////////////////////////////////////////////////////////////////
    if (controlData[currentEbugIndex].stage == initializeSystem)
    {
        if (uncheckedEbugs.contains(currentEbugIndex))
        {
            uncheckedEbugs.removeAll(currentEbugIndex);
            controlData[currentEbugIndex].distanceToTargetSquared=calculateDistanceSquared(xDesired, yDesired, controlData[currentEbugIndex].xCurrent, controlData[currentEbugIndex].yCurrent);
            // qDebug("distanceSquaredToTarget = %d", controlData[currentEbugIndex].distanceToTargetSquared);

            // first ebug for consideration, automatically set it to leader first
            if (positionAssigned.isEmpty())
            {
                positionAssigned.insert(currentFormationPositionIndex, currentEbugIndex);
            }
            // current ebug is closer
            else if (controlData[currentEbugIndex].distanceToTargetSquared < controlData[positionAssigned.at(currentFormationPositionIndex)].distanceToTargetSquared)
            {
                positionAssigned.replace(currentFormationPositionIndex, currentEbugIndex);
            }
        }
        // a leader has been elected, check if program is ready to proceed to next stage
        else if ((uncheckedEbugs.isEmpty()) && (currentEbugIndex == positionAssigned.at(0)))
        {
            // rotate leader towards destination
            if (initializeFormation)
            {
                int leftWheelFreq;
                int rightWheelFreq;
                int leftWheelDirection;
                int rightWheelDirection;
                double ebugW = leaderWController(xDesired, yDesired, controlData[currentEbugIndex].xCurrent, controlData[currentEbugIndex].yCurrent, controlData[currentEbugIndex].angleCurrent, kAng);
                double angleError = ebugW/kAng;

                // once leader is facing the right position, stop and proceed to next stage
                if (fabs(angleError) < 10)
                {
                    if (!debug) xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
                    stageComplete = 1;
                }
                else
                {
                    //qDebug("Leader's Angle Error: %.2f", angleError);
                    convertVelocitiesToFreq(0, ebugW, &leftWheelFreq, &rightWheelFreq, &leftWheelDirection, &rightWheelDirection);
                    if (!debug) xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(leftWheelFreq,0,leftWheelDirection,0x03,true,rightWheelFreq,0,rightWheelDirection,0x03,true,true,0x00)));
                }
            }
            // not required to initialize formation
            else
            {
                stageComplete = 1;
            }

            if (stageComplete)
            {
                controlData[positionAssigned.at(0)].xDesired = xDesired;
                controlData[positionAssigned.at(0)].yDesired = yDesired;
                qDebug("ebug %d (ID: %d) is the leader", positionAssigned.at(currentFormationPositionIndex), controlData[positionAssigned.at(currentFormationPositionIndex)].ID);
                qDebug("Moving on to stage %d", initializeSystem+1);
                currentFormationPositionIndex++;
                stageComplete = 0;

                for (int j=0;j<maxNodes;j++)
                {
                    controlData[j].stage = assignFormationPos;
                    uncheckedEbugs.append(j);
                }

                uncheckedEbugs.removeAll(positionAssigned.at(0)); // omit leader from the second stage
                qDebug("Qlist length: %d", uncheckedEbugs.length());
            }
        }
    } // end initialization

    //////////////////////////////////////////////////////////////////////////////////
    // Assign Formation Position: Determine positions of ebugs in initial formation //
    //////////////////////////////////////////////////////////////////////////////////
    else if (controlData[currentEbugIndex].stage == assignFormationPos)
    {
        int constraintType = checkFormationConstraint(currentFormation, currentFormationPositionIndex);
        static int leader1 = positionAssigned.at(leaderVector[currentFormation][0][currentFormationPositionIndex]);
        static int distanceConstraint1 = constraints[currentFormation][constraintType].dist1;
        static double angleConstraint1 = constraints[currentFormation][constraintType].angle1;
        static int leader2 = -1;
        static int distanceConstraint2 = constraints[currentFormation][constraintType].dist2;
        static double angleConstraint2 = constraints[currentFormation][constraintType].angle2;

        if (uncheckedEbugs.contains(currentEbugIndex))
        {
            qDebug("Checking ebug %d (ID: %d) out of (%d) ebugs for position %d",currentEbugIndex, controlData[currentEbugIndex].ID, uncheckedEbugs.length(), currentFormationPositionIndex);
            uncheckedEbugs.removeAll(currentEbugIndex);
            //qDebug("Qlist length: %d", uncheckedEbugs.length());

            int leader1X;
            int leader1Y;
            double leader1Angle;
            double theta1;
            double distToLeader1;
            double normalError1 = 0;
            double forwardError1 = 0;
            int leader2X;
            int leader2Y;
            double leader2Angle;
            double theta2;
            double distToLeader2;
            double normalError2 = 0;
            double forwardError2 = 0;
            int thisX = controlData[currentEbugIndex].xCurrent;
            int thisY = controlData[currentEbugIndex].yCurrent;

            // calculate distance to target position first
            // set the angle and distance constraints according to formation
            leader1 = positionAssigned.at(leaderVector[currentFormation][0][currentFormationPositionIndex]);
            leader1X = controlData[leader1].xCurrent;
            leader1Y = controlData[leader1].yCurrent;
            leader1Angle = controlData[leader1].angleCurrent;
            //qDebug("leader1: ebug %d (ID: %d) at formation position %d", leader1, controlData[leader1].ID, leaderVector[currentFormation][0][currentFormationPositionIndex]);
            distanceConstraint1 = constraints[currentFormation][constraintType].dist1;
            angleConstraint1 = constraints[currentFormation][constraintType].angle1;
            //qDebug("Constraints 1: %.2f %d", angleConstraint1, distanceConstraint1);

            // calculate error vector between follower and leader 1
            //qDebug("Leader(%d,%d), Follower(%d,%d)",leader1X,leader1Y,thisX,thisY);
            readSensorInput(leader1X, leader1Y, thisX, thisY, refAngle, &theta1, &distToLeader1);
            calculateErrorVector(theta1, distToLeader1, angleConstraint1, distanceConstraint1, &normalError1, &forwardError1);

            // calculate distance between current position and current formation position of interest for:
            // 2-leader follower
            if (leaderVector[currentFormation][1][currentFormationPositionIndex] != -1)
            {
                leader2 = positionAssigned.at(leaderVector[currentFormation][1][currentFormationPositionIndex]);
                leader2X = controlData[leader2].xCurrent;
                leader2Y = controlData[leader2].yCurrent;
                leader2Angle = controlData[leader2].angleCurrent;
                //qDebug("leader2: ebug %d (ID: %d) at formation position %d", leader2, controlData[leader2].ID, leaderVector[currentFormation][1][currentFormationPositionIndex]);
                angleConstraint2 = constraints[currentFormation][constraintType].angle2;
                distanceConstraint2 = constraints[currentFormation][constraintType].dist2;
                //qDebug("Constraints 2: %.2f %d", angleConstraint2, distanceConstraint2);

                // calculate error vector between follower and leader 2
                readSensorInput(leader2X, leader2Y, thisX, thisY, refAngle, &theta2, &distToLeader2);
                calculateErrorVector(theta2, distToLeader2, angleConstraint2, distanceConstraint2, &normalError2, &forwardError2);

                controlData[currentEbugIndex].distanceToTargetSquared = pow((normalError1+normalError2)/2,2) + pow((forwardError1+forwardError2)/2,2); // calculate average distance error squared
            }
            // 1-leader follower
            else
            {
                leader2 = -1;
                controlData[currentEbugIndex].distanceToTargetSquared = pow(normalError1,2) + pow(forwardError1,2); // calculate distance error squared
            }

            // first ebug for consideration, automatically set it to follower 1 first
            if (positionAssigned.length() < currentFormationPositionIndex+1)
            {
                //qDebug("ebug %d (ID: %d) is first for consideration", currentEbugIndex, controlData[currentEbugIndex].ID);
                positionAssigned.insert(currentFormationPositionIndex, currentEbugIndex);
                //qDebug("Set ebug %d (ID: %d) to position %d", currentEbugIndex, controlData[currentEbugIndex].ID, currentFormationPositionIndex);
            }
            // if current ebug is closer to position than the previously closest ebug, replace
            else if (controlData[currentEbugIndex].distanceToTargetSquared < controlData[positionAssigned.at(currentFormationPositionIndex)].distanceToTargetSquared)
            {
                //qDebug("ebug %d (ID: %d) is closer than ebug %d (ID: %d).", currentEbugIndex, controlData[currentEbugIndex].ID, positionAssigned.at(currentFormationPositionIndex), controlData[positionAssigned.at(currentFormationPositionIndex)].ID);
                positionAssigned.replace(currentFormationPositionIndex, currentEbugIndex);
                //qDebug("Set ebug %d (ID: %d) to position %d", currentEbugIndex, controlData[currentEbugIndex].ID, currentFormationPositionIndex);
            }
        }
        // only 1 ebug (ie the leader) is in ROI
        else if (nActiveNodes == 1)
        {
            stageComplete = 1;
        }
        // all ebugs have been checked for currentFormationPositionIndex
        else if (uncheckedEbugs.isEmpty())
        {
            // assign local leaders and constraints to the chosen follower for currentFormationPositionIndex
            int assignedEbug = positionAssigned.at(currentFormationPositionIndex);
            controlData[assignedEbug].leader1 = leader1;
            controlData[assignedEbug].angleConstraint1 = angleConstraint1;
            controlData[assignedEbug].distanceConstraint1 = distanceConstraint1;
            controlData[assignedEbug].leader2 = leader2;
            controlData[assignedEbug].angleConstraint2 = angleConstraint2;
            controlData[assignedEbug].distanceConstraint2 = distanceConstraint2;

            qDebug("ebug %d (ID: %d) is follower %d", assignedEbug, controlData[assignedEbug].ID, currentFormationPositionIndex);
            //qDebug("angleConstraints: [%.2f, %.2f] distanceConstraints: [%d, %d]", angleConstraint1, angleConstraint2, distanceConstraint1, distanceConstraint2);

            // all formation positions have been allocated
            if (currentFormationPositionIndex == nActiveNodes - 1)
            {
                stageComplete = 1;
            }
            else
            {
                qDebug("Rebuilding checklist");
                // rebuild checklist to include ebugs that have not been assigned a formation position
                for (int j=0;j<maxNodes;j++)
                {
                    if (!positionAssigned.contains(j))
                    {
                        uncheckedEbugs.append(j);
                    }
                }

                currentFormationPositionIndex++; // move on to the next formation position
                qDebug("Moving to next position");
            }
        }
        else
        {
            qDebug("ebug %d (ID: %d) has been assigned a position", currentEbugIndex, controlData[currentEbugIndex].ID);
        }

        if (stageComplete)
        {
            stageComplete = 0;

            for (int j=0;j<maxNodes;j++)
            {
                if (initializeFormation)
                {
                        controlData[j].stage = buildFormation;
                }
                else
                {
                        controlData[j].stage = moveFormation;
                }
                uncheckedEbugs.append(j);
            }

            qDebug("Moving on to stage %d", assignFormationPos+1);
        }
    }

    ////////////////////////////////////////////////////////////////////
    // Build Formation: set followers in right position before moving //
    ////////////////////////////////////////////////////////////////////
    else if (controlData[currentEbugIndex].stage == buildFormation)
    {
        stageComplete = 1;

        if (stageComplete)
        {
            stageComplete = 0;

            for (int j=0;j<maxNodes;j++)
            {
                controlData[j].stage = moveFormation;
                uncheckedEbugs.append(j);
            }

            qDebug("Moving on to stage %d", buildFormation+1);
        }
    }

    ///////////////////////////////////////////////////
    // Move Formation: Move with Obstacle Avoidance! //
    ///////////////////////////////////////////////////
    else if (controlData[currentEbugIndex].stage == moveFormation)
    {
        double ebugV;
        double ebugW;
        int leftWheelFreq;
        int rightWheelFreq;
        int leftWheelDirection;
        int rightWheelDirection;
        int thisX = controlData[currentEbugIndex].xCurrent;
        int thisY = controlData[currentEbugIndex].yCurrent;
        double thisAngle = controlData[currentEbugIndex].angleCurrent;
        double normalizedThisAngle = thisAngle - refAngle;
        correctDisplayAngle(&normalizedThisAngle);
        int controllerReady = 0;

        // move leader towards target coordinates
        if (currentEbugIndex == positionAssigned.at(0))
        {
            //qDebug("currentEbugIndex: %d, positionAssigned.at(0): %d", currentEbugIndex, positionAssigned.at(0));
            int distanceToTargetSquared = calculateDistanceSquared(xDesired, yDesired, thisX, thisY);

            if (distanceToTargetSquared > MIN_STOPPING_DISTANCE)
            {
                ebugV = formationV;
                ebugW = leaderWController(xDesired, yDesired, thisX, thisY, thisAngle, kAng);
                controllerReady = 1;
            }
            // reached target coordinates, stop ebug
            else
            {
                controllerReady = 0;
            }
        }
        // followers
        else
        {
            double theta1;
            double distToLeader1;
            double normalError1 = 0;
            double forwardError1 = 0;
            int leader1 = controlData[currentEbugIndex].leader1;
            int leader1X = controlData[leader1].xCurrent;
            int leader1Y = controlData[leader1].yCurrent;
            int leader1Stopped = controlData[leader1].hasStopped;
            int distanceConstraint1 = controlData[currentEbugIndex].distanceConstraint1;
            double angleConstraint1 = controlData[currentEbugIndex].angleConstraint1;
            double distanceToTargetSquared = controlData[currentEbugIndex].distanceToTargetSquared;
            double forwardV;

            //qDebug("leader1: %d (%d, %d), angle: %.2f, distance: %d", leader1, leader1X, leader1Y, angleConstraint1, distanceConstraint1);

            // one leader follower
            if (controlData[currentEbugIndex].leader2 == -1)
            {
                if ((!leader1Stopped) || (distanceToTargetSquared > MIN_STOPPING_DISTANCE))
                {
                    readSensorInput(leader1X, leader1Y, thisX, thisY, refAngle, &theta1, &distToLeader1);
                    calculateErrorVector(theta1, distToLeader1, angleConstraint1, distanceConstraint1, &normalError1, &forwardError1);

                    // calculate error distance to desired position
                    controlData[currentEbugIndex].distanceToTargetSquared = pow(normalError1,2)+pow(forwardError1,2);
                    forwardV = followerVController(forwardError1, kDist);

                    // convert V forward into V ebug
                    double angleTemp = normAngleToTarget(normalError1, forwardError1) - normalizedThisAngle; // instead of 180, use angle to target position
                    correctDisplayAngle(&angleTemp);
                    ebugV = forwardV*cos(PI/180*(angleTemp)); // formation direction angle (always 180 from refAngle) - current ebug angle
                    ebugW = followerWController(normalError1, forwardError1, normalizedThisAngle, deadZone, kAng);

                    controllerReady = 1;

                    // update follower's desired position on GUI
                    double angletemp = -refAngle - angleConstraint1; // 180 - leader's angle - desired angle from leader
                    controlData[currentEbugIndex].xDesired = leader1X - scaleFactor*distanceConstraint1*cos(PI/180*angletemp);
                    controlData[currentEbugIndex].yDesired = leader1Y - scaleFactor*distanceConstraint1*sin(PI/180*angletemp);
                    //qDebug("Updating desired position of ebug %d (ID: %d) to (%d, %d)", currentEbugIndex, controlData[currentEbugIndex].ID, controlData[currentEbugIndex].xDesired, controlData[currentEbugIndex].yDesired);
                }
                else
                {
                    controllerReady = 0;
                }
            }
            // two leader follower
            else
            {
                double theta2;
                double distToLeader2;
                double normalError2 = 0;
                double forwardError2 = 0;
                int leader2 = controlData[currentEbugIndex].leader2;
                int leader2X = controlData[leader2].xCurrent;
                int leader2Y = controlData[leader2].yCurrent;
                int leader2Stopped = controlData[leader2].hasStopped;
                int distanceConstraint2 = controlData[currentEbugIndex].distanceConstraint2;
                double angleConstraint2 = controlData[currentEbugIndex].angleConstraint2;

                //qDebug("leader2: %d (%d, %d), angle: %.2f, distance: %d", leader2, leader2X, leader2Y, angleConstraint2, distanceConstraint2);

                // not all leaders have stopped, or ebug is still far away from desired position
                if (((!leader1Stopped) && (!leader2Stopped)) || (distanceToTargetSquared > MIN_STOPPING_DISTANCE))
                {
                    readSensorInput(leader1X, leader1Y, thisX, thisY, refAngle, &theta1, &distToLeader1);
                    calculateErrorVector(theta1, distToLeader1, angleConstraint1, distanceConstraint1, &normalError1, &forwardError1);
                    readSensorInput(leader2X, leader2Y, thisX, thisY, refAngle, &theta2, &distToLeader2);
                    calculateErrorVector(theta2, distToLeader2, angleConstraint2, distanceConstraint2, &normalError2, &forwardError2);

                    double normalErrorAve = (normalError1+normalError2)/2;
                    double forwardErrorAve = (forwardError1+forwardError2)/2;

                    // calculate error distance to desired position
                    controlData[currentEbugIndex].distanceToTargetSquared = pow(normalErrorAve,2)+pow(forwardErrorAve,2);
                    forwardV = followerVController(forwardErrorAve, kDist);

                    // convert forward V into V ebug
                    double angleTemp = normAngleToTarget(normalErrorAve, forwardErrorAve) - normalizedThisAngle; // instead of 180, use angle to target position
                    correctDisplayAngle(&angleTemp);
                    ebugV = forwardV*cos(PI/180*(angleTemp)); // formation direction angle (always 180 from refAngle) - current ebug angle
                    ebugW = followerWController(normalErrorAve, forwardErrorAve, normalizedThisAngle, deadZone, kAng);

                    controllerReady = 1;

                    // update follower's desired position on GUI
                    double angletemp = -refAngle - angleConstraint1; // 180 - leader's angle - desired angle from leader
                    int xDesired1 = leader1X - scaleFactor*distanceConstraint1*cos(PI/180*angletemp);
                    int yDesired1 = leader1Y - scaleFactor*distanceConstraint1*sin(PI/180*angletemp);
                    angletemp = -refAngle - angleConstraint2; // 180 - leader's angle - desired angle from leader
                    int xDesired2 = leader2X - scaleFactor*distanceConstraint2*cos(PI/180*angletemp);
                    int yDesired2 = leader2Y - scaleFactor*distanceConstraint2*sin(PI/180*angletemp);
                    controlData[currentEbugIndex].xDesired = (xDesired1 + xDesired2)/2;
                    controlData[currentEbugIndex].yDesired = (yDesired1 + yDesired2)/2;
                    //qDebug("Updating desired position of ebug %d (ID: %d) to (%d, %d)", currentEbugIndex, controlData[currentEbugIndex].ID, controlData[currentEbugIndex].xDesired, controlData[currentEbugIndex].yDesired);

                }
                // within stopping range and both leaders have stopped
                else
                {
                    controllerReady = 0;
                }
            }

            // ensure ebugs maintain a minimum speed, formation turning may not be as sharp as without this, but should solve some issue where ebug is stuck when facing obstacles
            /*
            if (ebugV < 0)
            {
                ebugV = formationV/2;
            }
            else if ((ebugV > 0) && (ebugV < formationV))
            {
                ebugV = formationV;
            }
            */
            if ((ebugV < 0) && (ebugV > -formationV))
            {
                ebugV = -formationV;
            }
            else if ((ebugV > 0) && (ebugV < formationV))
            {
                ebugV = formationV;
            }
        }

        if (controllerReady)
        {
            /////////////////////////
            // collision avoidance //
            /////////////////////////
            double correctedEbugV = ebugV;
            double correctedEbugW = 0;
            double ebugV2 = ebugV;
            int printDebug = 1;
            int obstacleDetected = 0;

            qDebug("Collision Avoidance for ebug %d (ID:%d), V: %.2f, W: %.2f", currentEbugIndex, controlData[currentEbugIndex].ID, ebugV, ebugW);

            for(int i=0; i<maxNodes; i++)
            {
                if (i != currentEbugIndex)
                {
                    double obstacleAngle;
                    double obstacleDistance;
                    double objectAngleToCenter;
                    double absObjectAngleToCenter;
                    double collisionProx = collisionProxFactor*EBUG_RADIUS;
                    int obstacleX = controlData[i].xCurrent;
                    int obstacleY = controlData[i].yCurrent;
                    double thisRefAngle = thisAngle - 180;
                    double conversionAngle = refAngle - thisRefAngle;
                    // detect location of each object wrt current ebug
                    readSensorInput(obstacleX, obstacleY, thisX, thisY, refAngle, &obstacleAngle, &obstacleDistance);

                    // need to convert obstacleAngle such that it is taken wrt angle of current ebug, not refAngle
                    obstacleAngle += conversionAngle;
                    correctDisplayAngle(&obstacleAngle);

                    // use information in sensorMatrix to alter ebugV and ebugW:
                    // first detect if obstacles are within proximity
                    if (obstacleDistance < collisionProx)
                    {
                        // if ebug is moving forward and obstacle is within front arc
                        if ((ebugV2 >= 0) && (fabs(obstacleAngle) >= 90))
                        {
                            obstacleDetected = 1;
                            objectAngleToCenter = absObjectAngleToCenter = 180 - fabs(obstacleAngle);

                            if (obstacleAngle < 0)
                            {
                                objectAngleToCenter = -absObjectAngleToCenter;
                            }

                            if (printDebug) qDebug("Object detected in front at %.2f deg, %.2f away", obstacleAngle, obstacleDistance);
                        }
                        // if ebug is moving backwards and obstacle is within back arc
                        else if ((ebugV2 < 0) && (fabs(obstacleAngle) <= 90))
                        {
                            obstacleDetected = 2;
                            objectAngleToCenter = absObjectAngleToCenter = fabs(obstacleAngle);

                            if (obstacleAngle > 0)
                            {
                                objectAngleToCenter = -absObjectAngleToCenter;
                            }

                            if (printDebug) qDebug("Object detected behind at %.2f deg, %.2f away", obstacleAngle, obstacleDistance);
                        }
                        // obstacle is not blocking ebug
                        else
                        {
                            //obstacleDetected = 0;
                            //qDebug("Object detected but not blocking ebug.");
                        }

                        if (obstacleDetected)
                        {
                            //qDebug("objectAngleToCenter: %.2f, absObjectAngleToCenter: %.2f", objectAngleToCenter, absObjectAngleToCenter);

                            // ebugV:
                            ebugV2 = correctedEbugV;
                            double vMultiplier1 = 0;
                            double vMultiplier2 = 0;

                            // the closer the object, the lower the ebugV.
                            vMultiplier1 = ((obstacleDistance - 2*EBUG_RADIUS)/collisionProx);
                            // Absolute stop when ebugs are touching
                            if (vMultiplier1 < 0)
                            {
                                vMultiplier1 = 0;
                                vMultiplier2 = 0;
                            }
                            // the closer the object is to the center front, the lower the ebugV
                            // MIGHT WANT TO REMOVE THIS, OR SET IT TO 1 TEMPORARILY
                            else
                            {
                                //vMultiplier2 = (absObjectAngleToCenter/90);
                                vMultiplier2 = 1;
                            }

                            correctedEbugV = ebugV2*(vMultiplier1*vMultiplier2); // in future, after optimization, merge vMultiplier1 and vMultiplier2
                            if (printDebug) qDebug("ebugV = %.2f * %.2f * %.2f = %.2f", ebugV2, vMultiplier1, vMultiplier2, correctedEbugV);

                            // ebugW:
                            static const double MAX_W_MULTIPLIER = 1.5;
                            static const double MIN_W_MULTIPLIER = 0.5;
                            double currentEbugWCorrection;

                            // first calculate required angle to turn away, the closer the object is to the center, the further away it should push
                            if (objectAngleToCenter > 0)
                            {
                                currentEbugWCorrection = 90 - objectAngleToCenter;
                            }
                            else
                            {
                                currentEbugWCorrection = -90 - objectAngleToCenter;
                            }

                            // then scale the angle by distance to the obstacle (0.5 -> 1.5? straight line decay)
                            double gradient = (MIN_W_MULTIPLIER - MAX_W_MULTIPLIER)/(EBUG_RADIUS*(collisionProxFactor - 2));
                            double constant = (2*MIN_W_MULTIPLIER - collisionProxFactor*MAX_W_MULTIPLIER)/(2 - collisionProxFactor);
                            double wMultiplier1 = ((obstacleDistance*gradient) + constant);
                            if (printDebug) qDebug("wMultiplier1 = %.2f*%.2f + %.2f = %.2f", obstacleDistance, gradient, constant, wMultiplier1);
                            double tempEbugW = currentEbugWCorrection;
                            currentEbugWCorrection *= wMultiplier1;
                            if (printDebug) qDebug("currentEbugWCorrection = %.2f * %.2f = %.2f", tempEbugW, wMultiplier1, currentEbugWCorrection);

                            if (absObjectAngleToCenter > sideThreshold)
                            {
                                qDebug("Obstacle on the side");
                                qDebug("%.2f * %.2f = %.2f", objectAngleToCenter, ebugW, objectAngleToCenter*ebugW);

                                // if ebug is turning into the direction of obstacle
                                if ((objectAngleToCenter*ebugW) < 0)
                                {
                                    qDebug("ebug running into obstacle");

                                    switch (generic)
                                    {
                                    case 0:
                                        currentEbugWCorrection *= 2;
                                        break;
                                    case 1:
                                        // remove vMultiplier by this obstacle
                                        correctedEbugV = ebugV2;
                                        qDebug("Reset correctedEbugV to %.2f", correctedEbugV);
                                        break;
                                    }
                                }
                            }
                            correctedEbugW += currentEbugWCorrection;
                        }
                    }
                    else
                    {
                        //qDebug("V: %.2f", ebugV);
                    }
                }
            }

            // ignore wController if obstacles are present
            if (obstacleDetected)
            {
                ebugV = correctedEbugV;
                ebugW = correctedEbugW;
            }

            convertVelocitiesToFreq(ebugV, ebugW, &leftWheelFreq, &rightWheelFreq, &leftWheelDirection, &rightWheelDirection);
            if (!debug) xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(leftWheelFreq,0,leftWheelDirection,0x03,true,rightWheelFreq,0,rightWheelDirection,0x03,true,true,0x00)));
            controlData[currentEbugIndex].hasStopped = 0;

            //qDebug("angle:%.2f", angleTemp);
            qDebug("V_ebug: %.2f\tW: %.2f", ebugV, ebugW);
            //qDebug("left: (%d)%d\tright: (%d)%d:", leftWheelDirection, leftWheelFreq, rightWheelDirection, rightWheelFreq);
            //qDebug("%d: %.2f - %.2f = %.2f; V = %.2f * cos(%.2f) = %.2f", currentEbugIndex, normAngleToTarget(normalError1, forwardError1), normalizedThisAngle, angleTemp, forwardV, angleTemp, ebugV);
        }
        else
        {
            if (!debug) xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(true,true,true,0x00)));
            controlData[currentEbugIndex].hasStopped = 1;
        }
    }

    return 0;

}// formationControl()

static double calculateDistanceSquared(int x1, int y1, int x2, int y2)
{
    int x = pow((x2-x1),2);
    int y = pow((y2-y1),2);
    return x + y;
} // calculateDistanceSquared()

static int convertVelocitiesToFreq(double V, double ebugW, int *leftWheelFreq, int *rightWheelFreq, int *leftWheelDirection, int *rightWheelDirection)
{
    // convert V and ebugW to stepper motor frequencies
        int baseFreq = V_TO_FREQ_CONSTANT*V;
    int deltaFreq = W_TO_FREQ_CONSTANT*ebugW;

    //qDebug("BaseFreq: %d, deltaFreq: %d", baseFreq, deltaFreq);

    // limit stepper motor frequency to prevent stalling (Freq limit programmed in firmware)
    if (baseFreq + fabs(deltaFreq) >= MAX_STEPPER_MOTOR_FREQ)
    {
        baseFreq = MAX_STEPPER_MOTOR_FREQ - fabs(deltaFreq);
        //qDebug("Freq too high, Limitting basefreq to %d", baseFreq);
    }
    else if (baseFreq - fabs(deltaFreq) <= -MAX_STEPPER_MOTOR_FREQ)
    {
        baseFreq = -MAX_STEPPER_MOTOR_FREQ + fabs(deltaFreq);
        //qDebug("Freq too low, Limitting basefreq to %d", baseFreq);
    }

    *leftWheelFreq = baseFreq - deltaFreq;
    *rightWheelFreq = baseFreq + deltaFreq;
    *leftWheelDirection = true;
    *rightWheelDirection = true;

    if (*leftWheelFreq < 0)
    {
        *leftWheelFreq = -(*leftWheelFreq);
        *leftWheelDirection = false; // backwards
    }

    if (*rightWheelFreq < 0)
    {
        *rightWheelFreq = -(*rightWheelFreq);
        *rightWheelDirection = false; // backwards
    }

    return 0;
} // convertVelocitiesToFreq()

// ensures that all angles are within -180 to 180 degrees
static int correctDisplayAngle(double *angle)
{
    while ((*angle) < -180)
    {
        (*angle) += 360;
    }

    while ((*angle) > 180)
    {
        (*angle) -= 360;
    }

    return 0;
} // correctDisplayAngle()

static int calculateErrorVector(double theta, double l, double thetaDesired, double lDesired, double *normalError, double *forwardError)
{
    *normalError = lDesired*sin(PI/180*thetaDesired) - l*sin(PI/180*theta);
    *forwardError =  lDesired*cos(PI/180*thetaDesired) - l*cos(PI/180*theta);
    //qDebug("normalError: %.2f forwardError: %.2f", *normalError, *forwardError);

    return 0;
} // calculateErrorVector()

// returns inputs from laser range finder, indicating local leader's distance and angle from this ebug
static int readSensorInput(int x1, int y1, int x2, int y2, double referenceAngle, double *theta, double *l)
{
    // x1: leader x
    // x2: follower x

    double angleToLeader = stdAngleToTarget(x1 - x2, y1 - y2); // try
    *theta = angleToLeader - referenceAngle;
    correctDisplayAngle(theta);
    *l = sqrt(calculateDistanceSquared(x2, y2, x1, y1));

    // qDebug("Theta: %.2f Distance: %.2f", *theta, *l);

    return 0;
} // readSensorInput()

static int checkFormationConstraint(int currentFormation, int currentFormationPositionIndex)
{
    switch (currentFormation)
    {
        case triangle:
        {
            double index = currentFormationPositionIndex+1;

            if (currentFormationPositionIndex == 1)
            {
                // qDebug("currentFormationPositionIndex: %d has constraint 1.", currentFormationPositionIndex);
                return 0;
            }
            if (fmod(-1.5 + sqrt(9.0/4.0 - (2*(1 - index))), 1) == 0)
            {
                // qDebug("currentFormationPositionIndex: %d has constraint 2.", currentFormationPositionIndex);
                return 1;
            }
            else if (fmod(-1.5 + sqrt(9.0/4.0 + 2*index), 1) == 0)
            {
                // qDebug("currentFormationPositionIndex: %d has constraint 3.", currentFormationPositionIndex);
                return 2;
            }
            else
            {
                // qDebug("currentFormationPositionIndex: %d has constraint 4.", currentFormationPositionIndex);
                return 3;
            }

            break;
        }
        case square:
        {
            return 4;
            break;
        }
        case circle:
        {
            return 5;
            break;
        }
        case line:
        {
            return 6;
            break;
        }
    }

    qDebug("Error in calculating constraint type.");
    return -1;
} // checkFormationConstraint()

static double leaderWController(int targetX, int targetY, int leaderX, int leaderY, double ebugAngle, double kAng)
{
    double angleToPosition = stdAngleToTarget(targetX - leaderX, targetY - leaderY);

    double angleError = angleToPosition - ebugAngle;
    correctDisplayAngle(&angleError);

    // qDebug("Leader to Dest (angleToPosition: %.2f angleError: %.2f)", angleToPosition, angleError);

    double ebugW = kAng*angleError;
    // qDebug("ebugW = %.2f*%.2f = %.2f", kAng, angleError, ebugW);

    return ebugW;
} // leaderWController()

static double followerVController(double forwardError, double kDist)
{
    double V = 0;

    if (forwardError > 0)
    {
        if (forwardError <= 1)
        {
            forwardError = 1.1;
        }

        V = kDist*log(forwardError);
    }
    else if (forwardError < 0)
    {
        if (forwardError >= -1)
        {
            forwardError = -1.1;
        }

        V = kDist*log(-forwardError);
    }

    return V;
} // followerVController()

static double followerWController(double normalError, double forwardError, double followerAngle, double deadZone, double kAng)
{
    double ebugW;
    double correctionAngle;
    double distToPosition = sqrt(pow(normalError,2) + pow(forwardError, 2));

    double angleToPosition = normAngleToTarget(normalError, forwardError);
    double positionAngleError = angleToPosition - followerAngle;
    correctDisplayAngle(&positionAngleError);

    double formationAngleError = 180 - followerAngle;
    correctDisplayAngle(&formationAngleError);


    if (distToPosition > deadZone)
    {
            correctionAngle = positionAngleError;
    }
    else
    {
        correctionAngle = formationAngleError - (formationAngleError - positionAngleError)*(distToPosition/deadZone);
    }

    ebugW = kAng*correctionAngle;
    // qDebug("ebugW = %.2f*%.2f = %.2f", kAng, correctionAngle, ebugW);

    return ebugW;
} // followerWController()

// Can only be used for finding angle in ebug GUI XY coordinates, measuring angle of target wrt current position of interest
// note diffX/Y is given by targetX/Y - currentX/Y
static double stdAngleToTarget(double diffX, double diffY)
{
    double angleDesired = -180/PI*atan2(diffY, diffX);
    correctDisplayAngle(&angleDesired);

    return angleDesired;
} // stdAngleToTarget()

// Can only be used for finding angle in normalized coordinates (ie after considering refAngle)
static double normAngleToTarget(double normalError, double forwardError)
{
    double angleDesired = (180/PI*atan2(normalError, forwardError)) + 180;
    correctDisplayAngle(&angleDesired);

    return angleDesired;
} // normAngleToTarget()
