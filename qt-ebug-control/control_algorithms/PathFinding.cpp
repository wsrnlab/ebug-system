#include "PathFinding.h"
#include "math.h"
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

//static quint8 convertCommandToSpeedStage2(double,int,int);
//static quint8 convertCommandToSpeedStage3(double,int,int);
static int getNumberOfCollisions(bool *, int);
static double roundNumber(double r);
static int angleFreq(double angleErr);
static quint8 angleStepMode(double angleErr);
static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs);
static int getNumberOfCollisions(bool *row, int nNodes);
static int minVal(int value1, int value2);
static int maxVal(int value1, int value2);
static int iMinVal(int value1, int value2);
static int iMaxVal(int value1, int value2);
static int normalizeAngle(int angle, int scope);
static int rectifiedXCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug);
static int rectifiedYCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug);
static int quadrantLocation(int xThisEbug, int yThisEbug, int xOtherEbug, int yOtherEbug);

int firstRun = 1; //used to run Stage 0
int stage1counter = 0; //used to run through location array in stage 1

using namespace std;

// Class definition
class coordinates{
        public:
        int x;
        int y;
        void setvalues (int,int);
        coordinates ();
        friend bool operator == (coordinates &coord1, coordinates &coord2);
};

void coordinates::setvalues (int a, int b){
        x = a;
        y = b;
}

bool operator== (coordinates &coord1, coordinates &coord2){
        return(coord1.x == coord2.x && coord1.y == coord2.y);
}

coordinates::coordinates(){

}

//Function Prototypes
coordinates testPath(coordinates current, coordinates desired);
void get_obstacles();
//Global Variable
coordinates pointsArray[20]; //stores all the points to be reached. This is what the rest of the program references.

coordinates *obstacleArray; //stroes all obstacles allocated size to be set in get_obstacles()
int finalDest;

int pathFinding(XBee *xBee, struct XBeeNodeTable xBeeNodeTable,
                    struct EBugData* eBugData, int eBugDataIndex, int currentIndex, int nNodes, cv::Rect currentROI,
                    struct ControlDataPathFinding* controlData,
                    double* parameters, bool &resetControlFlag)
{
    ////////////////////////////
    // FUNCTION RETURN VALUES //
    ////////////////////////////
    // 0: NO CONTROL ACTION
    // 1: CONTROL ACTION: Stop eBug
    // 2: CONTROL ACTION: Move eBug

    ////////////////
    // PARAMETERS //
    ////////////////
    // parameters[0]:   PID Controller [angle] : Kp
    // parameters[1]:   PID Controller [angle] : Ki
    // parameters[2]:   PID Controller [angle] : Kd
    // parameters[3]:   PID Controller [angle] : Error threshold
    // parameters[4].   PID Controller [angle] : Step time (ms)
    // parameters[5]:   Deadzone (pixels)
    // parameters[6]:   STAGE 2 [Set Trajectory] : Angle error termination threshold (degrees)
    // parameters[7]:   STAGE 2 [Set Trajectory] : Angle error termination count
    // parameters[8]:   STAGE 2 [Set Trajectory] : Stepper motor MIN speed
    // parameters[9]:   STAGE 2 [Set Trajectory] : Stepper motor MAX speed
    // parameters[10]:  STAGE 3 [Move eBug] : Collision avoidance (ON=1,OFF=0)
    // parameters[11]:  STAGE 3 [Move eBug] : Collision avoidance threshold
    // parameters[12]:  STAGE 3 [Move eBug] : Reset trajectory threshold (degrees)
    // parameters[13]:  STAGE 3 [Move eBug] : Desired position threshold
    // parameters[14]:  STAGE 3 [Move eBug] : Get new random position threshold
    // parameters[15]:  STAGE 3 [Move eBug] : New random projection projection (pixels)
    // parameters[16]:  STAGE 3 [Move eBug] : Stepper motor MIN speed
    // parameters[17]:  STAGE 3 [Move eBug] : Stepper motor MAX speed
    // parameters[18]:  STAGE 3 [Move eBug] : Stepper motor BASE speed [recommended value: maxSpeed+((minSpeed-maxSpeed)/2)]
    // parameters[19]:  RGB LEDs (ON=1,OFF=0)

    ///////////////////////////
    // INTERNAL CONTROL DATA //
    ///////////////////////////
    static bool collisionMatrix[MAX_NEBUGTRACK][MAX_NEBUGTRACK]={{false}};
    static int collisionVector[MAX_NEBUGTRACK]={0};
    static bool stopVector[MAX_NEBUGTRACK]={false};
    static bool newRandPos[MAX_NEBUGTRACK]={false};

    // Create PID controller(s)
    // (one controller for each eBug)
    static class PIDController anglePID[MAX_NEBUGTRACK];
    static class PIDController speedPID[MAX_NEBUGTRACK];

    //coordinates pointsArray[20]; //stores all the points to be reached. This is what the rest of the program references.
    ///////////
    // RESET //
    ///////////

    int positionNumber;

    if(resetControlFlag)
    {
        // Reset internal control data to initial values
        for(int i=0;i<MAX_NEBUGTRACK;i++)
        {
            collisionVector[i]=0;
            stopVector[i]=false;
            newRandPos[i]=false;
            for(int j=0;j<MAX_NEBUGTRACK;j++)
                collisionMatrix[i][j]=false;
        }
        controlData[currentIndex].stage=0;
        resetControlFlag=false;
    }

    // Save current data
    controlData[currentIndex].xCurrent=eBugData[eBugDataIndex].x;
    controlData[currentIndex].yCurrent=eBugData[eBugDataIndex].y;
    controlData[currentIndex].angleCurrent=eBugData[eBugDataIndex].angle;

    ////////////////////////////////////////////////////////////////////////
    // STAGE 0: Handling the maze and determining locations to reach      //
    ////////////////////////////////////////////////////////////////////////

    if(controlData[currentIndex].stage==0 && firstRun)
    {
        finalDest=0;
        positionNumber=0;
        firstRun = 0;
        int fileOpen = 1; //flag to get stage 0 to run
        int Xval = 0, Yval = 0; // Final X & Y positions
        int currentPosition[2]; //currentPosition[0] = X, currentPosition[1] = Y;
        int rawPosInt[10];
        currentPosition[0] = 1; currentPosition[1] = 1; //Robot Always starts at (1,1)

        struct grid {
            int myLocus[8];
            int myAllele[8];
            int switchpoint[2];
        }desiredGrid;

        if(fileOpen){

        // Open file (.txt) that was output from Waleed's Algorithm

            //QString cDir = QDir::currentPath();
            //std::cout << cDir << std::endl;
            qDebug() << "App path: " << qApp->applicationDirPath();
            QFile solutionFile ("/home/nick/svn/ebug_system/trunk/src/ebug-control/control_algorithms/pathfind/solution.txt");
            if(solutionFile.open(QIODevice::ReadOnly) && fileOpen){
                fileOpen = 0; //flag so we do not repeatedly complete STAGE 0
                int done = 0;
                //const char *cstring;
            // Store integers from Waleed's algorithm in an array
                QTextStream in(&solutionFile);
                while (!in.atEnd() && !done){
                    qDebug("TEST!!!!!!!!!!!!!!!!!!!!!");
                    QString line = in.readLine();
                    //convert to regular old string
                    //line.toStdString();
                    QByteArray byteArray = line.toUtf8();
                    const char* cString = byteArray.constData();

                    //cout << "File Contents:" << line << endl;
                    qDebug("File Content: %s", cString);
                    int c = 0;
                    char value;
                    for(int x=0; x<10; x++){
                        value = cString[x*c];
                        rawPosInt[x] = atoi(&value);
                        c = 10;
                        //cout << rawPosInt[x] << endl;
                    } //for
                    done = 1;
                } //while
                //solutionFile.close(); // close file after data is extracted
            } //if
            else{
                qDebug("ERROR: File Not Opened");
                return 0;
            }

            //Convert Raw data from files into coordinates (Alleles and Loci).
            for(int x=0; x<8; x++){
                desiredGrid.myAllele[x]= rawPosInt[x];
                //cout << "myAllele = " << desiredGrid.myAllele[x] << endl; //DEBUGGING
                desiredGrid.myLocus[x] = x+1;
            } //for
            desiredGrid.switchpoint[0] = rawPosInt[8];
            desiredGrid.switchpoint[1] = rawPosInt[9];
            //cout << "File Content Extracted." << endl;

        // Incorporate Swtich points and get coordinates
            int switchcase = 0;

            coordinates currentPos;
            coordinates desiredPos;
            coordinates turningPoint;


        // DEFINE KNOWN OBSTACLES!
            //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            //coordinates *obstacleArray = get_obstacles(); //define array of coordinates of obstacles
            get_obstacles();

            qDebug("TESTING OBSTACLE ARRAY!!!!!! first obstacle is (%d,%d)",obstacleArray[0].x,obstacleArray[0].y);

            currentPos.setvalues (1,1); // Starting point for Robot Path will always be (1,1) with this model.
            //cout << "Start Position: (" << currentPos.x << "," <<currentPos.y << ")" << endl;
           for(int x=0; x<8; x++){
                //Determine if the Locus in question is a switch point
                if(desiredGrid.myLocus[x] == desiredGrid.switchpoint[0] || desiredGrid.myLocus[x] == desiredGrid.switchpoint[1]){
                    // Once switch case is triggered it must remain switch until anothe switch point it reached
                    if(switchcase == 1) switchcase = 0;
                    else switchcase = 1;
                } //if
                if (switchcase){ //if the Locus is a switch poing X = Allele & Y = Locus
                    Xval = desiredGrid.myAllele[x];
                    Yval = desiredGrid.myLocus[x];
                } //if
                else{ // if the Locus is not a switch point X = Locus & Y = Allele
                    Yval = desiredGrid.myAllele[x];
                    Xval = desiredGrid.myLocus[x];
                } //else

                // NB: Current Position will be previous values

                desiredPos.setvalues (Xval, Yval); // Set desired position based on file input

            // TEST PATH FROM CURRENT TO DESIRED.

                if(currentPos == desiredPos); // Do this part ONLY if current position != desired position
                else{
                    // Use testPath function to get the turning point required for robot propogation...
                    turningPoint = testPath(currentPos, desiredPos);
                    currentPos.setvalues(desiredPos.x, desiredPos.y);
                    if(turningPoint == desiredPos){
                            //cout << "Next Point: (" << desiredPos.x << "," << desiredPos.y << ")" << endl;
                            pointsArray[positionNumber].setvalues(desiredPos.x,desiredPos.y); // put points in final array.
                            qDebug("pointsArray value saved: (%d,%d)",pointsArray[positionNumber].x,pointsArray[positionNumber].y);
                            positionNumber++;
                    }
                    else{
                            // Store Values in Array (Dynamic Array?)
                            //cout << "Next Point: (" << turningPoint.x << "," << turningPoint.y << ")" << endl;
                            pointsArray[positionNumber].setvalues(turningPoint.x,turningPoint.y); // put points in final array.
                            qDebug("pointsArray value saved: (%d,%d)",pointsArray[positionNumber].x,pointsArray[positionNumber].y);
                            positionNumber++;
                            //cout << "Next Point: (" << desiredPos.x << "," << desiredPos.y << ")" << endl;
                            pointsArray[positionNumber].setvalues(desiredPos.x,desiredPos.y); // put points in final array.
                            qDebug("pointsArray value saved: (%d,%d)",pointsArray[positionNumber].x,pointsArray[positionNumber].y);
                            positionNumber++;

                    }//else
                }//else

            } //for
            //cout << "Key Points Calculated" << endl;

            // Pass Pixel locations to Next STAGE!
        qDebug("position number final %d\n",positionNumber);
         } //if
         else{ // else if file not open!
           qDebug("ERROR: File could not be opened.");
           return 0;
         } //else

        fileOpen = 0;
    } //if(to run stage 0)
    // AFTER STAGE 0...
    // need to pass this... pointsArray[i] instead of random position.
    // Once point is reached, increment "i" and pass next point to the random position.
    // Until all positions are reached. Number of positions to be reached is positionNumber

    ////////////////////////////////////////////////////////////////////////
    // STAGE 1: Initialization and calculation of random desired position //
    ////////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage==0)
    {
        // Initialize PID controller
        anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
        // Desired position range: [deadzone,deadzone]->[currentROI.width-deadzone,currentROI.height-deadzone]
        int xMin=(int)parameters[5];
        int yMin=(int)parameters[5];
        int xMax=currentROI.width-xMin;
        int yMax=currentROI.height-yMin;
        qDebug("xMin:%d yMin:%d xMax:%d yMax:%d", xMin, yMin, xMax, yMax);

        // CALCULATE CONVERSION RATE FOR GRID 2 PIXELS
        int one16thX = xMax/16;
        int one16thY = yMax/16;
        int one8thX = xMax/8;
        int one8thY = yMax/8;

        // Save desired position
        // Print Out Some Stuff for Debug
        qDebug("Stage1Counter= %d",stage1counter);

        if(pointsArray[stage1counter].x)
            controlData[currentIndex].xDesired = (int)(pointsArray[stage1counter].x-1)*(int)one8thX + (int)one16thX;
        controlData[currentIndex].yDesired = (int)(pointsArray[stage1counter].y-1)*(int)one8thY + (int)one16thY;
        qDebug("Going to Grid Location: (%d,%d)",pointsArray[stage1counter].x,pointsArray[stage1counter].y);
        qDebug("Going to Pixel Location: (%d,%d)",controlData[currentIndex].xDesired,controlData[currentIndex].yDesired);

        // Increment stage1counter

        if((pointsArray[stage1counter].x==0) && (pointsArray[stage1counter].y==0)){
            finalDest=1;
            qDebug("Last Point Reached\n");
        }
        stage1counter++;
        // Increment stage index
        controlData[currentIndex].stage++;
    }

    ////////////////////
    // PID CONTROLLER //
    ////////////////////
    // Calculate desired angle
    double angleDesired=(180/PI)*atan2(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired,controlData[currentIndex].xDesired-eBugData[eBugDataIndex].x);
    // Calculate angle error
    double angleErr=angleDesired-eBugData[eBugDataIndex].angle;
    // Adjust angle error (ensures appropriate rotation direction for angle errors above 180 degrees)
    if(fabs(angleErr)>180)
    {
        if(angleErr<0)
            angleErr=360-fabs(angleErr);
        else if(angleErr>0)
            angleErr=-(360-fabs(angleErr));
    }
    // Calculate PID output (command/control action)
    double command=anglePID[currentIndex].Update(angleErr);
    // qDebug("Command: %.2f, eBugDataIndex: %d, currentIndex: %d", command, eBugDataIndex, currentIndex);

    ///////////////////////////////////////////////////////////////////////
    // STAGE 2: Set eBug trajectory (no motion in the x or y directions) //
    ///////////////////////////////////////////////////////////////////////
    if(controlData[currentIndex].stage==1)
    {
        if(newRandPos[currentIndex])
            controlData[currentIndex].stage++;
        else
        {
            // Stop if at correct trajectory
            // fabs(): returns absolute double value of angleErr (i.e. removes sign)
            if(fabs(angleErr)<parameters[6])
            {
                // Increment counter
                controlData[currentIndex].angleErrTermCount++;
                // Stop stepper motors
                if(controlData[currentIndex].angleErrTermCount==(int)parameters[7])
                {
                    // Reset counter
                    controlData[currentIndex].angleErrTermCount=0;
                    // Increment stage index
                    controlData[currentIndex].stage++;
                    // Reset PID controller
                    anglePID[currentIndex].Initialize(parameters[0],parameters[1],parameters[2],parameters[3],parameters[4]);
                    // Stop eBug
                    for (int n=0;n>5;n++)
                    {
                        xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                    }
                    // Turn ON RED LEDs
                    if((bool)parameters[19])
                        //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                    return 1;
                }
            }
            // Perform control
            else
            {
                // Turn clockwise
                if(command<0)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                    return 2;
                }
                // Turn counter-clockwise
                else if(command>0)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorCCWStep(angleFreq(angleErr),0,true,angleStepMode(angleErr),true,true,0x00)));
                    return 2;
                }
            }
        }
    }

    /////////////////////////////////////////////////
    // STAGE 3: Move eBug towards desired position //
    /////////////////////////////////////////////////
    else if(controlData[currentIndex].stage==2)
    {
        // Collision avoidance
        if((bool)parameters[10])
        {
            qDebug("Collision Avoidance is TRUE");
            // Reset sum
            controlData[currentIndex].collisionDistanceSquaredSumCurrent=0;
            // Initialize stopVector
            for(int i=0;i<nNodes;i++)
                stopVector[i]=false;
            // Get data
            for(int j=0;j<nNodes;j++)
            {
                // Store current distance squared
                controlData[currentIndex].distanceSquaredCurrent=(abs(eBugData[eBugDataIndex].x-eBugData[j].x)*abs(eBugData[eBugDataIndex].x-eBugData[j].x))+(abs(eBugData[eBugDataIndex].y-eBugData[j].y)*abs(eBugData[eBugDataIndex].y-eBugData[j].y));
                // Check current eBug position with all other eBug positions
                if(j!=eBugDataIndex)
                {
                    // If current eBug is too close to any other eBug:
                    qDebug("currentIndex.distSquared=%d",controlData[currentIndex].distanceSquaredCurrent);
                    qDebug("collisionAvoidanceThreshold=%d",(int)parameters[11]*(int)parameters[11]);
                    if((controlData[currentIndex].distanceSquaredCurrent<(((int)parameters[11])*((int)parameters[11]))))
                    {
                        // Add distance square to sum
                        controlData[currentIndex].collisionDistanceSquaredSumCurrent+=controlData[currentIndex].distanceSquaredCurrent;
                        // Set collision matrix elements
                        collisionMatrix[eBugDataIndex][j]=true;
                        collisionMatrix[j][eBugDataIndex]=true;
                        // Stop other eBug j involved in collision only if current eBug is NOT stopped
                        // (this prevents all eBugs involved in a collision stopping)
                        if(!stopVector[eBugDataIndex])
                            stopVector[j]=true;
                    }
                    // Set collision matrix elements
                    else
                    {
                        collisionMatrix[eBugDataIndex][j]=false;
                        collisionMatrix[j][eBugDataIndex]=false;
                    }
                }
                // Set collision matrix elements
                else
                   collisionMatrix[currentIndex][currentIndex]=false;
            }

            // Calculate collisionDistanceSquaredSumDifference
            controlData[currentIndex].collisionDistanceSquaredSumDifference=controlData[currentIndex].collisionDistanceSquaredSumCurrent-controlData[currentIndex].collisionDistanceSquaredSumPrev;
            // Store temporary copy of collisionDistanceSquaredSumPrev
            int collisionDistanceSquaredSumPrevTemp=controlData[currentIndex].collisionDistanceSquaredSumPrev;
            // Store current distance squared as previous
            controlData[currentIndex].collisionDistanceSquaredSumPrev=controlData[currentIndex].collisionDistanceSquaredSumCurrent;
            // Update collision vector
            collisionVector[currentIndex]=getNumberOfCollisions(collisionMatrix[currentIndex],nNodes);

            // Validate new random position
            if(newRandPos[currentIndex])
            {
                // Reset sum
                int collisionDistanceSquaredSumCurrentLocal=0;
                // Get data
                for(int j=0;j<nNodes;j++)
                {
                    // Store current distance squared
                    int distanceSquaredCurrentLocal=((abs((eBugData[eBugDataIndex].x+(int)roundNumber((int)parameters[15]*cos(angleDesired*(PI/180))))-eBugData[j].x))*(abs((eBugData[eBugDataIndex].x+(int)roundNumber((int)parameters[15]*cos(angleDesired*(PI/180))))-eBugData[j].x)))+
                                                        ((abs((eBugData[eBugDataIndex].y-(int)roundNumber((int)parameters[15]*sin(angleDesired*(PI/180))))-eBugData[j].y))*(abs((eBugData[eBugDataIndex].y-(int)roundNumber((int)parameters[15]*sin(angleDesired*(PI/180))))-eBugData[j].y)));
                    // Check current eBug position with all other eBug positions
                    if(j!=currentIndex)
                    {
                        // If current eBug is too close to any other eBug:
                        if((distanceSquaredCurrentLocal<(((int)parameters[11])*((int)parameters[11]))))
                        {
                            // Add distance square to sum
                            collisionDistanceSquaredSumCurrentLocal+=distanceSquaredCurrentLocal;
                        }
                    }
                }
                // Set trajectory for new random position
                if(collisionDistanceSquaredSumCurrentLocal==0)
                {
                    // Set flag
                    newRandPos[currentIndex]=false;
                    // Set index
                    controlData[currentIndex].stage=1;
                    return 0;
                }
                // Get another new random position
                else
                {
                    // Get new random position
                    newRandPos[currentIndex]=true;
                    // Set index
                    controlData[currentIndex].stage=0;
                    return 0;
                }
            }

            // In collision and set to STOPPED: stop eBug
            qDebug("stopVector[%d]=%d",currentIndex,stopVector[currentIndex]);
            qDebug("collisionVector[%d]=%d",currentIndex,collisionVector[currentIndex]);
            if((stopVector[currentIndex])&&(collisionVector[currentIndex]>0))
            {
                // Stop eBug
                qDebug("stopVector[%d] && collVector[%d]>0",currentIndex,currentIndex);
                qDebug("Stage 3: (Collision Avoidance) Stop eBug");
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                // Turn ON RED LEDs
                if((bool)parameters[19])
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                return 1;
            }
            // Check if eBug has MOVED INTO (detect edge) OR is MOVING FURTHER into a collision, if so, get new random position
            // MOVING FURTHER INTO COLLISION: collisionDistanceSquaredSumDifference=NEGATIVE
            // MOVING OUT OF COLLISION: collisionDistanceSquaredSumDifference=POSITIVE
            else if((collisionVector[currentIndex]>0)&&(((collisionDistanceSquaredSumPrevTemp==0)&&(controlData[currentIndex].collisionDistanceSquaredSumCurrent!=0))
                    ||((controlData[currentIndex].collisionDistanceSquaredSumDifference)<parameters[14])))
            {
                // Get new random position
                newRandPos[currentIndex]=true;
                // Set index
                controlData[currentIndex].stage=0;
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
                // Turn ON RED LEDs
                if((bool)parameters[19])
                    //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                return 1;
            }
            // Allow current eBug to move (no longer in collision)
            else
               stopVector[currentIndex]=false;

            // Store current distance squared as previous
            controlData[currentIndex].collisionDistanceSquaredSumPrev=controlData[currentIndex].collisionDistanceSquaredSumCurrent;
            // Reset flag
            newRandPos[currentIndex]=false;
        }

        // Desired position reached
        if((abs(eBugData[eBugDataIndex].x-controlData[currentIndex].xDesired)<parameters[13])&&(abs(eBugData[eBugDataIndex].y-controlData[currentIndex].yDesired)<parameters[13]))
        {
            // Reset index
            controlData[currentIndex].stage=0;
            // Turn ON RED LEDs
            if((bool)parameters[19])
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
            return 1;
        }
        // Reset trajectory if absolute angle error is above a certain threshold
        else if(fabs(angleErr)>parameters[12])
        {
            // Set index
            controlData[currentIndex].stage=1;
            // Turn ON RED LEDs
            if((bool)parameters[19])
                //xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,TLC5947_SetAllRed(0xFF,0xFF)));
                // Stop eBug
                for (int n=0;n>5;n++)
                {
                    xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorStopBoth(false,false,true,0x00)));
                }
            return 1;
        }
        // Perform control
        else
        {
            QVector<int> distanceSquaredToOtherEbugs;
            int minDistanceSquaredToOtherEbugs;
            distanceSquaredToOtherEbugs.clear();
            for (int j=0;j<nNodes;j++)
            {
                if(j!=eBugDataIndex)
                {
                    // Store current distance squared
                    distanceSquaredToOtherEbugs.append((int)((abs(eBugData[eBugDataIndex].x-eBugData[j].x))*(abs(eBugData[eBugDataIndex].x-eBugData[j].x)))+
                                                        (int)((abs(eBugData[eBugDataIndex].y-eBugData[j].y))*(abs(eBugData[eBugDataIndex].y-eBugData[j].y))));
                    qDebug("eBug[%d] to [%d] = %d", eBugDataIndex, j, distanceSquaredToOtherEbugs.last());
                }
            }

            minDistanceSquaredToOtherEbugs = 1000000;
            qDebug("initial min dist = %d", minDistanceSquaredToOtherEbugs);
            for (int k=0;k<nNodes-1;k++)
            {
                if(distanceSquaredToOtherEbugs.at(k)<minDistanceSquaredToOtherEbugs)
                {
                    qDebug("%d < %d",distanceSquaredToOtherEbugs.at(k),minDistanceSquaredToOtherEbugs );
                    minDistanceSquaredToOtherEbugs = distanceSquaredToOtherEbugs.at(k);
                }
                qDebug("minDistance of Ebug[%d]=%d",eBugDataIndex,minDistanceSquaredToOtherEbugs);
            }

            qDebug("xCurrent=%d yCurrent=%d", controlData[currentIndex].xCurrent, controlData[currentIndex].yCurrent);
            qDebug("xDesired=%d yDesired=%d", controlData[currentIndex].xDesired, controlData[currentIndex].yDesired);
            int xDifference = abs(controlData[currentIndex].xCurrent - controlData[currentIndex].xDesired);
            int xSquared = xDifference*xDifference;
            int yDifference = abs(controlData[currentIndex].yCurrent - controlData[currentIndex].yDesired);
            int ySquared = yDifference*yDifference;
            controlData[currentIndex].distanceSquaredToDesired = (xSquared+ySquared);
            qDebug("ssd=%d",controlData[currentIndex].distanceSquaredToDesired);
            // Steer right
            if(command<0)
            {
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs),0,true,0x08,true,
                                                                                                                round(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs)*0.9),0,true,0x08,true,true,0x00)));
                return 2;
            }
            // Steer left
            else if(command>0)
            {
                xBee->addPacket(xBee->makePacket_eBugPacketToXBeePacket(xBeeNodeTable,StepperMotorLeftRightStep(round(speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs)*0.9),0,true,0x08,true,
                                                                                                                speedFreq(controlData[currentIndex].distanceSquaredToDesired,minDistanceSquaredToOtherEbugs),0,true,0x08,true,true,0x00)));
                return 2;
            }
        }
    }
    qDebug("Checking FinalDest Var\n");
    if(finalDest) return 99;
    return 0;
} // pathFinding()

static int getNumberOfCollisions(bool *row, int nNodes)
{
    // Local variables
    int count=0;
    // Read row vector
    for(int i=0;i<nNodes;i++)
    {
        if(row[i])
            count++;
    }
    return count;
} // getNumberOfCollisions()

static double roundNumber(double r)
{
    // This function rounds a double to a whole number (also double)
    // e.g: roundNumber(4.5)=5.0, roundNumber(-4.5)=-5.0
    return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5);
} // roundNumber()

static int angleFreq(double angleErr)
{
    int freqVal;

    if(fabs(angleErr)>100)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>60)
    {
        freqVal=250;
    }

    else if(fabs(angleErr)>30)
    {
        freqVal=200;
    }

    else if(fabs(angleErr)>10)
    {
        freqVal=100;
    }

    else
        freqVal=100;

    return freqVal;
} // angleFreq()

static quint8 angleStepMode(double angleErr)
{
    quint8 stepModeVal;

    if(fabs(angleErr)>100)
    {
        stepModeVal=0x00;
    }

    else if(fabs(angleErr)>60)
    {
        stepModeVal=0x01;
    }

    else if(fabs(angleErr)>10)
    {
        stepModeVal=0x02;
    }

    else if(fabs(angleErr)>5)
    {
        stepModeVal=0x03;
    }

    else
        stepModeVal=0x08;

    return stepModeVal;
} // angleStepMode()

static int speedFreq(int sumOfSquaredDistance, int sumOfSquaredDistanceOtherEbugs)
{
    int speedFreqVal;
    int minSquaredDistance;

    if (sumOfSquaredDistanceOtherEbugs/2 < sumOfSquaredDistance)
    {
        minSquaredDistance = sumOfSquaredDistanceOtherEbugs*0.5;
    }
    else
        minSquaredDistance = sumOfSquaredDistance;

    if(fabs(minSquaredDistance)>20000)
    {
        speedFreqVal=6000;
    }

    else if(fabs(minSquaredDistance)>10000)
    {
        speedFreqVal=5000;
    }

    else if(fabs(minSquaredDistance)>2000)
    {
        speedFreqVal=3000;
    }

    else if(fabs(minSquaredDistance)>500)
    {
        speedFreqVal=2000;
    }

    else
        speedFreqVal=1000;
    qDebug("min SSD to Goal=%d",sumOfSquaredDistance);
    qDebug("min SSD to other eBugs=%d",sumOfSquaredDistanceOtherEbugs);
    qDebug("minSquaredDistance=%d",minSquaredDistance);
    qDebug("speedFreqVal=%d",speedFreqVal);
    return speedFreqVal;
} // speedFreqVal()

static int normalizeAngle(int angle, int scope)
{
    int normalizedAngleVal;
    int tempAngleVal;
    tempAngleVal=angle+scope;
    qDebug("tempAngleVal=%d + %d = %d",angle, scope, tempAngleVal);

    if(tempAngleVal>=360)
    {
        normalizedAngleVal=tempAngleVal-360;
    }
    else if(tempAngleVal<0)
    {
        normalizedAngleVal=tempAngleVal+360;
    }
    else
        normalizedAngleVal=tempAngleVal;
    qDebug("normalizedAngleVal=%d",normalizedAngleVal);
    return normalizedAngleVal;
} // normalizeAngle()

static int maxVal(int value1, int value2)
{
    if(value1>value2)
        return value1;
    else
        return value2;
} // maxVal()

static int iMaxVal(int value1, int value2)
{
    int index;
    if(value1>value2)
        index=1;
    else index=2;
        return index;
} // iMaxVal()

static int minVal(int value1, int value2)
{
    if(value1<value2)
        return value1;
    else
        return value2;
} // minVal()

static int iMinVal(int value1, int value2)
{
    int index;
    if(value1<value2)
        index=1;
    else index=2;
        return index;
} // iMinVal()

static int rectifiedXCoord(int quadNode, int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug)
{
    double angleTemp;
    double angleDiff;
    double newAngle;
    double newXCoord;

    if (angle<0)
        angle=angle+360;

    if (quadNode==11||quadNode==12)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=angleDiff;
    }
    else if (quadNode==21||quadNode==22)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180-angleDiff;
    }
    else if (quadNode==31||quadNode==32)
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180+angleDiff;
    }
    else
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=360-angleDiff;
    }
    qDebug("angleDiff between eBugs=%f",angleDiff);

    if (quadNode/10==4&&quadGP/10==1 || quadNode/10==1&&quadGP/10==4)
    {
        if(angle>angleTemp)
        {
            angleThreshold=-angleThreshold;
            qDebug("in quad 4,1 angle>angleTemp");
        }
    }
    else if (angle<angleTemp)
    {
        angleThreshold=-angleThreshold;

        qDebug("not in quad 4,1 angle<angleTemp");
    }

    qDebug("angleThresh=%d",angleThreshold);

    //newAngle=angleThreshold+angleTemp;
    newAngle=angle+angleThreshold;
    qDebug("raw newAngle=%f",newAngle);

    do {newAngle=newAngle-360;} while (newAngle>=360);
    if (newAngle>180) {newAngle=newAngle-360;}
    else if (newAngle<-180) {newAngle=newAngle+360;}
    qDebug("rectified newAngle=%f",newAngle);

    if (newAngle>=-90 && newAngle<90)
    {
        newXCoord=xThisEbug+distanceThreshold*cos(fabs(newAngle*PI/180));
    }
    else
        newXCoord=xThisEbug-distanceThreshold*cos((180-fabs(newAngle))*PI/180);
    qDebug("xCurrent=%d, newxCoord=%f",xThisEbug,newXCoord);
    qDebug("currentAngle=%d, newAngle=%f",angle,newAngle);
    return round(newXCoord);
} //rectifiedXCoord()

static int rectifiedYCoord(int quadNode,int quadGP, int angle, int angleThreshold, int distanceThreshold,
                           int yThisEbug, int yOtherEbug, int xThisEbug, int xOtherEbug)
{
    double angleTemp;
    double angleDiff;
    double newAngle;
    double newYCoord;

    if (angle<0)
        angle=angle+360;

    if (quadNode==11||quadNode==12)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=angleDiff;
    }
    else if (quadNode==21||quadNode==22)
    {
        angleDiff = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180-angleDiff;
    }
    else if (quadNode==31||quadNode==32)
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        angleTemp=180+angleDiff;
    }
    else
    {
        angleDiff = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xOtherEbug-(double)xThisEbug));
        angleTemp=360-angleDiff;
    }

    if (quadNode/10==4&&quadGP/10==1 || quadNode/10==1&&quadGP/10==4)
    {
        if(angle>angleTemp)
        {
            angleThreshold=-angleThreshold;
            qDebug("in quad 4,1 angle>angleTemp");
        }
    }
    else if (angle<angleTemp)
    {
        angleThreshold=-angleThreshold;

        qDebug("not in quad 4,1 angle<angleTemp");
    }

    //newAngle=angleThreshold+angleTemp;
    newAngle=angle+angleThreshold;

    do {newAngle=newAngle-360;} while (newAngle>=360);
    if (newAngle>180) {newAngle=newAngle-360;}
    else if (newAngle<-180) {newAngle=newAngle+360;}

    if (newAngle>=0 && newAngle<180)
    {
        newYCoord=yThisEbug-distanceThreshold*sin(fabs(newAngle*PI/180));
    }
    else
        newYCoord=yThisEbug+distanceThreshold*sin((180-fabs(newAngle))*PI/180);
    qDebug("yCurrent=%d, newyCoord=%f",yThisEbug,newYCoord);
    qDebug("currentAngle=%d, newAngle=%f",angle,newAngle);
    return round(newYCoord);
} //rectifiedYCoord()

static int quadrantLocation(int xThisEbug, int yThisEbug, int xOtherEbug, int yOtherEbug)
{
    double angle;
    if(xThisEbug<=xOtherEbug && yThisEbug>=yOtherEbug)
    {
        qDebug("Part 1");
        angle = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xOtherEbug-(double)xThisEbug));
        if(angle>45)
        {
            qDebug("Returning 12");
            return 12;
        }
        else
        {
            qDebug("Returning 11");
            return 11;
        }
    }
    else if(xThisEbug>=xOtherEbug && yThisEbug>=yOtherEbug)
    {
        qDebug("Part 2");
        angle = (180/PI)*atan(((double)yThisEbug-(double)yOtherEbug)/((double)xThisEbug-(double)xOtherEbug));
        if(angle>45)
        {
            qDebug("Returning 21");
            return 21;
        }
        else
        {
            qDebug("Returning 22");
            return 22;
        }
    }
    else if(xThisEbug>=xOtherEbug && yThisEbug<=yOtherEbug)
    {
        qDebug("Part 3");
        angle = (180/PI)*atan(((double)yOtherEbug-(double)yThisEbug)/((double)xThisEbug-(double)xOtherEbug));
        if(angle>45)
        {
            qDebug("Returning 32");
            return 32;
        }
        else
        {
            qDebug("Returning 31");
            return 31;
        }
    }
    else
    {
        qDebug("Part 4");
        angle = (180/PI)*atan(((double)xOtherEbug-(double)xThisEbug)/((double)yOtherEbug-(double)yThisEbug));
        if(angle>45)
        {
            qDebug("Returning 42");
            return 42;
        }
        else
        {
            qDebug("Returning 41");
            return 41;
        }
    }
} //quadrantLocation()

/**********************************************************************************
                                FUNCTIONS - SAMIR GHANEM
***********************************************************************************/


coordinates testPath(coordinates current, coordinates desired){

/*
testPath function takes the current position, desired position and obstacles.
It calculates a right angle, single turn path to get from desired position to current position
First, it trys to move horizontally first then verticaly.
If this right angle path encounters an obstacle, algorithm trys to move vertically first
then horizontally.
On finding a path it returns the turning point*.

First Path Tested:			Second Path Tested:
----------------------------		---------------------------
CURRENT-------------->Turning		CURRENT
                        |		   |
                        |		   |
                        |		   |
                        |		   |
                        V		   V
                      DESIRED		Turning-------------->DESIRED


*If neither path return the desired position. (Let the robot move in a direct path)

*/
        coordinates turningPoint;
        coordinates testPoint;
        int path1fail = 0, path2fail = 0;
        //qDebug("TEST testPath obstacel[1] = (%d,%d)\n",obstacleArray[1].x,obstacleArray[1].y);

/**************************** TEST PATH 1 **************************************/

        // While loop to Check Path 1
        testPoint.setvalues(current.x,current.y);
        while(testPoint.x < desired.x){
        // Checking testpoint against obstacles
                //DEBUG: Print out obstacle Array
            for(int i=0; i<10; i++) qDebug("obstacle: (%d,%d)\n", obstacleArray[i].x, obstacleArray[i].y);
                testPoint.x++;
                for(int i=0; i<10; i++){
                        if(testPoint == obstacleArray[i]){
                                path1fail = 1;
                                //cout << "Path 1.1 Fail" << endl;
                                break; //exits for loop
                        }//if
                }//for
                if(path1fail) break; //exits while loop

                //if no break moving horizontal is obstacle free so test vertical
        }//while
        if(!path1fail){ //if not failed yet test rest of path
                turningPoint.setvalues(testPoint.x, testPoint.y);
                //testPoint.y++;
                while(testPoint.y < desired.y){
                // Checking testpoint against obstacles
                        testPoint.y++;
                        for(int i=0; i<10; i++){
                                if(testPoint == obstacleArray[i]){
                                        path1fail = 1;
                                        turningPoint.setvalues(0,0);
                                        //cout << "Path 1.2 Fail" << endl;
                                        break; //exits for loop
                                }//if
                        }//for
                        if(path1fail) break; //exits while loop

                //if no break moving horizontal is obstacle free so test vertical
                }//while
        } //if

/*************************** TEST PATH 2 *************************************/

        if(path1fail){
                //test path 2
                testPoint.setvalues(current.x,current.y);
                while(testPoint.y < desired.y){
                // Checking testpoint against obstacles
                        testPoint.y++;
                        for(int i=0; i<10; i++){
                                if(testPoint == obstacleArray[i]){
                                        path2fail = 1;
                                        //cout << "Path 2.1 Fail" << endl;
                                        break; //exits for loop
                                }//if
                        }//for
                        if(path2fail) break; //exits while loop

                        //if no break moving horizontal is obstacle free so test vertical
                }//while
                if(!path2fail){ //if not failed yet test rest of path
                        turningPoint.setvalues(testPoint.x, testPoint.y);
                        //testPoint.x++;
                        while(testPoint.x < desired.x){
                        // Checking testpoint against obstacles
                                testPoint.x++;
                                for(int i=0; i<10; i++){
                                        if(testPoint == obstacleArray[i]){
                                                path2fail = 1;
                                                turningPoint.setvalues(0,0);
                                                //cout << "Path 2.2 Fail" << endl;
                                                break; //exits for loop
                                        }//if
                                }//for
                                if(path2fail) break; //exits while loop

                        }//while
                } //if
        }//if

        if(path1fail && path2fail){
                //cout << "Both Paths Failed" << endl;
                return desired;
        } //if
return turningPoint; //if path works great success!
} //testPath


/************************************************************************************/
/************************************************************************************/

void get_obstacles(){
/*
get_obstacles() function
1. Open maze.txt file
2. Read entire file and count number of 1's (ie number of obstacles)
3. Create an obstacle array to store that number of obstacles
4. Identify the location of obstacles and store them in the array
5. Return the address of the obstacle array created to be referenced by the rest of the program.
*/
        string line; //stores lines extracted from file
        int counter = 0; //used to count number of obstacles

        unsigned int xpos = 0, ypos = 1;
        int k=0;
        QFile mazeFile ("/home/nick/svn/ebug_system/trunk/src/ebug-control/control_algorithms/pathfind/maze.txt");


        if(mazeFile.open(QIODevice::ReadOnly)){
        // Count Number of Obstacles
            QTextStream in(&mazeFile);
            while (!in.atEnd()){
                QString line = in.readLine();
                QByteArray byteArray = line.toUtf8();
                const char* cString = byteArray.constData();

                //if the character is "1" (ascii 0x31) increment the counter
                unsigned int i;
                for(i=0; i < strlen(cString); i++) if(line[i] == 0x31) counter++;
            } //while
        } //if
        mazeFile.close(); //close mazeFile so it can be reopened an cursor reset.
        //coordinates obstacleArray[counter]; //define array of coordinates of "counter" number of obstacles
        obstacleArray = (coordinates*) malloc (sizeof(coordinates)*counter);

        if(mazeFile.open(QIODevice::ReadOnly)){
        // Locate position of obstacles and create coordinates
            QTextStream in(&mazeFile);
            while (!in.atEnd()){
                xpos=0;
                QString line = in.readLine();
                QByteArray byteArray = line.toUtf8();
                const char* cString = byteArray.constData();
                //qDebug("File Line: %s\n",cString);

                for(xpos=0; xpos < strlen(cString);xpos++){
                        if(cString[xpos] == 0x31){
                                obstacleArray[k].setvalues((xpos+2)/2,ypos); //xpos+1 because array starts at 0 and grid starts at 1

                                //qDebug("Obstacle Stored: (%d,%d)\n",obstacleArray[k].x,obstacleArray[k].y);
                                k++;
                        }//if
                }//for

                ypos++;
            } //while

        } //if
//        i=0;
//        while(i<k){
//            qDebug("(%d,%d)\n",obstacleArray[i].x,obstacleArray[i].y);
//            i++;
//        }
mazeFile.close(); //close file because we finished with it
//return obstacleArray;
}//get_obstacles
