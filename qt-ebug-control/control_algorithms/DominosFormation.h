#ifndef DOMINOSFORMATION_H
#define DOMINOSFORMATION_H

// eBug-II API
#include <eBugAPI.h>
// XBee class
#include <XBee.h>
// PID controller
#include "pidcontroller.h"

// ControlDataDominosFormation structure definition
struct ControlDataDominosFormation{
    int stage;
    int xDesired;
    int yDesired;
    int xCurrent;
    int yCurrent;
    int angleCurrent;
    int angleErrTermCount;
    int distanceSquaredCurrent;
    int collisionDistanceSquaredSumCurrent;
    int collisionDistanceSquaredSumPrev;
    int collisionDistanceSquaredSumDifference;
    int nNodes;
    int nTracked;
    int index;
    int sumSquaredDistanceToGoalPoint;
    int iSumSquaredDistanceToGoalPoint;
    int minSumSquaredDistanceToOtherEbugs;
    int iMinSumSquaredDistanceToOtherEbugs;
    int minAvoidanceAngleThreshold;
    int maxAvoidanceAngleThreshold;
    bool desiredGoalPointReached;
    bool avoidanceActive;
    bool nodeStop;
    QMap<int,int> SSD_iGP;
    int iGP;
    QVector<int> SSD;
    bool conflictedGP;
    int iSSDValue;
    bool collisionCleared;
    int collidingNodeIndex;
};

int dominosFormation(XBee *, struct XBeeNodeTable, struct EBugData*, int, int, int, cv::Rect, struct ControlDataDominosFormation*, double *, bool &, bool);

#endif // DOMINOSFORMATION_H
