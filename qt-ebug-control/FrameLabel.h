/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* FrameLabel.h                                                         */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy #include "aedynarray.h"
of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef FRAMELABEL_H
#define FRAMELABEL_H

#include "Structures.h"

// Qt header files
#include <QtGui>
// Control algorithms
#include <control_algorithms/RandomPosition.h>
#include <control_algorithms/LineFormation.h>
#include <control_algorithms/TriangleFormation.h>
#include <control_algorithms/FormationControl.h>
#include <control_algorithms/CircleFormation.h>
#include <control_algorithms/DiagonalFormation1.h>
#include <control_algorithms/DiagonalFormation2.h>
#include <control_algorithms/DominosFormation.h>
#include <control_algorithms/ECSEFormation.h>
#include <control_algorithms/PathFinding.h>
#include <control_algorithms/TargetTracking.h>

class FrameLabel : public QLabel
{
    Q_OBJECT

public:
    FrameLabel(QWidget *parent = 0);
    void setMouseCursorPos(QPoint);
    QPoint getMouseCursorPos();
private:
    MouseData mouseData;
    QPoint startPoint;
    QPoint mouseCursorPos;
    bool drawBox;

    // Flags for drawing control algorithms
    bool drawControlDataRandomPositionFlag;
    bool drawControlDataLineFormationFlag;
    bool drawControlDataTriangleFormationFlag;
    int drawControlDataFormationControlFlag;
    bool drawControlDataCircleFormationFlag;
    bool drawControlDataDiagonalFormation1Flag;
    bool drawControlDataDiagonalFormation2Flag;
    bool drawControlDataDominosFormationFlag;
    bool drawControlDataECSEFormationFlag;
    bool drawControlDataPathFindingFlag;
    bool drawControlDataTargetTrackingFlag;


    QRect *box;
    int nTracked;

    struct ControlDataRandomPosition controlDataRandomPosition[MAX_NEBUGTRACK];
    struct ControlDataLineFormation controlDataLineFormation[MAX_NEBUGTRACK];
    struct ControlDataTriangleFormation controlDataTriangleFormation[MAX_NEBUGTRACK];
    struct ControlDataCircleFormation controlDataCircleFormation[MAX_NEBUGTRACK];
    struct ControlDataFormationControl controlDataFormationControl[MAX_NEBUGTRACK];
    struct ControlDataDiagonalFormation1 controlDataDiagonalFormation1[MAX_NEBUGTRACK];
    struct ControlDataDiagonalFormation2 controlDataDiagonalFormation2[MAX_NEBUGTRACK];
    struct ControlDataDominosFormation controlDataDominosFormation[MAX_NEBUGTRACK];
    struct ControlDataECSEFormation controlDataECSEFormation[MAX_NEBUGTRACK];
    struct ControlDataPathFinding controlDataPathFinding[MAX_NEBUGTRACK];
    struct ControlDataTargetTracking controlDataTargetTracking[MAX_NEBUGTRACK];


    cv::Rect currentROI;
    double roundNumber(double);
    QVector<QPoint> trail[MAX_NEBUGTRACK];
    QVector<QPoint> shape;
protected:
    void mouseMoveEvent(QMouseEvent *ev);
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);
    void paintEvent(QPaintEvent *ev);
signals:
    void newMouseData(struct MouseData mouseData);
    void onMouseMoveEvent();
private slots:
    void drawControlDataRandomPosition(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataRandomPosition*);
    void drawControlDataLineFormation(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataLineFormation*);
    void drawControlDataTriangleFormation(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataTriangleFormation*);
    void drawControlDataFormationControl(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataFormationControl*,bool*);
    void drawControlDataCircleFormation(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataCircleFormation*);
    void drawControlDataDiagonalFormation1(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataDiagonalFormation1*);
    void drawControlDataDiagonalFormation2(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataDiagonalFormation2*);
    void drawControlDataDominosFormation(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataDominosFormation*);
    void drawControlDataECSEFormation(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataECSEFormation*);
    void drawControlDataPathFinding(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataPathFinding*);
    void drawControlDataTargetTracking(cv::Rect,int,int,QList<int>,QList<int>,struct ControlDataTargetTracking*,bool*);

};

#endif // FRAMELABEL_H
