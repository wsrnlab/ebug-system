/************************************************************************/
/* qt-opencv-multithreaded:                                             */
/* A multithreaded OpenCV application using the Qt framework.           */
/*                                                                      */
/* Config.h                                                             */
/*                                                                      */
/* Nick D'Ademo <nickdademo@gmail.com>                                  */
/*                                                                      */
/* Copyright (c) 2011 Nick D'Ademo                                      */
/*                                                                      */
/* Permission is hereby granted, free of charge, to any person          */
/* obtaining a copy of this software and associated documentation       */
/* files (the "Software"), to deal in the Software without restriction, */
/* including without limitation the rights to use, copy, modify, merge, */
/* publish, distribute, sublicense, and/or sell copies of the Software, */
/* and to permit persons to whom the Software is furnished to do so,    */
/* subject to the following conditions:                                 */
/*                                                                      */
/* The above copyright notice and this permission notice shall be       */
/* included in all copies or substantial portions of the Software.      */
/*                                                                      */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      */
/* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF   */
/* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                */
/* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS  */
/* BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN   */
/* ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN    */
/* CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE     */
/* SOFTWARE.                                                            */
/*                                                                      */
/************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

// OpenCV header files
#include <opencv/cv.h>
#include <opencv/highgui.h>
// ARToolKitPlus
#include <ARToolKitPlus/TrackerSingleMarker.h>
#include <ARToolKitPlus/TrackerMultiMarker.h>
// QSerialDevice header file
#include <qserialdevice/abstractserial.h>

#define PI 3.14159265

// Duration (in number of points) of trail history
#define DRAW_TRAIL_NO_OF_POINTS 10000
// eBug size (in pixels)
#define EBUG_SIZE_PIXELS 50
// Maximum number of allowed control parameters
#define MAX_CONTROL_PARAMETERS 26 //originally 20
// Default node discovery duration (seconds)
#define DEFAULT_XBEE_ND_DURATION 10
// Default MINIMUM node discovery duration (seconds)
#define DEFAULT_MIN_XBEE_ND_DURATION 10
// Default XBee packet buffer size
#define DEFAULT_XBEE_PACKET_BUFFER_SIZE 100
// Default serial device parameters
#define DEFAULT_BAUDRATE AbstractSerial::BaudRate115200
#define DEFAULT_DATABITS AbstractSerial::DataBits8
#define DEFAULT_STOPBITS AbstractSerial::StopBits1
#define DEFAULT_PARITY AbstractSerial::ParityNone
#define DEFAULT_FLOWCONTROL AbstractSerial::FlowControlOff
// Mapping
#define DEFAULT_MAPPING_ANGLE_DIFF_MAG_SUM_THRESH 2000
#define DEFAULT_MAPPING_INITIAL_ANGLE_SAMPLES 16
// Default control thread time step (in ms)
#define DEFAULT_CONTROL_TIME_STEP_MS 150
// Maximum number of eBugs that can be tracked
#define MAX_NEBUGTRACK 8
// The RF Data contained in the ZigBee API frame is the eBug packet
// Note: XBee ZB firmware includes a command (ATNP) that returns the maximum number of RF payload bytes
//       that can be sent in a unicast transmission (API TX Request Frame).
// The following assumes: Security not enabled, source routing and fragmentation not used
#define MAX_EBUG_RXPACKET_LENGTH 84
// ARToolKitPlus
#define DEFAULT_ARTKP_PATTWIDTH 6
#define DEFAULT_ARTKP_PATTHEIGHT 6
#define DEFAULT_ARTKP_MAXIMAGEPATTERNS 8
#define DEFAULT_ARTKP_PATTSAMPLES 6
#define DEFAULT_ARTKP_MAXLOADPATTERNS 0
#define DEFAULT_ARTKP_TRACKINGHISTORY true
#define DEFAULT_ARTKP_BORDERWIDTH 0.08
#define DEFAULT_ARTKP_AUTOTHRESHOLD false
#define DEFAULT_ARTKP_NUMAUTOTHRESHRETRIES 5
#define DEFAULT_ARTKP_THRESHOLD 190
#define DEFAULT_ARTKP_VC false
#define DEFAULT_ARTKP_VC_NCORNERS 0
#define DEFAULT_ARTKP_VC_NLEFTRIGHT 0
#define DEFAULT_ARTKP_VC_NTOPBOTTOM 0
#define DEFAULT_ARTKP_PIXELFORMAT ARToolKitPlus::PIXEL_FORMAT_BGR
#define DEFAULT_ARTKP_UNDISTMODE ARToolKitPlus::UNDIST_NONE
#define DEFAULT_ARTKP_POSEESTIMATOR ARToolKitPlus::POSE_ESTIMATOR_RPP
#define DEFAULT_ARTKP_IMAGEPROCMODE ARToolKitPlus::IMAGE_FULL_RES
#define DEFAULT_ARTKP_MARKERMODE ARToolKitPlus::MARKER_ID_BCH
#define DEFAULT_ARTKP_CAMERACALIBRATIONFILE "Sony_PS3_PlayStation_Eye.cal"
// Queue lengths for FPS statistics
#define DEFAULT_PROCESSING_FPS_STAT_QUEUE_LENGTH 32
#define DEFAULT_CAPTURE_FPS_STAT_QUEUE_LENGTH 32
// Image buffer size
#define DEFAULT_IMAGE_BUFFER_SIZE 1
// SMOOTH
#define DEFAULT_SMOOTH_TYPE 0 // Options: [BLUR=0,GAUSSIAN=1,MEDIAN=2]
#define DEFAULT_SMOOTH_PARAM_1 3
#define DEFAULT_SMOOTH_PARAM_2 3
#define DEFAULT_SMOOTH_PARAM_3 0
#define DEFAULT_SMOOTH_PARAM_4 0
// DILATE
#define DEFAULT_DILATE_ITERATIONS 1
// ERODE
#define DEFAULT_ERODE_ITERATIONS 1
// FLIP
#define DEFAULT_FLIP_CODE 0 // Options: [x-axis=0,y-axis=1,both axes=-1]

#endif // CONFIG_H
